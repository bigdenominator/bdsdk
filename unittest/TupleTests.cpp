#ifndef INCLUDE_H_TUPLETESTS
#define INCLUDE_H_TUPLETESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/SmartMap.h"
#include "containers/SmartVector.h"
#include "containers/Tuple.h"

class TupleTests : public testing::Test
{
public:
	typedef CSmartMap < CDate, CVectorIntPtr > TestMap;
	typedef CSmartPtr<TestMap> TestMapPtr;
	typedef TypeList<int, double, TestMapPtr> TestList;
	typedef Tuple<TestList> TestTuple;
	typedef SmartTuple<TestList> TestSmartTuple;

	const CDate mdt1 = 20150101;
	const CDate mdt2 = 20160101;
	const int miTest = 0;
	const double mdTest = 1;
	const TestMapPtr mcTestPtr = CreateSmart<TestMap>();

	void SetUp(void)
	{
		mcTestPtr->add(mdt1, CreateSmart<CVectorInt>(10, 1));
		mcTestPtr->add(mdt2, CreateSmart<CVectorInt>(20, 2));
	}

	void TearDown(void) {}
};

TEST_F(TupleTests, Tuple)
{
	TestTuple myTuple;
	myTuple.At<0>() = miTest;
	myTuple.At<1>() = mdTest;
	myTuple.At<2>() = mcTestPtr;

	TestTuple::Types<0>::At iTest = myTuple.At<0>();
	TestTuple::Types<1>::At dTest = myTuple.At<1>();
	TestTuple::Types<2>::At cTestPtr = myTuple.At<2>();

	EXPECT_EQ(0, iTest);
	EXPECT_EQ(1.0, dTest);
	EXPECT_EQ(2, cTestPtr->size());
	
	auto itr(cTestPtr->find(mdt1));
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(10, itr->second->size());
	EXPECT_EQ(1, (*itr->second)[0]); 

	itr = cTestPtr->find(mdt2);
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(20, itr->second->size());
	EXPECT_EQ(2, (*itr->second)[0]);
}

TEST_F(TupleTests, SmartTuple)
{
	TestSmartTuple myTuple, dest;
	myTuple.At<0>() = miTest;
	myTuple.At<1>() = mdTest;
	myTuple.At<2>() = mcTestPtr;

	TestSmartTuple::Types<0>::At iTest = myTuple.At<0>();
	TestSmartTuple::Types<1>::At dTest = myTuple.At<1>();
	TestSmartTuple::Types<2>::At cTestPtr = myTuple.At<2>();

	EXPECT_EQ(0, iTest);
	EXPECT_EQ(1.0, dTest);
	EXPECT_EQ(2, cTestPtr->size());

	auto itr(cTestPtr->find(mdt1));
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(10, itr->second->size());
	EXPECT_EQ(1, (*itr->second)[0]);

	itr = cTestPtr->find(mdt2);
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(20, itr->second->size());
	EXPECT_EQ(2, (*itr->second)[0]);

	CopyViaPack(myTuple, dest);

	SSerial sSource, sDest;
	myTuple.Serialize(sSource);
	dest.Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);
}
#endif