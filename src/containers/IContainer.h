#ifndef INCLUDE_H_ICONTAINER
#define INCLUDE_H_ICONTAINER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/Strings.h"
#include "utils/SmartPointers.h"

typedef int32_t TYPELIST_t;

enum class SUMMARY_FORMAT { PACKED, SERIALIZED };

template<SUMMARY_FORMAT TFormat> struct IContainerSummary
{
	typedef size_t size_type;

	virtual ~IContainerSummary(void) {}

	inline SUMMARY_FORMAT format(void) { return TFormat; }

	virtual inline void reset(void) = 0;

	virtual size_type size(void) const = 0;

	virtual TYPELIST_t type(void) const = 0;
};

struct SPack : public IContainerSummary<SUMMARY_FORMAT::PACKED>
{
	typedef SPack	_Myt;

	typedef size_t	size_type;

	typedef char* const const_pointer;

	char*		mBuffer;
	size_type	miCapacity;
	size_type	miPosition;

	SPack(void) :miCapacity(0), miPosition(0), mBuffer(nullptr) {}

	SPack(size_type inp_iSize) :miCapacity(inp_iSize), miPosition(0), mBuffer(new char[inp_iSize]) {}

	~SPack(void) { delete mBuffer; }

	inline size_type capacity(void) const { return miCapacity; }

	inline const_pointer position(void) const { return mBuffer + miPosition; }

	inline void reserve(size_type inp_iSize)
	{
		if (inp_iSize > miCapacity)
		{
			delete mBuffer;
			mBuffer = new char[inp_iSize];
			miCapacity = inp_iSize;
		}

		miPosition = 0;
	}

	inline void reset(void) { miPosition = 0; }

	size_type size(void) const { return *reinterpret_cast<SPack::size_type*>(mBuffer + sizeof(TYPELIST_t)); }

	TYPELIST_t type(void) const { return *reinterpret_cast<TYPELIST_t*>(mBuffer); }
};

struct SSerial : public IContainerSummary<SUMMARY_FORMAT::SERIALIZED>
{
	typedef SSerial	_Myt;

	typedef string::size_type size_type;

	string		msBuffer;
	size_type	miPosition;

	SSerial(void) :miPosition(0), msBuffer(sNULL) {}

	inline void reset(void) { miPosition = 0; }

	size_type size(void) const { return strlen(msBuffer.c_str()); }

	TYPELIST_t type(void) const 
	{
		TYPELIST_t tmp;
		to(msBuffer.substr(0, strchr(msBuffer.c_str(), cCOMMA) - msBuffer.c_str()), tmp);
		return tmp;
	}
};

class IContainer
{
public:
	virtual ~IContainer(void){};
	virtual bool Deserialize(SSerial&) = 0;
	virtual TYPELIST_t GetType(void) const = 0; 
	virtual void Pack(SPack&) const = 0;
	virtual SPack::size_type PackSize(void) const = 0;
	virtual void Serialize(SSerial&) const = 0;
	virtual bool Unpack(SPack&) = 0;
};
typedef IContainer* CContainerIntrPtr;

template<typename T> struct IsContainer
{
	enum :int { value = std::is_base_of<IContainer, T>::value };
};
#endif