#ifndef INCLUDE_H_SMARTCONTAINER
#define INCLUDE_H_SMARTCONTAINER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/IContainer.h"

static const string	sContainerEmptyOrEOF("Container is empty or already at the end");
static const string	sEmptyContainer("Container is empty");
static const string	sEntryForIndexNotFound("Entry for Index not Found");
static const string	sEntryNotFound("Entry not found");
static const string	sInvalidIndex("Invalid Index");
static const string	sNoDataReturned("No data returned for GetCurrentValue");

#define LOOP(iterator) for (iterator.begin(); !iterator.isEnd(); iterator++)
#define FOREACH(container, itrName) for (auto itrName(container.begin()); itrName != container.end(); itrName++)

enum FRM_TYPELIST :TYPELIST_t { NONSCHEMATIC = 0, CFEEDBACK, STESTCASE, SLOG, SERROR, SLICENSE, SSTATUS, CONTROL_BLOCK, FRM_SCHEMATICEND = 1000 };

namespace SPUD
{
	template<typename TData, bool bContainer = std::is_base_of<IContainer, TData>::value> struct spud;
	template<typename TData> struct spud < TData, false >
	{
		static inline bool deserialize(SSerial& inp_sSerial, TData& inp_Data)
		{
			to(CString::DelimitedSubString(inp_sSerial.msBuffer, sComma, inp_sSerial.miPosition), inp_Data);

			return true;
		}
		static inline SPack::size_type packSize(const TData& inp_Data)
		{
			return sizeof(TData);
		}
		static inline void pack(SPack& inp_sPack, const TData& inp_Data)
		{
			*(reinterpret_cast<TData*>(inp_sPack.position())) = inp_Data;
			inp_sPack.miPosition += sizeof(TData);
		}
		static inline void serialize(SSerial& inp_sSerial, const TData& inp_Data)
		{
			inp_sSerial.msBuffer += sComma + to_string(inp_Data);
		}
		static inline bool unpack(SPack& inp_sPack, TData& inp_Data)
		{
			inp_Data = *reinterpret_cast<TData*>(inp_sPack.position());
			inp_sPack.miPosition += sizeof(TData);
			return true;
		}
	};
	template<> struct spud < string, false >
	{
		static inline bool deserialize(SSerial& inp_sSerial, string& inp_Data)
		{
			inp_Data = CString::DelimitedSubString(inp_sSerial.msBuffer, sComma, inp_sSerial.miPosition);

			return true;
		}
		static inline SPack::size_type packSize(const string& inp_Data)
		{
			return static_cast<SPack::size_type>(inp_Data.size() * sizeof(char) + sizeof(SPack::size_type));
		}
		static inline void pack(SPack& inp_sPack, const string& inp_Data)
		{
			const SPack::size_type iChars(inp_Data.size());

			spud<SPack::size_type, false>::pack(inp_sPack, iChars);
			memcpy(inp_sPack.position(), inp_Data.c_str(), iChars);

			inp_sPack.miPosition += iChars;
		}
		static inline void serialize(SSerial& inp_sSerial, const string& inp_Data)
		{
			inp_sSerial.msBuffer += sComma + inp_Data;
		}
		static inline bool unpack(SPack& inp_sPack, string& inp_Data)
		{
			SPack::size_type iChars;

			spud<SPack::size_type, false>::unpack(inp_sPack, iChars);
			inp_Data = string(inp_sPack.position(), iChars);

			return true;
		}
	};
	template<typename TData> struct spud < TData, true >
	{
		static inline bool deserialize(SSerial& inp_sSerial, TData& inp_Data)
		{
			return inp_Data.Deserialize(inp_sSerial);
		}
		static inline SPack::size_type packSize(const TData& inp_Data)
		{
			return inp_Data.PackSize();
		}
		static inline void pack(SPack& inp_sPack, const TData& inp_Data)
		{
			inp_Data.Pack(inp_sPack);
		}
		static inline void serialize(SSerial& inp_sSerial, const TData& inp_Data)
		{
			inp_sSerial.msBuffer += sComma;
			inp_Data.Serialize(inp_sSerial);
		}
		static inline bool unpack(SPack& inp_sPack, TData& inp_Data)
		{
			return inp_Data.Unpack(inp_sPack);
		}
	};
	
	static inline bool deserializeTools(SSerial& inp_sSerial) { return true; }
	template<typename TData> static inline bool deserializeTools(SSerial& inp_sSerial, TData& inp_Data)
	{
		return spud<TData>::deserialize(inp_sSerial, inp_Data);
	}
	template<typename TData> static inline bool deserializeTools(SSerial& inp_sSerial, CSmartPtr<TData>& inp_DataPtr)
	{
		if (!inp_DataPtr) inp_DataPtr = CreateSmart<TData>();

		return spud<TData>::deserialize(inp_sSerial, *inp_DataPtr);
	}
	template<typename TData, typename ...Stuff> static inline bool deserializeTools(SSerial& inp_sSerial, TData& inp_Data, Stuff&... myStuff)
	{
		if (!deserializeTools(inp_sSerial, inp_Data)) return false;

		return deserializeTools(inp_sSerial, myStuff...);
	}
	
	static inline SPack::size_type packSizeTools() { return 0; }
	template<typename TData> static inline SPack::size_type packSizeTools(const TData& inp_Data)
	{
		return spud<TData>::packSize(inp_Data);
	}
	template<typename TData> static inline SPack::size_type packSizeTools(CSmartPtr<TData> inp_DataPtr)
	{
		return spud<TData>::packSize(*inp_DataPtr);
	}
	template<typename TData, typename ...Stuff> static inline SPack::size_type packSizeTools(const TData& inp_Data, const Stuff&... myStuff)
	{
		return packSizeTools(inp_Data) + packSizeTools(myStuff...);
	}

	static inline void packTools(SPack& inp_sPack) {}
	template<typename TData> static inline void packTools(SPack& inp_sPack, const TData& inp_Data)
	{
		spud<TData>::pack(inp_sPack, inp_Data);
	}
	template<typename TData> static inline void packTools(SPack& inp_sPack, CSmartPtr<TData> inp_DataPtr)
	{
		spud<TData>::pack(inp_sPack, *inp_DataPtr);
	}
	template<typename TData, typename ...Stuff> static inline void packTools(SPack& inp_sPack, const TData& inp_Data, const Stuff&... myStuff)
	{
		packTools(inp_sPack, inp_Data);
		packTools(inp_sPack, myStuff...);
	}

	static inline void serializeTools(SSerial& inp_sSerial) {}
	template<typename TData> static inline void serializeTools(SSerial& inp_sSerial, const TData& inp_Data)
	{
		spud<TData>::serialize(inp_sSerial, inp_Data);
	}
	template<typename TData> static inline void serializeTools(SSerial& inp_sSerial, CSmartPtr<TData> inp_DataPtr)
	{
		spud<TData>::serialize(inp_sSerial, *inp_DataPtr);
	}
	template<typename TData, typename ...Stuff> static inline void serializeTools(SSerial& inp_sSerial, const TData& inp_Data, const Stuff&... myStuff)
	{
		serializeTools(inp_sSerial, inp_Data);
		serializeTools(inp_sSerial, myStuff...);
	}

	static inline bool unpackTools(SPack& inp_sPack) { return true; }
	template<typename TData> static inline bool unpackTools(SPack& inp_sPack, TData& inp_Data)
	{
		return spud<TData>::unpack(inp_sPack, inp_Data);
	}
	template<typename TData> static inline bool unpackTools(SPack& inp_sPack, CSmartPtr<TData>& inp_DataPtr)
	{
		if (!inp_DataPtr) inp_DataPtr = CreateSmart<TData>();

		return spud<TData>::unpack(inp_sPack, *inp_DataPtr);
	}
	template<typename TData, typename ...Stuff> static inline bool unpackTools(SPack& inp_sPack, TData& inp_Data, Stuff&... myStuff)
	{
		if (!unpackTools(inp_sPack, inp_Data)) return false;

		return unpackTools(inp_sPack, myStuff...);
	}
};


template<typename T> static bool CopyViaPack(const T& inp_cFrom, T& inp_cTo)
{
	SPack sPack(inp_cFrom.PackSize());
	inp_cFrom.Pack(sPack);
	sPack.reset();
	return inp_cTo.Unpack(sPack);
}

template<typename T> static bool CopyViaSerial(const T& inp_cFrom, T& inp_cTo)
{
	SSerial sSerial;
	inp_cFrom.Serialize(sSerial);
	sSerial.reset();
	return inp_cTo.Deserialize(sSerial);
}

template<TYPELIST_t iTYPE> class CContainer : public IContainer
{
public:
	typedef CContainer<iTYPE>	_Myt;
	typedef IContainer	_Mybase;

	enum :TYPELIST_t { TypeId = iTYPE };

	inline bool Deserialize(SSerial& inp_sSerial)
	{
		int32_t iType(iTYPE);

		if (!SPUD::deserializeTools(inp_sSerial, iType)) return false;
		if (iType != iTYPE) return false;

		return deserialize(inp_sSerial);
	}
	inline TYPELIST_t GetType(void) const { return iTYPE; }
	inline void Pack(SPack& inp_sPack) const
	{
		SPUD::packTools(inp_sPack, iTYPE, PackSize());
		pack(inp_sPack);
	}
	inline SPack::size_type PackSize(void) const { return SPUD::packSizeTools(iTYPE) + sizeof(SPack::size_type) + packSize(); }
	inline void Serialize(SSerial& inp_sSerial) const
	{
		inp_sSerial.msBuffer += to_string(iTYPE);
		serialize(inp_sSerial);
	}
	inline bool Unpack(SPack& inp_sPack)
	{
		TYPELIST_t iType(iTYPE);
		SPack::size_type iSize(0);
		if (!SPUD::unpackTools(inp_sPack, iType, iSize)) return false;
		if (iType != iTYPE) return false;

		return unpack(inp_sPack);
	}

protected:
	CContainer(void) :_Mybase() {}

	virtual bool deserialize(SSerial&) = 0;
	virtual void pack(SPack&) const = 0;
	virtual SPack::size_type packSize(void) const = 0;
	virtual void serialize(SSerial&) const = 0;
	virtual bool unpack(SPack&) = 0;
};

HASTYPE(HasRootType, _Myroot)

template<typename T, bool hasValueType = HasValueType<T>::value> struct ValueHlpr
{
	typedef typename T::value_type value_type;
};
template<typename T> struct ValueHlpr<T, false>
{
	typedef T value_type;
};

#define SPUD(...) \
	virtual bool deserialize(SSerial& inp_sSerial) { return SPUD::deserializeTools(inp_sSerial, __VA_ARGS__); } \
	virtual void pack(SPack& inp_sPack) const { SPUD::packTools(inp_sPack, __VA_ARGS__); } \
	virtual SPack::size_type packSize(void) const { return SPUD::packSizeTools(__VA_ARGS__); } \
	virtual void serialize(SSerial& inp_sSerial) const { SPUD::serializeTools(inp_sSerial, __VA_ARGS__); } \
	virtual bool unpack(SPack& inp_sPack) { return SPUD::unpackTools(inp_sPack, __VA_ARGS__); }

#define DERIVED_SPUD(...) \
	virtual bool deserialize(SSerial& inp_sSerial) \
	{ \
		if (!SPUD::deserializeTools(inp_sSerial, __VA_ARGS__)) return false; \
		return _Mybase::deserialize(inp_sSerial); \
	} \
	virtual void pack(SPack& inp_sPack) const \
	{ \
		SPUD::packTools(inp_sPack, __VA_ARGS__); \
		_Mybase::pack(inp_sPack); \
	} \
	virtual SPack::size_type packSize(void) const \
	{ \
		return SPUD::packSizeTools(__VA_ARGS__) + _Mybase::packSize(); \
	} \
	virtual void serialize(SSerial& inp_sSerial) const \
	{ \
		SPUD::serializeTools(inp_sSerial, __VA_ARGS__); \
		_Mybase::serialize(inp_sSerial); \
	} \
	virtual bool unpack(SPack& inp_sPack) \
	{ \
		if (!SPUD::unpackTools(inp_sPack, __VA_ARGS__)) return false; \
		return _Mybase::unpack(inp_sPack); \
	}
#endif
