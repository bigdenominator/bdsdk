#ifndef INCLUDE_H_ALIGNEDALLOCATOR
#define INCLUDE_H_ALIGNEDALLOCATOR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

template<class T, size_t Alignment> class AlignedAllocator
{
public:
	typedef T value_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef void* void_pointer;
	typedef const void* const_void_pointer;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T& reference;
	typedef const T& const_reference;

	template<class U> struct rebind
	{
		typedef AlignedAllocator<U, Alignment> other;
	};

	enum :size_type { ALIGN = Alignment, MAX_SIZE = ((size_t)(-1) / sizeof(T)) };

	AlignedAllocator() {}

	template<class U> AlignedAllocator(const AlignedAllocator<U, Alignment>&) {}

	pointer address(reference inp_Address) const { return std::addressof(inp_Address); }

	const_pointer address(const_reference inp_Address) const { return std::addressof(inp_Address); }

	pointer allocate(size_type inp_iSize) const
	{
		if (inp_iSize == 0) return nullptr;
		if (inp_iSize > max_size()) return nullptr;

		void* pTmp = MemoryAllocation(sizeof(T) * inp_iSize, ALIGN);

		return static_cast<T*>(pTmp);
	}

	void deallocate(pointer inp_Pointer, size_type) const
	{
		MemoryFree(inp_Pointer);
	}

	size_type max_size(void) const { return MAX_SIZE; }

	template<class U, class ...Args> void construct(U* inp_Pointer, Args&&... myArgs)
	{
		void* p = inp_Pointer;
		::new(p)U(std::forward<Args>(myArgs)...);
	}

	template<class U> void construct(U* inp_Pointer)
	{
		void* p = inp_Pointer;
		::new(p)U();
	}

	template<class U> void destroy(U* inp_Pointer)
	{
		(void)inp_Pointer;
		inp_Pointer->~U();
	}
};

template<size_t Alignment> class AlignedAllocator<void, Alignment>
{
public:
	typedef void value_type;
	typedef void* pointer;
	typedef const void* const_pointer;

	template<class U> struct rebind
	{
		typedef AlignedAllocator<U, Alignment> other;
	};
};

template<class T1, class T2, size_t Alignment> inline bool operator==(const AlignedAllocator<T1, Alignment>&, const AlignedAllocator<T2, Alignment>&)
{
	return true;
}

template<class T1, class T2, size_t Alignment> inline bool operator!=(const AlignedAllocator<T1, Alignment>&, const AlignedAllocator<T2, Alignment>&)
{
	return false;
}
#endif