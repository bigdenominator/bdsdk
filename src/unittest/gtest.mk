ifndef SETUP_GTEST
SETUP_GTEST = TRUE


TEST_MAIN	= $(BDSDK_INCLUDE)/unittest/gtest_main.cc
GTEST_HEADERS 	= $(GTEST_INCLUDE)/gtest/*.h $(GTEST_INCLUDE)/gtest/internal/*.h
GTEST_SRCS	= $(GTEST_ROOT)/src/*.cc $(GTEST_ROOT)/src/*.h $(GTEST_HEADERS)

all : $(TARGET_REL) $(TARGET_DBG)

clean :
	rm -f 	$(TARGET_REL) $(INTER_DIR_REL)/gtest-all.* $(INTER_DIR_REL)/gtest_main.* $(INTER_DIR_REL)/*.o $(INTER_DIR_REL)/*.a
	rm -f 	$(TARGET_DBG) $(INTER_DIR_DBG)/gtest-all.* $(INTER_DIR_DBG)/gtest_main.* $(INTER_DIR_DBG)/*.o $(INTER_DIR_DBG)/*.a 

$(INTER_DIR_REL)/gtest-all.o : $(GTEST_SRCS)
	$(CC) $(INCLUDE_DIRS) -I$(GTEST_ROOT)               $(CFLAGS_REL) -c $(GTEST_ROOT)/src/gtest-all.cc -o $@

$(INTER_DIR_DBG)/gtest-all.o : $(GTEST_SRCS)
	$(CC) $(INCLUDE_DIRS) -I$(GTEST_ROOT)               $(CFLAGS_DBG) -c $(GTEST_ROOT)/src/gtest-all.cc -o $@

$(INTER_DIR_REL)/gtest_main.o : $(TEST_MAIN) $(GTEST_SRCS) 
	$(CC) $(INCLUDE_DIRS) -I$(GTEST_ROOT) -I$(THIS_DIR) $(CFLAGS_REL) -c $< -o $@

$(INTER_DIR_DBG)/gtest_main.o : $(TEST_MAIN) $(GTEST_SRCS) 
	$(CC) $(INCLUDE_DIRS) -I$(GTEST_ROOT) -I$(THIS_DIR) $(CFLAGS_DBG) -c $< -o $@


$(INTER_DIR_REL)/gtest-all.a : $(INTER_DIR_REL)/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

$(INTER_DIR_DBG)/gtest-all.a : $(INTER_DIR_DBG)/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

$(INTER_DIR_REL)/gtest_main.a : $(INTER_DIR_REL)/gtest-all.o $(INTER_DIR_REL)/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^

$(INTER_DIR_DBG)/gtest_main.a : $(INTER_DIR_DBG)/gtest-all.o $(INTER_DIR_DBG)/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^


#RELEASE BUILD

$(INTER_DIR_REL)/%.o: $(THIS_DIR)/%.cpp
	@mkdir -p $(@D)
	$(CC) $(CFLAGS_REL) $(INCLUDE_DIRS) -c $< -o $@

$(TARGET_REL): $(OBJECTS_REL) $(INTER_DIR_REL)/gtest_main.a
	@mkdir -p $(@D)
	$(CC) $(CFLAGS_REL) -lpthread $^ -o $@ $(LFLAGS_REL) 

#DEBUG BUILD

$(INTER_DIR_DBG)/%.o: $(THIS_DIR)/%.cpp
	@mkdir -p $(@D)
	$(CC) $(CFLAGS_DBG) $(INCLUDE_DIRS) -c $< -o $@

$(TARGET_DBG): $(OBJECTS_DBG) $(INTER_DIR_DBG)/gtest_main.a
	@mkdir -p $(@D)
	$(CC) $(CFLAGS_DBG) -lpthread $^ -o $@ $(LFLAGS_DBG)
 
endif
