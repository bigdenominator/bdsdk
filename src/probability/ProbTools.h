#ifndef INCLUDE_H_PROBABILITYTOOLS
#define INCLUDE_H_PROBABILITYTOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

static bool isValidProbability(const double& inp_dProbability)
{
	return inp_dProbability >= 0 && inp_dProbability <= 1;
}

#endif