#ifndef INCLUDE_H_TYPELISTTESTS
#define INCLUDE_H_TYPELISTTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "utils/TypeLists.h"
#include "containers/SmartMap.h"
#include "containers/SmartVector.h"

typedef double(*TestFn)(CVectorDblPtr);

typedef TypeList<CDate> TestList1;
typedef TypeList<int, int, double, double, float, long double, CDate> TestList2;
typedef TypeList<int, double, CSmartPtr<CSmartMap<CDate, CVectorDblPtr>>, TestFn> TestList3;

typedef TestList1::Types<0>::SubList TestSub1;
typedef TestList2::Types<4>::SubList TestSub2;
typedef TestList3::Types<1>::SubList TestSub3;

TEST(TypeListTests, Length)
{
	EXPECT_EQ(1, TestList1::length);
	EXPECT_EQ(7, TestList2::length);
	EXPECT_EQ(4, TestList3::length);
}

TEST(TypeListTests, InList)
{
	bool bInList;

	bInList = TestList2::HasType<float>::value;
	EXPECT_TRUE(bInList);
	
	bInList = TestList2::HasType<CSmartPtr<double>>::value;
	EXPECT_FALSE(bInList);
}

TEST(TypeListTests, TestSub)
{
	EXPECT_EQ(1, TestSub1::length);
	EXPECT_EQ(3, TestSub2::length);
	EXPECT_EQ(3, TestSub3::length);
}

TEST(TypeListTests, TypeCheck)
{
	bool bAgree;

	bAgree = TypeCheck<int, int>::agree;
	EXPECT_TRUE(bAgree);

	bAgree = TypeCheck<int, CSmartPtr<int>>::agree;
	EXPECT_FALSE(bAgree);
}

TEST(TypeListTests, TypeAt)
{
	bool bAgree;

	bAgree = TypeCheck<CDate, TestList1::Types<0>::At>::agree;
	EXPECT_TRUE(bAgree);

	bAgree = TypeCheck<int, TestList1::Types<0>::At>::agree;
	EXPECT_FALSE(bAgree);

	bAgree = TypeCheck<float, TestList2::Types<4>::At>::agree;
	EXPECT_TRUE(bAgree);

	bAgree = TypeCheck<CDate, TestList2::Types<4>::At>::agree;
	EXPECT_FALSE(bAgree);

	bAgree = TypeCheck<TestFn, TestList3::Types<3>::At>::agree;
	EXPECT_TRUE(bAgree);

	bAgree = TypeCheck<int, TestList3::Types<3>::At>::agree;
	EXPECT_FALSE(bAgree);
}
#endif
