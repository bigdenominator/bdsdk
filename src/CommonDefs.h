#ifndef INCLUDE_H_COMMONDEFS
#define INCLUDE_H_COMMONDEFS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include <memory>

using namespace std;

#ifdef WINGCC
#define WINDOWS
#elif _WIN32
#define WINMSVC
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define WINDOWS
#pragma once
#pragma warning( disable : 4800 )
#pragma warning( disable : 4786 )
#pragma warning( disable : 4503 )
#pragma warning( disable : 4373 )
#pragma warning( disable : 4275 )
#pragma warning( disable : 4274 )
#pragma warning( disable : 4251 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4099 )
//#pragma warning( disable : 4267 )
#include "windows.h"
#endif

#ifdef WINDOWS
const static bool IsLinux = false;
const static bool IsWindows = true;
const static bool IsOSX = false;
#elif __APPLE__
	#define OSX
const static bool IsLinux = false;
const static bool IsWindows = false;
const static bool IsOSX = true;
#else
	//	Assume Linux
	#define UNIX
	#include <stdlib.h>
	#include "string.h"
const static bool IsLinux = true;
const static bool IsWindows = false;
const static bool IsOSX = false;
#endif

#include <assert.h>
#include <chrono>
#include <cstdint>
#include <string>
#include <stdexcept>
#include <utility>

#ifndef OSX // all but MAC
	#include <malloc.h>
#endif

static const int32_t   THREAD_DEFAULT(30);
#ifdef USE_MIC
static const int32_t   THREAD_LIMIT(120);
#else
static const int32_t   THREAD_LIMIT(30);
#endif

static inline void* MemoryAllocation(const uint64_t inp_lSize, const uint64_t inp_lAlignOn)
{
#ifndef USE_JEMALLOC
#if defined UNIX 				// Linux
	void* raw(nullptr);
	posix_memalign(&raw, inp_lAlignOn, inp_lSize);
	return raw;
#elif defined WINDOWS			// WIN32 (including GCC on Windows)
	return _aligned_malloc(inp_lSize, inp_lAlignOn);
#elif defined OSX				// Apple 
	return malloc(inp_lSize);
#endif
#else
	return je_aligned_alloc(inp_lSize, inp_lAlignOn);
#endif
}

static inline void MemoryFree(void* inp_Memory)
{
#ifndef USE_JEMALLOC
#if defined WIN32				// WIN32 (Including GCC on Windows)
	return _aligned_free(inp_Memory); 
#elif defined UNIX 				// Linux
	return free(inp_Memory);	//memalign(inp_lAlignOn, inp_lSize);
#elif defined OSX				// Apple
	return malloc(inp_lSize);
#endif
#else
	return je_free(inp_Memory);
#endif
}

static const char cNULL('\0');
static const string	sGCCException("GCC Exception");
static const string	sGeneralException("Undefined General Exception");
static const string	sNULL("");
static const int32_t    NPOS(-1);

enum class EXCEPTION_CODES{ APPEXCEPTION = -1, STANDARDEXCEPTION = -2 };
enum class OS_ENV{ WIN = 1, LINUX, OSX };

#define DUMMY(...) __VA_ARGS__

template<typename TData, typename TOut> using ifEnum = std::enable_if<std::is_enum<TData>::value, TOut>;

static bool CheckEnv(OS_ENV iEnv)
{
	switch (iEnv)
	{
	case OS_ENV::LINUX: return IsLinux;
	case OS_ENV::WIN:   return IsWindows;
	case OS_ENV::OSX:   return IsOSX;
	default:            return false;
	}
}

#define HASTYPE(structName, typeName) \
	template<typename T> struct structName \
	{ \
		template<typename C> static int64_t test(typename C::typeName*); \
		template<typename C> static int32_t test(...); \
		enum :int { value = sizeof(test<T>(nullptr)) == sizeof(int64_t) }; \
	};

HASTYPE(HasValueType, value_type)

class CException : public exception
{
private:
	string	msWhat;
	string msFileLocation;
	int32_t miLineNum;
public:
	CException(void) throw() : exception(), msWhat(sGCCException){}
	CException(string const& inp_sInfoString) throw() : exception(), msWhat(inp_sInfoString){}
	CException(string const& inp_sInfoString, char const* inp_cFileNamePtr, uint64_t inp_iLineNum) throw() : exception(), msFileLocation(inp_cFileNamePtr),
		miLineNum(inp_iLineNum), msWhat(inp_sInfoString){}

	~CException(void) throw() {}
	string GetFile(void){ return msFileLocation; }
	uint64_t GetLine(void){ return miLineNum; }
	string GetWhat(void){ return msWhat; }
};
#endif
