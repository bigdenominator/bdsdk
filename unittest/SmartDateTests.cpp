#ifndef INCLUDE_H_SMARTDATETESTS
#define INCLUDE_H_SMARTDATETESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include <cmath>
#include "containers/SmartDate.h"

TEST(SmartDateTests, constructors)
{
	CDate cTest0;
	EXPECT_TRUE(CDate(CDate::INITIALIZE) == cTest0);

	CDate cTest1(20140614);
	EXPECT_TRUE(static_cast<int>(cTest1) == 20140614);

	CDate cTest2("20140101");
	EXPECT_TRUE(CDate(20140101) == cTest2);

	CDate cTest3(cTest1);
	EXPECT_TRUE(CDate(20140614) == cTest3);

	CDate cTest4(2014, MONTHSOFYEAR::MARCH, 15);
	EXPECT_TRUE(CDate(20140315) == cTest4);

	CDate cTest5(2014, MONTHSOFYEAR::JUNE, DAYSOFWEEK::SATURDAY, POSITIONINMONTH::THIRD);
	EXPECT_TRUE(CDate(20140621) == cTest5);

	EXPECT_TRUE(CDate(20100101) == CDate("1/1/2010"));
	EXPECT_TRUE(CDate(20130611) == CDate("06-11-2013"));
	EXPECT_TRUE(CDate(20130611) == CDate("20130611"));
	EXPECT_FALSE(CDate(20130611) == CDate("11-06-2013"));

	//Bad dates - should give errors
	EXPECT_THROW(CDate cTestA(20141301), CException);
	EXPECT_THROW(CDate cTestA(20141332), CException);
}

TEST(SmartDateTests, AssignmentOperators)
{
	CDate cTest;

	int32_t iTest(20140614);
	EXPECT_TRUE(CDate(20140614) == (cTest = iTest));
	EXPECT_TRUE(CDate(20140614) == cTest);

	int64_t lTest(20140714);
	EXPECT_TRUE(CDate(20140714) == (cTest = lTest));
	EXPECT_TRUE(CDate(20140714) == cTest);

	CDate cTemp(20140101);
	EXPECT_TRUE(CDate(20140101) == (cTest = cTemp));
	EXPECT_TRUE(CDate(20140101) == cTest);
}

TEST(SmartDateTests, ComparisonOperators)
{
	CDate cComp0(20140614), cComp1(20140614), cComp2(20140101);

	EXPECT_TRUE(cComp0 == cComp1);
	EXPECT_FALSE(cComp0 == cComp2);

	EXPECT_TRUE(cComp0 != cComp2);
	EXPECT_FALSE(cComp0 != cComp1);

	EXPECT_TRUE(cComp2 < cComp0);
	EXPECT_FALSE(cComp1 < cComp2);
	EXPECT_FALSE(cComp0 < cComp1);
	EXPECT_TRUE(cComp0 < 20141231LL);
	EXPECT_FALSE(cComp0 < 20140614LL);
	EXPECT_FALSE(cComp0 < 20140101LL);
	EXPECT_TRUE(cComp0 < 20141231);
	EXPECT_FALSE(cComp0 < 20140614);
	EXPECT_FALSE(cComp0 < 20140101);

	EXPECT_TRUE(cComp2 <= cComp0);
	EXPECT_FALSE(cComp1 <= cComp2);
	EXPECT_TRUE(cComp0 <= cComp1);
	EXPECT_TRUE(cComp0 <= 20141231LL);
	EXPECT_TRUE(cComp0 <= 20140614LL);
	EXPECT_FALSE(cComp0 <= 20140101LL);
	EXPECT_TRUE(cComp0 <= 20141231);
	EXPECT_TRUE(cComp0 <= 20140614);
	EXPECT_FALSE(cComp0 <= 20140101);

	EXPECT_FALSE(cComp2 > cComp0);
	EXPECT_TRUE(cComp1 > cComp2);
	EXPECT_FALSE(cComp0 > cComp1);
	EXPECT_FALSE(cComp0 > 20141231LL);
	EXPECT_FALSE(cComp0 > 20140614LL);
	EXPECT_TRUE(cComp0 > 20140101LL);
	EXPECT_FALSE(cComp0 > 20141231);
	EXPECT_FALSE(cComp0 > 20140614);
	EXPECT_TRUE(cComp0 > 20140101);

	EXPECT_FALSE(cComp2 >= cComp0);
	EXPECT_TRUE(cComp1 >= cComp2);
	EXPECT_TRUE(cComp0 >= cComp1);
	EXPECT_FALSE(cComp0 >= 20141231LL);
	EXPECT_TRUE(cComp0 >= 20140614LL);
	EXPECT_TRUE(cComp0 >= 20140101LL);
	EXPECT_FALSE(cComp0 >= 20141231);
	EXPECT_TRUE(cComp0 >= 20140614);
	EXPECT_TRUE(cComp0 >= 20140101);
}

TEST(SmartDateTests, IncrementOperators)
{
	CDate cTest0(20140614);
	CDate cTest1(20140614);

	SPeriod sPlus(FREQUENCY::MONTHLY);
	SPeriod sMinus(sPlus.mUnitType, -sPlus.miUnitCount);

	// +=
	cTest0 += 16L;
	EXPECT_TRUE(CDate(20140630) == cTest0);
	cTest0 += -16L;
	EXPECT_TRUE(CDate(20140614) == cTest0);

	cTest0 += 18;
	EXPECT_TRUE(CDate(20140702) == cTest0);
	cTest0 += -18;
	EXPECT_TRUE(CDate(20140614) == cTest0);

	cTest0 += sPlus;
	EXPECT_TRUE(CDate(20140714) == cTest0);

	cTest0 += sMinus;
	EXPECT_TRUE(CDate(20140614) == cTest0);
	
	// -=
	cTest0 -= -16L;
	EXPECT_TRUE(CDate(20140630) == cTest0);
	cTest0 -= 16L;
	EXPECT_TRUE(CDate(20140614) == cTest0);

	cTest0 -= -18;
	EXPECT_TRUE(CDate(20140702) == cTest0);
	cTest0 -= 18;
	EXPECT_TRUE(CDate(20140614) == cTest0);

	cTest0 -= sPlus;
	EXPECT_TRUE(CDate(20140514) == cTest0);

	cTest0 -= sMinus;
	EXPECT_TRUE(CDate(20140614) == cTest0);

	// + -
	EXPECT_TRUE(CDate(20140630) == (cTest1 + 16LL));
	EXPECT_TRUE(CDate(20140529) == (cTest1 - 16LL));

	EXPECT_TRUE(CDate(20140630) == (cTest1 + 16));
	EXPECT_TRUE(CDate(20140529) == (cTest1 - 16));

	EXPECT_TRUE(CDate(20140714) == (cTest1 + sPlus));
	EXPECT_TRUE(CDate(20140514) == (cTest1 - sPlus));

	// ++ --
	EXPECT_TRUE(CDate(20140614) == (cTest1++));
	EXPECT_TRUE(CDate(20140615) == cTest1);
	EXPECT_TRUE(CDate(20140615) == (cTest1--));
	EXPECT_TRUE(CDate(20140614) == cTest1);
}

TEST(SmartDateTests, SubtractionOperator)
{
	CDate cTemp1, cTemp2;

	cTemp1 = 20140614; cTemp2 = 20140531;
	EXPECT_EQ(14, cTemp1 - cTemp2);
	EXPECT_EQ(-14, cTemp2 - cTemp1);

	cTemp1 = 20140301; cTemp2 = 20140228;
	EXPECT_EQ(1, cTemp1 - cTemp2);

	cTemp1 = 20120301; cTemp2 = 20120228;
	EXPECT_EQ(2, cTemp1 - cTemp2);

	cTemp1 = 20120301; cTemp2 = 20120229;
	EXPECT_EQ(1, cTemp1 - cTemp2);

	cTemp1 = 20120301; cTemp2 = 20100228;
	EXPECT_EQ(1 + 365 + 366, cTemp1 - cTemp2);
}

TEST(SmartDateTests, CastOperators)
{
	CDate cTest(20140614);

	double dTest(static_cast<double>(cTest));
	EXPECT_EQ(20140614.0, dTest);

	float fTest(static_cast<float>(cTest));
	EXPECT_EQ(20140614.0, fTest);

	int64_t lTest(static_cast<int64_t>(cTest));
	EXPECT_EQ(20140614LL, lTest);

	int32_t iTest(static_cast<int32_t>(cTest));
	EXPECT_EQ(20140614, iTest);
}

TEST(SmartDateTests, Add)
{
	CDate cTest1(20140614), cTest2(20140630);

	//DAY
	EXPECT_TRUE(CDate(20140629) == cTest1.Add(PERIODS::DAY, 15, false));	EXPECT_TRUE(CDate(20140531) == cTest1.Add(PERIODS::DAY, -14, false));
	EXPECT_TRUE(CDate(20140629) == cTest1.Add(PERIODS::DAY, 15, true));		EXPECT_TRUE(CDate(20140531) == cTest1.Add(PERIODS::DAY, -14, true));

	EXPECT_TRUE(CDate(20140715) == cTest2.Add(PERIODS::DAY, 15, false));	EXPECT_TRUE(CDate(20140616) == cTest2.Add(PERIODS::DAY, -14, false));
	EXPECT_TRUE(CDate(20140715) == cTest2.Add(PERIODS::DAY, 15, true));	EXPECT_TRUE(CDate(20140616) == cTest2.Add(PERIODS::DAY, -14, true));

	//WEEK
	EXPECT_TRUE(CDate(20140705) == cTest1.Add(PERIODS::WEEK, 3, false));	EXPECT_TRUE(CDate(20140524) == cTest1.Add(PERIODS::WEEK, -3, false));
	EXPECT_TRUE(CDate(20140705) == cTest1.Add(PERIODS::WEEK, 3, true));		EXPECT_TRUE(CDate(20140524) == cTest1.Add(PERIODS::WEEK, -3, true));

	EXPECT_TRUE(CDate(20140721) == cTest2.Add(PERIODS::WEEK, 3, false));	EXPECT_TRUE(CDate(20140609) == cTest2.Add(PERIODS::WEEK, -3, false));
	EXPECT_TRUE(CDate(20140721) == cTest2.Add(PERIODS::WEEK, 3, true));		EXPECT_TRUE(CDate(20140609) == cTest2.Add(PERIODS::WEEK, -3, true));

	//MONTH
	EXPECT_TRUE(CDate(20150714) == cTest1.Add(PERIODS::MONTH, 13, false));	EXPECT_TRUE(CDate(20130514) == cTest1.Add(PERIODS::MONTH, -13, false));
	EXPECT_TRUE(CDate(20150714) == cTest1.Add(PERIODS::MONTH, 13, true));	EXPECT_TRUE(CDate(20130514) == cTest1.Add(PERIODS::MONTH, -13, true));

	EXPECT_TRUE(CDate(20150730) == cTest2.Add(PERIODS::MONTH, 13, false));	EXPECT_TRUE(CDate(20130530) == cTest2.Add(PERIODS::MONTH, -13, false));
	EXPECT_TRUE(CDate(20150731) == cTest2.Add(PERIODS::MONTH, 13, true));	EXPECT_TRUE(CDate(20130531) == cTest2.Add(PERIODS::MONTH, -13, true));

	//QUARTER
	EXPECT_TRUE(CDate(20151214) == cTest1.Add(PERIODS::QUARTER, 6, false));	EXPECT_TRUE(CDate(20121214) == cTest1.Add(PERIODS::QUARTER, -6, false));
	EXPECT_TRUE(CDate(20151214) == cTest1.Add(PERIODS::QUARTER, 6, true));		EXPECT_TRUE(CDate(20121214) == cTest1.Add(PERIODS::QUARTER, -6, true));

	EXPECT_TRUE(CDate(20151230) == cTest2.Add(PERIODS::QUARTER, 6, false));	EXPECT_TRUE(CDate(20121230) == cTest2.Add(PERIODS::QUARTER, -6, false));
	EXPECT_TRUE(CDate(20151231) == cTest2.Add(PERIODS::QUARTER, 6, true));		EXPECT_TRUE(CDate(20121231) == cTest2.Add(PERIODS::QUARTER, -6, true));

	//YEAR
	EXPECT_TRUE(CDate(20200614) == cTest1.Add(PERIODS::YEAR, 6, false));	EXPECT_TRUE(CDate(20080614) == cTest1.Add(PERIODS::YEAR, -6, false));
	EXPECT_TRUE(CDate(20200614) == cTest1.Add(PERIODS::YEAR, 6, true));	EXPECT_TRUE(CDate(20080614) == cTest1.Add(PERIODS::YEAR, -6, true));

	EXPECT_TRUE(CDate(20200630) == cTest2.Add(PERIODS::YEAR, 6, false));	EXPECT_TRUE(CDate(20080630) == cTest2.Add(PERIODS::YEAR, -6, false));
	EXPECT_TRUE(CDate(20200630) == cTest2.Add(PERIODS::YEAR, 6, true));	EXPECT_TRUE(CDate(20080630) == cTest2.Add(PERIODS::YEAR, -6, true));

	// Period
	EXPECT_TRUE(CDate(20140714) == cTest1.Add(SPeriod(FREQUENCY::MONTHLY), false));
}

TEST(SmartDateTests, Day)
{
	EXPECT_EQ(20, CDate(20140620).Day());
}

TEST(SmartDateTests, DayOfWeek)
{
	EXPECT_EQ(DAYSOFWEEK::SATURDAY, CDate(20140614).DayOfWeek());
	EXPECT_EQ(DAYSOFWEEK::TUESDAY, CDate(20150616).DayOfWeek());
}

TEST(SmartDateTests, DayOfYear)
{
	EXPECT_EQ(31, CDate(20140131).DayOfYear());
	EXPECT_EQ(105, CDate(20140415).DayOfYear());
}

TEST(SmartDateTests, DaysInMonth)
{
	EXPECT_EQ(31, CDate(20000101).DaysInMonth());
	EXPECT_EQ(29, CDate(20000201).DaysInMonth());
	EXPECT_EQ(31, CDate(20000301).DaysInMonth());
	EXPECT_EQ(30, CDate(20000401).DaysInMonth());
	EXPECT_EQ(31, CDate(20000501).DaysInMonth());
	EXPECT_EQ(30, CDate(20000601).DaysInMonth());
	EXPECT_EQ(31, CDate(20000701).DaysInMonth());
	EXPECT_EQ(31, CDate(20000801).DaysInMonth());
	EXPECT_EQ(30, CDate(20000901).DaysInMonth());
	EXPECT_EQ(31, CDate(20001001).DaysInMonth());
	EXPECT_EQ(30, CDate(20001101).DaysInMonth());
	EXPECT_EQ(31, CDate(20001201).DaysInMonth());
	EXPECT_EQ(31, CDate(20010101).DaysInMonth());
	EXPECT_EQ(28, CDate(20010201).DaysInMonth());
	EXPECT_EQ(31, CDate(20010301).DaysInMonth());
	EXPECT_EQ(30, CDate(20010401).DaysInMonth());
	EXPECT_EQ(31, CDate(20010501).DaysInMonth());
	EXPECT_EQ(30, CDate(20010601).DaysInMonth());
	EXPECT_EQ(31, CDate(20010701).DaysInMonth());
	EXPECT_EQ(31, CDate(20010801).DaysInMonth());
	EXPECT_EQ(30, CDate(20010901).DaysInMonth());
	EXPECT_EQ(31, CDate(20011001).DaysInMonth());
	EXPECT_EQ(30, CDate(20011101).DaysInMonth());
	EXPECT_EQ(31, CDate(20011201).DaysInMonth());
}

TEST(SmartDateTests, DaysInYear)
{
	EXPECT_EQ(365, CDate(19900101).DaysInYear());
	EXPECT_EQ(365, CDate(19910101).DaysInYear());
	EXPECT_EQ(366, CDate(19920101).DaysInYear());
	EXPECT_EQ(365, CDate(19930101).DaysInYear());
	EXPECT_EQ(365, CDate(19940101).DaysInYear());
	EXPECT_EQ(365, CDate(19950101).DaysInYear());
	EXPECT_EQ(366, CDate(19960101).DaysInYear());
	EXPECT_EQ(365, CDate(19970101).DaysInYear());
	EXPECT_EQ(365, CDate(19980101).DaysInYear());
	EXPECT_EQ(365, CDate(19990101).DaysInYear());

	EXPECT_EQ(366, CDate(20000101).DaysInYear());
	EXPECT_EQ(365, CDate(20010101).DaysInYear());
	EXPECT_EQ(365, CDate(20020101).DaysInYear());
	EXPECT_EQ(365, CDate(20030101).DaysInYear());
	EXPECT_EQ(366, CDate(20040101).DaysInYear());
	EXPECT_EQ(365, CDate(20050101).DaysInYear());
	EXPECT_EQ(365, CDate(20060101).DaysInYear());
	EXPECT_EQ(365, CDate(20070101).DaysInYear());
	EXPECT_EQ(366, CDate(20080101).DaysInYear());
	EXPECT_EQ(365, CDate(20090101).DaysInYear());
}

TEST(SmartDateTests, Diff)
{
	CDate cTemp(20140620);

	EXPECT_EQ(11, CDate(20140701).Diff(cTemp, PERIODS::DAY));
	EXPECT_EQ(1, CDate(20140701).Diff(cTemp, PERIODS::WEEK));
	EXPECT_EQ(1, CDate(20140701).Diff(cTemp, PERIODS::MONTH));
	EXPECT_EQ(1, CDate(20140701).Diff(cTemp, PERIODS::QUARTER));
	EXPECT_EQ(-1, CDate(20130701).Diff(cTemp, PERIODS::YEAR));
}

TEST(SmartDateTests, IsLeapYear)
{
	EXPECT_FALSE(CDate(20130601).IsLeapYear());
	EXPECT_TRUE(CDate(20120512).IsLeapYear());
	EXPECT_TRUE(CDate(20001231).IsLeapYear());
	EXPECT_FALSE(CDate(19000726).IsLeapYear());
}

TEST(SmartDateTests, IsWeekday)
{
	EXPECT_FALSE(CDate(20140614).IsWeekday());
	EXPECT_FALSE(CDate(20140615).IsWeekday());
	EXPECT_TRUE(CDate(20140616).IsWeekday());
	EXPECT_TRUE(CDate(20140617).IsWeekday());
	EXPECT_TRUE(CDate(20140618).IsWeekday());
	EXPECT_TRUE(CDate(20140619).IsWeekday());
	EXPECT_TRUE(CDate(20140620).IsWeekday());
}

TEST(SmartDateTests, IsWeekend)
{
	EXPECT_TRUE(CDate(20140614).IsWeekend());
	EXPECT_TRUE(CDate(20140615).IsWeekend());
	EXPECT_FALSE(CDate(20140616).IsWeekend());
	EXPECT_FALSE(CDate(20140617).IsWeekend());
	EXPECT_FALSE(CDate(20140618).IsWeekend());
	EXPECT_FALSE(CDate(20140619).IsWeekend());
	EXPECT_FALSE(CDate(20140620).IsWeekend());
}

TEST(SmartDateTests, Month)
{
	EXPECT_EQ(MONTHSOFYEAR::JUNE, CDate(20130620).Month());
}

TEST(SmartDateTests, Move)
{
	CDate cTest;

	//DAY
	cTest = 20140614; cTest.Move(PERIODS::DAY, 15, false);	EXPECT_TRUE(CDate(20140629) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::DAY, -14, false);	EXPECT_TRUE(CDate(20140531) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::DAY, 15, true);	EXPECT_TRUE(CDate(20140629) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::DAY, -14, true);	EXPECT_TRUE(CDate(20140531) == cTest);

	cTest = 20140630; cTest.Move(PERIODS::DAY, 15, false);	EXPECT_TRUE(CDate(20140715) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::DAY, -14, false);	EXPECT_TRUE(CDate(20140616) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::DAY, 15, true);	EXPECT_TRUE(CDate(20140715) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::DAY, -14, true);	EXPECT_TRUE(CDate(20140616) == cTest);

	//WEEK
	cTest = 20140614; cTest.Move(PERIODS::WEEK, 3, false);	EXPECT_TRUE(CDate(20140705) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::WEEK, -3, false);	EXPECT_TRUE(CDate(20140524) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::WEEK, 3, true);	EXPECT_TRUE(CDate(20140705) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::WEEK, -3, true);	EXPECT_TRUE(CDate(20140524) == cTest);

	cTest = 20140630; cTest.Move(PERIODS::WEEK, 3, false);	EXPECT_TRUE(CDate(20140721) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::WEEK, -3, false);	EXPECT_TRUE(CDate(20140609) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::WEEK, 3, true);	EXPECT_TRUE(CDate(20140721) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::WEEK, -3, true);	EXPECT_TRUE(CDate(20140609) == cTest);

	//MONTH
	cTest = 20140614; cTest.Move(PERIODS::MONTH, 13, false);	EXPECT_TRUE(CDate(20150714) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::MONTH, -13, false);	EXPECT_TRUE(CDate(20130514) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::MONTH, 13, true);		EXPECT_TRUE(CDate(20150714) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::MONTH, -13, true);	EXPECT_TRUE(CDate(20130514) == cTest);

	cTest = 20140630; cTest.Move(PERIODS::MONTH, 13, false);	EXPECT_TRUE(CDate(20150730) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::MONTH, -13, false);	EXPECT_TRUE(CDate(20130530) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::MONTH, 13, true);		EXPECT_TRUE(CDate(20150731) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::MONTH, -13, true);	EXPECT_TRUE(CDate(20130531) == cTest);

	//QUARTER
	cTest = 20140614; cTest.Move(PERIODS::QUARTER, 6, false);	EXPECT_TRUE(CDate(20151214) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::QUARTER, -6, false);	EXPECT_TRUE(CDate(20121214) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::QUARTER, 6, true);	EXPECT_TRUE(CDate(20151214) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::QUARTER, -6, true);	EXPECT_TRUE(CDate(20121214) == cTest);

	cTest = 20140630; cTest.Move(PERIODS::QUARTER, 6, false);	EXPECT_TRUE(CDate(20151230) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::QUARTER, -6, false);	EXPECT_TRUE(CDate(20121230) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::QUARTER, 6, true);	EXPECT_TRUE(CDate(20151231) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::QUARTER, -6, true);	EXPECT_TRUE(CDate(20121231) == cTest);

	//YEAR
	cTest = 20140614; cTest.Move(PERIODS::YEAR, 6, false);	EXPECT_TRUE(CDate(20200614) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::YEAR, -6, false);	EXPECT_TRUE(CDate(20080614) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::YEAR, 6, true);	EXPECT_TRUE(CDate(20200614) == cTest);
	cTest = 20140614; cTest.Move(PERIODS::YEAR, -6, true);	EXPECT_TRUE(CDate(20080614) == cTest);

	cTest = 20140630; cTest.Move(PERIODS::YEAR, 6, false);	EXPECT_TRUE(CDate(20200630) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::YEAR, -6, false);	EXPECT_TRUE(CDate(20080630) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::YEAR, 6, true);	EXPECT_TRUE(CDate(20200630) == cTest);
	cTest = 20140630; cTest.Move(PERIODS::YEAR, -6, true);	EXPECT_TRUE(CDate(20080630) == cTest);

	// Period
	cTest = 20140614; cTest.Move(SPeriod(FREQUENCY::MONTHLY), true);	EXPECT_TRUE(CDate(20140714) == cTest);
}

TEST(SmartDateTests, Quarter)
{
	EXPECT_EQ(QUARTERS::FIRST, CDate(20130101).Quarter());
	EXPECT_EQ(QUARTERS::FIRST, CDate(20130331).Quarter());

	EXPECT_EQ(QUARTERS::SECOND, CDate(20130401).Quarter());
	EXPECT_EQ(QUARTERS::SECOND, CDate(20130630).Quarter());

	EXPECT_EQ(QUARTERS::THIRD, CDate(20130701).Quarter());
	EXPECT_EQ(QUARTERS::THIRD, CDate(20130930).Quarter());

	EXPECT_EQ(QUARTERS::FOURTH, CDate(20131001).Quarter());
	EXPECT_EQ(QUARTERS::FOURTH, CDate(20131231).Quarter());
}

TEST(SmartDateTests, SetDay)
{
	CDate cTemp;

	cTemp = 20140620;	cTemp.SetDay(15);	EXPECT_TRUE(CDate(20140615) == cTemp);
	cTemp = 20140620;	cTemp.SetDay(31);	EXPECT_TRUE(CDate(20140630) == cTemp);
	cTemp = 20140620;	cTemp.SetDay(0);	EXPECT_TRUE(CDate(20140601) == cTemp);
}

TEST(SmartDateTests, SetMonth)
{
	CDate cTemp;

	cTemp = 20140731;	cTemp.SetMonth(MONTHSOFYEAR::FEBRUARY);	EXPECT_TRUE(CDate(20140228) == cTemp);
	cTemp = 20140731;	cTemp.SetMonth(MONTHSOFYEAR::MARCH);		EXPECT_TRUE(CDate(20140331) == cTemp);
	cTemp = 20140731;	cTemp.SetMonth(MONTHSOFYEAR::SEPTEMBER);	EXPECT_TRUE(CDate(20140930) == cTemp);
}

TEST(SmartDateTests, SetToFirstOfMonth)
{
	CDate cTemp;

	cTemp = 20120120;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120101) == cTemp);
	cTemp = 20120220;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120201) == cTemp);
	cTemp = 20120320;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120301) == cTemp);
	cTemp = 20120420;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120401) == cTemp);
	cTemp = 20120520;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120501) == cTemp);
	cTemp = 20120620;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120601) == cTemp);
	cTemp = 20120720;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120701) == cTemp);
	cTemp = 20120820;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120801) == cTemp);
	cTemp = 20120920;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20120901) == cTemp);
	cTemp = 20121020;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20121001) == cTemp);
	cTemp = 20121120;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20121101) == cTemp);
	cTemp = 20121220;	cTemp.SetToFirstOfMonth();	EXPECT_TRUE(CDate(20121201) == cTemp);
}

TEST(SmartDateTests, SetToFirstOfYear)
{
	CDate cTemp;
	
	cTemp = 20140620;	cTemp.SetToFirstOfYear();	EXPECT_TRUE(CDate(20140101) == cTemp);
}

TEST(SmartDateTests, SetToLastOfMonth)
{
	CDate cTemp;

	cTemp = 20120120;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120131) == cTemp);
	cTemp = 20120220;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120229) == cTemp);
	cTemp = 20120320;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120331) == cTemp);
	cTemp = 20120420;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120430) == cTemp);
	cTemp = 20120520;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120531) == cTemp);
	cTemp = 20120620;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120630) == cTemp);
	cTemp = 20120720;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120731) == cTemp);
	cTemp = 20120820;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120831) == cTemp);
	cTemp = 20120920;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20120930) == cTemp);
	cTemp = 20121020;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20121031) == cTemp);
	cTemp = 20121120;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20121130) == cTemp);
	cTemp = 20121220;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20121231) == cTemp);

	cTemp = 20140120;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140131) == cTemp);
	cTemp = 20140220;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140228) == cTemp);
	cTemp = 20140320;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140331) == cTemp);
	cTemp = 20140420;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140430) == cTemp);
	cTemp = 20140520;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140531) == cTemp);
	cTemp = 20140620;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140630) == cTemp);
	cTemp = 20140720;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140731) == cTemp);
	cTemp = 20140820;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140831) == cTemp);
	cTemp = 20140920;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20140930) == cTemp);
	cTemp = 20141020;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20141031) == cTemp);
	cTemp = 20141120;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20141130) == cTemp);
	cTemp = 20141220;	cTemp.SetToLastOfMonth();	EXPECT_TRUE(CDate(20141231) == cTemp);
}

TEST(SmartDateTests, SetToLastOfYear)
{
	CDate cTemp;

	cTemp = 20140620;	cTemp.SetToLastOfYear();	EXPECT_TRUE(CDate(20141231) == cTemp);
}

TEST(SmartDateTests, SetYear)
{
	CDate cTemp;

	cTemp = 20140228;	cTemp.SetYear(2013);	EXPECT_TRUE(CDate(20130228) == cTemp);
	cTemp = 20140228;	cTemp.SetYear(2012);	EXPECT_TRUE(CDate(20120228) == cTemp);
	cTemp = 20120229;	cTemp.SetYear(2013);	EXPECT_TRUE(CDate(20130228) == cTemp);
}

TEST(ToolsStringTests, DateToString)
{
	CDate dtTest(20140620);
	EXPECT_EQ("20140620", to_string(dtTest));
}

TEST(SmartDateTests, Year)
{
	EXPECT_EQ(2013, CDate(20130620).Year());
}

TEST(SmartDateTests, YearShare)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, std::fabs(CDate(20140131).YearShare() - 31.0 / 365.0));
	EXPECT_GE(dTolerance, std::fabs(CDate(20140715).YearShare() - (31.0 + 28.0 + 31.0 + 30.0 + 31.0 + 30.0 + 15.0) / 365.0));
	EXPECT_GE(dTolerance, std::fabs(CDate(20120715).YearShare() - (31.0 + 29.0 + 31.0 + 30.0 + 31.0 + 30.0 + 15.0) / 366.0));
}
#endif
