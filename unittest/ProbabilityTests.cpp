#ifndef INCLUDE_H_PROBABILITYTESTS
#define INCLUDE_H_PROBABILITYTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "probability/all.h"

class BinomialTests : public testing::Test
{
protected:
	const double	mdTolerance = 0.00000001;
	const int32_t	miTrials = 10;
	const double	mdNum = CMath::factorial(miTrials);

	const double	mdProbHeads = 0.9;
	const double	mdProbTails = 1.0 - mdProbHeads;

	CSmartPtr<CBinomial> mcDistPtr;

public:
	void SetUp(void)
	{
		mcDistPtr = CreateSmart<CBinomial>(miTrials, mdProbHeads);
	}

	void TearDown(void) {}
};

TEST_F(BinomialTests, CDF)
{
	double dEst(0.0);
	for (int32_t iHeads(0), iTails(miTrials); iHeads <= miTrials; iHeads++, iTails--)
	{
		dEst += mdNum / (CMath::factorial(iHeads) * CMath::factorial(iTails)) * pow(mdProbHeads, iHeads) * pow(mdProbTails, iTails);
		EXPECT_GT(mdTolerance, fabs(mcDistPtr->CDF(iHeads) - dEst));
	}
}

TEST_F(BinomialTests, InverseCDF)
{
	EXPECT_EQ(0, mcDistPtr->InverseCDF(0));
	EXPECT_EQ(1, mcDistPtr->InverseCDF(1.001e-10));
	EXPECT_EQ(3, mcDistPtr->InverseCDF(1e-6));
	EXPECT_EQ(6, mcDistPtr->InverseCDF(0.01));
	EXPECT_EQ(8, mcDistPtr->InverseCDF(0.1));
	EXPECT_EQ(10, mcDistPtr->InverseCDF(1.0));
}

TEST_F(BinomialTests, PDF)
{
	double dEst(0.0);
	for (int32_t iHeads(0), iTails(miTrials); iHeads <= miTrials; iHeads++, iTails--)
	{
		dEst = mdNum / (CMath::factorial(iHeads) * CMath::factorial(iTails)) * pow(mdProbHeads, iHeads) * pow(mdProbTails, iTails);
		EXPECT_GT(mdTolerance, fabs(mcDistPtr->PDF(iHeads) - dEst));
	}
}


class PoissonTests : public testing::Test
{
protected:
	const double	mdTolerance = 0.00000001;
	const double	mdRate = 2.5;

	const int32_t		miThreshold = 20;
	CSmartPtr<CPoisson> mcDistPtr;

public:
	void SetUp(void)
	{
		mcDistPtr = CreateSmart<CPoisson>(mdRate);
	}

	void TearDown(void) {}
};

TEST_F(PoissonTests, CDF)
{
	double dEst(0.0);
	for (int32_t iEventCnt(0); iEventCnt < miThreshold; iEventCnt++)
	{
		dEst += CMath::exp(-mdRate) * pow(mdRate, iEventCnt) / CMath::factorial(iEventCnt);
		EXPECT_GT(mdTolerance, fabs(mcDistPtr->CDF(iEventCnt) - dEst));
	}
}

TEST_F(PoissonTests, InverseCDF)
{
	EXPECT_EQ(0, mcDistPtr->InverseCDF(0));
	EXPECT_EQ(1, mcDistPtr->InverseCDF(0.085));
	EXPECT_EQ(3, mcDistPtr->InverseCDF(0.55));
	EXPECT_EQ(7, mcDistPtr->InverseCDF(0.99));
	EXPECT_EQ(12, mcDistPtr->InverseCDF(0.99999));
}

TEST_F(PoissonTests, PDF)
{
	double dEst(0.0);
	for (int32_t iEventCnt(0); iEventCnt < miThreshold; iEventCnt++)
	{
		dEst = CMath::exp(-mdRate) * pow(mdRate, iEventCnt) / CMath::factorial(iEventCnt);
		EXPECT_GT(mdTolerance, fabs(mcDistPtr->PDF(iEventCnt) - dEst));
	}
}


class LogisticTests : public testing::Test
{
protected:
	const double	mdTolerance = 0.00000001;

	CSmartPtr<CLogistic> mcDistPtr;

public:
	void SetUp(void)
	{
		mcDistPtr = CreateSmart<CLogistic>(0.0, 1.0);
	}

	void TearDown(void) {}
};

TEST_F(LogisticTests, CDF)
{
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->CDF(-dLN2) - 0.3333333333333333));
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->CDF(0.0) - 0.5));
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->CDF(dLN2) - 0.666666666666667));
	for (int32_t iCount = 1; iCount < 20; iCount++)
	{
		EXPECT_GE(mdTolerance, fabs(mcDistPtr->CDF(1.0 * iCount) + mcDistPtr->CDF(-1.0 * iCount) - 1.0));
	}
}

TEST_F(LogisticTests, InverseCDF)
{
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->InverseCDF(0.3333333333333333) + dLN2));
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->InverseCDF(0.5) - 0.0));
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->InverseCDF(0.6666666666666667) - dLN2));
	for (int32_t iCount = 1; iCount < 5; iCount++)
	{
		EXPECT_GE(mdTolerance, fabs(mcDistPtr->InverseCDF(0.1 * iCount) + mcDistPtr->InverseCDF(1.0 - 0.1 * iCount)));
	}

}

TEST_F(LogisticTests, PDF)
{
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->PDF(-dLN2) - 0.2222222222222222));
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->PDF(0.0) - 0.25));
	EXPECT_GE(mdTolerance, fabs(mcDistPtr->PDF(dLN2) - 0.2222222222222222));
	for (int32_t iCount = 1; iCount < 20; iCount++)
	{
		EXPECT_GE(mdTolerance, fabs(mcDistPtr->PDF(1.0 * iCount) - mcDistPtr->PDF(-1.0 * iCount)));
	}
}

TEST_F(LogisticTests, Consistency)
{
	const double dStep = 0.00001;

	for (double dValue(dStep); dValue < 1.0; dValue += dStep)
	{
		EXPECT_GE(mdTolerance, fabs(mcDistPtr->CDF(mcDistPtr->InverseCDF(dValue)) - dValue));
	}
}


class NormalTests : public testing::Test
{
protected:
	CSmartPtr<CNormal> mcDistPtr;

public:
	void SetUp(void)
	{
		mcDistPtr = CreateSmart<CNormal>(0.0, 1.0);
	}

	void TearDown(void) {}
};

TEST_F(NormalTests, CDF)
{
	const double dTolerance = 7.5E-8; //maximum guaranteed error by algorithm

	EXPECT_GE(dTolerance, fabs(mcDistPtr->CDF(0.0) - 0.5));
	EXPECT_GE(dTolerance, fabs(mcDistPtr->CDF(0.6744897502234225) - 0.75));
	EXPECT_GE(dTolerance, fabs(mcDistPtr->CDF(2.053748909003034) - 0.98));
	for (int32_t iCount = 1; iCount < 20; iCount++)
	{
		EXPECT_GE(dTolerance, fabs(mcDistPtr->CDF(1.0 * iCount) + mcDistPtr->CDF(-1.0 * iCount) - 1.0));
	}
}

TEST_F(NormalTests, InverseCDF)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(mcDistPtr->InverseCDF(0.5) - 0.0));
	EXPECT_GE(dTolerance, fabs(mcDistPtr->InverseCDF(0.75) - 0.6744897502234225));
	EXPECT_GE(dTolerance, fabs(mcDistPtr->InverseCDF(0.98) - 2.053748909003034));
	for (int32_t iCount = 1; iCount < 5; iCount++)
	{
		EXPECT_GE(dTolerance, fabs(mcDistPtr->InverseCDF(0.1 * iCount) + mcDistPtr->InverseCDF(1.0 - 0.1 * iCount)));
	}
}

TEST_F(NormalTests, PDF)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(mcDistPtr->PDF(0.0) - 1 / dSQRT2PI));
	EXPECT_GE(dTolerance, fabs(mcDistPtr->PDF(sqrt(2 * dLN2)) - 0.5 / dSQRT2PI));
	for (int32_t iCount = 1; iCount < 20; iCount++)
	{
		EXPECT_GE(dTolerance, fabs(mcDistPtr->PDF(1.0 * iCount) - mcDistPtr->PDF(-1.0 * iCount)));
	}
}

TEST_F(NormalTests, Consistency)
{
	const double dTolerance = 7.5E-8;
	const double dStep = 0.00001;

	for (double dValue(dStep); dValue < 1.0; dValue += dStep)
	{
		EXPECT_GE(dTolerance, fabs(mcDistPtr->CDF(mcDistPtr->InverseCDF(dValue)) - dValue));
	}
}
#endif
