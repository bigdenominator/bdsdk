#ifndef INCLUDE_H_WAITRELEASE
#define INCLUDE_H_WAITRELEASE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <mutex>
#include <condition_variable>

class BDWaitRelease
{
private:
	typedef std::unique_lock<std::mutex>	MutexLock;

    std::condition_variable     mcCVSignal;
    std::mutex                  mcMLock;

    bool                        mbRelease;

public:
    BDWaitRelease(void) : mbRelease(false) {}

    void WaitForRelease(void)
    {
		MutexLock cLock(mcMLock);
		mcCVSignal.wait(cLock, [this](void){ return mbRelease; });
    }

    void Release(void)
    {
		MutexLock cLock(mcMLock);
		mbRelease = true;
        mcCVSignal.notify_all();
    }
};
#endif