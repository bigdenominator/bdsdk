#ifndef INCLUDE_H_ARRAYTOOLKIT
#define INCLUDE_H_ARRAYTOOLKIT
#include "CommonDefs.h"				// Must be first - Defines WINDOWS and UNIX
#ifdef WINDOWS
#pragma once
#endif // WINDOWS

#include "SmartArray.h"
#include "SmartVector.h"
#include "ToolsMath.h"


struct ArrayTools
{
	template<typename T, typename U> static inline bool abs(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		for (typename T::size_type iIndx(0); iIndx < inp_cResultPtr->size(); iIndx++)
		{
			(*inp_cResultPtr)[iIndx] = std::abs((*inp_cDataPtr)[iIndx]);
		}
		return true;
	}

	template<typename T, typename U, typename TData> static inline bool add(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, const TData& inp_Const)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		SIMD::add(inp_cResultPtr->data(), inp_cDataPtr->data(), static_cast < typename T::value_type > (inp_Const), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename V> static inline bool add(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cArray1Ptr, CSmartPtr<V> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		SIMD::add(inp_cResultPtr->data(), inp_cArray1Ptr->data(), inp_cArray2Ptr->data(), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U> static inline bool cos(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr)
	{
		typedef typename T::value_type value_type;

		return inp_cResultPtr->transform(*inp_cDataPtr, [](value_type d){ return CMath::cos(d); });
	}

	template<typename T, typename U, typename TData> static inline bool divide(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, const TData& inp_Const)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		SIMD::divide(inp_cResultPtr->data(), inp_cDataPtr->data(), static_cast<typename T::value_type>(inp_Const), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename TData> static inline bool divide(CSmartPtr<T> inp_cResultPtr, const TData& inp_Const, CSmartPtr<U> inp_cDataPtr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		SIMD::divide(inp_cResultPtr->data(), static_cast<typename T::value_type>(inp_Const), inp_cDataPtr->data(), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename V> static inline bool divide(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cArray1Ptr, CSmartPtr<V> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		SIMD::divide(inp_cResultPtr->data(), inp_cArray1Ptr->data(), inp_cArray2Ptr->data(), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename TData> static inline bool dotProduct(TData& inp_cResult, CSmartPtr<T> inp_cArray1Ptr, CSmartPtr<U> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		CSmartPtr<T> cTempPtr(T::CreateInstance(inp_cArray1Ptr));
		*cTempPtr *= *inp_cArray2Ptr;
		inp_cResult = cTempPtr->addElements();

		return true;
	}

	template<typename T, typename U> static inline typename T::value_type dotProduct(CSmartPtr<T> inp_cArray1Ptr, CSmartPtr<U> inp_cArray2Ptr, bool& inp_bValid)
	{
		typename T::value_type Result;
		inp_bValid = dotProduct(Result, inp_cArray1Ptr, inp_cArray2Ptr);

		return Result;
	}

	template<typename T, typename U> static inline bool exp(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr)
	{
		typedef typename T::value_type value_type;

		return inp_cResultPtr->transform(*inp_cDataPtr, [](value_type d){ return CMath::exp(d); });
	}

	template<typename T, typename U, typename V> static inline bool join(CSmartPtr<T> inp_cResultsPtr, CSmartPtr<U> inp_cArray1Ptr, CSmartPtr<V> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cResultsPtr, inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		SIMD::join(inp_cResultsPtr->data(), inp_cArray1Ptr->data(), inp_cArray2Ptr->data(), inp_cResultsPtr->size());
		return true;
	}

	template<typename T, typename U, typename V, typename TData> static inline bool linearCombo(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cBasePointPtr, CSmartPtr<V> inp_cOtherPointPtr, const TData& inp_dFrac)
	{
		if (ArrayTools::subtract(inp_cResultPtr, inp_cOtherPointPtr, inp_cBasePointPtr))
		{
			*inp_cResultPtr *= inp_dFrac;
			*inp_cResultPtr += *inp_cBasePointPtr;
			return true;
		}
		return false;
	}

	template<typename T, typename U> static inline bool ln(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr)
	{
		typedef typename T::value_type value_type;

		return inp_cResultPtr->transform(*inp_cDataPtr, [](value_type d){ return CMath::ln(d); });
	}

	template<typename TOut, typename TMatrix, typename TVec> static inline bool matrixMultiply(CSmartPtr<TOut> inp_cResultPtr, CSmartPtr<TMatrix> inp_cMatrixPtr, CSmartPtr<TVec> inp_cDataPtr)
	{
		typedef typename TOut::size_type	size_type;
		typedef typename TOut::value_type	value_type;

		bool bValid;

		if (!sizeAgree(inp_cResultPtr, inp_cMatrixPtr)) return false;
		for (size_type iIndx(0); iIndx < inp_cResultPtr->size(); iIndx++)
		{
			if (!sizeAgree(inp_cDataPtr, (*inp_cMatrixPtr)[iIndx])) return false;
			(*inp_cResultPtr)[iIndx] = static_cast<value_type>(dotProduct((*inp_cMatrixPtr)[iIndx], inp_cDataPtr, bValid));
		}
		return true;
	}

	template<typename T, typename U, typename V> static inline bool meet(CSmartPtr<T> inp_cResultsPtr, CSmartPtr<U> inp_cArray1Ptr, CSmartPtr<V> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cResultsPtr, inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		SIMD::meet(inp_cResultsPtr->data(), inp_cArray1Ptr->data(), inp_cArray2Ptr->data(), inp_cResultsPtr->size());
		return true;
	}

	template<typename T, typename U, typename TData> static inline bool multiply(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, const TData& inp_Const)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		SIMD::multiply(inp_cResultPtr->data(), inp_cDataPtr->data(), static_cast < typename T::value_type > (inp_Const), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename V> static inline bool multiply(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cArray1Ptr, CSmartPtr<V> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		SIMD::multiply(inp_cResultPtr->data(), inp_cArray1Ptr->data(), inp_cArray2Ptr->data(), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename TCof> static inline bool polynomial(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, CSmartPtr<TCof> inp_cCoefPtr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		CSmartPtr<U> cTempPtr;
		if (inp_cResultPtr == inp_cDataPtr)
		{
			cTempPtr = U::CreateInstance(inp_cDataPtr);
		}
		else
		{
			cTempPtr = inp_cDataPtr;
		}

		*inp_cResultPtr = static_cast<typename T::value_type>((*inp_cCoefPtr)[0]);
		for (typename T::size_type iIndx(1); iIndx < inp_cCoefPtr->size(); iIndx++)
		{
			*inp_cResultPtr *= *cTempPtr;
			*inp_cResultPtr += static_cast<typename T::value_type>((*inp_cCoefPtr)[iIndx]);
		}
		return true;
	}

	template<typename T, typename U, typename TExp> static inline bool pow(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, const TExp& inp_Exponent)
	{
		typedef typename T::value_type value_type;

		return inp_cResultPtr->transform(*inp_cDataPtr, [&inp_Exponent](value_type d){ return std::pow(d, inp_Exponent); });
	}

	template<typename T, typename U, typename TExp> static inline bool pow(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, CSmartPtr<TExp> inp_cExponentsPtr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr, inp_cExponentsPtr)) return false;

		for (typename T::size_type iIndx(0); iIndx < inp_cResultPtr->size(); iIndx++)
		{
			(*inp_cResultPtr)[iIndx] = std::pow((*inp_cDataPtr)[iIndx], (*inp_cExponentsPtr)[iIndx]);
		}
		return true;
	}

	template<typename T, typename U> static inline bool sin(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr)
	{
		typedef typename T::value_type value_type;

		return inp_cResultPtr->transform(*inp_cDataPtr, [](value_type d){ return CMath::sin(d); });
	}
	
	template<typename T, typename U> inline static bool sizeAgree(CSmartPtr<T> firstPtr, CSmartPtr<U> secondPtr)
	{
		return (firstPtr->size() == secondPtr->size());
	}

	template<typename T, typename U, typename ...Stuff> inline static bool sizeAgree(CSmartPtr<T> firstPtr, CSmartPtr<U> secondPtr, Stuff... myStuff)
	{
		if (sizeAgree(firstPtr, secondPtr)) return sizeAgree(secondPtr, myStuff...);

		return false;
	}

	template<typename T, typename U> static inline bool sqrt(CSmartPtr<T> inp_cResultsPtr, CSmartPtr<U> inp_cDataPtr)
	{
		if (!sizeAgree(inp_cResultsPtr, inp_cDataPtr)) return false;

		SIMD::sqrt(inp_cResultsPtr->data(), inp_cDataPtr->data(), inp_cResultsPtr->size());
		return true;
	}

	template<typename T, typename U, typename TData> static inline bool subtract(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr, const TData& inp_Const)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		SIMD::subtract(inp_cResultPtr->data(), inp_cDataPtr->data(), static_cast<typename T::value_type>(inp_Const), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename TData> static inline bool subtract(CSmartPtr<T> inp_cResultPtr, const TData& inp_Const, CSmartPtr<U> inp_cDataPtr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cDataPtr)) return false;

		SIMD::subtract(inp_cResultPtr->data(), static_cast<typename T::value_type>(inp_Const), inp_cDataPtr->data(), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U, typename V> static inline bool subtract(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cArray1Ptr, CSmartPtr<V> inp_cArray2Ptr)
	{
		if (!sizeAgree(inp_cResultPtr, inp_cArray1Ptr, inp_cArray2Ptr)) return false;

		SIMD::subtract(inp_cResultPtr->data(), inp_cArray1Ptr->data(), inp_cArray2Ptr->data(), inp_cResultPtr->size());
		return true;
	}

	template<typename T, typename U> static inline bool tan(CSmartPtr<T> inp_cResultPtr, CSmartPtr<U> inp_cDataPtr)
	{
		typedef typename T::value_type value_type;

		return inp_cResultPtr->transform(*inp_cDataPtr, [](value_type d){ return CMath::cos(d); });
	}
};
#endif
