#ifndef INCLUDE_H_POISSONPROBABILITY
#define INCLUDE_H_POISSONPROBABILITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/SmartVector.h"

class CPoisson
{
public:
	CPoisson(const double& inp_dRateParam) :mdRateParam(inp_dRateParam), mdExpFactor(CMath::exp(-inp_dRateParam)),
		mcCdf(1, mdExpFactor), mcPdf(1, mdExpFactor) {}

	double CDF(CVectorDbl::size_type inp_iValue)
	{
		if (inp_iValue < mcCdf.size())
		{
			return mcCdf[inp_iValue]; 
		}
		else
		{
			extend(inp_iValue);
			return mcCdf[inp_iValue];
		}
	}

	size_t InverseCDF(double inp_dProbability)
	{
		if (inp_dProbability < 0.0) { return 0; }
		else if (dEPSILON > 1.0 - inp_dProbability) { return std::numeric_limits<size_t>::infinity(); }
		else
		{
			CVectorDbl::size_type iIndx(0);
			for (; iIndx < mcCdf.size(); iIndx++)
			{
				if (mcCdf[iIndx] >= inp_dProbability) { return iIndx; }
			}
			while (CDF(iIndx) < inp_dProbability) { iIndx++; }
			return iIndx;
		}
	}

	double PDF(size_t inp_iValue)
	{
		if (inp_iValue < mcPdf.size())
		{
			return mcPdf[inp_iValue]; 
		}
		else
		{
			extend(inp_iValue);
			return mcPdf[inp_iValue];
		}
	}

protected:
	const double	mdRateParam;
	const double	mdExpFactor;

	CVectorDbl	mcCdf;
	CVectorDbl	mcPdf;

	void extend(const CVectorDbl::size_type& inp_lNewMax)
	{
		const CVectorDbl::size_type iNewSize(inp_lNewMax + 1);
		const CVectorDbl::size_type iOldSize(mcCdf.size());

		mcCdf.reserve(iNewSize);
		mcPdf.reserve(iNewSize);

		for (CVectorDbl::size_type iIndx(iOldSize); iIndx < iNewSize; iIndx++)
		{
			mcPdf[iIndx] = mcPdf[iIndx - 1] * mdRateParam / static_cast<double>(iIndx);
			mcCdf[iIndx] = mcCdf[iIndx - 1] + mcPdf[iIndx];
		}
	}
};
#endif
