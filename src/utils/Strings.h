#ifndef INCLUDE_H_TOOLSSTRING
#define INCLUDE_H_TOOLSSTRING

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#ifdef WINDOWS
#pragma warning( disable : 4242 )
#endif
#ifndef _WIN32 
#include <cstring>
#include <cstdlib>
#endif

#include <algorithm>
#include <sstream>
#include <type_traits>

static const int32_t	NAMESTRINGSIZE(256);

static const char		cBACKSLASH('\\');
static const char		cCOMMA(',');
static const char		cCOLON(':');
static const char		cDASH('-');
static const char		cDECIMAL('.');
static const char		cEOL('\n');
static const char		cLINEFEED('\0');
static const char		cPARENLEFT('(');
static const char		cPARENRIGHT(')');
static const char		cSLASH('/');
static const char		cSPACE(' ');
static const char		cUNDERSCORE('_');
static const char		cZERO('0');


static const string		sBackSlash("\\");
static const string		sComma(",");
static const string		sColon(":");
static const string		sDash("-");
static const string		sDecimal(".");
static const string		sEOL("\n");
static const string		sLinefeed("\0");
static const string		sParenLeft("(");
static const string		sParenRight(")");
static const string		sSlash("/");
static const string		sSpace(" ");
static const string		sUnderscore("_");
static const string		sZero("0");

static const string		sCStringEOLError("Unexpected End of Line");
static const string		sCStringError("Exception in ToString()");
static const string		sErrorCStringUnpackSize("CString Unpack size too large");
static const string		sFALSE("FALSE");
static const string		sTRUE("TRUE");
static const string		sZERO("ZERO");

static const string		sLinux("LINUX");
static const string		sOSX("OSX");
static const string		sWindows("WINDOWS");


class CString
{
public:
	static inline string DelimitedSubString(const string& inp_sTargetString, const string& inp_sDelimiter, string::size_type& inp_iStartLoc)
	{
		const string::size_type iSize(inp_sTargetString.size());

		string::size_type iEndLoc(inp_sTargetString.find(inp_sDelimiter, inp_iStartLoc));

		if (iEndLoc == string::npos) { iEndLoc = iSize; }

		string sResult(sNULL);
		if (inp_iStartLoc < iEndLoc)
		{
			sResult = inp_sTargetString.substr(inp_iStartLoc, iEndLoc - inp_iStartLoc);
			RemoveLeadingChar(sResult, cSPACE);
			inp_iStartLoc = iEndLoc + 1;
		}
		else
		{
			inp_iStartLoc++;
		}

		return sResult;
	}

	static inline string RemoveLeadingChar(string& inp_sString, char inp_cChar)
	{
		const string::size_type iSize(inp_sString.size() - 1);
		string::size_type iNewStart(0);

		while (iNewStart < iSize)
		{
			if (inp_sString[iNewStart] == inp_cChar){ iNewStart++; }
			else { break; }
		}
		if (iNewStart > 0){ inp_sString.erase(0, iNewStart); }
			
		return inp_sString;
	}

	static inline string ToLower(string inp_sInputString)
	{
		const string::size_type iSize(inp_sInputString.size());
			
		char* cBufferPtr(const_cast<char*>(inp_sInputString.c_str()));
		for (string::size_type iIndx(0); iIndx < iSize; iIndx++)
		{
			cBufferPtr[iIndx] = tolower(cBufferPtr[iIndx]);
		}
			
		return string(cBufferPtr);
	}

	static inline string ToUpper(string inp_sInputString)
	{
		const string::size_type iSize(inp_sInputString.size());

		char* cBufferPtr(const_cast<char*>(inp_sInputString.c_str()));
		for (string::size_type iIndx(0); iIndx < iSize; iIndx++)
		{
			cBufferPtr[iIndx] = toupper(cBufferPtr[iIndx]);
		}

		return string(cBufferPtr); 
	}
};

static inline void to(const string& inp_sFrom, bool& out_data)
{
	string sTmpStr(CString::ToUpper(inp_sFrom));
	if (sTmpStr == sFALSE) { out_data = false; }
	else if (sTmpStr == sZERO) { out_data = false; }
	else { out_data = (sTmpStr.size() > 0); }
}

static inline void to(const string& inp_sFrom, double& out_data)
{
	out_data = stod(inp_sFrom);
}

static inline void to(const string& inp_sFrom, float& out_data)
{
	out_data = stof(inp_sFrom);
}

static inline void to(const string& inp_sFrom, long double& out_data)
{
	out_data = stold(inp_sFrom);
}

static inline void to(const string& inp_sFrom, int& out_data)
{
	out_data = stoi(inp_sFrom);
}

static inline void to(const string& inp_sFrom, long& out_data)
{
	out_data = stol(inp_sFrom);
}

static inline void to(const string& inp_sFrom, long long& out_data)
{
	out_data = stoll(inp_sFrom);
}

static inline void to(const string& inp_sFrom, short& out_data)
{
	out_data = static_cast<short>(stoi(inp_sFrom));
}

static inline void to(const string& inp_sFrom, unsigned int& out_data)
{
	out_data = static_cast<unsigned int>(stoul(inp_sFrom));
}

static inline void to(const string& inp_sFrom, unsigned long& out_data)
{
	out_data = stoul(inp_sFrom);
}

static inline void to(const string& inp_sFrom, unsigned long long& out_data)
{
	out_data = stoull(inp_sFrom);
}

static inline void to(const string& inp_sFrom, unsigned short& out_data)
{
	out_data = static_cast<short>(stoul(inp_sFrom));
}

static inline void to(const string& inp_sFrom, string& out_data)
{
	out_data = inp_sFrom;
}

template<typename TData> static inline typename ifEnum<TData, void>::type to(string inp_sFrom, TData& out_data)
{
	out_data = static_cast<TData>(stoi(inp_sFrom));
}

template<typename TData> static inline typename ifEnum<TData, string>::type to_string(TData inp_ToConvert)
{
	typedef typename std::underlying_type<TData>::type base_t;

	return to_string(static_cast<base_t>(inp_ToConvert));
}

static inline string to_string(bool inp_bToConvert) { return to_string(static_cast<int>(inp_bToConvert)); }

static inline string to_string(string inp_sToConvert)
{
	return inp_sToConvert;
}
#endif
