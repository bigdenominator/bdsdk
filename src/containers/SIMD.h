#ifndef INCLUDE_H_SIMD
#define INCLUDE_H_SIMD

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <immintrin.h>
#include "utils/TypeLists.h"
#include "containers/SmartDate.h"

struct SIMD
{
	typedef TypeList<double, float, int32_t, int64_t> simd_types;
	typedef TypeList<double, float> float_types;

	template<typename T> struct isSIMD
	{
		enum :int { value = simd_types::HasType<T>::value };
	};
	template<typename T> struct isFloating
	{
		enum :int { value = float_types::HasType<T>::value };
	};

	template<size_t IRegister, typename T> struct reg
	{
		enum :size_t { COUNT = IRegister / (8 * sizeof(T)) };
	};

	// generic
	template<typename T> static inline void add(T *out_ArrayPtr, const T *inp_Array1Ptr, const T *inp_Array2Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) + *(inp_Array2Ptr + iIndx);
		}
	}

	template<typename T> static inline void add(T *out_ArrayPtr, const T *inp_Array1Ptr, const T& inp_dConst, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) + inp_dConst;
		}
	}

	template<typename T> static inline void divide(T *out_ArrayPtr, const T *inp_Array1Ptr, const T *inp_Array2Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) / *(inp_Array2Ptr + iIndx);
		}
	}

	template<typename T> static inline void divide(T *out_ArrayPtr, const T& inp_dConst, const T *inp_Array1Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = inp_dConst / *(inp_Array1Ptr + iIndx);
		}
	}

	template<typename T> static inline void divide(T *out_ArrayPtr, const T *inp_Array1Ptr, const T& inp_dConst, size_t inp_iSize)
	{
		multiply(out_ArrayPtr, inp_Array1Ptr, static_cast<T>(1) / inp_dConst, inp_iSize);
	}

	template<typename T> static inline void join(T* out_ArrayPtr, const T* inp_Array1Ptr, const T* inp_Array2Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = max(*(inp_Array1Ptr + iIndx), *(inp_Array2Ptr + iIndx));
		}
	}

	template<typename T> static inline void load(T *out_ArrayPtr, const T& inp_dConst, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = inp_dConst;
		}
	}

	template<typename T> static inline void load(T *out_ArrayPtr, const T *inp_Array1Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx);
		}
	}

	template<typename T> static inline void meet(T* out_ArrayPtr, const T* inp_Array1Ptr, const T* inp_Array2Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = std::min(*(inp_Array1Ptr + iIndx), *(inp_Array2Ptr + iIndx));
		}
	}

	template<typename T> static inline void multiply(T *out_ArrayPtr, const T *inp_Array1Ptr, const T *inp_Array2Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) * *(inp_Array2Ptr + iIndx);
		}
	}

	template<typename T> static inline void multiply(T *out_ArrayPtr, const T *inp_Array1Ptr, const T& inp_dConst, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) * inp_dConst;
		}
	}

	template<typename T> static inline void subtract(T *out_ArrayPtr, const T *inp_Array1Ptr, const T *inp_Array2Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) - *(inp_Array2Ptr + iIndx);
		}
	}

	template<typename T> static inline void subtract(T *out_ArrayPtr, const T& inp_dConst, const T *inp_Array1Ptr, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = inp_dConst - *(inp_Array1Ptr + iIndx);
		}
	}

	template<typename T> static inline void subtract(T *out_ArrayPtr, const T *inp_Array1Ptr, const T& inp_dConst, size_t inp_iSize)
	{
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx++)
		{
			*(out_ArrayPtr + iIndx) = *(inp_Array1Ptr + iIndx) - inp_dConst;
		}
	}

	// double
	static inline void add(double *out_ArrayPtr, double *inp_Array1Ptr, double *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256d* src1 = (__m256d*)inp_Array1Ptr; __m256d* src2 = (__m256d*)inp_Array2Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_add_pd(*src1++, *src2++);
		}
	}

	static inline void add(double *out_ArrayPtr, const double *inp_Array1Ptr, const double& inp_dConst, size_t inp_iSize)
	{
		const __m256d	ymm1(_mm256_set1_pd(inp_dConst));

		__m256d* src = (__m256d*)inp_Array1Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_add_pd(*src++, ymm1);
		}
	}

	static inline void divide(double *out_ArrayPtr, const double *inp_Array1Ptr, const double *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256d* src1 = (__m256d*)inp_Array1Ptr; __m256d* src2 = (__m256d*)inp_Array2Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_div_pd(*src1++, *src2++);
		}
	}

	static inline void divide(double *out_ArrayPtr, const double& inp_dConst, const double *inp_Array1Ptr, size_t inp_iSize)
	{
		const __m256d	ymm1(_mm256_set1_pd(inp_dConst));

		__m256d* src = (__m256d*)inp_Array1Ptr;__m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_div_pd(ymm1, *src++);
		}
	}

	static inline void divide(double *out_ArrayPtr, const double *inp_Array1Ptr, const double& inp_dConst, size_t inp_iSize)
	{
		multiply(out_ArrayPtr, inp_Array1Ptr, 1.0 / inp_dConst, inp_iSize);
	}

	static inline void join(double* out_ArrayPtr, const double* inp_Array1Ptr, const double* inp_Array2Ptr, size_t inp_iSize)
	{
		__m256d* src1 = (__m256d*)inp_Array1Ptr; __m256d* src2 = (__m256d*)inp_Array2Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_max_pd(*src1++, *src2++);
		}
	}

	static inline void load(double *out_ArrayPtr, const double& inp_dConst, size_t inp_iSize)
	{
		const __m256d	ymm1(_mm256_set1_pd(inp_dConst));

		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			_mm256_store_pd(out_ArrayPtr + iIndx, ymm1);
		}
	}

	static inline void load(double *out_ArrayPtr, const double *inp_Array1Ptr, size_t inp_iSize)
	{
		__m256d* src = (__m256d*)inp_Array1Ptr; 
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			_mm256_store_pd(out_ArrayPtr + iIndx, *src++);
		}
	}

	static inline void meet(double* out_ArrayPtr, const double* inp_Array1Ptr, const double* inp_Array2Ptr, size_t inp_iSize)
	{
		__m256d* src1 = (__m256d*)inp_Array1Ptr; __m256d* src2 = (__m256d*)inp_Array2Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_min_pd(*src1++, *src2++);
		}
	}

	static inline void multiply(double *out_ArrayPtr, const double *inp_Array1Ptr, const double *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256d* src1 = (__m256d*)inp_Array1Ptr; __m256d* src2 = (__m256d*)inp_Array2Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_mul_pd(*src1++, *src2++);
		}
	}

	static inline void multiply(double *out_ArrayPtr, const double *inp_Array1Ptr, const double& inp_dConst, size_t inp_iSize)
	{
		const __m256d	ymm1(_mm256_set1_pd(inp_dConst));

		__m256d* src = (__m256d*)inp_Array1Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_mul_pd(*src++, ymm1);
		}
	}

	static inline void sqrt(double* out_ArrayPtr, const double* inp_Array1Ptr, size_t inp_iSize)
	{
		__m256d* src = (__m256d*)inp_Array1Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_sqrt_pd(*src++);
		}
	}

	static inline void subtract(double *out_ArrayPtr, const double *inp_Array1Ptr, const double *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256d* src1 = (__m256d*)inp_Array1Ptr; __m256d* src2 = (__m256d*)inp_Array2Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_sub_pd(*src1++, *src2++);
		}
	}

	static inline void subtract(double *out_ArrayPtr, const double& inp_dConst, const double *inp_Array1Ptr, size_t inp_iSize)
	{
		const __m256d	ymm1(_mm256_set1_pd(inp_dConst));
		
		__m256d* src = (__m256d*)inp_Array1Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_sub_pd(ymm1, *src++);
		}
	}

	static inline void subtract(double *out_ArrayPtr, const double *inp_Array1Ptr, const double& inp_dConst, size_t inp_iSize)
	{
		const __m256d	ymm1(_mm256_set1_pd(inp_dConst));
		
		__m256d* src = (__m256d*)inp_Array1Ptr; __m256d* out = (__m256d*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, double>::COUNT)
		{
			*out++ = _mm256_sub_pd(*src++, ymm1);
		}
	}

	// float
	static inline void add(float *out_ArrayPtr, const float *inp_Array1Ptr, const float *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256* src1 = (__m256*)inp_Array1Ptr; __m256* src2 = (__m256*)inp_Array2Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_add_ps(*src1++, *src2++);
		}
	}

	static inline void add(float *out_ArrayPtr, const float *inp_Array1Ptr, const float& inp_fConst, size_t inp_iSize)
	{
		const __m256	ymm1(_mm256_set1_ps(inp_fConst));

		__m256* src = (__m256*)inp_Array1Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_add_ps(*src++, ymm1);
		}
	}

	static inline void divide(float *out_ArrayPtr, const float *inp_Array1Ptr, const float *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256* src1 = (__m256*)inp_Array1Ptr; __m256* src2 = (__m256*)inp_Array2Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_div_ps(*src1++, *src2++);
		}
	}

	static inline void divide(float *out_ArrayPtr, const float& inp_fConst, const float *inp_Array1Ptr, size_t inp_iSize)
	{
		const __m256	ymm1(_mm256_set1_ps(inp_fConst));

		__m256* src = (__m256*)inp_Array1Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_div_ps(ymm1, *src++);
		}
	}

	static inline void divide(float *out_ArrayPtr, const float *inp_Array1Ptr, const float& inp_fConst, size_t inp_iSize)
	{
		multiply(out_ArrayPtr, inp_Array1Ptr, 1.f / inp_fConst, inp_iSize);
	}

	static inline void join(float* out_ArrayPtr, const float* inp_Array1Ptr, const float* inp_Array2Ptr, size_t inp_iSize)
	{
		__m256* src1 = (__m256*)inp_Array1Ptr; __m256* src2 = (__m256*)inp_Array2Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_max_ps(*src1++, *src2++);
		}
	}

	static inline void load(float *out_ArrayPtr, const float& inp_fConst, size_t inp_iSize)
	{
		const __m256	ymm1(_mm256_set1_ps(inp_fConst));

		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			_mm256_store_ps(out_ArrayPtr + iIndx, ymm1);
		}
	}

	static inline void load(float *out_ArrayPtr, const float *inp_Array1Ptr, size_t inp_iSize)
	{
		__m256* src = (__m256*)inp_Array1Ptr; 
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			_mm256_store_ps(out_ArrayPtr + iIndx, *src++);
		}
	}

	static inline void meet(float* out_ArrayPtr, const float* inp_Array1Ptr, const float* inp_Array2Ptr, size_t inp_iSize)
	{
		__m256* src1 = (__m256*)inp_Array1Ptr; __m256* src2 = (__m256*)inp_Array2Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_min_ps(*src1++, *src2++);
		}
	}

	static inline void multiply(float *out_ArrayPtr, const float *inp_Array1Ptr, const float *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256* src1 = (__m256*)inp_Array1Ptr; __m256* src2 = (__m256*)inp_Array2Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_mul_ps(*src1++, *src2++);
		}
	}

	static inline void multiply(float *out_ArrayPtr, const float *inp_Array1Ptr, const float& inp_fConst, size_t inp_iSize)
	{
		const __m256	ymm1(_mm256_set1_ps(inp_fConst));

		__m256* src = (__m256*)inp_Array1Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_mul_ps(*src++, ymm1);
		}
	}

	static inline void sqrt(float* out_ArrayPtr, const float* inp_Array1Ptr, size_t inp_iSize)
	{
		__m256* src = (__m256*)inp_Array1Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_sqrt_ps(*src++);
		}
	}

	static inline void subtract(float *out_ArrayPtr, const float *inp_Array1Ptr, const float *inp_Array2Ptr, size_t inp_iSize)
	{
		__m256* src1 = (__m256*)inp_Array1Ptr; __m256* src2 = (__m256*)inp_Array2Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_sub_ps(*src1++, *src2++);
		}
	}

	static inline void subtract(float *out_ArrayPtr, const float& inp_fConst, const float *inp_Array1Ptr, size_t inp_iSize)
	{
		const __m256	ymm1(_mm256_set1_ps(inp_fConst));

		__m256* src = (__m256*)inp_Array1Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_sub_ps(ymm1, *src++);
		}
	}

	static inline void subtract(float *out_ArrayPtr, const float *inp_Array1Ptr, const float& inp_fConst, size_t inp_iSize)
	{
		const __m256	ymm1(_mm256_set1_ps(inp_fConst));

		__m256* src = (__m256*)inp_Array1Ptr; __m256* out = (__m256*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, float>::COUNT)
		{
			*out++ = _mm256_sub_ps(*src++, ymm1);
		}
	}

	// int32_t
	static inline void add(int32_t* out_ArrayPtr, const int32_t* inp_Array1Ptr, const int32_t* inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_add_epi32(*src1++, *src2++);
		}
	}

	static inline void add(int32_t *out_ArrayPtr, const int32_t *inp_Array1Ptr, const int32_t& inp_iConst, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi32(inp_iConst));

		__m128i* src = (__m128i*)inp_Array1Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_add_epi32(*src++, xmm);
		}
	}

	static inline void join(int32_t *out_ArrayPtr, const int32_t *inp_Array1Ptr, const int32_t *inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_max_epi32(*src1++, *src2++);
		}
	}

	static inline void load(int32_t *out_ArrayPtr, int32_t inp_iConst, size_t inp_iSize)
	{
		const __m256i xmm(_mm256_set1_epi32(inp_iConst));

		__m256i* out = (__m256i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, int32_t>::COUNT)
		{
			_mm256_store_si256(out++, xmm);
		}
	}

	static inline void load(int32_t *out_ArrayPtr, const int32_t *inp_ArrayPtr, size_t inp_iSize)
	{
		__m256i*	src((__m256i*)inp_ArrayPtr); __m256i* out((__m256i*)out_ArrayPtr);
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, int32_t>::COUNT)
		{
			_mm256_store_si256(out++, *src++);
		}
	}

	static inline void meet(int32_t *out_ArrayPtr, const int32_t *inp_Array1Ptr, const int32_t *inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_min_epi32(*src1++, *src2++);
		}
	}

	static inline void multiply(int32_t *out_ArrayPtr, const int32_t *inp_Array1Ptr, const int32_t *inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_mullo_epi32(*src1++, *src2++);
		}
	}

	static inline void multiply(int32_t *out_ArrayPtr, const int32_t *inp_Array1Ptr, const int32_t& inp_iConst, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi32(inp_iConst));
		
		__m128i* src = (__m128i*)inp_Array1Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_mullo_epi32(*src++, xmm);
		}
	}

	static inline void subtract(int32_t* out_ArrayPtr, const int32_t* inp_Array1Ptr, const int32_t* inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_sub_epi32(*src1++, *src2++);
		}
	}

	static inline void subtract(int32_t* out_ArrayPtr, const int32_t* inp_Array1Ptr, const int32_t& inp_iConst, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi32(inp_iConst));
		
		__m128i* src((__m128i*)inp_Array1Ptr); __m128i* out((__m128i*)out_ArrayPtr);
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_sub_epi32(*src++, xmm);
		}
	}

	static inline void subtract(int32_t* out_ArrayPtr, const int32_t& inp_iConst, const int32_t* inp_Array1Ptr, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi32(inp_iConst));

		__m128i* src((__m128i*)inp_Array1Ptr); __m128i* out((__m128i*)out_ArrayPtr);
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int32_t>::COUNT)
		{
			*out++ = _mm_sub_epi32(xmm, *src++);
		}
	}

	// int64_t
	static inline void add(int64_t* out_ArrayPtr, const int64_t* inp_Array1Ptr, const int64_t* inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int64_t>::COUNT)
		{
			*out++ = _mm_add_epi64(*src1++, *src2++);
		}
	}

	static inline void add(int64_t *out_ArrayPtr, const int64_t *inp_Array1Ptr, const int64_t& inp_lConst, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi64x(inp_lConst));

		__m128i* src = (__m128i*)inp_Array1Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int64_t>::COUNT)
		{
			*out++ = _mm_add_epi64(*src++, xmm);
		}
	}

	static inline void load(int64_t* out_ArrayPtr, const int64_t& inp_lConst, size_t inp_iSize)
	{
		const __m256i xmm(_mm256_set1_epi64x(inp_lConst));

		__m256i* out = (__m256i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, int64_t>::COUNT)
		{
			_mm256_store_si256(out++, xmm);
		}
	}

	static inline void load(int64_t *out_ArrayPtr, const int64_t *inp_Array1Ptr, size_t inp_iSize)
	{
		__m256i*	src((__m256i*)inp_Array1Ptr); __m256i* out((__m256i*)out_ArrayPtr);
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<256, int64_t>::COUNT)
		{
			_mm256_store_si256(out++, *src++);
		}
	}

	static inline void subtract(int64_t* out_ArrayPtr, const int64_t* inp_Array1Ptr, const int64_t* inp_Array2Ptr, size_t inp_iSize)
	{
		__m128i* src1 = (__m128i*)inp_Array1Ptr; __m128i* src2 = (__m128i*)inp_Array2Ptr; __m128i* out = (__m128i*)out_ArrayPtr;
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int64_t>::COUNT)
		{
			*out++ = _mm_sub_epi64(*src1++, *src2++);
		}
	}

	static inline void subtract(int64_t* out_ArrayPtr, const int64_t* inp_Array1Ptr, const int64_t& inp_lConst, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi64x(inp_lConst));

		__m128i* src((__m128i*)inp_Array1Ptr); __m128i* out((__m128i*)out_ArrayPtr);
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int64_t>::COUNT)
		{
			*out++ = _mm_sub_epi64(*src++, xmm);
		}
	}

	static inline void subtract(int64_t* out_ArrayPtr, const int64_t& inp_lConst, const int64_t* inp_Array1Ptr, size_t inp_iSize)
	{
		const __m128i xmm(_mm_set1_epi64x(inp_lConst));

		__m128i* src((__m128i*)inp_Array1Ptr); __m128i* out((__m128i*)out_ArrayPtr);
		for (size_t iIndx(0); iIndx < inp_iSize; iIndx += reg<128, int64_t>::COUNT)
		{
			*out++ = _mm_sub_epi64(xmm, *src++);
		}
	}
};

template<typename T> struct SimdHlpr
{
	typedef SimdHlpr<T> _Myt;

	typedef T	simd_type;
	typedef T	primitive_type;

	enum :size_t { BYTES = sizeof(simd_type), STEP = BYTES / sizeof(primitive_type) };

	static inline simd_type load(void) { return simd_type(); }

	static inline simd_type load(primitive_type inp_dValue) { return inp_dValue; }

	static inline simd_type load(const primitive_type* inp_dValuePtr) { return *inp_dValuePtr; }

	static inline void store(primitive_type* out_dest, simd_type inp_cValue) { *out_dest = inp_cValue; }
};

template<> struct SimdHlpr<double>
{
	typedef SimdHlpr<double> _Myt;

	typedef __m256d	simd_type;
	typedef double	primitive_type;

	enum :size_t { BYTES = sizeof(simd_type), STEP = BYTES / sizeof(primitive_type) };

	static inline simd_type load(void) { return _mm256_setzero_pd(); }

	static inline simd_type load(primitive_type inp_dValue) { return _mm256_set1_pd(inp_dValue); }

	static inline simd_type load(const primitive_type* inp_dValuePtr) { return _mm256_load_pd(inp_dValuePtr); }

	static inline void store(primitive_type* out_dest, simd_type inp_cValue) { _mm256_store_pd(out_dest, inp_cValue); }
};

template<> struct SimdHlpr<float>
{
	typedef SimdHlpr<float> _Myt;

	typedef __m256	simd_type;
	typedef float	primitive_type;

	enum :size_t { BYTES = sizeof(simd_type), STEP = BYTES / sizeof(primitive_type) };

	static inline simd_type load(void) { return _mm256_setzero_ps(); }

	static inline simd_type load(primitive_type inp_dValue) { return _mm256_set1_ps(inp_dValue); }

	static inline simd_type load(const primitive_type* inp_dValuePtr) { return _mm256_load_ps(inp_dValuePtr); }

	static inline void store(primitive_type* out_dest, simd_type inp_cValue) { _mm256_store_ps(out_dest, inp_cValue); }
};

template<> struct SimdHlpr<int32_t>
{
	typedef SimdHlpr<int32_t> _Myt;

	typedef __m128i	simd_type;
	typedef int32_t	primitive_type;

	enum :size_t { BYTES = sizeof(simd_type), STEP = BYTES / sizeof(primitive_type) };

	static inline simd_type load(void) { return _mm_setzero_si128(); }

	static inline simd_type load(primitive_type inp_dValue) { return _mm_set1_epi32(inp_dValue); }

	static inline simd_type load(const primitive_type* inp_dValuePtr) { return _mm_load_si128(reinterpret_cast<const simd_type*>(inp_dValuePtr)); }

	static inline void store(primitive_type* out_dest, simd_type inp_cValue) { _mm_store_si128(reinterpret_cast<simd_type*>(out_dest), inp_cValue); }
};

template<> struct SimdHlpr<int64_t>
{
	typedef SimdHlpr<int64_t> _Myt;

	typedef __m128i	simd_type;
	typedef int64_t	primitive_type;

	enum :size_t { BYTES = sizeof(simd_type), STEP = BYTES / sizeof(primitive_type) };

	static inline simd_type load(void) { return _mm_setzero_si128(); }

	static inline simd_type load(primitive_type inp_dValue) { return _mm_set1_epi64x(inp_dValue); }

	static inline simd_type load(const primitive_type* inp_dValuePtr) { return _mm_load_si128(reinterpret_cast<const simd_type*>(inp_dValuePtr)); }

	static inline void store(primitive_type* out_dest, simd_type inp_cValue) { _mm_store_si128(reinterpret_cast<simd_type*>(out_dest), inp_cValue); }
};

template<> struct SimdHlpr<CDate>
{
	typedef SimdHlpr<CDate> _Myt;

	typedef __m128i	simd_type;
	typedef CDate	primitive_type;

	enum :size_t { BYTES = sizeof(simd_type), STEP = BYTES / sizeof(primitive_type) };

	static inline simd_type load(void) { return _mm_set1_epi32(CDate::INITIALIZE); }

	static inline simd_type load(primitive_type inp_dValue) { return _mm_set1_epi32(inp_dValue); }

	static inline simd_type load(const primitive_type* inp_dValuePtr) { return _mm_load_si128(reinterpret_cast<const simd_type*>(inp_dValuePtr)); }

	static inline void store(primitive_type* out_dest, simd_type inp_cValue) { _mm_store_si128(reinterpret_cast<simd_type*>(out_dest), inp_cValue); }
};

template<typename T> struct IsSimdHlprImpl
{
	enum :int { value = 0 };
};
template<typename T> struct IsSimdHlprImpl<SimdHlpr<T>>
{
	enum :int { value = 1 };
};
template<typename T> using IsSimdHlpr = IsSimdHlprImpl<T>;

template<typename T, typename U> using ifSimdHlpr = std::enable_if<IsSimdHlpr<T>::value, U>;

namespace simd
{
	namespace unary
	{
		template<typename T> using TransformFn = T(*)(T);

		template <typename T, typename TData> class HasOperate
		{
			typedef SimdHlpr<TData> simd_hlpr;

			template<typename U> static uint32_t test_sig(TransformFn<U>);
			template<typename U> static uint64_t test_sig(...);

			template<typename U> static decltype(test_sig(&U::operate)) test(decltype(U::operate(simd_hlpr::load()))*);
			template<typename U> static uint64_t test(...);

		public:
			enum :int { value = (sizeof(test<T>(nullptr)) == sizeof(uint32_t)) };
		};

		template<typename T, TransformFn<T> Func, size_t ISize = SimdHlpr<T>::STEP> struct TransformHlpr
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type inp_Value)
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pTmp(reinterpret_cast<T*>(&inp_Value));

				for (size_t i(0); i < ISize; i++)
				{
					pOut[i] = Func(pTmp[i]);
				}
				return dOut;
			}
		};
		template<typename T, TransformFn<T> Func> struct TransformHlpr<T, Func, 1>
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type inp_Value)
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pTmp(reinterpret_cast<T*>(&inp_Value));
				pOut[0] = Func(pTmp[0]);

				return dOut;
			}
		};

#define UNARY_HLPR(structName, transformName) \
		template<typename T> struct structName \
		{ \
			typedef typename SimdHlpr<T>::simd_type simd_type; \
			static inline simd_type operate(simd_type inp_Value) \
			{ \
				return simd::unary::TransformHlpr<T, transformName>::operate(inp_Value); \
			} \
		};

		template<typename T, typename Func, size_t ISize = SimdHlpr<T>::STEP> struct FunctorHlpr
		{
			typedef FunctorHlpr<T, Func, ISize>	_Myt;
			typedef typename SimdHlpr<T>::simd_type simd_type;

			Func	mFn;

			FunctorHlpr(Func inp_Fn) :mFn(inp_Fn) {}

			inline simd_type operator()(simd_type inp_Value) const
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pTmp(reinterpret_cast<T*>(&inp_Value));

				for (size_t i(0); i < ISize; i++)
				{
					pOut[i] = mFn(pTmp[i]);
				}
				return dOut;
			}
		};
		template<typename T, typename Func> struct FunctorHlpr<T, Func, 1>
		{
			typedef FunctorHlpr<T, Func, 1>	_Myt;
			typedef typename SimdHlpr<T>::simd_type simd_type;

			Func	mFn;

			FunctorHlpr(Func inp_Fn) :mFn(inp_Fn) {}

			inline simd_type operator()(simd_type inp_Value) const
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pTmp(reinterpret_cast<T*>(&inp_Value));
				pOut[0] = mFn(pTmp[0]);

				return dOut;
			}
		};
		
		// abs
		UNARY_HLPR(abs, std::abs)
		template<> struct abs<int32_t>
		{
			static inline __m128i operate(__m128i inp_Value)
			{
				return _mm_abs_epi32(inp_Value);
			}
		};

		// sqrt
		UNARY_HLPR(sqrt, std::sqrt)
		template<> struct sqrt<double>
		{
			static inline __m256d operate(__m256d inp_Value)
			{
				return _mm256_sqrt_pd(inp_Value);
			}
		};
		template<> struct sqrt<float>
		{
			static inline __m256 operate(__m256 inp_Value)
			{
				return _mm256_sqrt_ps(inp_Value);
			}
		};

		// non-SIMD helpers
		UNARY_HLPR(cos, std::cos)
		UNARY_HLPR(exp, std::exp)
		UNARY_HLPR(ln, std::log)
		UNARY_HLPR(sin, std::sin)
		UNARY_HLPR(tan, std::tan)
	}

	namespace binary
	{
		template<typename T> using TransformFn = T(*)(T, T);

		template <typename T, typename TData> class HasOperate
		{
			typedef SimdHlpr<TData> simd_hlpr;

			template<typename U> static uint32_t test_sig(TransformFn<U>);
			template<typename U> static uint64_t test_sig(...);

			template<typename U> static decltype(test_sig(&U::operate)) test(decltype(U::operate(simd_hlpr::load(), simd_hlpr::load()))*);
			template<typename U> static uint64_t test(...);

		public:
			enum :int { value = (sizeof(test<T>(nullptr)) == sizeof(uint32_t)) };
		};

		template<typename T, TransformFn<T> Func, size_t ISize = SimdHlpr<T>::STEP> struct TransformHlpr
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pLhs(reinterpret_cast<T*>(&lhs));
				T* pRhs(reinterpret_cast<T*>(&rhs));

				for (size_t i(0); i < ISize; i++)
				{
					pOut[i] = Func(pLhs[i], pRhs[i]);
				}
				return dOut;
			}
		};
		template<typename T, TransformFn<T> Func> struct TransformHlpr<T, Func, 1>
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pLhs(reinterpret_cast<T*>(&lhs));
				T* pRhs(reinterpret_cast<T*>(&rhs));

				pOut[0] = Func(pLhs[0], pRhs[0]);

				return dOut;
			}
		};

		template<typename T, typename Func, size_t ISize = SimdHlpr<T>::STEP> struct FunctorHlpr
		{
			typedef FunctorHlpr<T, Func, ISize>	_Myt;
			typedef typename SimdHlpr<T>::simd_type simd_type;

			Func	mFn;

			FunctorHlpr(Func inp_Fn) :mFn(inp_Fn) {}

			inline simd_type operator()(simd_type lhs, simd_type rhs) const
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pLhs(reinterpret_cast<T*>(&lhs));
				T* pRhs(reinterpret_cast<T*>(&rhs));

				for (size_t i(0); i < ISize; i++)
				{
					pOut[i] = mFn(pLhs[i], pRhs[i]);
				}
				return dOut;
			}
		};
		template<typename T, typename Func> struct FunctorHlpr<T, Func, 1>
		{
			typedef FunctorHlpr<T, Func, 1>	_Myt;
			typedef typename SimdHlpr<T>::simd_type simd_type;

			Func	mFn;

			FunctorHlpr(Func inp_Fn) :mFn(inp_Fn) {}

			inline simd_type operator()(simd_type lhs, simd_type rhs) const
			{
				simd_type dOut;
				T* pOut(reinterpret_cast<T*>(&dOut));
				T* pLhs(reinterpret_cast<T*>(&lhs));
				T* pRhs(reinterpret_cast<T*>(&rhs));

				pOut[0] = mFn(pLhs[0], pRhs[0]);

				return dOut;
			}
		};
		
		// add
		template<typename T> struct add
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				return TransformHlpr<T, hlpr>::operate(lhs,  rhs);
			}
			static inline T hlpr(T lhs, T rhs) { return lhs + rhs; }
		};
		template<> struct add<double>
		{
			static inline __m256d operate(__m256d lhs, __m256d rhs)
			{
				return _mm256_add_pd(lhs, rhs);
			}
		};
		template<> struct add<float>
		{
			static inline __m256 operate(__m256 lhs, __m256 rhs)
			{
				return _mm256_add_ps(lhs, rhs);
			}
		};
		template<> struct add<int32_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_add_epi32(lhs, rhs);
			}
		};
		template<> struct add<int64_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_add_epi64(lhs, rhs);
			}
		};

		// div
		template<typename T> struct div
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				return TransformHlpr<T, hlpr>::operate(lhs, rhs);
			}
			static inline T hlpr(T lhs, T rhs) { return lhs / rhs; }
		};
		template<> struct div<double>
		{
			static inline __m256d operate(__m256d lhs, __m256d rhs)
			{
				return _mm256_div_pd(lhs, rhs);
			}
		};
		template<> struct div<float>
		{
			static inline __m256 operate(__m256 lhs, __m256 rhs)
			{
				return _mm256_div_ps(lhs, rhs);
			}
		};

		// max
		template<typename T> struct max
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				return TransformHlpr<T, hlpr>::operate(lhs, rhs);
			}
			static inline T hlpr(T lhs, T rhs) { return std::max(lhs, rhs); }
		};
		template<> struct max<double>
		{
			static inline __m256d operate(__m256d lhs, __m256d rhs)
			{
				return _mm256_max_pd(lhs, rhs);
			}
		};
		template<> struct max<float>
		{
			static inline __m256 operate(__m256 lhs, __m256 rhs)
			{
				return _mm256_max_ps(lhs, rhs);
			}
		};
		template<> struct max<int32_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_max_epi32(lhs, rhs);
			}
		};

		// min
		template<typename T> struct min
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				return TransformHlpr<T, hlpr>::operate(lhs, rhs);
			}
			static inline T hlpr(T lhs, T rhs) { return std::min(lhs, rhs); }
		};
		template<> struct min<double>
		{
			static inline __m256d operate(__m256d lhs, __m256d rhs)
			{
				return _mm256_min_pd(lhs, rhs);
			}
		};
		template<> struct min<float>
		{
			static inline __m256 operate(__m256 lhs, __m256 rhs)
			{
				return _mm256_min_ps(lhs, rhs);
			}
		};
		template<> struct min<int32_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_min_epi32(lhs, rhs);
			}
		};

		// mul
		template<typename T> struct mul
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				return TransformHlpr<T, hlpr>::operate(lhs, rhs);
			}
			static inline T hlpr(T lhs, T rhs) { return lhs * rhs; }
		};
		template<> struct mul<double>
		{
			static inline __m256d operate(__m256d lhs, __m256d rhs)
			{
				return _mm256_mul_pd(lhs, rhs);
			}
		};
		template<> struct mul<float>
		{
			static inline __m256 operate(__m256 lhs, __m256 rhs)
			{
				return _mm256_mul_ps(lhs, rhs);
			}
		};
		template<> struct mul<int32_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_mullo_epi32(lhs, rhs);
			}
		};

		// sub
		template<typename T> struct sub
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;
			static inline simd_type operate(simd_type lhs, simd_type rhs)
			{
				return TransformHlpr<T, hlpr>::operate(lhs, rhs);
			}
			static inline T hlpr(T lhs, T rhs) { return lhs - rhs; }
		};
		template<> struct sub<double>
		{
			static inline __m256d operate(__m256d lhs, __m256d rhs)
			{
				return _mm256_sub_pd(lhs, rhs);
			}
		};
		template<> struct sub<float>
		{
			static inline __m256 operate(__m256 lhs, __m256 rhs)
			{
				return _mm256_sub_ps(lhs, rhs);
			}
		};
		template<> struct sub<int32_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_sub_epi32(lhs, rhs);
			}
		};
		template<> struct sub<int64_t>
		{
			static inline __m128i operate(__m128i lhs, __m128i rhs)
			{
				return _mm_sub_epi64(lhs, rhs);
			}
		};
	}

	namespace unary
	{
		template<typename T, typename TCoef> struct polynomial
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;

			const TCoef*	mdCoefPtr;
			size_t		miSize;

			polynomial(const TCoef* inp_dCoefPtr, size_t inp_iSize) :mdCoefPtr(inp_dCoefPtr), miSize(inp_iSize) {}

			inline simd_type operator()(simd_type inp_Data) const
			{
				simd_type dResult(SimdHlpr<T>::load(mdCoefPtr[0]));
				for (size_t iIndx(1); iIndx < miSize; iIndx++)
				{
					dResult = simd::binary::add<T>::operate(simd::binary::mul<T>::operate(dResult, inp_Data), SimdHlpr<T>::load(mdCoefPtr[iIndx]));
				}

				return dResult;
			}
		};

		template<typename T> struct pow
		{
			typedef typename SimdHlpr<T>::simd_type simd_type;

			int miPower;

			pow(int inp_iPower) :miPower(inp_iPower) {}

			inline simd_type operator()(simd_type inp_Data) const
			{
				if (miPower < 0)
				{
					return simd::binary::div<T>::operate(SimdHlpr<T>::load(1), eval(inp_Data, -miPower));
				}
				else
				{
					return eval(inp_Data, miPower);
				}
			}

			static simd_type eval(simd_type inp_Data, size_t inp_iPower)
			{
				switch (inp_iPower)
				{
				case 0:
					return SimdHlpr<T>::load(1);
				case 1:
					return inp_Data;
				case 2:
					return simd::binary::mul<T>::operate(inp_Data, inp_Data);
				default:
					if (inp_iPower % 2) // odd
					{
						return simd::binary::mul<T>::operate(inp_Data, eval(inp_Data, --inp_iPower));
					}
					else // even
					{
						return eval(simd::binary::mul<T>::operate(inp_Data, inp_Data), inp_iPower / 2);
					}
				}
			}
		};
	}
}
#endif
