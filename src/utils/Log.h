#ifndef INCLUDE_H_LOGFILE
#define INCLUDE_H_LOGFILE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <fstream>
#include "utils/Strings.h"

namespace FlatFile
{
	typedef fstream				CHandle;

	typedef std::streamsize		size_type;
	typedef fstream::pos_type	pos_type;
}

class CLogFile 
{
public:
	enum :size_t { DEFAULT_BUFFER_SIZE = 32768 };

	CLogFile(const string& inp_sFilename) :msFilename(inp_sFilename) {}

	void close(void)
	{
		if (mcHandle.is_open()) mcHandle.close();
	}

	bool open(void)
	{
		if (!mcHandle.is_open())
		{
			mcHandle.open(msFilename, getOpenMode());
			if (!(mcHandle.is_open())) { return false; }
		}
		return true;
	}

	bool put(const string& inp_sMsg)
	{
		mcHandle.write(inp_sMsg.c_str(), inp_sMsg.size());
		mcHandle.write(&cEOL, 1);
		mcHandle.flush();
		mcHandle.flush();

		if (!isGood()) { return false; }

		return true;
	}

private:
	FlatFile::CHandle	mcHandle;
	string				msFilename;

	static ios::openmode getOpenMode(void)
	{
		return (ios::binary | ios::out);
	}

	bool isEnd(void) const { return mcHandle.eof(); }
	bool isGood(void) const { return mcHandle.good(); }
};
#endif