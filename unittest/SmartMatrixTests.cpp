#ifndef INCLUDE_H_SMARTMATRIXTESTS
#define INCLUDE_H_SMARTMATRIXTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/SmartMatrix.h"

TEST(MatrixTests, Matrix)
{
	CSmartMatrix<int, 5, 5> matrix;
	CSmartArray<int, 5> before;

	for (int iIndx(0); iIndx < 5; iIndx++)
	{
		before[iIndx] = iIndx;
		matrix[iIndx] = 0;
	}
	matrix[1][0] = 1;
	matrix[2][1] = 1;
	matrix[3][2] = 1;
	matrix[4][3] = 1;
	matrix[0][4] = 1;

	auto after(matrix * before);
	EXPECT_EQ(4, after[0]);
	EXPECT_EQ(0, after[1]);
	EXPECT_EQ(1, after[2]);
	EXPECT_EQ(2, after[3]);
	EXPECT_EQ(3, after[4]);
}

TEST(MatrixTests, RandomMatrix)
{
	CRandomMatrix<int, 5, 5> matrix;
	CSmartArray<CVectorInt, 5> before;

	for (int iIndx(0); iIndx < 5; iIndx++)
	{
		before[iIndx] = CVectorInt(10, iIndx);
		for (int jIndx(0); jIndx < 5; jIndx++)
		{
			matrix[iIndx][jIndx] = CVectorInt(10, 0);
		}
	}
	matrix[1][0] = 1;
	matrix[2][1] = 1;
	matrix[3][2] = 1;
	matrix[4][3] = 1;
	matrix[0][4] = 1;

	auto after(matrix * before);
	EXPECT_EQ(10, after[0].size());	EXPECT_EQ(4, after[0][0]);
	EXPECT_EQ(10, after[1].size());	EXPECT_EQ(0, after[1][0]);
	EXPECT_EQ(10, after[2].size());	EXPECT_EQ(1, after[2][0]);
	EXPECT_EQ(10, after[3].size());	EXPECT_EQ(2, after[3][0]);
	EXPECT_EQ(10, after[4].size());	EXPECT_EQ(3, after[4][0]);
}
#endif