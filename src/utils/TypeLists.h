#ifndef INCLUDE_H_TYPELIST
#define INCLUDE_H_TYPELIST

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

// TypeList and associated Utilities
// TypeLists are created by Meta Programming, and are what they say, pre-compiler 
// lists of types that can used in many ways.  Basically all of the uses of 
// TypeLists depend on recursion used by the pre-scan of the compiler.  

// Check to see if two types are equal

// If the two types are different, return 0
template<typename T, typename U> struct TypeCheck
{
	enum { agree = 0 };
};

// If the two types are the same (T), return 1
template<typename T> struct TypeCheck < T, T >
{
	enum { agree = 1 };
};


// ListMeta is a helper structure for finding the type at a given offset
// and the sub-list beginning at a given offset
template<typename TList, uint32_t TIndx> struct ListMeta
{
	typedef typename ListMeta<typename TList::Tail, TIndx - 1>::SubList SubList;
	typedef typename ListMeta<typename TList::Tail, TIndx - 1>::TypeAt TypeAt;
};
template<typename TList> struct ListMeta<TList, 0>
{
	typedef typename TList::Head	TypeAt;
	typedef TList					SubList;
};


// TypeList
template<typename ...T> struct TypeList;

// This is a generic definition of a list of types. The T is the "current" type,
// and the variadic U is "the rest of the list"
template<typename T, typename ...U> struct TypeList<T, U...>
{
	typedef TypeList<T, U...>	_Myt;

	typedef T					Head;
	typedef TypeList<U...>	Tail;

	enum :int { length = 1 + sizeof...(U) };

	template<typename TCheck> struct HasType
	{
		enum :int { value = (TypeCheck<T, TCheck>::agree == 1 || Tail::template HasType<TCheck>::value) };
	};

	template<uint32_t TIndx> struct Types
	{
		typedef typename ListMeta<_Myt, TIndx>::SubList	SubList;
		typedef typename ListMeta<_Myt, TIndx>::TypeAt	At;
	};
};

// TypeList termination
template<> struct TypeList<>
{
	typedef TypeList<>	_Myt;

	enum :int { length = 0 };

	template<typename TCheck> struct HasType
	{
		enum :int { value = 0 };
	};

	template<uint32_t TIndx> struct Types
	{
		typedef typename ListMeta<_Myt, TIndx>::SubList	SubList;
		typedef typename ListMeta<_Myt, TIndx>::TypeAt	At;
	};
};


// concatenate TypeLists
template<class... L> struct TypeListConcatImpl;
template<class... L> using TypeListConcat = typename TypeListConcatImpl<L...>::type;

template<> struct TypeListConcatImpl<>
{
	using type = TypeList<>;
};
template<template<class...> class L, class... T> struct TypeListConcatImpl<L<T...>>
{
	using type = L<T...>;
};
template<template<class...> class L1, class... T1, template<class...> class L2, class... T2, class... Lr>
struct TypeListConcatImpl<L1<T1...>, L2<T2...>, Lr...>
{
	using type = TypeListConcat<L1<T1..., T2...>, Lr...>;
};
#endif