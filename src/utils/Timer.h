#ifndef INCLUDE_H_TIMER
#define INCLUDE_H_TIMER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/PerfTools.h"
#include "utils/Strings.h"
class Timer
{
public:
	Timer(void)
	{
		miStartCycleCnt = 0;
		miEndingCycleCnt = 0;
		miCycleCnt = 0;
	}

	void begin(void)
	{
		miStartCycleCnt = ClockCycleCnt();
	}

	uint64_t cycles(void) const { return miCycleCnt; }

	void end(void)
	{
		miEndingCycleCnt = ClockCycleCnt();
		miCycleCnt = miEndingCycleCnt - miStartCycleCnt;
	}

	string log(string inp_sHeader) const
	{
		return inp_sHeader + sColon + sSpace + to_string(miCycleCnt);
	}

private:
	uint64_t	miStartCycleCnt;
	uint64_t    miEndingCycleCnt;
	uint64_t	miCycleCnt;
};
#endif
