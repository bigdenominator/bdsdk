#ifndef INCLUDE_H_ARRAYBASED
#define INCLUDE_H_ARRAYBASED

/* (C) Copyright Big Denominator, LLC 2016.
 *  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
 *  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
 */

#include "CommonDefs.h"

#include "utils/AlignedAllocator.h"
#include "containers/Expressions.h"
#include "containers/SmartContainers.h"
#include "containers/SimpleMath.h"

template<typename TData> struct ArrayExpression
{
public:
	typedef ArrayExpression<TData>	_Myt;

	typedef SimdHlpr<TData>						simd_hlpr;
	typedef typename simd_hlpr::simd_type		simd_type;
	typedef typename simd_hlpr::primitive_type	primitive_type;

	ArrayExpression(const primitive_type* inp_RawPtr, size_t inp_iSize) :mcRawPtr(inp_RawPtr), miSize(inp_iSize) {}

	ArrayExpression(const _Myt& inp_cBase) :mcRawPtr(inp_cBase.mcRawPtr), miSize(inp_cBase.miSize) {}

	ArrayExpression(_Myt&& inp_cBase) :mcRawPtr(std::move(inp_cBase.mcRawPtr)), miSize(inp_cBase.miSize)
	{
		inp_cBase.mcRawPtr = nullptr;
	}

	inline simd_type operator[](size_t inp_iIndx) const { return simd_hlpr::load(mcRawPtr + inp_iIndx); }

	inline size_t size(void) const { return miSize; }

protected:
	const primitive_type*	mcRawPtr;
	size_t					miSize;
};

template<typename TArray> class _array_const_iterator
{
public:
	typedef TArray						_Myarray;
	typedef _array_const_iterator<_Myarray>	_Myt;

	typedef typename _Myarray::size_type		size_type;
	typedef typename _Myarray::value_type		value_type;
	typedef typename _Myarray::difference_type	difference_type;
	typedef typename _Myarray::const_pointer	pointer;
	typedef typename _Myarray::const_reference	reference;

	typedef typename _Myarray::pointer _Tptr;

	_array_const_iterator(void) {}

	_array_const_iterator(_Tptr inp_cRawPtr) :mcRawPtr(inp_cRawPtr) {}

	inline reference operator*(void) const { return *mcRawPtr; }

	inline pointer operator->(void) const
	{
		return (std::pointer_traits<pointer>::pointer_to(**this));
	}

	inline _Myt& operator++(void)
	{
		++mcRawPtr;
		return (*this);
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return (temp);
	}

	inline _Myt& operator--(void)
	{
		--mcRawPtr;
		return (*this);
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--*this;
		return (temp);
	}

	inline _Myt& operator+=(difference_type inp_Shift)
	{
		mcRawPtr += inp_Shift;
		return (*this);
	}

	inline _Myt operator+(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp += inp_Shift);
	}

	inline _Myt& operator-=(difference_type inp_Shift)
	{
		mcRawPtr -= inp_Shift;
		return (*this);
	}

	inline _Myt operator-(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp -= inp_Shift);
	}

	inline difference_type operator-(const _Myt& rhs) const
	{
		return (mcRawPtr - rhs.mcRawPtr);
	}

	inline reference operator[](difference_type inp_Shift) const
	{
		return (*(*this + inp_Shift));
	}

	inline bool operator==(const _Myt& rhs) const
	{
		return (mcRawPtr == rhs.mcRawPtr);
	}

	inline bool operator!=(const _Myt& rhs) const
	{
		return (!(*this == rhs));
	}

	inline bool operator<(const _Myt& rhs) const
	{
		return (mcRawPtr < rhs.mcRawPtr);
	}

	inline bool operator>(const _Myt& rhs) const
	{
		return (rhs < *this);
	}

	inline bool operator<=(const _Myt& rhs) const
	{
		return (!(rhs < *this));
	}

	inline bool operator>=(const _Myt& rhs) const
	{
		return (!(*this < rhs));
	}

protected:
	pointer		mcRawPtr;
};

template<typename TArray> class _array_iterator : public _array_const_iterator<TArray>
{
public:
	typedef TArray				_Myarray;

	typedef _array_const_iterator<_Myarray>	_Mybase;
	typedef _array_iterator<_Myarray>		_Myt;

	typedef typename _Myarray::size_type		size_type;
	typedef typename _Myarray::value_type		value_type;
	typedef typename _Myarray::difference_type	difference_type;
	typedef typename _Myarray::pointer			pointer;
	typedef typename _Myarray::reference		reference;

	_array_iterator(void) :_Mybase() {}

	_array_iterator(pointer inp_cRawPtr) :_Mybase(inp_cRawPtr) {}

	inline reference operator*(void) const { return ((reference)**(_Mybase *)this); }

	inline pointer operator->(void) const
	{
		return (std::pointer_traits<pointer>::pointer_to(**this));
	}

	inline _Myt& operator++(void)
	{
		++_Mybase::mcRawPtr;
		return (*this);
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return (temp);
	}

	inline _Myt& operator--(void)
	{
		--_Mybase::mcRawPtr;
		return (*this);
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--*this;
		return (temp);
	}

	inline _Myt& operator+=(difference_type inp_Shift)
	{
		_Mybase::mcRawPtr += inp_Shift;
		return (*this);
	}

	inline _Myt operator+(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp += inp_Shift);
	}

	inline _Myt& operator-=(difference_type inp_Shift)
	{
		_Mybase::mcRawPtr -= inp_Shift;
		return (*this);
	}

	inline _Myt operator-(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp -= inp_Shift);
	}

	inline difference_type operator-(const _Myt& rhs) const
	{
		return (_Mybase::mcRawPtr - rhs.mcRawPtr);
	}

	inline reference operator[](difference_type inp_Shift) const
	{
		return (*(*this + inp_Shift));
	}
};

template<typename TData> class CArrayAccess : public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
public:
	typedef CArrayAccess<TData>	_Myt;

	typedef size_t			size_type;
	typedef TData			value_type;
	typedef ptrdiff_t		difference_type;
	typedef TData&			reference;
	typedef const TData&	const_reference;
	typedef TData*			pointer;
	typedef const TData*	const_pointer;

	typedef _array_iterator<_Myt>		iterator;
	typedef _array_const_iterator<_Myt>	const_iterator;

	typedef AlignedAllocator<value_type, 32>	allocator_type;

	typedef ArrayExpression<value_type>			expression_type;

	typedef std::initializer_list<value_type>	init_list;

	template<typename T> using ifAssignable = std::enable_if<IsExpr<T>::value, _Myt>;

	template<typename T, typename TResult> using ifRhsConst = std::enable_if<IsExprCompatibleWithConst<expression_type, T>::value, TResult>;
	template<typename T, typename TResult> using ifRhsExpr = std::enable_if<AreExprsCompatible<expression_type, T>::value, TResult>;

	virtual ~CArrayAccess(void) { deallocate(miAllocatedSize, mcRawPtr); }

	operator expression_type(void) const { return expression_type(mcRawPtr, size()); }

	template<typename T> inline auto operator +=(const T& rhs) -> typename ifAssignable<decltype(*this + rhs)>::type&
	{
		load(*this + rhs);

		return *this;
	}

	template<typename T> inline auto operator /=(const T& rhs) -> typename ifAssignable<decltype(*this / rhs)>::type&
	{
		load(*this / rhs);

		return *this;
	}

	template<typename T> inline auto operator |=(const T& rhs) -> typename ifAssignable<decltype(*this | rhs)>::type&
	{
		load(*this | rhs);

		return *this;
	}

	template<typename T> inline auto operator &=(const T& rhs) -> typename ifAssignable<decltype(*this & rhs)>::type&
	{
		load(*this & rhs);

		return *this;
	}

	template<typename T> inline auto operator *=(const T& rhs) -> typename ifAssignable<decltype(*this * rhs)>::type&
	{
		load(*this * rhs);

		return *this;
	}

	template<typename T> inline auto operator -=(const T& rhs) -> typename ifAssignable<decltype(*this - rhs)>::type&
	{
		load(*this - rhs);

		return *this;
	}

	template<typename T> inline reference operator[](T inp_Index) { return mcRawPtr[static_cast<size_type>(inp_Index)]; }
	template<typename T> inline const_reference operator[](size_type inp_Index) const { return mcRawPtr[static_cast<size_type>(inp_Index)]; }

	inline reference operator[](size_type inp_iIndex) { return mcRawPtr[inp_iIndex]; }
	inline const_reference operator[](size_type inp_iIndex) const { return mcRawPtr[inp_iIndex]; }

	template<size_type TIndx> inline reference at(void) { return mcRawPtr[TIndx]; }
	template<size_type TIndx> inline const_reference at(void) const { return mcRawPtr[TIndx]; }

	template<typename T> inline reference at(T inp_Index) { return mcRawPtr[static_cast<size_type>(inp_Index)]; }
	template<typename T> inline const_reference at(T inp_Index) const { return mcRawPtr[static_cast<size_type>(inp_Index)]; }

	inline reference at(size_type inp_iIndex) { return mcRawPtr[inp_iIndex]; }
	inline const_reference at(size_type inp_iIndex) const { return mcRawPtr[inp_iIndex]; }

	inline reference back(void) { return mcRawPtr[size() - 1]; }
	inline const_reference back(void) const { return mcRawPtr[size() - 1]; }

	inline iterator begin(void) { return iterator(mcRawPtr); }
	inline const_iterator begin(void) const { return const_iterator(mcRawPtr); }

	inline const_iterator cbegin(void) const { return const_iterator(mcRawPtr); }
	inline const_iterator cend(void) const { return const_iterator(data() + size()); }

	inline pointer data(void) { return mcRawPtr; }
	inline const_pointer data(void) const { return mcRawPtr; }

	inline bool empty(void) const { return (size() == 0); }

	inline iterator end(void) { return iterator(mcRawPtr + size()); }
	inline const_iterator end(void) const { return const_iterator(mcRawPtr + size()); }

	template<typename F> inline void foreach(F inp_Function)
	{
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			inp_Function(mcRawPtr[iIndx]);
		}
	}

	inline reference front(void) { return *mcRawPtr; }
	inline const_reference front(void) const { return *mcRawPtr; }

	allocator_type get_allocator(void) const { return mcAllocator; }

	virtual size_type max_size(void) const = 0;

	template<typename T> inline bool repeat(const T& inp_cData)
	{
		const size_type iSizeIn(inp_cData.size());
		const size_type iSizeOut(size());
		const size_type iReps(iSizeOut / iSizeIn);

		if (iSizeOut != iSizeIn * iReps) return false;

		for (size_type lOutIndx(0); lOutIndx < iSizeOut;)
		{
			for (size_type lInIndx(0); lInIndx < iSizeIn; lInIndx++, lOutIndx++)
			{
				mcRawPtr[lOutIndx] = inp_cData[lInIndx];
			}
		}

		return true;
	}

	template<typename T> inline bool repeatElements(const T& inp_cData)
	{
		const size_type iSizeIn(inp_cData.size());
		const size_type iSizeOut(size());
		const size_type iReps(iSizeOut / iSizeIn);

		if (iSizeOut != iSizeIn * iReps) return false;

		for (size_type lInIndx(0), iOutIndx(0); lInIndx < iSizeIn; lInIndx++)
		{
			const value_type dThis(inp_cData[lInIndx]);
			for (const size_type iBlockEnd(iOutIndx + iReps); iOutIndx < iBlockEnd; iOutIndx++)
			{
				mcRawPtr[iOutIndx] = dThis;
			}
		}
		return true;
	}

	virtual size_type size(void) const = 0;

	inline void sort(void)
	{
		const size_type iInsertionSize(7), iStackSize(128);
		const size_type iIndxCnt(size());

		size_type iIndx, iIndxLeft(0), iIndxRight(iIndxCnt - 1), iJndx, iPivot;
		size_type iStack[iStackSize];
		int32_t	iIndxStack(-1);

		value_type	dPivot;

		for (;;)
		{
			if (iIndxRight - iIndxLeft < iInsertionSize)
			{
				for (iJndx = iIndxLeft + 1; iJndx <= iIndxRight; iJndx++)
				{
					dPivot = mcRawPtr[iJndx];
					for (iIndx = iJndx - 1; iIndx >= iIndxLeft; iIndx--)
					{
						if (mcRawPtr[iIndx] <= dPivot) { break; }
						mcRawPtr[iIndx + 1] = mcRawPtr[iIndx];
					}
					mcRawPtr[iIndx + 1] = dPivot;
				}
				if (iIndxStack < 0) { break; }
				iIndxRight = iStack[iIndxStack--];
				iIndxLeft = iStack[iIndxStack--];
			}
			else
			{
				iPivot = (iIndxLeft + iIndxRight) >> 1;
				std::swap(mcRawPtr[iPivot], mcRawPtr[iIndxLeft + 1]);
				if (mcRawPtr[iIndxLeft] >mcRawPtr[iIndxRight]) std::swap(mcRawPtr[iIndxLeft], mcRawPtr[iIndxRight]);
				if (mcRawPtr[iIndxLeft + 1] > mcRawPtr[iIndxRight]) std::swap(mcRawPtr[iIndxLeft + 1], mcRawPtr[iIndxRight]);
				if (mcRawPtr[iIndxLeft] > mcRawPtr[iIndxLeft + 1]) std::swap(mcRawPtr[iIndxLeft], mcRawPtr[iIndxLeft + 1]);
				iIndx = iIndxLeft + 1;
				iJndx = iIndxRight;
				dPivot = mcRawPtr[iIndxLeft + 1];
				for (;;)
				{
					do iIndx++; while (mcRawPtr[iIndx] < dPivot);
					do iJndx--; while (mcRawPtr[iJndx] > dPivot);
					if (iJndx < iIndx) { break; }
					std::swap(mcRawPtr[iIndx], mcRawPtr[iJndx]);
				}
				mcRawPtr[iIndxLeft + 1] = mcRawPtr[iJndx];
				mcRawPtr[iJndx] = dPivot;
				iIndxStack += 2;
				if (iIndxStack >= iStackSize) { throw("NSTACK too small in sort."); }
				if (iIndxRight - iIndx + 1 >= iJndx - 1)
				{
					iStack[iIndxStack] = iIndxRight;
					iStack[iIndxStack - 1] = iIndx;
					iIndxRight = iJndx - 1;
				}
				else
				{
					iStack[iIndxStack] = iJndx - 1;
					iStack[iIndxStack - 1] = iIndxLeft;
					iIndxLeft = iIndx;
				}
			}
		}
	}

protected:
	enum :size_type { ALIGN = 32, VALUE_SIZE = sizeof(value_type), BLOCK_SIZE = lcm<ALIGN, VALUE_SIZE>::value, CNT_PER_BLOCK = BLOCK_SIZE / VALUE_SIZE };

	allocator_type	mcAllocator;

	size_type		miAllocatedSize;
	pointer			mcRawPtr;

	CArrayAccess(void) :miAllocatedSize(0), mcRawPtr(nullptr) {}

	virtual void alloc(size_type& inp_iSize, pointer& inp_Pointer) const
	{
		if (inp_iSize % _Myt::CNT_PER_BLOCK != 0) inp_iSize = (inp_iSize / _Myt::CNT_PER_BLOCK + 1) * _Myt::CNT_PER_BLOCK;

		inp_Pointer = mcAllocator.allocate(inp_iSize);
		for (size_type i(0); i < inp_iSize; i++)
		{
			::new ((void *)(inp_Pointer + i)) value_type();
		}
	}

	virtual void deallocate(size_type inp_iSize, pointer inp_Pointer)
	{
		mcAllocator.deallocate(inp_Pointer, inp_iSize);
	}

	virtual bool deserialize(SSerial& inp_sSerial)
	{
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			SPUD::deserializeTools(inp_sSerial, this->data()[iIndx]);
		}

		return true;
	}

	template<typename TRhs> typename std::enable_if<IsExpr<TRhs>::value, size_t>::type load(const TRhs& inp_Expression)
	{
		const size_t iSize(std::min(size(), inp_Expression.size()));
		for (size_t iIndx(0); iIndx < size(); iIndx += SimdHlpr<value_type>::STEP)
		{
			SimdHlpr<value_type>::store(this->mcRawPtr + iIndx, inp_Expression[iIndx]);
		}

		return iSize;
	}

	virtual size_t load(const init_list& inp_cInitList)
	{
		const size_t iSize(std::min(size(), inp_cInitList.size()));
		
		auto	cInpIterator(inp_cInitList.begin()), cInpIteratorEnd(inp_cInitList.end());
		for (size_type iIndx(0); iIndx < size(); iIndx++, cInpIterator++)
		{
			if (cInpIterator == cInpIteratorEnd) cInpIterator = inp_cInitList.begin();
			this->data()[iIndx] = *cInpIterator;
		}

		return iSize;
	}

	virtual void pack(SPack& inp_sPack) const
	{
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			SPUD::packTools(inp_sPack, this->data()[iIndx]);
		}
	}

	virtual SPack::size_type packSize(void) const
	{
		SPack::size_type iSum(0);
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			iSum += SPUD::packSizeTools(this->data()[iIndx]);
		}

		return iSum;
	}

	virtual void serialize(SSerial& inp_sSerial) const
	{
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			SPUD::serializeTools(inp_sSerial, this->data()[iIndx]);
		}
	}

	virtual bool unpack(SPack& inp_sPack)
	{
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			SPUD::unpackTools(inp_sPack, this->data()[iIndx]);
		}

		return true;
	}
};

template<typename TData> class CArrayBased : public CArrayAccess<TData> {};

template<> class CArrayBased<CDate> : public CArrayAccess<CDate>
{
public:
	typedef CArrayAccess<CDate>		_Mybase;
	typedef CArrayBased<CDate>		_Myt;

	typedef _Mybase::size_type			size_type;
	typedef _Mybase::value_type			value_type;
	typedef _Mybase::difference_type	difference_type;
	typedef _Mybase::reference			reference;
	typedef _Mybase::const_reference	const_reference;
	typedef _Mybase::pointer			pointer;
	typedef _Mybase::const_pointer 		const_pointer;

	inline _Myt& operator +=(const int32_t& rhs)
	{
		for (size_type iIndx(0); iIndx < size(); iIndx++)
		{
			this->data()[iIndx] += rhs;
		}
		return *this;
	}

	inline _Myt& operator -=(const int32_t& rhs)
	{
		return operator+=(-rhs);
	}

	virtual size_type size(void) const = 0;

protected:
	virtual SPack::size_type packSize(void) const
	{
		return size() * sizeof(value_type);
	}
};

template<typename TArray> class const_ArrayItr
{
public:
	typedef TArray						_Myarray;
	typedef const_ArrayItr<_Myarray>	_Myt;

	typedef typename _Myarray::size_type		size_type;
	typedef typename _Myarray::value_type		value_type;
	typedef typename _Myarray::difference_type	difference_type;
	typedef typename _Myarray::const_pointer	pointer;
	typedef typename _Myarray::const_reference	reference;

	const_ArrayItr(void) {}

	const_ArrayItr(CSmartPtr<_Myarray> inp_cDataPtr) { attach(inp_cDataPtr); }

	inline reference operator*(void) const { return value(); }

	inline pointer operator->(void) const
	{
		return (std::pointer_traits<pointer>::pointer_to(**this));
	}

	inline _Myt& operator++(void)
	{
		++mcItr;
		return (*this);
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return (temp);
	}

	inline _Myt& operator--(void)
	{
		--mcItr;
		return (*this);
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--*this;
		return (temp);
	}

	inline _Myt& operator+=(difference_type inp_Shift)
	{
		mcItr += inp_Shift;
		return (*this);
	}

	inline _Myt operator+(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp += inp_Shift);
	}

	inline _Myt& operator-=(difference_type inp_Shift)
	{
		mcItr -= inp_Shift;
		return (*this);
	}

	inline _Myt operator-(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp -= inp_Shift);
	}

	inline difference_type operator-(const _Myt& rhs) const
	{
		return (mcItr - rhs.mcItr);
	}

	inline reference operator[](difference_type inp_Shift) const
	{
		return (*(*this + inp_Shift));
	}

	inline bool operator==(const _Myt& rhs) const
	{
		return (mcItr == rhs.mcItr);
	}

	inline bool operator!=(const _Myt& rhs) const
	{
		return (!(*this == rhs));
	}

	inline bool operator<(const _Myt& rhs) const
	{
		return (mcItr < rhs.mcItr);
	}

	inline bool operator>(const _Myt& rhs) const
	{
		return (rhs < *this);
	}

	inline bool operator<=(const _Myt& rhs) const
	{
		return (!(rhs < *this));
	}

	inline bool operator>=(const _Myt& rhs) const
	{
		return (!(*this < rhs));
	}

	inline _Myt& attach(CSmartPtr<_Myarray> inp_cDataPtr)
	{
		mcDataPtr = inp_cDataPtr;
		mcItr = mcDataPtr->begin();

		return *this;
	}


	inline _Myt& begin(void) { mcItr = mcDataPtr->begin(); return *this; }

	inline CSmartPtr<_Myarray> data(void) const { return mcDataPtr; }

	inline _Myt& end(void) { mcItr = mcDataPtr->end(); return *this; }

	inline bool isBegin(void) const { return (mcItr == mcDataPtr->begin()); }

	inline bool isEnd(void) const { return (mcItr == mcDataPtr->end()); }

	inline size_type key(void) const { return mcItr - mcDataPtr->begin(); }

	inline _Myt& last(void)
	{
		if (end().isEnd()) { this->operator--(); }
		return *this;
	}

	inline reference value(void) const { return *mcItr; }

protected:
	typedef typename _Myarray::iterator iterator;

	CSmartPtr<_Myarray>	mcDataPtr;
	iterator			mcItr;
};

template<typename TArray> class ArrayItr : public const_ArrayItr<TArray>
{
public:
	typedef TArray				_Myarray;

	typedef const_ArrayItr<_Myarray>	_Mybase;
	typedef ArrayItr<_Myarray>			_Myt;

	typedef typename _Myarray::size_type		size_type;
	typedef typename _Myarray::value_type		value_type;
	typedef typename _Myarray::difference_type	difference_type;
	typedef typename _Myarray::pointer			pointer;
	typedef typename _Myarray::reference		reference;

	ArrayItr(void) :_Mybase() {}

	ArrayItr(CSmartPtr<_Myarray> inp_cArrayPtr) :_Mybase(inp_cArrayPtr) {}

	inline reference operator*(void) const { return value(); }

	inline pointer operator->(void) const
	{
		return (std::pointer_traits<pointer>::pointer_to(**this));
	}

	inline _Myt& operator++(void)
	{
		++*(_Mybase *)this;
		return (*this);
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return (temp);
	}

	inline _Myt& operator--(void)
	{
		--*(_Mybase *)this;
		return (*this);
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--*this;
		return (temp);
	}

	inline _Myt& operator+=(difference_type inp_Shift)
	{
		*(_Mybase*)this += inp_Shift;
		return (*this);
	}

	inline _Myt operator+(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp += inp_Shift);
	}

	inline _Myt& operator-=(difference_type inp_Shift)
	{
		*(_Mybase*)this -= inp_Shift;
		return (*this);
	}

	inline _Myt operator-(difference_type inp_Shift) const
	{
		_Myt temp(*this);
		return (temp -= inp_Shift);
	}

	inline difference_type operator-(const _Myt& rhs) const
	{
		return *(_Mybase*)this - rhs;
	}

	inline reference operator[](difference_type inp_Shift) const
	{
		return (*(*this + inp_Shift));
	}

	inline _Myt& begin(void) { _Mybase::begin(); return *this; }

	inline _Myt& end(void) { _Mybase::end(); return *this; }

	inline _Myt& last(void) { _Mybase::last(); return *this; }

	inline reference value(void) const { return *_Mybase::mcItr; }
};

template<typename T, bool hasValueType = HasValueType<T>::value> struct IsArrayImpl
{
	enum :int { value = 0 };
};
template<typename T> struct IsArrayImpl<T, true>
{
	enum :int { value = std::is_base_of<CArrayAccess<typename T::value_type>, T>::value };
};
template<typename T> using IsArray = IsArrayImpl<T>;

template<typename T, typename U> using ifArray = std::enable_if<IsArray<T>::value, U>;

namespace iterators
{
	template<typename T> static inline typename ifArray<T, const_ArrayItr<T>>::type GetConstItr(CSmartPtr<T> inp_cArrayPtr) { return const_ArrayItr<T>(inp_cArrayPtr); }
	template<typename T> static inline typename ifArray<T, ArrayItr<T>>::type GetItr(CSmartPtr<T> inp_cArrayPtr) { return ArrayItr<T>(inp_cArrayPtr); }
}
#endif
