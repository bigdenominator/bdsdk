#ifndef INCLUDE_H_ITRBUNDLETESTS
#define INCLUDE_H_ITRBUNDLETESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/ItrBundle.h"
#include "containers/SmartArray.h"
#include "containers/SmartMap.h"
#include "containers/SmartVector.h"

class ItrBundleTests : public testing::Test
{
public:
	CSmartPtr<CSmartArray<CDate, 4>>	mcArrayPtr;
	CSmartPtr<CSmartMap<int, double>>	mcMapPtr;
	CSmartPtr<CSmartVector<float>>		mcVectorPtr;

	void SetUp(void)
	{
		mcArrayPtr = CreateSmart<CSmartArray<CDate, 4>>();
		mcMapPtr = CreateSmart<CSmartMap<int, double>>();
		mcVectorPtr = CreateSmart<CSmartVector<float>>();

		(*mcArrayPtr)[0] = CDate(20140101);
		(*mcArrayPtr)[1] = CDate(20150101);
		(*mcArrayPtr)[2] = CDate(20160101);
		(*mcArrayPtr)[3] = CDate(20170101);

		mcMapPtr->add(1, 10);
		mcMapPtr->add(2, 20);
		mcMapPtr->add(3, 30);
		mcMapPtr->add(4, 40);

		mcVectorPtr->resize(4);
		(*mcVectorPtr)[0] = 100;
		(*mcVectorPtr)[1] = 200;
		(*mcVectorPtr)[2] = 300;
		(*mcVectorPtr)[3] = 400;
	}

	void TearDown(void) {}
};

TEST_F(ItrBundleTests, Bundle)
{
	auto itrArray(iterators::GetItr(mcArrayPtr));
	auto itrMap(iterators::GetItr(mcMapPtr));
	auto itrVector(iterators::GetItr(mcVectorPtr));

	int iIndx(1);

	auto itrTest1(iterators::Bundle(itrArray, itrMap, itrVector));
	LOOP(itrTest1)
	{
		EXPECT_EQ(itrTest1.At<0>().value(), CDate(20140101).Add(PERIODS::YEAR, iIndx - 1));
		EXPECT_EQ(itrTest1.At<1>().key(), iIndx);
		EXPECT_EQ(itrTest1.At<1>().value(), static_cast<double>(iIndx * 10));
		EXPECT_EQ(itrTest1.At<2>().value(), static_cast<float>(iIndx * 100));

		iIndx++;
	}

	iIndx = 1;

	auto itrTest2(iterators::Bundle(itrArray, itrMap, itrVector));
	LOOP(itrTest2)
	{
		EXPECT_EQ(itrTest2.At<0>().value(), CDate(20140101).Add(PERIODS::YEAR, iIndx - 1));
		EXPECT_EQ(itrTest2.At<1>().key(), iIndx);
		EXPECT_EQ(itrTest2.At<1>().value(), static_cast<double>(iIndx * 10));
		EXPECT_EQ(itrTest2.At<2>().value(), static_cast<float>(iIndx * 100));

		iIndx++;
	}
}

TEST_F(ItrBundleTests, MakeBundle)
{
	typedef ItrBundle<ArrayItr<CSmartArray<CDate, 4>>, MapItr<CSmartMap<int, double>>, ArrayItr<CSmartVector<float>>> MyBundle;

	int iIndx(1);

	MyBundle itrTest(iterators::MakeBundle(mcArrayPtr, mcMapPtr, mcVectorPtr));
	LOOP(itrTest)
	{
		EXPECT_EQ(itrTest.At<0>().value(), CDate(20140101).Add(PERIODS::YEAR, iIndx - 1));
		EXPECT_EQ(itrTest.At<1>().key(), iIndx);
		EXPECT_EQ(itrTest.At<1>().value(), static_cast<double>(iIndx * 10));
		EXPECT_EQ(itrTest.At<2>().value(), static_cast<float>(iIndx * 100));

		iIndx++;
	}
}
#endif
