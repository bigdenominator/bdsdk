#ifndef INCLUDE_H_FEEDBACK
#define INCLUDE_H_FEEDBACK

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/SmartContainers.h"

class CFeedback : public CContainer<FRM_TYPELIST::CFEEDBACK>
{
public:
	typedef CContainer<FRM_TYPELIST::CFEEDBACK>	_Mybase;
	typedef CFeedback							_Myt;

	int32_t		miNum;
	int32_t		miLine;
	string		msModule;
	string		msInfo;

	CFeedback(void) :_Mybase(), miNum(0), miLine(0), msModule(sNULL), msInfo(sNULL) {}
	CFeedback(const int32_t inp_iErrorNum, const int32_t inp_iLine, const string& inp_sModule, const string& inp_sInfo) :_Myt() 
	{
		Set(inp_iErrorNum, inp_iLine, inp_sModule, inp_sInfo); 
	}
	CFeedback(const _Myt& rhs) :_Myt() { clone(rhs); }
	CFeedback(_Myt&& rhs) :_Myt() { clone(std::forward<_Myt>(rhs)); }
	CFeedback& operator=(const _Myt& rhs) { clone(rhs); return *this; }
	CFeedback& operator=(_Myt&& rhs) { clone(std::forward<_Myt>(rhs)); return *this; }
	void Clear(void) { Set(0, 0, sNULL, sNULL); }
	void Set(const int32_t inp_iErrorNum, const int32_t inp_iLine, const string& inp_sModule, const string& inp_sInfo)
	{
		miNum = inp_iErrorNum;
		miLine = inp_iLine;
		msModule = inp_sModule;
		msInfo = inp_sInfo;
	}
	operator bool(void){ return miNum == 0; }

protected:
	inline void clone(const _Myt& rhs) 
	{
		miNum = rhs.miNum;
		miLine = rhs.miLine;
		msModule = rhs.msModule;
		msInfo = rhs.msInfo;
	}
	inline void clone(_Myt&& rhs)
	{
		miNum = rhs.miNum;
		miLine = rhs.miLine;
		msModule = std::move(rhs.msModule);
		msInfo = std::move(rhs.msInfo);
	}
	
	SPUD(miNum, miLine, msModule, msInfo)
};

#define FEEDBACK(X, Y, Z) { (X).Set((Y), __LINE__, __FILE__, (Z)); }
#endif
