#ifndef INCLUDE_H_EVENTDEF
#define INCLUDE_H_EVENTDEF

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <atomic>
#include "containers/IContainer.h"
#include "nodes/Feedback.h"

struct STransport;
typedef STransport* STransportPtr;

class SBlackBox 
{
public:
	virtual ~SBlackBox(void) {}

	virtual TYPELIST_t GetTypeId(void) const = 0;

protected:
	SBlackBox() {}
};

typedef CUniquePtr<SBlackBox> SBlackBoxPtr;
typedef CContainerIntrPtr(*fnSchematic)(SBlackBoxPtr&);

template<typename T> struct SGreyBox : public SBlackBox
{
	typedef SBlackBox	_Mybase;
	typedef SGreyBox<T>	_Myt;

	CSmartPtr<T>  msSchematicPtr;

	SGreyBox(CSmartPtr<T> inp_sSchematicPtr) :_Mybase(), msSchematicPtr(inp_sSchematicPtr) {}

	static inline CSmartPtr<T> Extract(SBlackBoxPtr& inp_sBlackBoxPtr)
	{
		return (reinterpret_cast<SGreyBox<T>*>(inp_sBlackBoxPtr.get()))->msSchematicPtr;
	}

	TYPELIST_t GetTypeId(void) const { return T::TypeId; }
};

struct SReturnInfo
{
public:
	atomic<bool>	mbRequestWaiting;
	STransportPtr	msPrevTransPtr;

	SReturnInfo(void) { initialize(); }

	void initialize(void)
	{
		mbRequestWaiting = false;
		msPrevTransPtr = nullptr;
	}
};

enum class PROCESS_STATUS { DONE, FAIL, PENDING };
struct ICallback
{
	virtual PROCESS_STATUS operator()(void) = 0;
	virtual TYPELIST_t GetTypeId(void) = 0;
};
typedef CUniquePtr<ICallback> SCallbackPtr;

struct STransport
{
public:
	uint64_t		miTransID;

	SBlackBoxPtr	msDataHldrPtr;
	SCallbackPtr	msCallbackPtr;

	SReturnInfo		msReturnInfo;

	CFeedback		mcFeedback;

	static STransportPtr CreateInstance(void) { return STransportPtr(new STransport()); }

	static void DestroyInstance(STransportPtr inp_sInstancePtr)
	{
		delete inp_sInstancePtr;
	}

	void initialize(void)
	{
		msDataHldrPtr.reset();
		msCallbackPtr.reset();

		msReturnInfo.initialize();

		mcFeedback.Clear();
	}

protected:
	STransport(void) { initialize(); }

	~STransport(void) {}
};
#endif