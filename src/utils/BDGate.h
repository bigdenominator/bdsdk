#ifndef INCLUDE_H_BDGATE
#define INCLUDE_H_BDGATE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <atomic>

class BDGate
{
public:
	BDGate(void) { mbLock.clear(); }

	void Enter(void)
	{
		while (mbLock.test_and_set()) {}
	}

	void Exit(void)
	{
		mbLock.clear();
	}

private:
	atomic_flag	mbLock;
};
#endif