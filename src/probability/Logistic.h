#ifndef INCLUDE_H_LOGISTICPROBABILITY
#define INCLUDE_H_LOGISTICPROBABILITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

class CLogistic 
{
public:
	CLogistic(const double& inp_dLocation, const double& inp_dScale) :mdInvScale(1.0 / inp_dScale), mdLocation(inp_dLocation), mdScale(inp_dScale) {}

	inline double CDF(const double& inp_dValue)
	{
		return StandardCDF(center(inp_dValue));
	}
	inline double InverseCDF(const double& inp_dProbability)
	{
		return mdLocation + mdScale * StandardInverseCDF(inp_dProbability);
	}
	inline double PDF(const double& inp_dValue)
	{
		return StandardPDF(center(inp_dValue)) * mdInvScale;
	}

	template<typename T> inline auto CDF(const T& inp_cValues) -> decltype(StandardCDF(center(inp_cValues)))
	{
		return StandardCDF(center(inp_cValues));
	}
	template<typename T> inline auto InverseCDF(const T& inp_cProbabilities) -> decltype((StandardInverseCDF(inp_cProbabilities) * (double)1) + (double)0) // hard coded types replace member-variable usage per GCC
	{
		return (StandardInverseCDF(inp_cProbabilities) * mdScale) + mdLocation;
	}
	template<typename T> inline auto PDF(const T& inp_cValues)-> decltype(StandardPDF(center(inp_cValues)) * (double)1) // hard coded types replace member-variable usage per GCC
	{
		return StandardPDF(center(inp_cValues)) * mdInvScale;
	}

	static inline double StandardCDF(const double& inp_dValue)
	{
		return 1.0 / (1.0 + CMath::exp(-inp_dValue));
	}
	static inline double StandardInverseCDF(const double& inp_dProbability)
	{
		return -CMath::ln((1.0 / inp_dProbability) - 1.0);
	}
	static inline double StandardPDF(const double& inp_dValue)
	{
		double dExp(CMath::exp(-inp_dValue));
		double dTemp(1.0 / (1.0 + dExp));

		return dExp * dTemp * dTemp;
	}

	template<typename T> static inline auto StandardCDF(const T& inp_cValues) -> decltype(1.0 / (std::exp(-1.0 * inp_cValues) + 1.0))
	{
		return 1.0 / (std::exp(-1.0 * inp_cValues) + 1.0);
	}
	template<typename T> static inline auto StandardInverseCDF(const T& inp_cProbabilities) -> decltype(-1.0 * std::ln(1.0 / inp_cProbabilities - 1.0))
	{
		return -1.0 * std::ln(1.0 / inp_cProbabilities - 1.0);
	}
	template<typename T> static inline auto StandardPDF(const T& inp_cValues) -> decltype(std::exp(-1.0 * inp_cValues) / std::pow((1.0 + std::exp(-1.0 * inp_cValues)), 2))
	{
		return std::exp(-1.0 * inp_cValues) / std::pow((1.0 + std::exp(-1.0 * inp_cValues)), 2);
	}

protected:
	const double mdInvScale;
	const double mdLocation;
	const double mdScale;

	template<typename T> inline auto center(const T& inp_cValues) -> decltype((inp_cValues - mdLocation) * mdInvScale)
	{
		return (inp_cValues - mdLocation) * mdInvScale;
	}
};
#endif
