#ifndef INCLUDE_H_ITRBUNDLE
#define INCLUDE_H_ITRBUNDLE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/TypeLists.h"
#include "containers/Tuple.h"

namespace iterators {}

template<typename T, uint32_t TIndx> struct ItrBundleHlpr
{
	typedef typename T::ItrList ItrList;

	enum :uint32_t { iNEXT = TIndx - 1, iLENGTH = ItrList::length, iREV = iLENGTH - TIndx };

	typedef ItrBundleHlpr<T, iNEXT>	_Myback;
	typedef ItrBundleHlpr<T, TIndx>	_Myt;

	template<uint32_t iIndx> static typename ListMeta<ItrList, iIndx>::TypeAt& At(T& inp_Bundle)
	{
		return inp_Bundle.At<iIndx>();
	}

	static void begin(T& inp_Bundle)
	{
		At<iREV>(inp_Bundle).begin();
		_Myback::begin(inp_Bundle);
	}
	static void copy(T& lhs, T& rhs)
	{
		At<iREV>(lhs) = At<iREV>(rhs);
		_Myback::copy(lhs, rhs);
	}
	static void decrement(T& inp_Bundle)
	{
		At<iREV>(inp_Bundle)--;
		_Myback::decrement(inp_Bundle);
	}
	static void end(T& inp_Bundle)
	{
		At<iREV>(inp_Bundle).end();
		_Myback::end(inp_Bundle);
	}
	static void increment(T& inp_Bundle)
	{
		At<iREV>(inp_Bundle)++;
		_Myback::increment(inp_Bundle);
	}
	static void last(T& inp_Bundle)
	{
		At<iREV>(inp_Bundle).last();
		_Myback::last(inp_Bundle);
	}
	template<typename TItr, typename ...TItrs> static void set(T& inp_Bundle, const TItr& itr, const TItrs&... iterators)
	{
		At<iREV>(inp_Bundle) = itr;
		_Myback::set(inp_Bundle, iterators...);
	}
};

template<typename T> struct ItrBundleHlpr<T, 0>
{
	static void begin(T& inp_Bundle) {}
	static void copy(T& lhs, T& rhs) {}
	static void decrement(T& inp_Bundle) {}
	static void end(T& inp_Bundle) {}
	static void increment(T& inp_Bundle) {}
	static void set(T& inp_Bundle) {}
	static void last(T& inp_Bundle) {}
};

template <typename ...Itrs> struct ItrBundle : public Tuple<TypeList<Itrs...>>
{
public:
	typedef TypeList<Itrs...>	ItrList;
	typedef Tuple<ItrList>		_Mybase;
	typedef ItrBundle<Itrs...>	_Myt;

	typedef ItrBundleHlpr<_Myt, ItrList::length> _Myhlpr;

	template<uint32_t TIndx> using Types = typename ItrList::template Types<TIndx>;
	
	ItrBundle(void) {}

	ItrBundle(const Itrs&... iterators) { _Myhlpr::set(*this, iterators...); }

	_Myt& operator=(_Myt& rhs)
	{
		_Myhlpr::copy(*this, rhs);
		return *this;
	}

	template<uint32_t TIndx> inline const typename Types<TIndx>::At& At(void) const
	{
		typedef typename Types<TIndx>::SubList	SubList;

		return Tuple<SubList>::myValue;
	}

	template<uint32_t TIndx> inline typename Types<TIndx>::At& At(void)
	{
		typedef typename Types<TIndx>::SubList	SubList;

		return Tuple<SubList>::myValue;	
	}

	_Myt& begin(void)
	{
		_Myhlpr::begin(*this);
		return *this;
	}

	_Myt& end(void)
	{
		_Myhlpr::end(*this);
		return *this;
	}

	bool isBegin(void) { return At<0>().isBegin(); }

	bool isEnd(void) { return At<0>().isEnd(); }

	_Myt& last(void)
	{
		_Myhlpr::last(*this);
		return *this;
	}

	inline _Myt& operator++(void)
	{
		_Myhlpr::increment(*this);
		return *this;
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return temp;
	}

	inline _Myt& operator--(void)
	{
		_Myhlpr::decrement(*this);
		return *this;
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--(*this);
		return temp;
	}
};

template<typename T> struct ItrSelector
{
	typedef decltype(iterators::GetConstItr<T>(CreateSmart<T>())) const_iterator;
	typedef decltype(iterators::GetItr<T>(CreateSmart<T>())) iterator;
};

template<typename ...TCntrs> struct BundleHlpr
{
	typedef ItrBundle<typename ItrSelector<TCntrs>::const_iterator...> const_type;
	typedef ItrBundle<typename ItrSelector<TCntrs>::iterator...> type;
};

namespace iterators
{
	template<typename ...Itrs> static inline ItrBundle<Itrs...> Bundle(const Itrs&... iterators) { return ItrBundle<Itrs...>(iterators...); }

	template<typename ...TCntrs> static inline typename BundleHlpr<TCntrs...>::const_type MakeBundleConst(CSmartPtr<TCntrs>... myCntrPtrs)
	{
		typedef typename BundleHlpr<TCntrs...>::const_type bundle_type;

		return bundle_type(GetConstItr(myCntrPtrs)...);
	}

	template<typename ...TCntrs> static inline typename BundleHlpr<TCntrs...>::type MakeBundle(CSmartPtr<TCntrs>... myCntrPtrs)
	{
		typedef typename BundleHlpr<TCntrs...>::type bundle_type;

		return bundle_type(GetItr(myCntrPtrs)...);
	}
}
#endif
