#ifndef INCLUDE_H_$itemname$
#define INCLUDE_H_$itemname$

/* (C) Copyright Big Denominator, LLC 2016.
* Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"
#ifdef WINDOWS
#pragma once
#endif

#include "nodes/TransformNode.h"
#include "Schematics.h"


#endif