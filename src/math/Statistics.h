#ifndef INCLUDE_H_STATISTICS
#define INCLUDE_H_STATISTICS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/SmartVector.h"

namespace statistics
{
	template <typename T> static inline typename ExprType<T>::type::primitive_type GetMean(const T& inp_cData)
	{
		typedef typename ExprType<T>::type::primitive_type value_type;

		const size_t iIndxCnt(inp_cData.size());
		if (iIndxCnt < 1) return(0.0);

		return std::sum(inp_cData) / static_cast<value_type>(iIndxCnt);
	}

	template <typename T, typename U> static inline typename ExprType<T>::type::primitive_type GetWeightedAvg(const T& inp_cData, const U& inp_cWeights)
	{
		auto dWeights(std::sum(inp_cWeights));
		if (dWeights == 0.0) return 0.0;

		return std::dot(inp_cData, inp_cWeights) / dWeights;
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetCentralMoment(const T& inp_cData, const uint32_t& inp_iMoment)
	{
		return GetMean(std::pow(inp_cData - GetMean(inp_cData), static_cast<int>(inp_iMoment)));
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetVariancePopulation(const T& inp_cData)
	{
		return max(GetCentralMoment(inp_cData, 2L), 0.0);
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetVarianceSample(const T& inp_cData)
	{
		typedef typename ExprType<T>::type::primitive_type value_type;

		const size_t iIndxCnt(inp_cData.size());

		if (iIndxCnt < 2) return  0.0;

		return GetVariancePopulation(inp_cData) * static_cast<value_type>(iIndxCnt) / static_cast<value_type>(iIndxCnt - 1);
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetStdDevPopulation(const T& inp_cData)
	{
		return  sqrt(GetVariancePopulation(inp_cData));
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetStdDevSample(const T& inp_cData)
	{
		return  sqrt(GetVarianceSample(inp_cData));
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetSkew(const T& inp_cData)
	{
		typedef typename ExprType<T>::type::primitive_type value_type;

		const size_t iIndxCnt(inp_cData.size());

		if (iIndxCnt < 3) return(0.0);

		value_type	dBiasAdj, dMoment2, dMoment3, dSkew;

		dBiasAdj = static_cast<value_type>(iIndxCnt * (iIndxCnt - 1)) / static_cast<value_type>(iIndxCnt - 2);

		dMoment2 = GetCentralMoment(inp_cData, 2);
		dMoment3 = GetCentralMoment(inp_cData, 3);

		dSkew = dMoment3 / pow(dMoment2, 1.5);
		dSkew *= dBiasAdj;

		return dSkew;
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetKurtosis(const T& inp_cData)
	{
		typedef typename ExprType<T>::type::primitive_type value_type;

		const size_t iIndxCnt(inp_cData.size());
		if (iIndxCnt < 4) return(0.0);

		T dBiasAdj, dMoment2, dMoment4, dKurtosis;

		dBiasAdj = static_cast<value_type>(iIndxCnt - 1) / static_cast<value_type>((iIndxCnt - 2) * (iIndxCnt - 3));

		dMoment2 = GetCentralMoment(inp_cData, 2);
		dMoment4 = GetCentralMoment(inp_cData, 4);

		dKurtosis = dMoment4 / (dMoment2 * dMoment2) - 3.0;
		dKurtosis *= static_cast<value_type>(iIndxCnt - 1);
		dKurtosis += 6.0;
		dKurtosis *= dBiasAdj;

		return  dKurtosis;
	}

	template <typename T> static inline typename ExprType<T>::type::primitive_type GetPercentile(const T& inp_cData, const uint32_t& inp_iPercentile)
	{
		typedef typename ExprType<T>::type::primitive_type value_type;
		typedef CSmartVector<value_type> temp_type;

		const  size_t iIndxCnt(inp_cData.size());

		value_type fLambda(static_cast<value_type>(inp_iPercentile) / 100.0 * static_cast<value_type>(iIndxCnt + 1));
		size_t iLower(floor(fLambda));
		size_t iUpper(iLower + 1);
		
		CUniquePtr<temp_type> tmpPtr(CreateUnique<temp_type>(inp_cData));
		tmpPtr->sort();
		
		if (iLower < 1) return (*tmpPtr)[0];
		else if (iUpper > iIndxCnt) return (*tmpPtr)[iIndxCnt - 1];
		else
		{
			fLambda -= static_cast<value_type>(iLower);
			iLower--;
			iUpper--;

			return  (*tmpPtr)[iLower] + fLambda * ((*tmpPtr)[iUpper] - (*tmpPtr)[iLower]);
		}
	}
};
#endif
