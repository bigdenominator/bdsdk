
/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "BDLink.h"

#ifdef WINDOWS
BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
#endif

extern CEvntHndlrIntrPtr mcEvntHndlrIntrPtr;

extern "C" BIGDENOMINATORLIB_API int BootstrapLib(SLibBootstrapRwPtr inp_sModelLibListPtr)
{
	mcEvntHndlrIntrPtr = inp_sModelLibListPtr->mcEvntHndlrPtr;
	LibraryEnv::NodeSpecLstHlpr<PublishedLibNodeLst>::add(inp_sModelLibListPtr->mcNodeSpecLstPtr);
	LibraryEnv::SchemaFnLstHlpr<PublishedLibNodeLst>::add(inp_sModelLibListPtr->mcSchemaFnLstPtr);
	LibraryEnv::TechSpecLstHlpr<PublishedLibTechLst>::add(inp_sModelLibListPtr->mcTechSpecLstPtr);

	return 0;
}