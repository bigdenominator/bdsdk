#ifndef INCLUDE_H_SMARTARRAY
#define INCLUDE_H_SMARTARRAY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/ArrayBased.h"

template<typename TData, size_t TSize> class CSmartArray : public CArrayBased<TData>
{
public:
	typedef CArrayBased<TData>			_Mybase;
	typedef CSmartArray<TData, TSize>	_Myt;

	typedef typename _Mybase::size_type			size_type;
	typedef typename _Mybase::value_type		value_type;
	typedef typename _Mybase::difference_type	difference_type;
	typedef typename _Mybase::reference			reference;
	typedef typename _Mybase::const_reference	const_reference;
	typedef typename _Mybase::pointer			pointer;
	typedef typename _Mybase::const_pointer 	const_pointer;

	typedef typename _Mybase::expression_type	expression_type;
	typedef typename _Mybase::init_list		init_list;

	enum :size_type { SIZE = TSize };

	CSmartArray(void) :_Mybase()
	{
		this->miAllocatedSize = SIZE;
		this->alloc(this->miAllocatedSize, this->mcRawPtr);
	}

	CSmartArray(const_reference inp_InitValue) :_Myt()
	{
		this->load(ConstExpression<value_type>(inp_InitValue, this->size()));
	}

	CSmartArray(const init_list& inp_cInitList) :_Myt()
	{
		this->load(inp_cInitList);
	}

	CSmartArray(const _Myt& inp_cArrayBased) :_Myt()
	{
		this->load(static_cast<expression_type>(inp_cArrayBased));
	}

	CSmartArray(const _Mybase& inp_cArrayBased) :_Myt()
	{
		this->load(static_cast<expression_type>(inp_cArrayBased));
	}

	CSmartArray(_Myt&& inp_cArray) :_Mybase()
	{
		this->mcRawPtr = std::move(inp_cArray.mcRawPtr);
		this->miAllocatedSize = std::move(inp_cArray.miAllocatedSize);

		inp_cArray.mcRawPtr = nullptr;
		inp_cArray.miAllocatedSize = 0;
	}

	template<typename T, typename U = typename _Mybase::template ifRhsExpr<T, T>::type> CSmartArray(const T& rhs) : _Myt()
	{
		typedef typename ExprType<T>::type rhs_type;
		this->load(static_cast<rhs_type>(rhs));
	}

	template<typename T> inline typename _Mybase::template ifRhsConst<T, _Myt>::type& operator =(const T& rhs)
	{
		this->load(ConstExpression<value_type>(rhs, this->size()));

		return *this;
	}

	inline _Myt& operator =(const init_list& rhs)
	{
		this->load(rhs);

		return *this;
	}

	inline _Myt& operator =(const _Myt& rhs)
	{
		this->load(static_cast<expression_type>(rhs));

		return *this;
	}

	inline _Myt& operator =(const _Mybase& rhs)
	{
		this->load(static_cast<expression_type>(rhs));

		return *this;
	}

	inline _Myt& operator =(_Myt&& rhs)
	{
		this->deallocate(this->miAllocatedSize, this->mcRawPtr);

		this->mcRawPtr = std::move(rhs.mcRawPtr);
		this->miAllocatedSize = std::move(rhs.miAllocatedSize);

		rhs.mcRawPtr = nullptr;
		rhs.miAllocatedSize = 0;

		return *this;
	}

	template<typename T> inline typename _Mybase::template ifRhsExpr<T, _Myt>::type& operator =(const T& rhs)
	{
		typedef typename ExprType<T>::type rhs_type;
		this->load(static_cast<rhs_type>(rhs));

		return *this;
	}

	inline size_type max_size(void) const { return _Myt::SIZE; }

	inline size_type size(void) const { return SIZE; }
};

template<size_t TSize> using CArrayDbl = CSmartArray<double, TSize>;
template<size_t TSize> using CArrayDblPtr = CSmartPtr<CArrayDbl<TSize>>;
template<size_t TSize> using CArrayFlt = CSmartArray<float, TSize>;
template<size_t TSize> using CArrayFltPtr = CSmartPtr<CArrayDbl<TSize>>;
template<size_t TSize> using CArrayInt = CSmartArray<int32_t, TSize>;
template<size_t TSize> using CArrayIntPtr = CSmartPtr<CArrayInt<TSize>>;
template<size_t TSize> using CArrayLng = CSmartArray<int64_t, TSize>;
template<size_t TSize> using CArrayLngPtr = CSmartPtr<CArrayLng<TSize>>;
template<size_t TSize> using CArrayDate = CSmartArray<CDate, TSize>;
template<size_t TSize> using CArrayDatePtr = CSmartPtr<CArrayDate<TSize>>;


template<size_t TSize> using const_ArrayDblItr = const_ArrayItr<CArrayDbl< TSize >> ;
template<size_t TSize> using const_ArrayFltItr = const_ArrayItr<CArrayFlt< TSize >>;
template<size_t TSize> using const_ArrayIntItr = const_ArrayItr<CArrayInt< TSize >>;
template<size_t TSize> using const_ArrayLngItr = const_ArrayItr<CArrayLng< TSize >>;
template<size_t TSize> using const_ArrayDateItr = const_ArrayItr<CArrayDate< TSize >>;


template<size_t TSize> using ArrayDblItr = ArrayItr<CArrayDbl< TSize >>;
template<size_t TSize> using ArrayFltItr = ArrayItr<CArrayFlt< TSize >>;
template<size_t TSize> using ArrayIntItr = ArrayItr<CArrayInt< TSize >>;
template<size_t TSize> using ArrayLngItr = ArrayItr<CArrayLng< TSize >>;
template<size_t TSize> using ArrayDateItr = ArrayItr<CArrayDate< TSize >>;
#endif
