#ifndef INCLUDE_H_SMARTMAPTESTS
#define INCLUDE_H_SMARTMAPTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/SmartMap.h"

class MapTests : public testing::Test
{
public:
	enum :size_t { START = 0, SIZE = 20 };

	template<typename T> void Add(T& inp_cMap)
	{
		typedef typename T::key_type key_type;
		typedef typename T::mapped_type mapped_type;

		for (size_t i(START); i < SIZE; i++)
		{
			inp_cMap.add(static_cast<key_type>(i), static_cast<mapped_type>(i * 10));
		}
	}
};

TEST_F(MapTests, SimpleMap)
{
	typedef CSimpleMap<int32_t, double> test_type;
	test_type cBase;
	EXPECT_EQ(0, cBase.size());

	Add(cBase);
	EXPECT_EQ(SIZE, cBase.size());
	EXPECT_EQ(100.0l, cBase[10]);

	auto res1(cBase.insert(test_type::value_type(10, 20.0l)));
	EXPECT_FALSE(res1.second);
	res1.first->second = 20.0l;
	EXPECT_EQ(SIZE, cBase.size());
	EXPECT_EQ(20.0l, cBase[10]);

	auto res2(cBase.insert(test_type::value_type(99, 20.0l)));
	EXPECT_TRUE(res2.second);
	EXPECT_EQ(SIZE + 1, cBase.size());
	EXPECT_EQ(20.0l, cBase[99]);

	EXPECT_TRUE(cBase.insert(test_type::value_type(98, 0.0l)).second);
	EXPECT_EQ(SIZE + 2, cBase.size());
	EXPECT_EQ(0.0l, cBase[98]);

	EXPECT_FALSE(cBase.insert(test_type::value_type(99, 0.0l)).second);
	EXPECT_EQ(SIZE + 2, cBase.size());
	EXPECT_EQ(20.0l, cBase[99]);

	EXPECT_EQ(1, cBase.erase(98));
	EXPECT_EQ(1, cBase.erase(99));
	EXPECT_EQ(SIZE, cBase.size());

	test_type cTest1;
	EXPECT_TRUE(cTest1.empty());
	EXPECT_EQ(0, cTest1.size());

	cTest1 = cBase;
	EXPECT_FALSE(cTest1.empty());
	EXPECT_EQ(cTest1.size(), cBase.size());

	for (int32_t iEntry(START); iEntry < cBase.size(); iEntry++)
	{
		EXPECT_EQ(cTest1[iEntry], cBase[iEntry]);
	}

	test_type cTest2(cBase);
	EXPECT_FALSE(cTest2.empty());
	EXPECT_EQ(cTest2.size(), cBase.size());

	for (int32_t iEntry(START); iEntry < cBase.size(); iEntry++)
	{
		EXPECT_EQ(cTest2[iEntry], cBase[iEntry]);
	}
}

TEST_F(MapTests, foreach)
{
	typedef CSmartMap<int, double> level3;
	typedef CSmartMap<int, level3> level2;
	typedef CSmartMap<int, level2> level1;

	level1 cBase;
	cBase.add(1, level2());
	cBase[1].add(10, level3());
	cBase[1][10].add(100, 1);
	cBase[1][10].add(200, 2);
	cBase[1].add(20, level3());
	cBase[1][20].add(100, 1);
	cBase[1][20].add(200, 2);
	cBase.add(2, level2());
	cBase[2].add(10, level3());
	cBase[2][10].add(100, 1);
	cBase[2][10].add(200, 2);
	cBase[2].add(20, level3());
	cBase[2][20].add(100, 1);
	cBase[2][20].add(200, 2);

	int iInst1(1), iInst2(10), iInst3(1000);
	auto foo3 = [&iInst3](level3::reference l3pair)
	{
		EXPECT_EQ(iInst3, l3pair.first);

		iInst3 += 100;
	};
	auto foo2 = [&iInst2, &iInst3, &foo3](level2::reference l2pair)
	{
		EXPECT_EQ(iInst2, l2pair.first);
		iInst3 = 100;
		l2pair.second.foreach(foo3);
		iInst2 += 10;
	};
	auto foo1 = [&iInst1, &iInst2, &foo2](level1::reference l1pair)
	{
		EXPECT_EQ(iInst1, l1pair.first);
		iInst2 = 10;
		l1pair.second.foreach(foo2);

		iInst1++;
	};

	cBase.foreach(foo1);
}

TEST_F(MapTests, Iterator)
{
	typedef CSimpleMap<int32_t, double> test_type;

	CSmartPtr<test_type> cBasePtr(CreateSmart<test_type>());
	Add(*cBasePtr);

	auto itrBase(iterators::GetItr(cBasePtr));

	itrBase.begin();
	EXPECT_EQ(0, itrBase.key());
	EXPECT_EQ(0.0l, itrBase.value());
	EXPECT_EQ(0, itrBase.pair().first);
	EXPECT_EQ(0.0l, itrBase.pair().second);
	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase++;
	EXPECT_EQ(10.0l, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase.last();
	EXPECT_EQ(190.0l, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase++;
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_TRUE(itrBase.isEnd());

	itrBase--;
	EXPECT_EQ(190.0l, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	for (itrBase.last(); !itrBase.isBegin(); itrBase--);
	EXPECT_EQ(0.0l, itrBase.value());

	EXPECT_TRUE(itrBase.find(10));
	EXPECT_EQ(100.0l, itrBase.value());

	EXPECT_FALSE(itrBase.find(20));
	EXPECT_TRUE(itrBase.isEnd());

	EXPECT_TRUE(itrBase.lower_bound(15));
	EXPECT_EQ(15, itrBase.key());

	EXPECT_TRUE(itrBase.lower_bound(-1));
	EXPECT_EQ(0, itrBase.key());

	EXPECT_FALSE(itrBase.lower_bound(20));

	EXPECT_TRUE(itrBase.upper_bound(15));
	EXPECT_EQ(16, itrBase.key());

	EXPECT_TRUE(itrBase.upper_bound(-1));
	EXPECT_EQ(0, itrBase.key());

	EXPECT_FALSE(itrBase.upper_bound(20));
};

TEST_F(MapTests, MultiMap)
{
	typedef CSimpleMultiMap<int32_t, double> test_type;
	
	CSmartPtr<test_type> cBasePtr(CreateSmart<test_type>());
	auto itrBase(iterators::GetItr(cBasePtr));

	EXPECT_EQ(0, cBasePtr->size());
	EXPECT_TRUE(cBasePtr->empty());
	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_TRUE(itrBase.isEnd());

	const int32_t	iIndxStart(0);
	const double	dValueStart(static_cast<double>(iIndxStart));

	const int32_t	iIndxMax(21);
	const double	dValueInc(1.0l);

	const int32_t	iRepStart(0);
	const int32_t	iRepCnt(2);

	double			dValue(dValueStart);
	int32_t			iEntryCnt(0);

	int32_t			iRepIndx;
	int32_t			iIndx;

	cBasePtr->add(iIndxStart, dValue);	iEntryCnt++;						// Add one initial element - only add one so we can test for BEGIN

	for (iRepIndx = iRepStart; iRepIndx < iRepCnt; iRepIndx++)
	{
		for (iIndx = iIndxStart + 1; iIndx < iIndxMax; iIndx++)
		{
			cBasePtr->add(iIndx, (dValue += dValueInc)); iEntryCnt++;
		}
	}
	cBasePtr->add(iIndxMax, (dValue += dValueInc)); iEntryCnt++;	// Add one last element - only add one so we can test for END 

	// Test initial load conditions and size
	EXPECT_EQ(iEntryCnt, cBasePtr->size());

	// Test Extraction, reset to first element added
	itrBase.begin();
	iIndx = iIndxStart;
	dValue = dValueStart;

	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());
	EXPECT_EQ(dValue, itrBase.value());

	for (itrBase++, iIndx++, dValue += dValueInc; iIndx < iIndxMax; iIndx++, dValue += dValueInc)
	{
		for (iRepIndx = iRepStart; iRepIndx < iRepCnt; iRepIndx++, itrBase++)
		{
			EXPECT_FALSE(itrBase.isBegin());
			EXPECT_FALSE(itrBase.isEnd());
			EXPECT_EQ(dValue + dValueInc * (iRepIndx * (iIndxMax - 1)), itrBase.value());
		}
	}

	// Test for Last element
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());
	EXPECT_EQ(dValue + dValueInc * (iRepIndx - 1) * (iIndxMax - 1), itrBase.value());
	itrBase++;
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_TRUE(itrBase.isEnd());

	//Test MultiMap copy
	CSmartPtr<test_type> cTargetPtr(CreateSmart<test_type>());
	auto itrTarget(iterators::GetItr(cTargetPtr));

	EXPECT_TRUE(cTargetPtr->empty());
	EXPECT_EQ(0, cTargetPtr->size());

	*cTargetPtr = *cBasePtr;

	EXPECT_FALSE(cTargetPtr->empty());
	EXPECT_EQ(cTargetPtr->size(), cBasePtr->size());

	itrBase.begin();
	itrTarget.begin();
	int32_t iEntry(0);
	dValue = dValueStart;
	test_type::size_type iStopAt(cBasePtr->size() / 2);
	while (iEntry <= iStopAt)
	{
		if (iEntry == iStopAt){ dValue = cBasePtr->size() - 1; }
		EXPECT_EQ(dValue, (*cTargetPtr)[iEntry]);
		EXPECT_EQ(dValue, (*cBasePtr)[iEntry]);
		EXPECT_EQ((*cTargetPtr)[iEntry], (*cBasePtr)[iEntry]);
		dValue += dValueInc;
		iEntry++;
	}

	dValue = dValueStart;
	EXPECT_EQ(dValue, itrBase.begin().value());
	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase++;
	dValue += dValueInc;
	EXPECT_EQ(dValue, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	double rLastValue(0);

	for (itrBase.last(); !itrBase.isBegin(); itrBase--);// rLastValue = cBase.GetPrevious());
	EXPECT_EQ(dValueStart, itrBase.value());

	EXPECT_TRUE(itrBase.find(10));
	EXPECT_EQ(10.0l, itrBase.value());
	itrBase++;
	EXPECT_EQ(30.0l, itrBase.value());			// When Repeat Count is 2 or greater

	EXPECT_TRUE(itrBase.lower_bound(15));
	EXPECT_EQ(15, itrBase.key());

	EXPECT_EQ(14, (--itrBase).key());
	EXPECT_TRUE(itrBase.find(10));
	EXPECT_EQ(10.0l, itrBase.key());
	EXPECT_EQ(29.0l, (--itrBase).value());
	itrBase++;
	EXPECT_EQ(10, itrBase.key());
	itrBase++;
	EXPECT_EQ(30.0l, itrBase.value());
	itrBase++;
	EXPECT_EQ(11.0l, itrBase.value());
	EXPECT_EQ(31.0l, (++itrBase).value());
	EXPECT_EQ(12.0l, (++itrBase).value());

	EXPECT_TRUE(itrBase.find(15));
	EXPECT_EQ(15.0l, itrBase.value());

	EXPECT_EQ(cBasePtr->size() - 1, itrBase.last().value());
	EXPECT_EQ(dValueStart, itrBase.begin().value());
	EXPECT_EQ(cBasePtr->size() / 2, itrBase.last().key());
	EXPECT_EQ(iIndxStart, itrBase.begin().key());

	cTargetPtr->clear();
	EXPECT_TRUE(cTargetPtr->empty());
	EXPECT_EQ(0, cTargetPtr->size());

	test_type::size_type iIteration(0);
	test_type::size_type iEnd(cBasePtr->size());
	for (itrBase.begin(); !itrBase.isEnd() && (iIteration <= iEnd); itrBase++)
	{
		rLastValue = itrBase.value();
	}
	EXPECT_EQ(cBasePtr->size() - 1, rLastValue);
};

TEST_F(MapTests, SPUD)
{
	typedef CSmartMap<int32_t, double> test_type;

	auto itrBase(iterators::GetItr(CreateSmart<test_type>())), itrTest1(iterators::GetItr(CreateSmart<test_type>())), itrTest2(iterators::GetItr(CreateSmart<test_type>()));

	Add(*itrBase.data());

	EXPECT_TRUE(CopyViaPack(*itrBase.data(), *itrTest1.data()));
	EXPECT_EQ(SIZE, itrTest1.data()->size());
	EXPECT_EQ(SIZE, itrBase.data()->size());

	test_type::key_type iIndx;
	for (iIndx = START, itrBase.begin(), itrTest1.begin(); !itrBase.isEnd(); iIndx++, itrBase++, itrTest1++)
	{
		EXPECT_EQ(iIndx, itrBase.key());
		EXPECT_EQ(iIndx, itrTest1.key());

		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrBase.value());
		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrTest1.value());
	}

	EXPECT_TRUE(CopyViaSerial(*itrBase.data(), *itrTest2.data()));
	EXPECT_EQ(SIZE, itrTest2.data()->size());
	EXPECT_EQ(SIZE, itrBase.data()->size());

	for (iIndx = START, itrBase.begin(), itrTest2.begin(); !itrBase.isEnd(); iIndx++, itrBase++, itrTest2++)
	{
		EXPECT_EQ(iIndx, itrBase.key());
		EXPECT_EQ(iIndx, itrTest2.key());

		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrBase.value());
		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrTest2.value());
	}
}
#endif
