#ifndef INCLUDE_H_BINOMIALPROBABILITY
#define INCLUDE_H_BINOMIALPROBABILITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/SmartVector.h"
#include "probability/ProbTools.h"

class CBinomial
{
public:
	CBinomial(const CVectorDbl::size_type& inp_iTrials, const double& inp_dProb) :miTrials(inp_iTrials), mdProb(inp_dProb),
		mcCdf(inp_iTrials + 1), mcPdf(inp_iTrials + 1)
	{
		const double dOdds(isValidProbability(mdProb) ? mdProb / (1.0 - mdProb) : 0.0);

		mcCdf[0] = mcPdf[0] = pow(1.0 - mdProb, miTrials);
		for (CVectorDbl::size_type iIndx(1); iIndx <= miTrials; iIndx++)
		{
			mcPdf[iIndx] = mcPdf[iIndx - 1] * dOdds * (miTrials - iIndx + 1) / static_cast<double>(iIndx);
			mcCdf[iIndx] = mcCdf[iIndx - 1] + mcPdf[iIndx];
		}
	}

	double CDF(CVectorDbl::size_type inp_iValue)
	{
		if (inp_iValue >= miTrials) { return 1.0; }
		return mcCdf[inp_iValue];
	}

	CVectorDbl::size_type InverseCDF(double inp_dProbability)
	{
		if (inp_dProbability < 0.0) { return 0; }
		else if (dEPSILON > 1.0 - inp_dProbability) { return miTrials; }
		else
		{
			for (CVectorDbl::size_type iIndx(0); iIndx < miTrials; iIndx++)
			{
				if (mcCdf[iIndx] >= inp_dProbability) return iIndx;
			}
			return miTrials;
		}
	}

	double PDF(CVectorDbl::size_type inp_iValue)
	{
		if (inp_iValue > miTrials) { return 0.0; }
		return mcPdf[inp_iValue];
	}

protected:
	const double				mdProb;
	const CVectorDbl::size_type	miTrials;

	CVectorDbl	mcCdf;
	CVectorDbl	mcPdf;
};
#endif
