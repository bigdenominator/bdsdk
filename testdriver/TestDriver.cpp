// TestDriver.cpp : Defines the entry point for the console application.
// Updated Aug 18 2015

#include <iostream>
#include <thread>
#include <atomic>
#include "CommonDefs.h"


void EXPECT_TRUE(bool inp_bTest) { std::cout << (inp_bTest ? "OK" : "Fail") << endl; }

void EXPECT_FALSE(bool inp_bTest) { EXPECT_TRUE(!inp_bTest); }

template<typename T, typename U> void EXPECT_EQ(T lhs, U rhs)
{
	EXPECT_TRUE(lhs == rhs);
	//std::cout << lhs << " v " << rhs << endl; 
}

template<typename T> void EXPECT_GT(T lhs, T rhs) { std::cout << (lhs > rhs ? "OK" : "Fail") << endl; }

int main(int argc, char* argv[])
{
	return 0;
}
	
