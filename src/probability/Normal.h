#ifndef INCLUDE_H_NORMALPROBABILITY
#define INCLUDE_H_NORMALPROBABILITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

class CNormal
{
public:
	UNARY_HLPR(cdf_hlpr, cdf)
	UNARY_HLPR(inverseCdf_hlpr, inverseCdf)

	CNormal(const double& inp_dLocation, const double& inp_dScale)
		:mdInvScale(1.0 / inp_dScale), mdLocation(inp_dLocation), mdScale(inp_dScale) {}

	inline double CDF(const double& inp_dValue)
	{
		return StandardCDF(center(inp_dValue));
	}
	inline double InverseCDF(const double& inp_dProbability)
	{
		return mdLocation + mdScale * StandardInverseCDF(inp_dProbability);
	}
	inline double PDF(const double& inp_dValue)
	{
		return StandardPDF(center(inp_dValue)) * mdInvScale;
	}

	template<typename T> inline auto CDF(const T& inp_cValues) -> decltype(StandardCDF(center(inp_cValues)))
	{
		return StandardCDF(center(inp_cValues));
	}
	template<typename T> inline auto InverseCDF(const T& inp_cProbabilities) -> decltype((StandardInverseCDF(inp_cProbabilities) * (double)1) + (double)0) // hard coded types replace member-variable usage per GCC
	{
		return (StandardInverseCDF(inp_cProbabilities) * mdScale) + mdLocation;
	}
	template<typename T> inline auto PDF(const T& inp_cValues)-> decltype(StandardPDF(center(inp_cValues)) * (double)1) // hard coded types replace member-variable usage per GCC
	{
		return StandardPDF(center(inp_cValues)) * mdInvScale;
	}

	static inline double StandardCDF(const double& inp_dValue) { return cdf(inp_dValue); }
	static inline double StandardInverseCDF(const double& inp_dProbability) { return inverseCdf(inp_dProbability); }

	UNARY_EXPR(StandardCDF, cdf_hlpr)
	UNARY_EXPR(StandardInverseCDF, inverseCdf_hlpr)

	template<typename T> static inline auto StandardPDF(const T& inp_cValues) -> decltype(std::exp(-0.5 * inp_cValues * inp_cValues) * d_SQRT2PI)
	{
		return std::exp(-0.5 * inp_cValues * inp_cValues) * d_SQRT2PI;
	}

protected:
	const double mdInvScale;
	const double mdLocation;
	const double mdScale;

	template<typename T> inline auto center(const T& inp_cValues) -> decltype((inp_cValues - mdLocation) * mdInvScale)
	{
		return (inp_cValues - mdLocation) * mdInvScale;
	}

	static inline double cdf(double inp_dValue)
	{
		// Zelen and Severo
		double cof[6] =
		{
			1.330274429,
			-1.821255978,
			1.781477937,
			-0.356563782,
			0.319381530,
			0.0
		};

		double dTemp(1.0 / (1.0 + 0.231641900 * fabs(inp_dValue)));
		double dApprox(CMath::polynomial(dTemp, cof, 6) * StandardPDF(inp_dValue));

		if (inp_dValue >= 0.0) 	return 1.0 - dApprox;

		return dApprox;
	}

	static inline double inverseCdf(double inp_dProbability)
	{
		// see http://home.online.no/~pjacklam/notes/invnorm/impl/misra/normsinv.html
		double PX1[6] =
		{
			-3.969683028665376e+01,
			2.209460984245205e+02,
			-2.759285104469687e+02,
			1.383577518672690e+02,
			-3.066479806614716e+01,
			2.506628277459239e+00
		};

		double QX1[6] =
		{
			-5.447609879822406e+01,
			1.615858368580409e+02,
			-1.556989798598866e+02,
			6.680131188771972e+01,
			-1.328068155288572e+01,
			1.0
		};

		double PX2[6] =
		{
			-7.784894002430293e-03,
			-3.223964580411365e-01,
			-2.400758277161838e+00,
			-2.549732539343734e+00,
			4.374664141464968e+00,
			2.938163982698783e+00
		};

		double QX2[5] =
		{
			7.784695709041462e-03,
			3.224671290700398e-01,
			2.445134137142996e+00,
			3.754408661907416e+00,
			1.0
		};

		double dProbTransform;
		double dPx, dQx;

		if (inp_dProbability <= 0.0){ return -dINFINITY; }
		else if (inp_dProbability >= 1.0){ return dINFINITY; }
		else
		{
			if (inp_dProbability < 0.02425)
			{
				dProbTransform = sqrt(-2.0 * CMath::ln(inp_dProbability));
				dPx = CMath::polynomial(dProbTransform, PX2, 6);
				dQx = CMath::polynomial(dProbTransform, QX2, 5);
			}
			else if (inp_dProbability > 0.97575)
			{
				dProbTransform = sqrt(-2.0 * CMath::ln(1.0 - inp_dProbability));
				dPx = -CMath::polynomial(dProbTransform, PX2, 6);
				dQx = CMath::polynomial(dProbTransform, QX2, 5);
			}
			else
			{
				dProbTransform = inp_dProbability - 0.5;
				dProbTransform *= dProbTransform;
				dPx = CMath::polynomial(dProbTransform, PX1, 6) * (inp_dProbability - 0.5);
				dQx = CMath::polynomial(dProbTransform, QX1, 6);
			}
			return dPx / dQx;
		}
	}
};
#endif
