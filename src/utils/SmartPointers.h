#ifndef INCLUDE_H_SMARTPOINTERS
#define INCLUDE_H_SMARTPOINTERS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

template<typename T> using CSmartPtr = std::shared_ptr<T>;
template<typename T> using CUniquePtr = std::unique_ptr<T>;

template<class T, class ...Args> static inline CSmartPtr<T> CreateSmart(Args&&... myArgs)
{	
	return std::make_shared<T>(std::forward<Args>(myArgs)...);
}

template<class T, class ...Args> static inline CUniquePtr<T> CreateUnique(Args&&... myArgs)
{
	return unique_ptr<T>(new T(std::forward<Args>(myArgs)...));
}
#endif