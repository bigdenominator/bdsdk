#ifndef INCLUDE_H_PERFTOOLS
#define INCLUDE_H_PERFTOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#ifdef WINDOWS
#include <intrin.h>
#else     //defined(UNIX)
#include <limits>
#include <x86intrin.h>
//#include zmmintrin.h       // This is for the Phi, not sure if included in x86intrin or not as Phi is not really an x86 CPU, it's x86 like
#include <cpuid.h>
#endif 

#include <time.h>
#include <vector>
#include <bitset>
#include <array>
#include <string>

static const int32_t   TIMEFIELDLENGTH(26);
static const int32_t   TIMEYEAROFFSET(19);
static const int32_t   TIMEYEARSIZE(5);
static const int32_t   TIMEOFDAYOFFSET(10);
static const int32_t   TIMEDAYMOSIZE(10);
static const int32_t   TIMEOFDAYSIZE(9);

static inline  uint64_t ClockCycleCnt(void)
{
	return __rdtsc();
}

template<typename TData> static inline void CurrentTimeCnt(TData& inp_cTime, int32_t inp_iDivisor = 1)
{
	time_t	cCurrentSeconds(time(nullptr));
	inp_cTime = static_cast<TData>(cCurrentSeconds);
}

template<> inline void CurrentTimeCnt(string& inp_cTime, int32_t inp_iDivisor)
{
	time_t	cCurrentSeconds(time(nullptr));
	inp_cTime = to_string(static_cast<uint64_t>(cCurrentSeconds/inp_iDivisor));
}

static void TimeStamp(string& inp_sTimeStamp)
{
	char		cBuf[TIMEFIELDLENGTH];
	char		cTimeBuf[TIMEFIELDLENGTH];
	time_t		cCurrentTime;
	string		sErrorLog;
	string		sLocalLine;

	memset(&cBuf, 0x00, TIMEFIELDLENGTH);
	memset(&cTimeBuf, 0x00, TIMEFIELDLENGTH);
	CurrentTimeCnt<time_t>(cCurrentTime);
#ifdef WINDOWS
	ctime_s(cBuf, sizeof(cBuf), &cCurrentTime);
	strncpy_s(cTimeBuf, TIMEFIELDLENGTH, cBuf, TIMEDAYMOSIZE);
	strncat_s(cTimeBuf, TIMEFIELDLENGTH, cBuf + TIMEYEAROFFSET, TIMEYEARSIZE);
	strncat_s(cTimeBuf, TIMEFIELDLENGTH, cBuf + TIMEOFDAYOFFSET, TIMEOFDAYSIZE);
	inp_sTimeStamp = cTimeBuf;
#else
	struct tm* timeinfo = localtime(&cCurrentTime);
	strftime(cBuf, sizeof(cBuf), "%a %b %d %Y %T", timeinfo);
	inp_sTimeStamp = cBuf;
#endif
}

static string TimeStamp(void)
{
	string sTemp(sNULL);
	TimeStamp(sTemp);

	return sTemp;
}

// ProcessorArch.cpp 
// Uses the __cpuid to get information about CPU extended instruction set support.

#ifdef WINDOWS
class CProcessorArch
{
public:
	CProcessorArch(void) {}

	~CProcessorArch() {}

	string Vendor(void) { return this->CPU_Rep.sVendor; }
	string Brand(void) { return this->CPU_Rep.sBrand; }

	bool SSE3(void) { return this->CPU_Rep.f_1_ECX_[0]; }
	bool PCLMULQDQ(void) { return this->CPU_Rep.f_1_ECX_[1]; }
	bool MONITOR(void) { return this->CPU_Rep.f_1_ECX_[3]; }
	bool SSSE3(void) { return this->CPU_Rep.f_1_ECX_[9]; }
	bool FMA(void) { return this->CPU_Rep.f_1_ECX_[12]; }
	bool CMPXCHG16B(void) { return this->CPU_Rep.f_1_ECX_[13]; }
	bool SSE41(void) { return this->CPU_Rep.f_1_ECX_[19]; }
	bool SSE42(void) { return this->CPU_Rep.f_1_ECX_[20]; }
	bool MOVBE(void) { return this->CPU_Rep.f_1_ECX_[22]; }
	bool POPCNT(void) { return this->CPU_Rep.f_1_ECX_[23]; }
	bool AES(void) { return this->CPU_Rep.f_1_ECX_[25]; }
	bool XSAVE(void) { return this->CPU_Rep.f_1_ECX_[26]; }
	bool OSXSAVE(void) { return this->CPU_Rep.f_1_ECX_[27]; }
	bool AVX(void) { return this->CPU_Rep.f_1_ECX_[28]; }
	bool F16C(void) { return this->CPU_Rep.f_1_ECX_[29]; }
	bool RDRAND(void) { return this->CPU_Rep.f_1_ECX_[30]; }

	bool MSR(void) { return this->CPU_Rep.f_1_EDX_[5]; }
	bool CX8(void) { return this->CPU_Rep.f_1_EDX_[8]; }
	bool SEP(void) { return this->CPU_Rep.f_1_EDX_[11]; }
	bool CMOV(void) { return this->CPU_Rep.f_1_EDX_[15]; }
	bool CLFSH(void) { return this->CPU_Rep.f_1_EDX_[19]; }
	bool MMX(void) { return this->CPU_Rep.f_1_EDX_[23]; }
	bool FXSR(void) { return this->CPU_Rep.f_1_EDX_[24]; }
	bool SSE(void) { return this->CPU_Rep.f_1_EDX_[25]; }
	bool SSE2(void) { return this->CPU_Rep.f_1_EDX_[26]; }

	bool FSGSBASE(void) { return this->CPU_Rep.f_7_EBX_[0]; }
	bool BMI1(void) { return this->CPU_Rep.f_7_EBX_[3]; }
	bool HLE(void) { return this->CPU_Rep.bIsIntel && CPU_Rep.f_7_EBX_[4]; }
	bool AVX2(void) { return this->CPU_Rep.f_7_EBX_[5]; }
	bool BMI2(void) { return this->CPU_Rep.f_7_EBX_[8]; }
	bool ERMS(void) { return this->CPU_Rep.f_7_EBX_[9]; }
	bool INVPCID(void) { return this->CPU_Rep.f_7_EBX_[10]; }
	bool RTM(void) { return this->CPU_Rep.bIsIntel && CPU_Rep.f_7_EBX_[11]; }
	bool AVX512F(void) { return this->CPU_Rep.f_7_EBX_[16]; }
	bool RDSEED(void) { return this->CPU_Rep.f_7_EBX_[18]; }
	bool ADX(void) { return this->CPU_Rep.f_7_EBX_[19]; }
	bool AVX512PF(void) { return this->CPU_Rep.f_7_EBX_[26]; }
	bool AVX512ER(void) { return this->CPU_Rep.f_7_EBX_[27]; }
	bool AVX512CD(void) { return this->CPU_Rep.f_7_EBX_[28]; }
	bool SHA(void) { return this->CPU_Rep.f_7_EBX_[29]; }

	bool PREFETCHWT1(void) { return this->CPU_Rep.f_7_ECX_[0]; }

	bool LAHF(void) { return this->CPU_Rep.f_81_ECX_[0]; }
	bool LZCNT(void) { return this->CPU_Rep.bIsIntel && CPU_Rep.f_81_ECX_[5]; }
	bool ABM(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_ECX_[5]; }
	bool SSE4a(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_ECX_[6]; }
	bool XOP(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_ECX_[11]; }
	bool TBM(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_ECX_[21]; }

	bool SYSCALL(void) { return this->CPU_Rep.bIsIntel && CPU_Rep.f_81_EDX_[11]; }
	bool MMXEXT(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_EDX_[22]; }
	bool RDTSCP(void) { return this->CPU_Rep.bIsIntel && CPU_Rep.f_81_EDX_[27]; }
	bool _3DNOWEXT(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_EDX_[30]; }
	bool _3DNOW(void) { return this->CPU_Rep.bIsAMD && CPU_Rep.f_81_EDX_[31]; }

private:
	class CInstructionSet_Internal
	{
	public:
		int32_t         iIDs;
		int32_t         iExIDs;
		string			sVendor;
		string			sBrand;
		bool			bIsIntel;
		bool			bIsAMD;
		std::bitset<32> f_1_ECX_;
		std::bitset<32> f_1_EDX_;
		std::bitset<32> f_7_EBX_;
		std::bitset<32> f_7_ECX_;
		std::bitset<32> f_81_ECX_;
		std::bitset<32> f_81_EDX_;
		std::vector<std::array<int32_t, 4>> iData;
		std::vector<std::array<int32_t, 4>> iExtData;

		CInstructionSet_Internal(void) : iIDs{ 0 }, iExIDs{ 0 }, bIsIntel{ false }, bIsAMD{ false }, f_1_ECX_{ 0 }, f_1_EDX_{ 0 }, f_7_EBX_{ 0 },
			f_7_ECX_{ 0 }, f_81_ECX_{ 0 }, f_81_EDX_{ 0 }, iData{}, iExtData{}
		{
			std::array<int32_t, 4> iCPUType;

			// Calling __cpuid with 0x0 as the function_id argument
			// gets the number of the highest valid function ID.
			__cpuid(iCPUType.data(), 0);
			iIDs = iCPUType[0];

			for (int iIndx(0); iIndx <= iIDs; iIndx++)
			{
				__cpuidex(iCPUType.data(), iIndx, 0);
				iData.push_back(iCPUType);
			}

			// Capture vendor string
			char cVendor[0x20];
			memset(cVendor, 0, sizeof(cVendor));
			*reinterpret_cast<int32_t*>(cVendor) = iData[0][1];
			*reinterpret_cast<int32_t*>(cVendor + 4) = iData[0][3];
			*reinterpret_cast<int32_t*>(cVendor + 8) = iData[0][2];
			sVendor = cVendor;
			if (sVendor == "GenuineIntel")
			{
				bIsIntel = true;
			}
			else if (sVendor == "AuthenticAMD")
			{
				bIsAMD = true;
			}

			// load bitset with flags for function 0x00000001
			if (iIDs >= 1)
			{
				f_1_ECX_ = iData[1][2];
				f_1_EDX_ = iData[1][3];
			}

			// load bitset with flags for function 0x00000007
			if (iIDs >= 7)
			{
				f_7_EBX_ = iData[7][1];
				f_7_ECX_ = iData[7][2];
			}

			// Calling __cpuid with 0x80000000 as the function_id argument
			// gets the number of the highest valid extended ID.
			__cpuid(iCPUType.data(), 0x80000000);
			iExIDs = iCPUType[0];

			char cBrand[0x40];
			memset(cBrand, 0, sizeof(cBrand));

			for (int32_t i = 0x80000000; i <= iExIDs; ++i)
			{
				__cpuidex(iCPUType.data(), i, 0);
				iExtData.push_back(iCPUType);
			}

			// load bitset with flags for function 0x80000001
			if (iExIDs >= 0x80000001)
			{
				f_81_ECX_ = iExtData[1][2];
				f_81_EDX_ = iExtData[1][3];
			}

			// Interpret CPU cBrand string if reported
			if (iExIDs >= 0x80000004)
			{
				memcpy(cBrand, iExtData[2].data(), sizeof(iCPUType));
				memcpy(cBrand + 16, iExtData[3].data(), sizeof(iCPUType));
				memcpy(cBrand + 32, iExtData[4].data(), sizeof(iCPUType));
				sBrand = cBrand;
			}
		};
	};

	CInstructionSet_Internal CPU_Rep;
};
#else
class CProcessorArch
{
public:
	CProcessorArch() {}

	~CProcessorArch() {}

	string Vendor(void) { return string("None"); }
	string Brand(void) { return  string("None"); }

	bool SSE3(void) {return false; }
	bool PCLMULQDQ(void) { return false; }
	bool MONITOR(void) { return false; }
	bool SSSE3(void) { return false; }
	bool FMA(void) { return false; }
	bool CMPXCHG16B(void) { return false; }
	bool SSE41(void) { return false; }
	bool SSE42(void) { return false; }
	bool MOVBE(void) { return false; }
	bool POPCNT(void) { return false; }
	bool AES(void) { return false; }
	bool XSAVE(void) { return false; }
	bool OSXSAVE(void) { return false; }
	bool AVX(void) { return false; }
	bool F16C(void) { return false; }
	bool RDRAND(void) { return false; }

	bool MSR(void) { return false; }
	bool CX8(void) { return false; }
	bool SEP(void) { return false; }
	bool CMOV(void) { return false; }
	bool CLFSH(void) { return false; }
	bool MMX(void) { return false; }
	bool FXSR(void) { return false; }
	bool SSE(void) { return false; }
	bool SSE2(void) { return false; }

	bool FSGSBASE(void) { return false; }
	bool BMI1(void) { return false; }
	bool HLE(void) { return false; }
	bool AVX2(void) { return false; }
	bool BMI2(void) { return false; }
	bool ERMS(void) { return false; }
	bool INVPCID(void) { return false; }
	bool RTM(void) { return false; }
	bool AVX512F(void) { return false; }
	bool RDSEED(void) { return false; }
	bool ADX(void) { return false; }
	bool AVX512PF(void) { return false; }
	bool AVX512ER(void) { return false; }
	bool AVX512CD(void) { return false; }
	bool SHA(void) { return false; }

	bool PREFETCHWT1(void) { return false; }

	bool LAHF(void) { return false; }
	bool LZCNT(void) { return false; }
	bool ABM(void) { return false; }
	bool SSE4a(void) { return false; }
	bool XOP(void) { return false; }
	bool TBM(void) { return false; }

	bool SYSCALL(void) { return false; }
	bool MMXEXT(void) { return false; }
	bool RDTSCP(void) { return false; }
	bool _3DNOWEXT(void) { return false; }
	bool _3DNOW(void) { return false; }
};
#endif
#endif
