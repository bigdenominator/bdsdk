#ifndef INCLUDE_H_ALLCONTAINERS
#define INCLUDE_H_ALLCONTAINERS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/ArrayBased.h"
#include "containers/DateMath.h"
#include "containers/Expressions.h"
#include "containers/IContainer.h"
#include "containers/ItrBundle.h"
#include "containers/Schematic.h"
#include "containers/SIMD.h"
#include "containers/SimpleMath.h"
#include "containers/SmartArray.h"
#include "containers/SmartContainers.h"
#include "containers/SmartDate.h"
#include "containers/SmartMap.h"
#include "containers/SmartMatrix.h"
#include "containers/SmartSet.h"
#include "containers/SmartVector.h"
#include "containers/Tuple.h"

#endif