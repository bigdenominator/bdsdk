#ifndef INCLUDE_H_SMARTVECTOR
#define INCLUDE_H_SMARTVECTOR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/ArrayBased.h"

template<typename TData> class CSmartVector : public CArrayBased<TData>
{
public:
	typedef CArrayBased<TData>	_Mybase;
	typedef CSmartVector<TData>	_Myt;

	typedef typename _Mybase::size_type			size_type;
	typedef typename _Mybase::value_type		value_type;
	typedef typename _Mybase::difference_type	difference_type;
	typedef typename _Mybase::reference			reference;
	typedef typename _Mybase::const_reference	const_reference;
	typedef typename _Mybase::pointer			pointer;
	typedef typename _Mybase::const_pointer 	const_pointer;
	
	typedef typename _Mybase::expression_type	expression_type;
	typedef typename _Mybase::init_list		init_list;

	enum :size_type { MAX_SIZE = ((size_t)(-1) / sizeof(value_type)) };

	CSmartVector(void) :_Myt(0) {}

	CSmartVector(size_type inp_iSize) :_Mybase(), miLogicalSize(inp_iSize)
	{
		alloc(inp_iSize, this->mcRawPtr);
	}

	CSmartVector(size_type inp_iSize, const_reference inp_InitValue) : _Myt(inp_iSize)
	{
		this->load(ConstExpression<value_type>(inp_InitValue, this->size()));
	}

	CSmartVector(const init_list& inp_cInitList) :_Myt(inp_cInitList.size())
	{
		this->load(inp_cInitList);
	}

	CSmartVector(const _Myt& inp_cArrayBased) :_Myt(inp_cArrayBased.size())
	{
		this->load(static_cast<expression_type>(inp_cArrayBased));
	}

	CSmartVector(const _Mybase& inp_cArrayBased) :_Myt(inp_cArrayBased.size())
	{
		this->load(static_cast<expression_type>(inp_cArrayBased));
	}

	CSmartVector(_Myt&& inp_cArrayBased) :_Mybase()
	{
		this->mcRawPtr = std::move(inp_cArrayBased.mcRawPtr);
		this->miAllocatedSize = std::move(inp_cArrayBased.miAllocatedSize);
		miLogicalSize = inp_cArrayBased.size();

		inp_cArrayBased.mcRawPtr = nullptr;
		inp_cArrayBased.miAllocatedSize = 0;
	}

	CSmartVector(_Mybase&& inp_cArrayBased) :_Mybase()
	{
		this->mcRawPtr = std::move(inp_cArrayBased.mcRawPtr);
		this->miAllocatedSize = std::move(inp_cArrayBased.miAllocatedSize);
		miLogicalSize = inp_cArrayBased.size();

		inp_cArrayBased.mcRawPtr = nullptr;
		inp_cArrayBased.miAllocatedSize = 0;
	}

	template<typename T, typename U = typename _Mybase::template ifRhsExpr<T, T>::type> CSmartVector(const T& rhs) : _Myt(rhs.size())
	{
		typedef typename ExprType<T>::type rhs_type;
		this->load(static_cast<rhs_type>(rhs));
	}

	template<typename T> inline typename _Mybase::template ifRhsConst<T, _Myt>::type& operator =(const T& rhs)
	{
		this->load(ConstExpression<value_type>(rhs, this->size()));

		return *this;
	}

	inline _Myt& operator =(const init_list& rhs)
	{
		refresh(rhs.size());
		miLogicalSize = rhs.size();
		this->load(rhs);

		return *this;
	}

	inline _Myt& operator =(const _Myt& rhs)
	{
		refresh(rhs.size());
		miLogicalSize = rhs.size();
		this->load(static_cast<expression_type>(rhs));

		return *this;
	}

	inline _Myt& operator =(const _Mybase& rhs)
	{
		refresh(rhs.size());
		miLogicalSize = rhs.size();
		this->load(static_cast<expression_type>(rhs));

		return *this;
	}

	inline _Myt& operator =(_Myt&& rhs)
	{
		this->deallocate(this->miAllocatedSize, this->mcRawPtr);

		this->mcRawPtr = std::move(rhs.mcRawPtr);
		this->miAllocatedSize = std::move(rhs.miAllocatedSize);
		this->miLogicalSize = rhs.miLogicalSize;

		rhs.mcRawPtr = nullptr;
		rhs.miAllocatedSize = 0;

		return *this;
	}

	inline _Myt& operator =(_Mybase&& rhs)
	{
		this->deallocate(this->miAllocatedSize, this->mcRawPtr);

		this->mcRawPtr = std::move(rhs.mcRawPtr);
		this->miAllocatedSize = std::move(rhs.miAllocatedSize);
		miLogicalSize = rhs.size();

		rhs.mcRawPtr = nullptr;
		rhs.miAllocatedSize = 0;

		return *this;
	}

	template<typename T> inline typename _Mybase::template ifRhsExpr<T, _Myt>::type& operator =(const T& rhs)
	{
		typedef typename ExprType<T>::type rhs_type;

		refresh(rhs.size());
		miLogicalSize = rhs.size();
		this->load(static_cast<rhs_type>(rhs));

		return *this;
	}

	inline size_type capacity(void) const { return _Mybase::miAllocatedSize; }

	inline size_type max_size(void) const { return _Myt::MAX_SIZE; }

	inline void pop_back(void) { miLogicalSize--; }

	inline void push_back(const value_type& inp_Value)
	{
		reserve(miLogicalSize + 1);
		*(this->mcRawPtr + miLogicalSize++) = inp_Value;
	}

	inline void push_back(value_type&& inp_Value)
	{
		reserve(miLogicalSize + 1);
		*(this->mcRawPtr + miLogicalSize++) = std::move(inp_Value);
	}

	inline void reserve(size_type inp_iNewCapacity)
	{
		if (inp_iNewCapacity > capacity()) { realloc(inp_iNewCapacity); }
	}

	inline void resize(size_type inp_iNewSize)
	{
		reserve(inp_iNewSize);
		miLogicalSize = inp_iNewSize;
	}

	inline void shrink_to_fit(void)
	{
		if (calcCapacity(miLogicalSize) != capacity()) { realloc(miLogicalSize); }
	}

	inline size_type size(void) const { return miLogicalSize; }

protected:
	virtual bool deserialize(SSerial& inp_sSerial)
	{
		if (!SPUD::deserializeTools(inp_sSerial, miLogicalSize)) return false;

		refresh(miLogicalSize);

		return _Mybase::deserialize(inp_sSerial);
	}

	virtual void pack(SPack& inp_sPack) const
	{
		SPUD::packTools(inp_sPack, size());
		_Mybase::pack(inp_sPack);
	}

	virtual SPack::size_type packSize(void) const
	{
		return SPUD::packSizeTools(size()) + _Mybase::packSize();
	}

	virtual void serialize(SSerial& inp_sSerial) const
	{
		SPUD::serializeTools(inp_sSerial, size());
		_Mybase::serialize(inp_sSerial);
	}

	virtual bool unpack(SPack& inp_sPack)
	{
		if (!SPUD::unpackTools(inp_sPack, miLogicalSize)) return false;

		refresh(miLogicalSize);

		return _Mybase::unpack(inp_sPack);
	}

private:
	enum :size_type { ALLOC_BLK = 1024 };

	size_type		miLogicalSize;

	inline void alloc(size_type& inp_iSize, pointer& inp_Pointer)
	{
		_Mybase::alloc(this->miAllocatedSize = calcCapacity(inp_iSize), inp_Pointer);
	}

	static inline size_type calcCapacity(size_type inp_iSize) { return ((inp_iSize / _Myt::ALLOC_BLK) + 1) * _Myt::ALLOC_BLK; }

	inline void refresh(size_type inp_iSize)
	{
		if (inp_iSize > capacity())
		{
			this->deallocate(this->miAllocatedSize, this->mcRawPtr);
			alloc(inp_iSize, this->mcRawPtr);
		}
	}

	inline void realloc(size_type inp_iSize)
	{
		const size_type miPriorCap(capacity());
		const size_type miValid(std::min(inp_iSize, size()));
		pointer cBackupPtr(this->mcRawPtr);

		alloc(inp_iSize, this->mcRawPtr);
		for (size_type iIndx(0); iIndx < miValid; iIndx++)
		{
			this->mcRawPtr[iIndx] = cBackupPtr[iIndx];
		}
		this->deallocate(miPriorCap, cBackupPtr);
	}
};

template <typename T> using CVectorPtr = CSmartPtr<CSmartVector<T>>;

typedef CSmartVector<double> CVectorDbl;
typedef CSmartPtr<CVectorDbl> CVectorDblPtr;
typedef CSmartVector<float> CVectorFlt;
typedef CSmartPtr<CVectorFlt> CVectorFltPtr;
typedef CSmartVector<int32_t> CVectorInt;
typedef CSmartPtr<CVectorInt> CVectorIntPtr;
typedef CSmartVector<int64_t> CVectorLng;
typedef CSmartPtr<CVectorLng> CVectorLngPtr;
typedef CSmartVector<CDate> CVectorDate;
typedef CSmartPtr<CVectorDate> CVectorDatePtr;


typedef const_ArrayItr<CVectorDbl> const_VectorDblItr;
typedef const_ArrayItr<CVectorFlt> const_VectorFltItr;
typedef const_ArrayItr<CVectorInt> const_VectorIntItr;
typedef const_ArrayItr<CVectorLng> const_VectorLngItr;
typedef const_ArrayItr<CVectorDate> const_VectorDateItr;


typedef ArrayItr<CVectorDbl> VectorDblItr;
typedef ArrayItr<CVectorFlt> VectorFltItr;
typedef ArrayItr<CVectorInt> VectoryIntItr;
typedef ArrayItr<CVectorLng> VectorLngItr;
typedef ArrayItr<CVectorDate> VectorDateItr;
#endif
