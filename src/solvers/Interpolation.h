#ifndef INCLUDE_H_INTERPOLATION
#define INCLUDE_H_INTERPOLATION

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/SmartPointers.h"
#include "containers/SmartMap.h"

namespace Solvers
{
	template<typename TKey, typename TValue> static inline
		TValue InterpolateLinear(const TKey& inp_Key1, const TValue& inp_Value1, const TKey& inp_Key2, const TValue& inp_Value2, const TKey& inp_Key, bool inp_bExtrapolate = true)
	{
		typedef typename ValueHlpr<TValue>::value_type cast_type;

		if (inp_Key1 == inp_Key2) { return inp_Value1; }

		if (!inp_bExtrapolate)
		{
			if (inp_Key <= inp_Key1) { return inp_Value1; }
			if (inp_Key >= inp_Key2) { return inp_Value2; }
		}

		return TValue(inp_Value1 + (inp_Value2 - inp_Value1) * static_cast<cast_type>(inp_Key - inp_Key1) / static_cast<cast_type>(inp_Key2 - inp_Key1));
	}

	template<typename TKey, typename TValue> static inline
		TValue InterpolateLinear(const CSmartMap<TKey, TValue>& inp_cMap, const TKey& inp_Key, const bool& inp_bExtrapolate = true)
	{
		typedef typename CSmartMap<TKey, TValue>::const_iterator	const_iterator;
		typedef typename CSmartMap<TKey, TValue>::const_reference	const_reference;

		if (inp_cMap.empty()) { return TValue(); }

		if (inp_cMap.size() == 1) { return inp_cMap.begin()->second; }

		const_iterator itr(inp_cMap.find(inp_Key));
		if (itr != inp_cMap.cend()) { return itr->second; }

		if ((itr = inp_cMap.lower_bound(inp_Key)) == inp_cMap.cend()) { itr--; }
		if (itr == inp_cMap.begin())
		{
			const_reference left(*(itr++));
			const_reference right(*itr--);

			return InterpolateLinear(left.first, left.second, right.first, right.second, inp_Key, inp_bExtrapolate);
		}
		else
		{
			const_reference right(*(itr--));
			const_reference left(*itr);

			return InterpolateLinear(left.first, left.second, right.first, right.second, inp_Key, inp_bExtrapolate);
		}
	}

	template<typename TKey, typename TValue> static inline
		TValue InterpolateLinear(const CSmartMap<TKey, CSmartPtr<TValue>>& inp_cMap, const TKey& inp_Key, const bool& inp_bExtrapolate = true)
	{
		typedef typename CSmartMap<TKey, CSmartPtr<TValue>>::const_iterator	const_iterator;
		typedef typename CSmartMap<TKey, CSmartPtr<TValue>>::const_reference	const_reference;

		if (inp_cMap.empty()) { return TValue(); }

		if (inp_cMap.size() == 1) { return *inp_cMap.begin()->second; }

		const_iterator itr(inp_cMap.find(inp_Key));
		if (itr != inp_cMap.cend()) { return *itr->second; }

		if ((itr = inp_cMap.lower_bound(inp_Key)) == inp_cMap.cend()) { itr--; }
		if (itr == inp_cMap.begin())
		{
			const_reference left(*(itr++));
			const_reference right(*itr--);

			return InterpolateLinear(left.first, *left.second, right.first, *right.second, inp_Key, inp_bExtrapolate);
		}
		else
		{
			const_reference right(*(itr--));
			const_reference left(*itr);

			return InterpolateLinear(left.first, *left.second, right.first, *right.second, inp_Key, inp_bExtrapolate);
		}
	}

	template<typename TKey, typename TValue> static inline
		TValue InterpolateQuadratic(const pair<TKey, TValue>& inp_Point1, const pair<TKey, TValue>& inp_Point2, const pair<TKey, TValue>& inp_Point3, const TKey& inp_Key)
	{
		TValue dX2(inp_Point2.first - inp_Point1.first), dX3(inp_Point3.first - inp_Point1.first);
		TValue dY2(inp_Point2.second - inp_Point1.second), dY3(inp_Point3.second - inp_Point1.second);
		TValue dDenom(dX2 * dX3 * (dX2 - dX3));
		TValue dTarget(inp_Key - inp_Point1.first);

		TValue cof[3];
		cof[0] = (dX3 * dY2 - dX2 * dY3) / dDenom;
		cof[1] = (dX2 * dX2 * dY3 - dX3 * dX3 * dY2) / dDenom;
		cof[2] = inp_Point1.second;

		return CMath::polynomial(dTarget, cof, 3);
	}

	template<typename TKey, typename TValue> static inline
		TValue InterpolateDoubleParabolic(const CSmartMap<TKey, TValue>& inp_cMap, const TKey& inp_Key, const bool& inp_bExtrapolate = true)
	{
		typedef pair<TKey, TValue> point_type;
		typedef typename CSmartMap<TKey, TValue>::const_iterator const_iterator;

		if (inp_cMap.size() < 3) { return InterpolateLinear(inp_cMap, inp_Key, inp_bExtrapolate); }

		const_iterator itr;
		if (!inp_bExtrapolate)
		{
			itr = inp_cMap.begin();
			if (inp_Key <= itr->first) { return itr->second; }

			itr = inp_cMap.last();
			if (inp_Key >= itr->first) { return itr->second; }
		}

		if ((itr = inp_cMap.find(inp_Key)) != inp_cMap.cend()) { return itr->second; }

		point_type cPoint1, cPoint2, cPoint3, cPoint4;

		if (inp_cMap.size() == 3)
		{
			itr = inp_cMap.begin();
			cPoint1 = *(itr++);
			cPoint2 = *(itr++);
			cPoint3 = *itr;

			return InterpolateQuadratic(cPoint1, cPoint2, cPoint3, inp_Key);
		}

		itr = inp_cMap.lower_bound(inp_Key);
		if (itr == inp_cMap.begin())
		{
			cPoint1 = *(itr++);
			cPoint2 = *(itr++);
			cPoint3 = *itr;

			return InterpolateQuadratic(cPoint1, cPoint2, cPoint3, inp_Key);
		}

		if (--itr == inp_cMap.begin())
		{
			cPoint1 = *(itr++);
			cPoint2 = *(itr++);
			cPoint3 = *itr;

			return InterpolateQuadratic(cPoint1, cPoint2, cPoint3, inp_Key);
		}

		if (itr == inp_cMap.last())
		{
			cPoint3 = *(itr--);
			cPoint2 = *(itr--);
			cPoint1 = *itr;

			return InterpolateQuadratic(cPoint1, cPoint2, cPoint3, inp_Key);
		}

		if (--itr == inp_cMap.last())
		{
			cPoint3 = *(itr--);
			cPoint2 = *(itr--);
			cPoint1 = *itr;

			return InterpolateQuadratic(cPoint1, cPoint2, cPoint3, inp_Key);
		}

		cPoint1 = *(itr++);
		cPoint2 = *(itr++);
		cPoint3 = *(itr++);
		cPoint4 = *itr;

		auto v1(InterpolateQuadratic(cPoint1, cPoint2, cPoint3, inp_Key));
		auto v2(InterpolateQuadratic(cPoint2, cPoint3, cPoint4, inp_Key));

		return InterpolateLinear(cPoint2.first, v1, cPoint3.first, v2, inp_Key, inp_bExtrapolate);
	}
}
#endif
