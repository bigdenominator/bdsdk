#ifndef INCLUDE_H_DATETOOLS
#define INCLUDE_H_DATETOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/Strings.h"

typedef int32_t JulianDate;
typedef int32_t LongDate;

enum class DAYSOFWEEK { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY };
enum class MONTHSOFYEAR { JANUARY = 1, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER };
enum class PERIODS {DAY, WEEK, MONTH, QUARTER, YEAR};
enum class QUARTERS { FIRST = 1, SECOND, THIRD, FOURTH };
enum class QUARTERMONTHS { FIRST = 1, MIDDLE, LAST };
enum class POSITIONINMONTH { LAST = -1, FIRST = 1, SECOND = 2, THIRD = 3, FOURTH = 4 };

static const string	sDateSunday("SUN");
static const string	sDateMonday("MON");
static const string	sDateTuesday("TUE");
static const string	sDateWednesday("WED");
static const string	sDateThursday("THU");
static const string	sDateFriday("FRI");
static const string	sDateSaturday("SAT");

static const string	sDateJanuary("JAN");
static const string	sDateFebruary("FEB");
static const string	sDateMarch("MAR");
static const string	sDateApril("APR");
static const string	sDateMay("MAY");
static const string	sDateJune("JUN");
static const string	sDateJuly("JUL");
static const string	sDateAugust("AUG");
static const string	sDateSeptember("SEP");
static const string	sDateOctober("OCT");
static const string	sDateNovember("NOV");
static const string	sDateDecember("DEC");

class CDateMath
{
private:
	static inline LongDate addDays(const LongDate& inp_lDate, const int32_t& inp_lNumDaysToAdd)
	{
		if (inp_lNumDaysToAdd == 0){ return(inp_lDate); }

		return(JulianDateToLongDate(LongDateToJulianDate(inp_lDate) + inp_lNumDaysToAdd));
	}

	static inline LongDate addMonths(const LongDate& inp_lDate, const int32_t& inp_lNumMonthsToAdd, const bool& inp_bPreserveMonthEnd)
	{
		int32_t  iYear, iDay, lTempMonth;
		MONTHSOFYEAR Month;
		int32_t  lNumYears, lLastDay;
		bool bEnforceEndOfMonth(false);

		if (inp_lNumMonthsToAdd == 0){ return(inp_lDate); }

		LongDateToMDY(inp_lDate, Month, iDay, iYear);

		if (inp_bPreserveMonthEnd && iDay == GetDaysInMonth(iYear, Month)) { bEnforceEndOfMonth = true; }

		lNumYears = static_cast<int32_t>(inp_lNumMonthsToAdd / 12);
		lTempMonth = static_cast<int32_t>(Month) + inp_lNumMonthsToAdd - lNumYears * 12;
		iYear += lNumYears;

		if (lTempMonth > 12)
		{
			lTempMonth -= 12;
			iYear += 1;
		}
		else if (lTempMonth < 1)
		{
			lTempMonth += 12;
			iYear -= 1;
		}
		
		Month = static_cast<MONTHSOFYEAR>(lTempMonth);
		lLastDay = GetDaysInMonth(iYear, Month);
		if (bEnforceEndOfMonth)	{ iDay = lLastDay; }
		else					{ iDay = std::min(iDay, lLastDay); }
		
		return(MDYToLongDate(Month, iDay, iYear));
	}

	static inline LongDate addQuarters(const LongDate& inp_lDate, const int32_t& inp_lNumQuartersToAdd, const bool& inp_bPreserveMonthEnd)
	{
		if (inp_lNumQuartersToAdd == 0){ return(inp_lDate); }

		return(addMonths(inp_lDate, 3 * inp_lNumQuartersToAdd, inp_bPreserveMonthEnd));
	}

	static inline LongDate addWeeks(const LongDate& inp_lDate, const int32_t& inp_lNumWeeksToAdd)
	{
		if (inp_lNumWeeksToAdd == 0){ return(inp_lDate); }

		return(addDays(inp_lDate, 7 * inp_lNumWeeksToAdd));
	}

	static inline LongDate addYears(const LongDate& inp_lDate, const int32_t& inp_lNumYearsToAdd, const bool& inp_bPreserveMonthEnd)
	{
		int32_t  iYear, iDay;
		MONTHSOFYEAR Month;
		int32_t  lLastDay;
		bool bEnforceEndOfMonth(false);

		if (inp_lNumYearsToAdd == 0){ return(inp_lDate); }

		LongDateToMDY(inp_lDate, Month, iDay, iYear);

		if (inp_bPreserveMonthEnd && iDay == GetDaysInMonth(iYear, Month)) { bEnforceEndOfMonth = true; }

		iYear += inp_lNumYearsToAdd;

		lLastDay = GetDaysInMonth(iYear, Month);
		if (bEnforceEndOfMonth)	{ iDay = lLastDay; }
		else					{ iDay = std::min(iDay, lLastDay); }

		return(MDYToLongDate(Month, iDay, iYear));
	}

	static inline int32_t diffDays(const LongDate& inp_lStartDate, const LongDate& inp_lEndDate)
	{
		return(LongDateToJulianDate(inp_lEndDate) - LongDateToJulianDate(inp_lStartDate));
	}

	static inline int32_t diffMonths(const LongDate& inp_lStartDate, const LongDate& inp_lEndDate)
	{
		return(12 * diffYears(inp_lStartDate, inp_lEndDate) + static_cast<int32_t>(GetMonth(inp_lEndDate)) - static_cast<int32_t>(GetMonth(inp_lStartDate)));
	}

	static inline int32_t diffQuarters(const LongDate& inp_lStartDate, const LongDate& inp_lEndDate)
	{
		return(4 * diffYears(inp_lStartDate, inp_lEndDate) + static_cast<int32_t>(GetQuarter(inp_lEndDate)) - static_cast<int32_t>(GetQuarter(inp_lStartDate)));
	}

	static inline int32_t diffWeeks(const LongDate& inp_lStartDate, const LongDate& inp_lEndDate)
	{
		return(diffDays(inp_lStartDate, inp_lEndDate) / 7);
	}

	static inline int32_t diffYears(const LongDate& inp_lStartDate, const LongDate& inp_lEndDate)
	{
		return(GetYear(inp_lEndDate) - GetYear(inp_lStartDate));
	}

public:
	CDateMath(void){}

	~CDateMath(void){}
	
	static inline LongDate DateAdd(const LongDate& inp_lDate, const PERIODS& inp_PeriodType, const int32_t& inp_NumPeriodsToAdd, const bool& inp_bPreserveMonthEnd = false)
	{
		switch (inp_PeriodType)
		{
		case PERIODS::DAY: return addDays(inp_lDate, inp_NumPeriodsToAdd); break;
		case PERIODS::WEEK: return addWeeks(inp_lDate, inp_NumPeriodsToAdd); break;
		case PERIODS::MONTH: return addMonths(inp_lDate, inp_NumPeriodsToAdd, inp_bPreserveMonthEnd); break;
		case PERIODS::QUARTER: return addQuarters(inp_lDate, inp_NumPeriodsToAdd, inp_bPreserveMonthEnd); break;
		case PERIODS::YEAR: return addYears(inp_lDate, inp_NumPeriodsToAdd, inp_bPreserveMonthEnd); break;
		}
		return(inp_lDate);
	}

	static inline int32_t DateDiff(const LongDate& inp_lStartDate, const LongDate& inp_lEndDate, const PERIODS& inp_PeriodType)
	{
		switch (inp_PeriodType)
		{
		case PERIODS::DAY: return diffDays(inp_lStartDate, inp_lEndDate); break;
		case PERIODS::WEEK: return diffWeeks(inp_lStartDate, inp_lEndDate); break;
		case PERIODS::MONTH: return diffMonths(inp_lStartDate, inp_lEndDate); break;
		case PERIODS::QUARTER: return diffQuarters(inp_lStartDate, inp_lEndDate); break;
		case PERIODS::YEAR: return diffYears(inp_lStartDate, inp_lEndDate); break;
		}
		return(0);
	}

	static inline bool Deserialize(const string& inp_sDate, const string inp_sDelimeter, LongDate& inp_lDate)
	{
		string::size_type iStart(0);

		string			sTemp(sNULL);
		MONTHSOFYEAR	Month(MONTHSOFYEAR::JANUARY);
		int32_t			iDay(0), iYear(0);

		if ((sTemp = CString::DelimitedSubString(inp_sDate, inp_sDelimeter, iStart)) == sNULL) return false;
		Month = static_cast<MONTHSOFYEAR>(stoi(sTemp));

		if ((sTemp = CString::DelimitedSubString(inp_sDate, inp_sDelimeter, iStart)) == sNULL) return false;
		iDay = stoi(sTemp);

		if ((sTemp = CString::DelimitedSubString(inp_sDate, inp_sDelimeter, iStart)) == sNULL) return false;
		iYear = stoi(sTemp);

		inp_lDate = CDateMath::MDYToLongDate(Month, iDay, iYear);

		return true;
	}

	static inline bool Deserialize(const string& inp_sDate, LongDate& inp_lDate)
	{
		int32_t iYear(stoi(inp_sDate.substr(0, 4)));
		MONTHSOFYEAR Month(static_cast<MONTHSOFYEAR>(stoi(inp_sDate.substr(4, 2))));
		int32_t iDay(stoi(inp_sDate.substr(6, 2)));

		inp_lDate = CDateMath::MDYToLongDate(Month, iDay, iYear);

		return true;
	}

	static inline LongDate FindOrdinalDayOfMonth(const int32_t& inp_lYear, const MONTHSOFYEAR& inp_Month, const DAYSOFWEEK& inp_DayOfWeek, const POSITIONINMONTH& inp_NumOfSuchDays)
	{
		LongDate	lDate;
		int32_t		lGetToDayOfWeek;

		if (inp_NumOfSuchDays == POSITIONINMONTH::LAST)
		{
			lDate = MDYToLongDate(inp_Month, GetDaysInMonth(inp_lYear, inp_Month), inp_lYear);
			lGetToDayOfWeek = static_cast<int32_t>(inp_DayOfWeek) - static_cast<int32_t>(GetDayOfWeek(lDate));

			if (lGetToDayOfWeek > 0) lGetToDayOfWeek -= 7;

			return(DateAdd(lDate, PERIODS::DAY, lGetToDayOfWeek));
		}
		else
		{
			lDate = MDYToLongDate(inp_Month, 1, inp_lYear);
			lGetToDayOfWeek = static_cast<int32_t>(inp_DayOfWeek)-static_cast<int32_t>(GetDayOfWeek(lDate));

			if (lGetToDayOfWeek < 0) lGetToDayOfWeek += 7;

			return DateAdd(lDate, PERIODS::DAY, lGetToDayOfWeek + 7 * (static_cast<int32_t>(inp_NumOfSuchDays) - 1));
		}
	}

	static inline int32_t GetDay(const LongDate& inp_lDate){ return abs(inp_lDate - (inp_lDate / 100L) * 100L); }

	static inline DAYSOFWEEK GetDayOfWeek(const LongDate& inp_lDate)
	{
		return(static_cast<DAYSOFWEEK>(LongDateToJulianDate(inp_lDate) % 7));
	}

	static inline int32_t GetDayOfYear(const LongDate& inp_lDate)
	{
		return(LongDateToJulianDate(inp_lDate) - MDYToJulianDate(MONTHSOFYEAR::JANUARY, 1, GetYear(inp_lDate)) + 1);
	}

	static inline int32_t GetDaysInMonth(const LongDate& inp_lLongDate)
	{
		return(GetDaysInMonth(GetYear(inp_lLongDate), GetMonth(inp_lLongDate)));
	}

	static inline int32_t GetDaysInMonth(const int32_t& inp_lYear, const MONTHSOFYEAR& inp_Month)
	{
		static int32_t lDaysInMonth[2][13] =
		{
			{ 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
			{ 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
		};
		
		int32_t lLeap(0);
		if (IsLeapYear(inp_lYear)) { lLeap = 1; }

		return(lDaysInMonth[lLeap][static_cast<int32_t>(inp_Month)]);
	}
	
	static inline int32_t GetDaysInYear(const LongDate& inp_lDate)
	{
		if (IsLeapYear(GetYear(inp_lDate))) return(366);

		return(365);
	}

	static inline MONTHSOFYEAR GetMonth(const LongDate& inp_lDate){ return static_cast<MONTHSOFYEAR>(abs((inp_lDate / 100L) - (GetYear(inp_lDate) * 100L))); }

	static inline QUARTERS GetQuarter(const LongDate& inp_lDate){ return static_cast<QUARTERS>((static_cast<int32_t>(GetMonth(inp_lDate)) - 1L) / 3L + 1L); }

	static inline int32_t GetYear(const LongDate& inp_lDate){ return inp_lDate / 10000L; }

	static inline bool IsLeapYear(const int32_t& inp_lYear)
	{
		return((((inp_lYear % 4 == 0) && (inp_lYear % 100 != 0)) || (inp_lYear % 400 == 0)));
	}

	static inline bool IsWeekday(const LongDate& inp_lDate)
	{
		return(!IsWeekend(inp_lDate));
	}

	static inline bool IsWeekend(const LongDate& inp_lDate)
	{
		switch (GetDayOfWeek(inp_lDate))
		{
		case DAYSOFWEEK::SATURDAY:
		case DAYSOFWEEK::SUNDAY:
			return true;
			break;
		}
		return(false);
	}	
	
	static inline LongDate JulianDateToLongDate(const JulianDate& inp_lJulianDate)
	{
		int32_t  iYear, iDay;
		MONTHSOFYEAR  Month;

		JulianDateToMDY(inp_lJulianDate, Month, iDay, iYear);
		return(MDYToLongDate(Month, iDay, iYear));
	}

	static inline void JulianDateToMDY(const JulianDate& inp_lJulianDate, MONTHSOFYEAR& inp_Month, int32_t& inp_lDay, int32_t& inp_lYear)
	{
		int32_t iDay, lMonth, iYear;
		int32_t lTemp, lRunning(inp_lJulianDate);
		int32_t lDaysPer400Years(146097L);
		int32_t lDaysPer4000YearsNaive(1461001L);
		
		if (inp_lJulianDate <= 2299161L)
		{
			lDaysPer400Years += 3;
			lRunning += 38;
		}
		
		lRunning += 68569L;
		lTemp = 4 * lRunning / lDaysPer400Years;
		lRunning -= (lDaysPer400Years * lTemp + 3) / 4;

		iYear = 4000 * (lRunning + 1) / lDaysPer4000YearsNaive;
		lRunning = lRunning - 1461 * iYear / 4 + 31;

		lMonth = 80 * lRunning / 2447;
		iDay = lRunning - 2447 * lMonth / 80;
		lRunning = lMonth / 11;
		lMonth = lMonth + 2 - 12 * lRunning;
		iYear = 100 * (lTemp - 49) + iYear + lRunning;

		if (iYear <= 0) { iYear--; }

		inp_lYear = static_cast<int32_t>(iYear);
		inp_Month = static_cast<MONTHSOFYEAR>(lMonth);
		inp_lDay = static_cast<int32_t>(iDay);
	}

	static inline JulianDate LongDateToJulianDate(const LongDate& inp_lLongDate)
	{
		int32_t  iYear, iDay;
		MONTHSOFYEAR  Month;

		LongDateToMDY(inp_lLongDate, Month, iDay, iYear);
		return(MDYToJulianDate(Month, iDay, iYear));
	}

	static inline void LongDateToMDY(const LongDate& inp_lDate, MONTHSOFYEAR& inp_Month, int32_t& inp_lDay, int32_t& inp_lYear)
	{
		inp_lYear = GetYear(inp_lDate);
		inp_Month = GetMonth(inp_lDate);
		inp_lDay = GetDay(inp_lDate);
	}

	static inline JulianDate MDYToJulianDate(const MONTHSOFYEAR& inp_Month, const int32_t& inp_lDay, const int32_t& inp_lYear)
	{
		int32_t iYear(inp_lYear), lMonth(static_cast<int32_t>(inp_Month)), iDay(inp_lDay), lFlag((14 - lMonth) / 12);

		if (iYear < 0) { iYear++; }

		iYear += 4800 - lFlag;
		lMonth += 12 * lFlag - 3;
		iDay += 365 * iYear + static_cast<int32_t>((153 * lMonth + 2) / 5) + static_cast<int32_t>(iYear / 4) - 32083;

		if (MDYToLongDate(inp_Month, inp_lDay, inp_lYear) >= 15821015L){ iDay += 38 - static_cast<int32_t>(iYear / 100) + static_cast<int32_t>(iYear / 400); }
		return(iDay);
	}

	static inline LongDate MDYToLongDate(const MONTHSOFYEAR& inp_Month, const int32_t& inp_lDay, const int32_t& inp_lYear)
	{
		LongDate lDate = static_cast<int32_t>(abs(inp_lYear)) * 10000L + static_cast<int32_t>(inp_Month)* 100L + static_cast<int32_t>(inp_lDay);

		if (inp_lYear < 0){ lDate *= -1; }

		return(lDate);
	}

	static inline MONTHSOFYEAR QuarterToMonth(const QUARTERS& inp_Quarter, const QUARTERMONTHS& inp_MonthInQuarter)
	{
		return static_cast<MONTHSOFYEAR>(3 * (static_cast<int8_t>(inp_Quarter)-1) + static_cast<int8_t>(inp_MonthInQuarter));
	}

	static inline bool TranslateMonth(const string& inp_sMonth, MONTHSOFYEAR& inp_Month)
	{
		if (inp_sMonth.compare(sDateJanuary) == 0)	{ inp_Month = MONTHSOFYEAR::JANUARY;	return(true); }
		if (inp_sMonth.compare(sDateFebruary) == 0)	{ inp_Month = MONTHSOFYEAR::FEBRUARY;	return(true); }
		if (inp_sMonth.compare(sDateMarch) == 0)	{ inp_Month = MONTHSOFYEAR::MARCH;		return(true); }
		if (inp_sMonth.compare(sDateApril) == 0)	{ inp_Month = MONTHSOFYEAR::APRIL;		return(true); }
		if (inp_sMonth.compare(sDateMay) == 0)		{ inp_Month = MONTHSOFYEAR::MAY;		return(true); }
		if (inp_sMonth.compare(sDateJune) == 0)		{ inp_Month = MONTHSOFYEAR::JUNE;		return(true); }
		if (inp_sMonth.compare(sDateJuly) == 0)		{ inp_Month = MONTHSOFYEAR::JULY;		return(true); }
		if (inp_sMonth.compare(sDateAugust) == 0)	{ inp_Month = MONTHSOFYEAR::AUGUST;		return(true); }
		if (inp_sMonth.compare(sDateSeptember) == 0){ inp_Month = MONTHSOFYEAR::SEPTEMBER;	return(true); }
		if (inp_sMonth.compare(sDateOctober) == 0)	{ inp_Month = MONTHSOFYEAR::OCTOBER;	return(true); }
		if (inp_sMonth.compare(sDateNovember) == 0)	{ inp_Month = MONTHSOFYEAR::NOVEMBER;	return(true); }
		if (inp_sMonth.compare(sDateDecember) == 0)	{ inp_Month = MONTHSOFYEAR::DECEMBER;	return(true); }
		return(false);
	}

	static inline bool TranslateMonth(const MONTHSOFYEAR& inp_Month, string& inp_sMonth)
	{
		switch (inp_Month)
		{
		case MONTHSOFYEAR::JANUARY:		inp_sMonth = sDateJanuary;		return(true); break;
		case MONTHSOFYEAR::FEBRUARY:	inp_sMonth = sDateFebruary;		return(true); break;
		case MONTHSOFYEAR::MARCH:		inp_sMonth = sDateMarch;		return(true); break;
		case MONTHSOFYEAR::APRIL:		inp_sMonth = sDateApril;		return(true); break;
		case MONTHSOFYEAR::MAY:			inp_sMonth = sDateMay;			return(true); break;
		case MONTHSOFYEAR::JUNE:		inp_sMonth = sDateJune;			return(true); break;
		case MONTHSOFYEAR::JULY:		inp_sMonth = sDateJuly;			return(true); break;
		case MONTHSOFYEAR::AUGUST:		inp_sMonth = sDateAugust;		return(true); break;
		case MONTHSOFYEAR::SEPTEMBER:	inp_sMonth = sDateSeptember;	return(true); break;
		case MONTHSOFYEAR::OCTOBER:		inp_sMonth = sDateOctober;		return(true); break;
		case MONTHSOFYEAR::NOVEMBER:	inp_sMonth = sDateNovember;		return(true); break;
		case MONTHSOFYEAR::DECEMBER:	inp_sMonth = sDateDecember;		return(true); break;
		}
		return(false);
	}

	static inline bool TranslateDayOfWeek(const string& inp_sDayOfWeek, DAYSOFWEEK& inp_DayOfWeek)
	{
		if (inp_sDayOfWeek.compare(sDateSunday) == 0)	{ inp_DayOfWeek = DAYSOFWEEK::SUNDAY;		return(true); }
		if (inp_sDayOfWeek.compare(sDateMonday) == 0)	{ inp_DayOfWeek = DAYSOFWEEK::MONDAY;		return(true); }
		if (inp_sDayOfWeek.compare(sDateTuesday) == 0)	{ inp_DayOfWeek = DAYSOFWEEK::TUESDAY;		return(true); }
		if (inp_sDayOfWeek.compare(sDateWednesday) == 0){ inp_DayOfWeek = DAYSOFWEEK::WEDNESDAY;	return(true); }
		if (inp_sDayOfWeek.compare(sDateThursday) == 0) { inp_DayOfWeek = DAYSOFWEEK::THURSDAY;		return(true); }
		if (inp_sDayOfWeek.compare(sDateFriday) == 0)	{ inp_DayOfWeek = DAYSOFWEEK::FRIDAY;		return(true); }
		if (inp_sDayOfWeek.compare(sDateSaturday) == 0) { inp_DayOfWeek = DAYSOFWEEK::SATURDAY;		return(true); }
		return(false);
	}

	static inline bool TranslateDayOfWeek(const DAYSOFWEEK& inp_DayOfWeek, string& inp_sDayOfWeek)
	{
		switch (inp_DayOfWeek)
		{
		case DAYSOFWEEK::SUNDAY:	inp_sDayOfWeek = sDateSunday;		return(true); break;
		case DAYSOFWEEK::MONDAY:	inp_sDayOfWeek = sDateMonday;		return(true); break;
		case DAYSOFWEEK::TUESDAY:	inp_sDayOfWeek = sDateTuesday;		return(true); break;
		case DAYSOFWEEK::WEDNESDAY:	inp_sDayOfWeek = sDateWednesday;	return(true); break;
		case DAYSOFWEEK::THURSDAY:	inp_sDayOfWeek = sDateThursday;		return(true); break;
		case DAYSOFWEEK::FRIDAY:	inp_sDayOfWeek = sDateFriday;		return(true); break;
		case DAYSOFWEEK::SATURDAY:	inp_sDayOfWeek = sDateSaturday;		return(true); break;
		}
		return(false);
	}

	static inline bool ValidateDate(const LongDate& inp_lDate)
	{
		int32_t iYear, iMonth, iDay;
		MONTHSOFYEAR Month;

		LongDateToMDY(inp_lDate, Month, iDay, iYear);
		if (iDay < 1) return false;
		if (iDay > GetDaysInMonth(iYear, Month)) return false;

		iMonth = static_cast<int32_t>(Month);
		if (iMonth < 1) return false;
		if (iMonth > 12) return false;

		return true;
	}

	static inline int32_t ValidDay(const int32_t& inp_lYear, const MONTHSOFYEAR& inp_Month, const int32_t& inp_lDay)
	{
		return(max(1, std::min(inp_lDay, GetDaysInMonth(inp_lYear, inp_Month))));
	}
};
#endif
