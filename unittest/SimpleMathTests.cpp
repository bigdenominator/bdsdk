#ifndef INCLUDE_H_SIMPLEMATHTESTS
#define INCLUDE_H_SIMPLEMATHTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/SimpleMath.h"

TEST(SimpleMathTests, cos)
{
	const double dAngleStep = dPI / 8;
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(CMath::cos(0.0) - 1.0));
	EXPECT_GE(dTolerance, fabs(CMath::cos(dPI / 4) - d_SQRT2));
	EXPECT_GE(dTolerance, fabs(CMath::cos(dPI / 2) - 0.0));

	double dAngle;
	for (int8_t iCount(-20); iCount < 20; iCount++)
	{
		dAngle = iCount * dAngleStep;
		EXPECT_GE(dTolerance, fabs(CMath::cos(dAngle) - cos(dAngle)));
	}
}

TEST(SimpleMathTests, exp)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(CMath::exp(0.0) - 1.0));
	EXPECT_GE(dTolerance, fabs(CMath::exp(1.0) - dEXP));
	EXPECT_GE(dTolerance, fabs(CMath::exp(dLN2) - 2.0));

	EXPECT_EQ(0.0, CMath::exp(-dMAXEXP - 1.0));
	EXPECT_TRUE(std::isinf(CMath::exp(dMAXEXP + 1)));

	EXPECT_GE(dTolerance, fabs(CMath::exp(-12.0) - exp(-12.0)));
	EXPECT_GE(dTolerance, fabs(CMath::exp(-0.5) - exp(-0.5)));
	EXPECT_GE(dTolerance, fabs(CMath::exp(0.5) - exp(0.5)));
	EXPECT_GE(dTolerance, fabs(CMath::exp(12.0) - exp(12.0)));
}

TEST(SimpleMathTests, ln)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(CMath::ln(1.0) - 0.0));
	EXPECT_GE(dTolerance, fabs(CMath::ln(dEXP) - 1.0));
	EXPECT_GE(dTolerance, fabs(CMath::ln(2.0) - dLN2));

	EXPECT_TRUE(std::isnan(CMath::ln(-1.0)));
	EXPECT_TRUE(std::isinf(CMath::ln(0.0)));

	EXPECT_GE(dTolerance, fabs(CMath::ln(0.5) - log(0.5)));
	EXPECT_GE(dTolerance, fabs(CMath::ln(dPI) - log(dPI)));
	EXPECT_GE(dTolerance, fabs(CMath::ln(12.0) - log(12.0)));
}

TEST(SimpleMathTests, polynomial)
{
	const double dTolerance = 0.00000001;

	double coef[] = { 8.0 * dPI, 4.0 * dPI, 2.0 * dPI, 1.0 * dPI };

	EXPECT_GE(dTolerance, fabs(CMath::polynomial(0.5, coef, 4) - 4.0 * dPI));
}

TEST(SimpleMathTests, RoundMultiple)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(dPI, ROUNDINGTYPE::NONE, 0.125) - dPI));
	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(dPI, ROUNDINGTYPE::NEAREST, 0.125) - 3.125));
	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(dPI, ROUNDINGTYPE::UP, 0.125) - 3.25));
	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(dPI, ROUNDINGTYPE::DOWN, 0.125) - 3.125));

	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(-dPI, ROUNDINGTYPE::NONE, 0.125) + dPI));
	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(-dPI, ROUNDINGTYPE::NEAREST, 0.125) + 3.125));
	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(-dPI, ROUNDINGTYPE::UP, 0.125) + 3.25));
	EXPECT_GE(dTolerance, fabs(CMath::roundMultiple(-dPI, ROUNDINGTYPE::DOWN, 0.125) + 3.125));
}

TEST(SimpleMathTests, RoundSignificantDigits)
{
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(dPI, ROUNDINGTYPE::NONE, 3L) - dPI));
	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(dPI, ROUNDINGTYPE::NEAREST, 3L) - 3.142));
	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(dPI, ROUNDINGTYPE::UP, 3L) - 3.142));
	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(dPI, ROUNDINGTYPE::DOWN, 3L) - 3.141));

	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(-dPI, ROUNDINGTYPE::NONE, 3L) + dPI));
	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(-dPI, ROUNDINGTYPE::NEAREST, 3L) + 3.142));
	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(-dPI, ROUNDINGTYPE::UP, 3L) + 3.142));
	EXPECT_GE(dTolerance, fabs(CMath::roundDigits(-dPI, ROUNDINGTYPE::DOWN, 3L) + 3.141));
}

TEST(SimpleMathTests, sin)
{
	const double dAngleStep = dPI / 8;
	const double dTolerance = 0.00000001;

	EXPECT_GE(dTolerance, fabs(CMath::sin(0.0) - 0.0));
	EXPECT_GE(dTolerance, fabs(CMath::sin(dPI / 4) - d_SQRT2));
	EXPECT_GE(dTolerance, fabs(CMath::sin(dPI / 2) - 1.0));

	double dAngle;
	for (int8_t iCount(-20); iCount < 20; iCount++)
	{
		dAngle = iCount * dAngleStep;
		EXPECT_GE(dTolerance, fabs(CMath::sin(dAngle) - sin(dAngle)));
	}
}

TEST(SimpleMathTests, Consistency)
{
	const double dTolerance = 0.00000001;
	const double dAngleStep = dPI / 8;

	EXPECT_GE(dTolerance, fabs(CMath::ln(pow(2.0, 63L)) - 63.0 * dLN2));

	double dAngle, dCos, dSin;
	for (int8_t iCount(-20); iCount < 20; iCount++)
	{
		dAngle = iCount * dAngleStep;
		CMath::SinCos(dAngle, dSin, dCos);
		
		EXPECT_GE(dTolerance, fabs(pow(dSin, 2L) + pow(dCos, 2L) - 1.0));
	}

	for (int8_t iCount(-10); iCount < 10; iCount++)
	{
		EXPECT_GE(dTolerance, fabs(CMath::ln(CMath::exp(dPI * iCount)) - dPI * iCount));
	}
}
#endif
