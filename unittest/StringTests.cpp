#ifndef INCLUDE_H_STRINGTESTS
#define INCLUDE_H_STRINGTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "utils/Strings.h"

TEST(StringTests, DelimitedSubString)
{
	string	targetString[]	= { "How are you?", "How are you?", " How are you?", " How! are you?"};
	string	delimiter[]		= { " ", "!", " ", "!"};
	string	expect[]		= { "How", "How are you?", "", "How"};
	string::size_type		start[] = { 0, 0, 0, 0 };
	string	inp_sReturnString; 
	
	for (string::size_type i(0); i < sizeof(targetString) / sizeof(*targetString); i++)
	{
		EXPECT_EQ(expect[i], CString::DelimitedSubString(targetString[i], delimiter[i], start[i]));
	}
}

TEST(StringTests, RemoveLeadingChar)
{
	string	inp_sString[] = { "good", "good", "good", "oood", "oooo", "o", "" };
	char inp_cChar[] = { 'g', 'o', 't', 'o', 'o', 'o', 'y' };
	string expect[] = { "ood", "good", "good", "d", "o", "o", "" };

	for (string::size_type i(0); i < sizeof(inp_sString) / sizeof(*inp_sString); i++)
	{
		EXPECT_EQ(expect[i], CString::RemoveLeadingChar(inp_sString[i], inp_cChar[i]));
	}
}

TEST(StringTests, ToLower)
{
	EXPECT_EQ("hello",CString::ToLower("HELLO"));
	EXPECT_EQ("",CString::ToLower(""));
}

TEST(StringTests, ToUpper)
{
	EXPECT_EQ("HELLO",CString::ToUpper("hello"));
	EXPECT_EQ("",CString::ToLower(""));
}
#endif