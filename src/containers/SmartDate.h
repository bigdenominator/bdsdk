#ifndef INCLUDE_H_SMARTDATE
#define INCLUDE_H_SMARTDATE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <ctime>
#include "containers/SmartContainers.h"
#include "containers/DateMath.h"

static const string	sDateBadDate("Badly Formatted CDate");

enum class FREQUENCY
{
	SINGLE = 0,
	ANNUAL = 1,
	SEMIANNUAL = 2, EVERY4THMONTH = 3, QUARTERLY = 4, BIMONTHLY = 6, MONTHLY = 12,
	EVERY4THWEEK = 13, BIWEEKLY = 26, WEEKLY = 52,
	DAILY = 365
};

struct SPeriod : public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
public:
	typedef SPeriod _Myt;

	PERIODS		mUnitType;
	int32_t		miUnitCount;

	SPeriod(void) :mUnitType(PERIODS::DAY), miUnitCount(1) {}

	SPeriod(const PERIODS& inp_UnitType, const int32_t& inp_iUnitCount) :mUnitType(inp_UnitType), miUnitCount(inp_iUnitCount) {}

	SPeriod(const FREQUENCY& inp_Frequency)
	{
		switch (inp_Frequency)
		{
		case FREQUENCY::ANNUAL:
			mUnitType = PERIODS::YEAR;
			miUnitCount = 1;
			break;
		case FREQUENCY::SEMIANNUAL:
		case FREQUENCY::EVERY4THMONTH:
		case FREQUENCY::QUARTERLY:
		case FREQUENCY::BIMONTHLY:
		case FREQUENCY::MONTHLY:
			mUnitType = PERIODS::MONTH;
			miUnitCount = 12 / static_cast<int32_t>(inp_Frequency);
			break;
		case FREQUENCY::EVERY4THWEEK:
		case FREQUENCY::BIWEEKLY:
		case FREQUENCY::WEEKLY:
			mUnitType = PERIODS::WEEK;
			miUnitCount = 52 / static_cast<int32_t>(inp_Frequency);
			break;
		default:
			mUnitType = PERIODS::YEAR;
			miUnitCount = 1;
			break;
		}
	}

	SPeriod(const _Myt& rhs) :mUnitType(rhs.mUnitType), miUnitCount(rhs.miUnitCount) {}

	SPeriod(_Myt&& rhs) :mUnitType(std::move(rhs.mUnitType)), miUnitCount(std::move(rhs.miUnitCount)) {}

	inline _Myt& operator =(const _Myt& rhs) = default;

	inline _Myt& operator =(_Myt&& rhs) = default;
	
	static uint32_t PeriodsPerYear(const FREQUENCY& inp_Frequency)
	{
		switch (inp_Frequency)
		{
		case FREQUENCY::SINGLE:
		case FREQUENCY::ANNUAL: return 1;
		case FREQUENCY::SEMIANNUAL: return 2;
		case FREQUENCY::EVERY4THMONTH: return 3;
		case FREQUENCY::QUARTERLY: return 4;
		case FREQUENCY::BIMONTHLY: return 6;
		case FREQUENCY::MONTHLY: return 12;
		case FREQUENCY::EVERY4THWEEK: return 13;
		case FREQUENCY::BIWEEKLY: return 26;
		case FREQUENCY::WEEKLY: return 52;
		default: return 365; //DAILY
		}
	}

	static uint32_t PeriodsPerYear(const PERIODS& inp_Period)
	{
		switch (inp_Period)
		{
		case PERIODS::YEAR: return 1;
		case PERIODS::QUARTER: return 4;
		case PERIODS::MONTH: return  12;
		case PERIODS::WEEK: return 52;
		default: return 365; //DAY
		}
	}

	template <typename T> static T PeriodsPerYear(const SPeriod& inp_sPeriod)
	{
		return static_cast<T>(_Myt::PeriodsPerYear(inp_sPeriod.mUnitType)) / static_cast<T>(inp_sPeriod.miUnitCount);
	}

protected:
	SPUD(mUnitType, miUnitCount)
};
typedef CSmartPtr<SPeriod> SPeriodPtr;

class CDate
{
public:
	enum :int { INITIALIZE = 19660419 };

	CDate(void)	{ mlDate = INITIALIZE; }

	CDate(const LongDate& inp_lDate)
	{
		mlDate = inp_lDate; 
		checkDate(mlDate); 
	}

	CDate(const CDate& inp_cDate)
	{ 
		mlDate = inp_cDate.mlDate;
	}

	CDate(const string& inp_sDate)
	{
		if (static_cast<int32_t>(inp_sDate.find(sSlash)) > 0)
		{
			CDateMath::Deserialize(inp_sDate, sSlash, mlDate);
		}
		else if (static_cast<int32_t>(inp_sDate.find(sDash)) > 0)
		{
			CDateMath::Deserialize(inp_sDate, sDash, mlDate);
		}
		else if (static_cast<int32_t>(inp_sDate.size()) == 8)
		{
			CDateMath::Deserialize(inp_sDate, mlDate);
		}

		checkDate(mlDate);
	}

	CDate(const int32_t& inp_lYear, const MONTHSOFYEAR& inp_Month, const int32_t& inp_lDay = 1)
	{
		mlDate = CDateMath::MDYToLongDate(inp_Month, inp_lDay, inp_lYear);
		checkDate(mlDate);
	}
	
	explicit CDate(const int32_t& inp_lYear, const MONTHSOFYEAR& inp_Month, const DAYSOFWEEK& inp_DayOfWeek, const POSITIONINMONTH& inp_NumOfSuchDays)
	{
		mlDate = CDateMath::FindOrdinalDayOfMonth(inp_lYear, inp_Month, inp_DayOfWeek, inp_NumOfSuchDays);
		checkDate(mlDate);
	}

	~CDate(void) {}

	template<typename T> CDate&  operator=(const T& rhs) { mlDate = static_cast<LongDate>(rhs); checkDate(mlDate); return *this; }

	bool	operator==(const CDate& rhs) const { return(mlDate == rhs.mlDate); }
	bool	operator!=(const CDate& rhs) const { return(mlDate != rhs.mlDate); }

	template<typename T> bool operator<(const T& rhs) const { return (mlDate < static_cast<LongDate>(rhs)); }
	template<typename T> bool operator<=(const T& rhs) const { return (mlDate <= static_cast<LongDate>(rhs)); }
	template<typename T> bool operator>(const T& rhs) const { return (mlDate > static_cast<LongDate>(rhs)); }
	template<typename T> bool operator>=(const T& rhs) const { return (mlDate >= static_cast<LongDate>(rhs)); }

	template<typename T> CDate& operator+=(const T& rhs) { mlDate = CDateMath::DateAdd(mlDate, PERIODS::DAY, static_cast<int32_t>(rhs)); return *this; }
	CDate& operator+=(const SPeriod& rhs) { mlDate = CDateMath::DateAdd(mlDate, rhs.mUnitType, rhs.miUnitCount); return *this; }

	template<typename T> CDate& operator-=(const T& rhs) { mlDate = CDateMath::DateAdd(mlDate, PERIODS::DAY, NPOS * static_cast<int32_t>(rhs)); return *this; }
	CDate&	operator-=(const SPeriod& rhs) { mlDate = CDateMath::DateAdd(mlDate, rhs.mUnitType, -rhs.miUnitCount); return *this; }

	template<typename T> CDate operator+(const T& rhs) const { return CDate(*this) += rhs; }
	CDate operator+(const SPeriod& rhs) const { return CDate(*this) += rhs; }

	template<typename T> CDate operator-(const T& rhs) const { return CDate(*this) -= rhs; }
	CDate operator-(const SPeriod& rhs) const { return CDate(*this) -= rhs; }
	
	int32_t	operator-(const CDate& rhs) const { return CDateMath::DateDiff(rhs.mlDate, mlDate, PERIODS::DAY); }

	CDate&	operator++(void) { mlDate = CDateMath::DateAdd(mlDate, PERIODS::DAY, 1); return *this; }  // prefix
	CDate	operator++(int)  // postfix
	{ 
		CDate cUnchanged(*this);
		mlDate = CDateMath::DateAdd(mlDate, PERIODS::DAY, 1);
		return cUnchanged;
	}		

	CDate&	operator--(void) { mlDate = CDateMath::DateAdd(mlDate, PERIODS::DAY, NPOS * 1); return *this; }	// prefix
	CDate	operator--(int)  // postfix
	{
		CDate cUnchanged(*this);
		mlDate = CDateMath::DateAdd(mlDate, PERIODS::DAY, NPOS * 1);
		return cUnchanged;
	}	

	operator double(void) const { return static_cast<double>(mlDate); }
	operator float(void) const { return static_cast<float>(mlDate); }
	operator int(void) const { return static_cast<int>(mlDate); }
	operator long(void) const { return static_cast<long>(mlDate); }
	operator long long(void) const { return static_cast<long long>(mlDate); }

	CDate Add(const PERIODS& inp_PeriodType, const int32_t& inp_lNumPeriodsToAdd, const bool& inp_bEnforceMonthEnd = false) const
	{
		return CDate(CDateMath::DateAdd(mlDate, inp_PeriodType, inp_lNumPeriodsToAdd, inp_bEnforceMonthEnd));
	}

	CDate Add(const SPeriod& inp_sPeriod, const bool& inp_bEnforceMonthEnd = false) const
	{
		return Add(inp_sPeriod.mUnitType, inp_sPeriod.miUnitCount, inp_bEnforceMonthEnd); 
	}

	static CDate CurrentDate(void)
	{
		typedef std::chrono::system_clock clock_t;

		time_t t(clock_t::to_time_t(clock_t::now()));
		char cBuffer[9];
#ifdef WINDOWS
#ifndef WINGCC
		tm ltm;
		localtime_s(&ltm, &t);
		strftime(cBuffer, sizeof(cBuffer), "%Y%m%d", &ltm);
#else
		tm* ltm(localtime(&t));
		strftime(cBuffer, sizeof(cBuffer), "%Y%m%d", ltm);
#endif   // its not Windows
#else
		tm* ltm(localtime(&t));
		strftime(cBuffer, sizeof(cBuffer), "%Y%m%d", ltm);
#endif
		return CDate(cBuffer);
	}

	int32_t Day(void) const { return CDateMath::GetDay(mlDate); }
	
	DAYSOFWEEK DayOfWeek(void) const { return(CDateMath::GetDayOfWeek(mlDate)); }

	int32_t DayOfYear(void) const { return(CDateMath::GetDayOfYear(mlDate)); }

	int32_t DaysInMonth(void) const { return CDateMath::GetDaysInMonth(mlDate); }

	int32_t DaysInYear(void) const { return(CDateMath::GetDaysInYear(mlDate)); }

	int32_t Diff(const CDate& inp_cDateFrom, const PERIODS& inp_PeriodType) const { return CDateMath::DateDiff(inp_cDateFrom.mlDate, mlDate, inp_PeriodType); }

	bool IsLeapYear(void) const { return(CDateMath::IsLeapYear(Year())); }

	bool IsWeekday(void) const { return(CDateMath::IsWeekday(mlDate)); }

	bool IsWeekend(void) const { return(CDateMath::IsWeekend(mlDate)); }

	MONTHSOFYEAR Month(void) const { return CDateMath::GetMonth(mlDate); }

	CDate& Move(const PERIODS& inp_PeriodType, const int32_t& inp_lNumPeriodsToAdd, const bool& inp_bEnforceMonthEnd = false)
	{
		mlDate = CDateMath::DateAdd(mlDate, inp_PeriodType, inp_lNumPeriodsToAdd, inp_bEnforceMonthEnd);
		return *this;
	}

	CDate& Move(const SPeriod& inp_sPeriod, const bool& inp_bEnforceMonthEnd = false) { return (*this) += inp_sPeriod; }

	QUARTERS Quarter(void) const { return(CDateMath::GetQuarter(mlDate)); }
	
	void SetDay(const int32_t& inp_lDay)
	{
		int32_t lYear(Year());
		MONTHSOFYEAR lMonth(Month());

		mlDate = CDateMath::MDYToLongDate(lMonth, CDateMath::ValidDay(lYear, lMonth, inp_lDay), lYear);
	}

	void SetMonth(const MONTHSOFYEAR& inp_Month)
	{
		int32_t lYear(Year());

		mlDate = CDateMath::MDYToLongDate(inp_Month, CDateMath::ValidDay(lYear, inp_Month, Day()), lYear);
	}

	void SetToFirstOfMonth(void) { mlDate = CDateMath::MDYToLongDate(Month(), 1, Year()); }

	void SetToFirstOfYear(void) { mlDate = CDateMath::MDYToLongDate(MONTHSOFYEAR::JANUARY, 1, Year()); }

	void SetToLastOfMonth(void) { mlDate = CDateMath::MDYToLongDate(Month(), DaysInMonth(), Year()); }

	void SetToLastOfYear(void) { mlDate = CDateMath::MDYToLongDate(MONTHSOFYEAR::DECEMBER, 31, Year()); }

	void SetYear(const int32_t& inp_lYear)
	{
		MONTHSOFYEAR lMonth(Month());

		mlDate = CDateMath::MDYToLongDate(lMonth, CDateMath::ValidDay(inp_lYear, lMonth, Day()), inp_lYear);
	}

	int32_t Year(void) const { return CDateMath::GetYear(mlDate); }

	double YearShare(void) const { return static_cast<double>(DayOfYear()) / static_cast<double>(DaysInYear()); }

protected:
	LongDate	mlDate;

	static void checkDate(const LongDate& inp_lDate)
	{
		if (!CDateMath::ValidateDate(inp_lDate)) { throw CException(sDateBadDate, __FILE__, __LINE__); }
	}
};

static inline void to(const string& inp_sFrom, CDate& out_data)
{
	out_data = stoi(inp_sFrom);
}

static inline string to_string(CDate inp_dtThis) { return to_string(static_cast<int>(inp_dtThis)); }
#endif