#ifndef INCLUDE_H_MYSCHEMATIC
#define INCLUDE_H_MYSCHEMATIC

/* (C) Copyright Big Denominator, LLC 2016.
* Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
* (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"
#ifdef WINDOWS
#pragma once
#endif

#include "containers/Schematic.h"

enum USER_TYPELIST :TYPELIST_t { USER_END };


#endif