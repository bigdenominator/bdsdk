
/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

MyArgs myArgs;
string sDatabasePath, sDatabaseFile, sTestCommand;
CSmartPtr<CLogFile> cLogPtr;

static const string sSOURCE = "source";
static const string sWORKING = "working";

int main(int argc, char **argv) 
{
	testing::InitGoogleTest(&argc, argv);
	sDatabasePath = sDatabaseFile = sTestCommand = sNULL;
	myArgs.msSourceFile = ParseArgs(argc, argv, sSOURCE);
	myArgs.msWorkingDir = ParseArgs(argc, argv, sWORKING);

	cLogPtr = CreateSmart<CLogFile>(sDatabasePath + "UnitTestLog.txt");

	return RUN_ALL_TESTS();
}
