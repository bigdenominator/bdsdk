#ifndef INCLUDE_H_TRANSFORMNODE
#define INCLUDE_H_TRANSFORMNODE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

/*
TransformNode has 4 responsibilities.

First it provides the simple communications/data access interface (REQUEST/REPLY/SEND).
Second it is the connection link between the model and the EventHandler/EventPump.
Third it provides the interface to the Directed Graph Engine that sequences workflow as specified by the model builder.
Fourth it provides the plug in socket for the customized model.
*/

#include "utils/all.h"
#include "containers/all.h"
#include "nodes/BaseNode.h"

static const string sRequestFailed = "Request failed: ";

SCHEMATIC(SLog, FRM_TYPELIST::SLOG, (string msMessage), (msMessage))
SCHEMATIC(SError, FRM_TYPELIST::SERROR, (CFeedback mcFeedback;), (mcFeedback))

template<typename TSchematic, typename TRequest, typename TSend, typename TView> class CTransformNode : public CBaseNode
{
public:
	typedef CBaseNode		_Mybase;
	typedef CTransformNode	_Myt;

	typedef _Myt			node_type;

	typedef TRequest		_Myrequests;
	typedef TSend			_Mysends;
	typedef TView			_Myviews;

	typedef TSchematic              SSchematic;
	typedef CSmartPtr<SSchematic>   SSchematicPtr;

	template<typename TList, typename T> using NodeAPI_bool = typename std::enable_if<TList::template HasType<T>::value, bool>::type;
	template<typename TList, typename T> using NodeAPI_PROC = typename std::enable_if<TList::template HasType<T>::value, PROCESS_STATUS>::type;

	enum :TYPELIST_t { TypeId = SSchematic::TypeId };

	void Log(const string& inp_sMessage)
	{
		SLogPtr sLogPtr(CreateSmart<SLog>());
		sLogPtr->msMessage = inp_sMessage;

		Send(sLogPtr);
	}
	
	void LogError(const CFeedback& inp_cFeedback)
	{
		SErrorPtr sErrPtr(CreateSmart<SError>());
		sErrPtr->mcFeedback = inp_cFeedback;

		Send(sErrPtr);
	}

	virtual PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr) { return PROCESS_STATUS::FAIL; }

	template<typename T> typename std::enable_if<T::TypeId != _Myt::TypeId, NodeAPI_bool<TRequest, T>>::type Request(CSmartPtr<T> inp_sSchematicPtr)
	{
		STransportPtr cTempPtr(mcEvntHndlrLocalPtr->GetTransport());
		cTempPtr->msDataHldrPtr = CreateUnique<SGreyBox<T>>(inp_sSchematicPtr);
		cTempPtr->msReturnInfo.mbRequestWaiting = true;

		if (!mcEvntHndlrLocalPtr->Request(cTempPtr))
		{
			FEEDBACK(mcFeedback, 501, sRequestFailed + to_string(T::TypeId));
			return false;
		}

		if (mcFeedback = cTempPtr->mcFeedback) { inp_sSchematicPtr = SGreyBox<T>::Extract(cTempPtr->msDataHldrPtr); }
		mcEvntHndlrLocalPtr->ReturnTransport(cTempPtr);

		return mcFeedback;
	}

	template<typename T, typename F, typename ...TParams> NodeAPI_PROC<TRequest, T> Request(CSmartPtr<T> inp_sSchematicPtr, F inp_Callback, TParams... myParams)
	{
		STransportPtr cTempPtr(mcEvntHndlrLocalPtr->GetTransport());
		cTempPtr->msDataHldrPtr = CreateUnique<SGreyBox<T>>(inp_sSchematicPtr);
		cTempPtr->msReturnInfo.msPrevTransPtr = msTransportPtr;
		msTransportPtr->msCallbackPtr = createCallback(inp_Callback, myParams...);

		return (mcEvntHndlrLocalPtr->Request(cTempPtr) ? PROCESS_STATUS::PENDING : PROCESS_STATUS::FAIL);
	}

	size_t Run(void)
	{
		size_t lCount(0);

		mcEvntHndlrLocalPtr->Attach();

		if (Initialize())
		{
			while (msTransportPtr = mcEvntHndlrLocalPtr->Receive(SSchematic::TypeId))
			{
				switch (evaluate())
				{
				case PROCESS_STATUS::FAIL:
					msTransportPtr->mcFeedback = GetFeedback();
				case PROCESS_STATUS::DONE:
					dispose();
					lCount++;
					break;
				default:
					break;
				}
			}
		}
		else
		{
			LogError(GetFeedback());
		}
		
		return lCount;
	}

	template<typename T> NodeAPI_bool<TSend, T> Send(CSmartPtr<T> inp_sSchematicPtr) const
	{
		STransportPtr cTempPtr(mcEvntHndlrLocalPtr->GetTransport());
		cTempPtr->msDataHldrPtr = CreateUnique<SGreyBox<T>>(inp_sSchematicPtr);

		return mcEvntHndlrLocalPtr->Send(cTempPtr);
	}

	template<typename T> NodeAPI_bool<TView, T> View(CSmartPtr<T>& inp_sSchematicPtr) const
	{
		STransportPtr sTmpPtr(mcEvntHndlrLocalPtr->View(T::TypeId));
		if (!sTmpPtr) return false;

		inp_sSchematicPtr = SGreyBox<T>::Extract(sTmpPtr->msDataHldrPtr);
		return true;
	}

protected:
	~CTransformNode(void) {}

	CTransformNode(void) :_Mybase() {}

private:
	template<typename Func> class CCallback : public ICallback
	{
	public:
		CCallback(Func inp_Fn) :mFn(inp_Fn) {}

		PROCESS_STATUS operator()(void) { return mFn(); }

		TYPELIST_t GetTypeId(void) { return SSchematic::TypeId; }

	private:
		Func	mFn;
	};

	STransportPtr msTransportPtr;

	template<typename F, typename ...TParams> SCallbackPtr createCallback(F&& inp_Fn, TParams&&... myParams) const
	{
		auto foo = std::bind(std::forward<F>(inp_Fn), std::forward<TParams>(myParams)...);

		return CreateUnique<CCallback<decltype(foo)>>(foo);
	}

	void dispose() const
	{
		if (msTransportPtr->msReturnInfo.mbRequestWaiting)
		{
			msTransportPtr->msReturnInfo.mbRequestWaiting = false;
		}
		else
		{
			if (msTransportPtr->msReturnInfo.msPrevTransPtr)
			{
				msTransportPtr->msReturnInfo.msPrevTransPtr->mcFeedback = mcFeedback;
				mcEvntHndlrIntrPtr->Send(msTransportPtr->msReturnInfo.msPrevTransPtr);
			}
			mcEvntHndlrLocalPtr->ReturnTransport(msTransportPtr);
		}
	}

	PROCESS_STATUS evaluate()
	{
		if (!msTransportPtr->mcFeedback) { return PROCESS_STATUS::FAIL; }

		if (msTransportPtr->msCallbackPtr)
		{
			SCallbackPtr sTempPtr(std::move(msTransportPtr->msCallbackPtr));
			if (sTempPtr->GetTypeId() != TypeId)
			{
				return PROCESS_STATUS::FAIL; 
			}
			else 
			{
				return (*sTempPtr)(); 
			}
		}

		return Process(SGreyBox<SSchematic>::Extract(msTransportPtr->msDataHldrPtr));
	}
};

typedef TypeList<SError, SLog> StdOutputSchematics;

#define NODE_API_LISTS(namePrefix, requests, sends, views) \
	typedef TypeListConcat< StdOutputSchematics, TypeList< DUMMY sends > > namePrefix##Sends; \
	typedef TypeList< DUMMY requests > namePrefix##Requests; \
	typedef TypeList< DUMMY views > namePrefix##Views; 

#define DEF_NODE(nodeName, schematic, requests, sends, views) \
	NODE_API_LISTS(nodeName, requests, sends, views) \
	class nodeName : public CTransformNode< schematic, nodeName##Requests, nodeName##Sends, nodeName##Views > 

#define DEF_NODE_COMPLETE(nodeName, schematic, requests, sends, views) \
	NODE_API_LISTS(nodeName, requests, sends, views) \
	struct nodeName : public CTransformNode< schematic, nodeName##Requests, nodeName##Sends, nodeName##Views > \
    { \
		typedef node_type _Mybase; \
		typedef nodeName _Myt; \
		void init(void); \
		PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr); \
	};

#endif
