#ifndef INCLUDE_H_ALLMATH
#define INCLUDE_H_ALLMATH

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "math/RandomNumbers.h"
#include "math/Statistics.h"

#endif