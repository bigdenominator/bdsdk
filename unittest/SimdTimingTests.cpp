#ifndef INCLUDE_H_SIMDTIMINGTESTS
#define INCLUDE_H_SIMDTIMINGTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#define BLAZE_ENFORCE_AVX
#pragma warning( disable : 4099 )
#pragma warning( disable : 4146 )

#include <blaze/Math.h>
#include "containers/SmartArray.h"
#include "containers/SmartVector.h"
#include "utils/Timer.h"

using blaze::DynamicVector;

class TimingTest : public testing::Test
{
public:
	enum :size_t { DBL = 65851, FLT = 131702, INT = 65851, LNG = 65851, ITERATIONS = 1000 };

	string		msType;

	Timer		mcTimer;

	void Log(string inp_sTest)
	{
		cLogPtr->put(msType + sComma + inp_sTest + sComma + to_string(mcTimer.cycles()));
	}

	void SetType(string inp_sType)
	{
		msType = inp_sType;
	}

	void SetUp(void)
	{
		msType = sNULL;
	}
};

TEST_F(TimingTest, Double)
{
	SetType("CVectorDbl");

	CVectorDbl cArray1(DBL);
	CVectorDbl cArray2(DBL);
	CVectorDbl cResult(DBL);

	//Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cArray1 = 2.0;
		cArray2 = 1.0;
		cResult = 0.0;
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + cArray2;
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + 2.0;
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / cArray2;
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / 2.0;
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 / cArray1;
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * cArray2;
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * 2.0;
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - cArray2;
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - 2.0;
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 - cArray1;
	}
	mcTimer.end();
	Log("Subtract into Const");
}

TEST_F(TimingTest, DoubleRaw)
{
	SetType("Raw Array Dbl");
	const size_t iSize(DBL);

	double*		Array1(new double[DBL]);
	double*		Array2(new double[DBL]);
	double*		Result(new double[DBL]);

	// Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Array1[iIndx] = 2.0;
			Array2[iIndx] = 1.0;
			Result[iIndx] = 0.0;
		}
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + 2.0;
		}
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / 2.0;
		}
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 / Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * 2.0;
		}
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - 2.0;
		}
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 - Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract into Const");

	delete Array1;
	delete Array2;
	delete Result;
}

TEST_F(TimingTest, DoubleBlaze)
{
	SetType("Blaze Array Dbl");

	typedef DynamicVector<double> CBlazeDbl;
	CBlazeDbl		Array1(DBL);
	CBlazeDbl		Array2(DBL);
	CBlazeDbl		Result(DBL);

	// Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		Array1 = 2.0;
		Array2 = 1.0;
		Result = 0.0;
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		Result = Array1 + Array2;
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		Result = Array1 / 2.0;
	}
	mcTimer.end();
	Log("Divide Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		Result = Array1 * Array2;
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		Result = Array1 * 2.0;
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		Result = Array1 - Array2;
	}
	mcTimer.end();
	Log("Subtract Array");
}

TEST_F(TimingTest, Float)
{
	SetType("CVectorFlt");

	CVectorFlt cArray1(FLT);
	CVectorFlt cArray2(FLT);
	CVectorFlt cResult(FLT);

	//Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cArray1 = 2.0;
		cArray2 = 1.0;
		cResult = 0.0;
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + cArray2;
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + 2.0;
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / cArray2;
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / 2.0;
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 / cArray1;
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * cArray2;
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * 2.0;
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - cArray2;
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - 2.0;
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 - cArray1;
	}
	mcTimer.end();
	Log("Subtract into Const");
}

TEST_F(TimingTest, FloatRaw)
{
	SetType("Raw Array Flt");
	const size_t iSize(FLT);

	float*		Array1(new float[FLT]);
	float*		Array2(new float[FLT]);
	float*		Result(new float[FLT]);

	// Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Array1[iIndx] = 2.0;
			Array2[iIndx] = 1.0;
			Result[iIndx] = 0.0;
		}
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + 2.0;
		}
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / 2.0;
		}
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 / Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * 2.0;
		}
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - 2.0;
		}
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 - Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract into Const");

	delete Array1;
	delete Array2;
	delete Result;
}

TEST_F(TimingTest, Int32)
{
	SetType("CVectorInt");

	CVectorInt cArray1(INT);
	CVectorInt cArray2(INT);
	CVectorInt cResult(INT);

	//Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cArray1 = 2.0;
		cArray2 = 1.0;
		cResult = 0.0;
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + cArray2;
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + 2.0;
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / cArray2;
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / 2.0;
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 / cArray1;
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * cArray2;
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * 2.0;
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - cArray2;
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - 2.0;
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 - cArray1;
	}
	mcTimer.end();
	Log("Subtract into Const");
}

TEST_F(TimingTest, Int32Raw)
{
	SetType("Raw Array Int32");
	const size_t iSize(INT);

	int32_t*		Array1(new int32_t[INT]);
	int32_t*		Array2(new int32_t[INT]);
	int32_t*		Result(new int32_t[INT]);

	// Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Array1[iIndx] = 2.0;
			Array2[iIndx] = 1.0;
			Result[iIndx] = 0.0;
		}
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + 2.0;
		}
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / 2.0;
		}
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 / Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * 2.0;
		}
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - 2.0;
		}
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 - Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract into Const");

	delete Array1;
	delete Array2;
	delete Result;
}

TEST_F(TimingTest, Int64)
{
	SetType("CVectorLng");

	CVectorLng cArray1(LNG);
	CVectorLng cArray2(LNG);
	CVectorLng cResult(LNG);

	//Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cArray1 = 2.0;
		cArray2 = 1.0;
		cResult = 0.0;
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + cArray2;
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 + 2.0;
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / cArray2;
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 / 2.0;
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 / cArray1;
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * cArray2;
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 * 2.0;
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - cArray2;
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = cArray1 - 2.0;
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		cResult = 2.0 - cArray1;
	}
	mcTimer.end();
	Log("Subtract into Const");
}

TEST_F(TimingTest, Int64Raw)
{
	SetType("Raw Array Int64");
	const size_t iSize(LNG);

	int64_t*		Array1(new int64_t[LNG]);
	int64_t*		Array2(new int64_t[LNG]);
	int64_t*		Result(new int64_t[LNG]);

	// Initialize timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Array1[iIndx] = 2.0;
			Array2[iIndx] = 1.0;
			Result[iIndx] = 0.0;
		}
	}
	mcTimer.end();
	Log("Init");

	// add timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Add Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] + 2.0;
		}
	}
	mcTimer.end();
	Log("Add Const");

	// divide timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] / 2.0;
		}
	}
	mcTimer.end();
	Log("Divide Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 / Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Divide into Const");

	// multiply timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Multiply Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] * 2.0;
		}
	}
	mcTimer.end();
	Log("Multiply Const");

	// subtract timing test
	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - Array2[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract Array");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = Array1[iIndx] - 2.0;
		}
	}
	mcTimer.end();
	Log("Subtract Const");

	mcTimer.begin();
	for (int i(0); i < ITERATIONS; i++)
	{
		for (size_t iIndx(0); iIndx < iSize; iIndx++)
		{
			Result[iIndx] = 2.0 - Array1[iIndx];
		}
	}
	mcTimer.end();
	Log("Subtract into Const");

	delete Array1;
	delete Array2;
	delete Result;
}
#endif
