#ifndef INCLUDE_H_EXPRESSIONS
#define INCLUDE_H_EXPRESSIONS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/Parameterize.h"
#include "containers/SIMD.h"

HASTYPE(HasSimdType, simd_type)
template <typename T> class HasExprMethods
{
	typedef typename T::simd_type simd_type;

	static T* make(void);

	struct HasGet
	{
		template<typename U> using fnGet = simd_type(U::*)(size_t) const;

		template<typename U> static uint32_t test_sig(fnGet<U>);
		template<typename U> static uint64_t test_sig(...);

		template<typename U> static decltype(test_sig(&U::operator[])) test(decltype(make()->operator[](0))*);
		template<typename U> static uint64_t test(...);

		enum :int { value = (sizeof(test<T>(nullptr)) == sizeof(uint32_t)) };

	};
	struct HasSize
	{
		template<typename U> using fnSize = size_t(U::*)(void) const;

		template<typename U> static uint32_t test_sig(fnSize<U>);
		template<typename U> static uint64_t test_sig(...);

		template<typename U> static decltype(test_sig(&U::size)) test(decltype(make()->size())*);
		template<typename U> static uint64_t test(...);

		enum :int { value = (sizeof(test<T>(nullptr)) == sizeof(uint32_t)) };
	};

public:
	enum { value = HasGet::value && HasSize::value };
};

template<typename T, bool hasSimdType = HasSimdType<T>::value> struct IsExprImpl
{
	enum :int { value = 0 };
};
template<typename T> struct IsExprImpl<T, true>
{
	enum :int { value = HasExprMethods<T>::value };
};
template<typename T> using IsExpr = IsExprImpl<T>;

HASTYPE(HasExpressionType, expression_type)
template<typename T, bool hasExprType = HasExpressionType<T>::value> struct IsExprConvertibleImpl
{
	enum :int { value = 0 };
};
template<typename T> struct IsExprConvertibleImpl<T, true>
{
	enum :int { value = HasExprMethods<typename T::expression_type>::value };
};
template<typename T> using IsExprConvertible = IsExprConvertibleImpl<T>;

template<typename T, bool isExpr = IsExpr<T>::value, bool isExprConvertible = IsExprConvertible<T>::value> struct IsExprLikeImpl
{
	enum :int { value = 0 };
};
template<typename T> struct IsExprLikeImpl<T, true, false>
{
	typedef T expression_type;
	enum :int { value = 1 };
};
template<typename T> struct IsExprLikeImpl<T, false, true>
{
	typedef typename T::expression_type expression_type;
	enum :int { value = 1 };
};
template<typename T> struct IsExprLikeImpl<T, true, true>
{
	typedef T expression_type;
	enum :int { value = 1 };
};
template<typename T> using IsExprLike = IsExprLikeImpl<T>;

template<typename TData, typename TResult> using ifExprLike = std::enable_if<IsExprLike<TData>::value, TResult>;

template<typename T, bool isExprLike = IsExprLike<T>::value> struct ExprType {};
template<typename T> struct ExprType<T, true>
{
	typedef typename IsExprLike<T>::expression_type type;
};

template<typename TExpr, typename TConst, bool isExprLike = IsExprLike<TExpr>::value> struct IsExprCompatibleWithConstImpl
{
	enum :int { value = 0 };
};
template<typename TExpr, typename TConst> struct IsExprCompatibleWithConstImpl<TExpr, TConst, true>
{
	enum :int { value = std::is_convertible<TConst, typename ExprType<TExpr>::type::primitive_type>::value };
};
template<typename TExpr, typename TConst> using IsExprCompatibleWithConst = IsExprCompatibleWithConstImpl<TExpr, TConst>;

template<typename TExpr, typename UExpr, bool isLhsExprLike = IsExprLike<TExpr>::value, bool isRhsExprLike = IsExprLike<UExpr>::value> struct AreExprsCompatibleImpl
{
	enum :int { value = 0 };
};
template<typename TExpr, typename UExpr> struct AreExprsCompatibleImpl<TExpr, UExpr, true, true>
{
	enum :int { value = TypeCheck<typename ExprType<TExpr>::type::primitive_type, typename ExprType<UExpr>::type::primitive_type>::agree };
};
template<typename TExpr, typename UExpr> using AreExprsCompatible = AreExprsCompatibleImpl<TExpr, UExpr>;

// unary expression
template<typename TExpr, typename UnaryOp, bool hasOperate = simd::unary::HasOperate<UnaryOp, typename TExpr::primitive_type>::value> struct UnaryExpression
{
public:
	typedef TExpr	data_type;

	typedef typename TExpr::primitive_type agree_type;

	typedef UnaryExpression<TExpr, UnaryOp>	_Myt;

	typedef SimdHlpr<agree_type>				simd_hlpr;
	typedef typename simd_hlpr::simd_type		simd_type;
	typedef typename simd_hlpr::primitive_type	primitive_type;

	data_type	mcData;

	UnaryExpression(data_type&& inp_cLhs) :mcData(std::move(inp_cLhs)) {}

	UnaryExpression(const _Myt& inp_cBase) :mcData(inp_cBase.mcData) {}

	UnaryExpression(_Myt&& inp_cBase) :mcData(std::move(inp_cBase.mcData)) {}

	inline simd_type operator[](size_t inp_iIndx) const { return UnaryOp::operate(mcData[inp_iIndx]); }

	inline size_t size(void) const { return mcData.size(); }
};
template<typename TExpr, typename UnaryOp> struct UnaryExpression<TExpr, UnaryOp, false>
{
public:
	typedef TExpr	data_type;
	typedef UnaryOp	op_type;

	typedef typename TExpr::primitive_type agree_type;

	typedef UnaryExpression<TExpr, UnaryOp>	_Myt;

	typedef SimdHlpr<agree_type>				simd_hlpr;
	typedef typename simd_hlpr::simd_type		simd_type;
	typedef typename simd_hlpr::primitive_type	primitive_type;

	data_type	mcData;
	op_type		mcOp;

	template<typename ...Args> UnaryExpression(data_type&& inp_cLhs, const Args&... myArgs) :mcData(std::move(inp_cLhs)), mcOp(myArgs...) {}

	UnaryExpression(const _Myt& inp_cBase) :mcData(inp_cBase.mcData), mcOp(inp_cBase.mcOp) {}

	UnaryExpression(_Myt&& inp_cBase) :mcData(std::move(inp_cBase.mcData)), mcOp(std::move(inp_cBase.mcOp)) {}

	inline simd_type operator[](size_t inp_iIndx) const { return mcOp(mcData[inp_iIndx]); }

	inline size_t size(void) const { return mcData.size(); }
};

template<typename TData, typename UnaryOp> using ifExprUnary = ifExprLike<TData, UnaryExpression<typename ExprType<TData>::type, UnaryOp>>;

#define UNARY_EXPR(methodName, UnaryOp) \
	template<typename TInput> static inline typename ifExprUnary<TInput, UnaryOp<typename ExprType<TInput>::type::primitive_type>>::type methodName(const TInput& inp_Data) \
	{ \
		typedef typename ExprType<TInput>::type expression_type; \
		typedef typename expression_type::primitive_type primitive_type; \
		typedef UnaryExpression<expression_type, UnaryOp<primitive_type>> result_t; \
		return result_t(static_cast<expression_type>(inp_Data)); \
	}

namespace std
{
	UNARY_EXPR(abs, simd::unary::abs)
	UNARY_EXPR(cos, simd::unary::cos)
	UNARY_EXPR(exp, simd::unary::exp)
	UNARY_EXPR(ln, simd::unary::ln)
	UNARY_EXPR(sin, simd::unary::sin)
	UNARY_EXPR(sqrt, simd::unary::sqrt)
	UNARY_EXPR(tan, simd::unary::tan)

	template<typename TExpr, typename F> static inline typename ifExprUnary<TExpr, simd::unary::FunctorHlpr<typename ExprType<TExpr>::type::primitive_type, F>>::type transform(const TExpr& inp_Data, F inp_Fn)
	{
		typedef typename ExprType<TExpr>::type::primitive_type primitive_type;
		typedef typename ExprType<TExpr>::type expression_type;
		typedef UnaryExpression<expression_type, simd::unary::FunctorHlpr<primitive_type, F>> result_t;

		return result_t(static_cast<expression_type>(inp_Data), inp_Fn);
	}

	template<typename TExpr, typename TCoef> static inline typename ifExprUnary<TExpr, simd::unary::polynomial<typename ExprType<TExpr>::type::primitive_type, TCoef>>::type polynomial(const TExpr& inp_Data, const TCoef* inp_dCoefPtr, size_t inp_iSize)
	{
		typedef typename ExprType<TExpr>::type::primitive_type primitive_type;
		typedef typename ExprType<TExpr>::type expression_type;
		typedef UnaryExpression<expression_type, simd::unary::polynomial<primitive_type, TCoef>> result_t;

		return result_t(static_cast<expression_type>(inp_Data), inp_dCoefPtr, inp_iSize);
	}

	template<typename TExpr> static inline typename ifExprUnary<TExpr, simd::unary::pow<typename ExprType<TExpr>::type::primitive_type>>::type pow(const TExpr& inp_Data, const int& inp_iPower)
	{
		typedef typename ExprType<TExpr>::type::primitive_type primitive_type;
		typedef typename ExprType<TExpr>::type expression_type;
		typedef UnaryExpression<expression_type, simd::unary::pow<primitive_type>> result_t;

		return result_t(static_cast<expression_type>(inp_Data), inp_iPower);
	}

	template<typename TExpr, bool isExprLike = IsExprLike<TExpr>::value> struct pow_hlpr
	{
		typedef typename ExprType<TExpr>::type::primitive_type primitive_type;
		typedef primitive_type(*fn_t)(primitive_type, primitive_type);
	};
	template<typename TExpr> struct pow_hlpr<TExpr, false> {};

	template<typename TExpr> static inline auto pow(const TExpr& inp_Data, const typename pow_hlpr<TExpr>::primitive_type& inp_dPower) -> decltype(std::transform(inp_Data, MakeParameterized(static_cast<typename pow_hlpr<TExpr>::fn_t>(std::pow), inp_dPower)))
	{
		typedef typename pow_hlpr<TExpr>::fn_t fn_t;

		return std::transform(inp_Data, MakeParameterized(static_cast<fn_t>(std::pow), inp_dPower));
	}
}

// const expression
template<typename TData> struct ConstExpression
{
public:
	typedef ConstExpression<TData>	_Myt;

	typedef SimdHlpr<TData>						simd_hlpr;
	typedef typename simd_hlpr::simd_type		simd_type;
	typedef typename simd_hlpr::primitive_type	primitive_type;

	simd_type	mcSimd;
	size_t		miSize;

	ConstExpression(const primitive_type& inp_dValue, size_t inp_iSize) : mcSimd(simd_hlpr::load(inp_dValue)), miSize(inp_iSize) {}

	ConstExpression(const _Myt& inp_cBase) :mcSimd(inp_cBase.mcSimd), miSize(inp_cBase.miSize) {}

	ConstExpression(_Myt&& inp_cBase) :mcSimd(std::move(inp_cBase.mcSimd)), miSize(inp_cBase.miSize) {}

	~ConstExpression(void) {}

	inline simd_type operator[](size_t inp_iIndx) const { return mcSimd; }

	inline size_t size(void) const { return miSize; }
};

// binary expression
template<typename TLhs, typename BinaryOp, typename TRhs, bool hasOperate = simd::binary::HasOperate<BinaryOp, typename TLhs::primitive_type>::value> struct BinaryExpression
{
public:
	typedef TLhs		lhs_type;
	typedef TRhs		rhs_type;

	typedef typename std::enable_if<TypeCheck<typename TLhs::primitive_type, typename TRhs::primitive_type>::agree, typename TLhs::primitive_type>::type agree_type;

	typedef BinaryExpression<TLhs, BinaryOp, TRhs>	_Myt;

	typedef SimdHlpr<agree_type>				simd_hlpr;
	typedef typename simd_hlpr::simd_type		simd_type;
	typedef typename simd_hlpr::primitive_type	primitive_type;

	lhs_type	mcLhs;
	rhs_type	mcRhs;

	BinaryExpression(lhs_type&& inp_cLhs, rhs_type&& inp_cRhs) :mcLhs(std::move(inp_cLhs)), mcRhs(std::move(inp_cRhs)) {}

	BinaryExpression(const _Myt& inp_cBase) :mcLhs(inp_cBase.mcLhs), mcRhs(inp_cBase.mcRhs) {}

	BinaryExpression(_Myt&& inp_cBase) :mcLhs(std::move(inp_cBase.mcLhs)), mcRhs(std::move(inp_cBase.mcRhs)) {}

	inline simd_type operator[](size_t inp_iIndx) const { return BinaryOp::operate(mcLhs[inp_iIndx], mcRhs[inp_iIndx]); }

	inline size_t size(void) const { return std::min(mcLhs.size(), mcRhs.size()); }
};
template<typename TLhs, typename BinaryOp, typename TRhs> struct BinaryExpression<TLhs, BinaryOp, TRhs, false>
{
public:
	typedef TLhs		lhs_type;
	typedef TRhs		rhs_type;
	typedef BinaryOp	op_type;

	typedef typename std::enable_if<TypeCheck<typename TLhs::primitive_type, typename TRhs::primitive_type>::agree, typename TLhs::primitive_type>::type agree_type;

	typedef BinaryExpression<TLhs, BinaryOp, TRhs>	_Myt;

	typedef SimdHlpr<agree_type>				simd_hlpr;
	typedef typename simd_hlpr::simd_type		simd_type;
	typedef typename simd_hlpr::primitive_type	primitive_type;

	lhs_type	mcLhs;
	rhs_type	mcRhs;
	op_type		mcOp;

	template<typename ...Args> BinaryExpression(lhs_type&& inp_cLhs, rhs_type&& inp_cRhs, const Args&... myArgs) :mcLhs(std::move(inp_cLhs)), mcRhs(std::move(inp_cRhs)), mcOp(myArgs...) {}

	BinaryExpression(const _Myt& inp_cBase) :mcLhs(inp_cBase.mcLhs), mcRhs(inp_cBase.mcRhs), mcOp(inp_cBase.mcOp) {}

	BinaryExpression(_Myt&& inp_cBase) :mcLhs(std::move(inp_cBase.mcLhs)), mcRhs(std::move(inp_cBase.mcRhs)), mcOp(std::move(inp_cBase.mcOp)) {}

	inline simd_type operator[](size_t inp_iIndx) const { return mcOp(mcLhs[inp_iIndx], mcRhs[inp_iIndx]); }

	inline size_t size(void) const { return std::min(mcLhs.size(), mcRhs.size()); }
};

template<typename TLhs, typename TRhs, typename TResult> using ifExprAndConst = std::enable_if<IsExprCompatibleWithConst<TLhs, TRhs>::value, TResult>;
template<typename TLhs, typename TRhs, typename TResult> using ifConstAndExpr = std::enable_if<IsExprCompatibleWithConst<TRhs, TLhs>::value, TResult>;
template<typename TLhs, typename TRhs, typename TResult> using ifExprAndExpr = std::enable_if<AreExprsCompatible<TLhs, TRhs>::value, TResult>;

template<typename TLhs, typename BinaryOp, typename TRhs> using ifExprAndConstBinary = ifExprAndConst<TLhs, TRhs, BinaryExpression<typename ExprType<TLhs>::type, BinaryOp, ConstExpression<typename ExprType<TLhs>::type::primitive_type>>>;
template<typename TLhs, typename BinaryOp, typename TRhs> using ifConstAndExprBinary = ifConstAndExpr<TLhs, TRhs, BinaryExpression<ConstExpression<typename ExprType<TRhs>::type::primitive_type>, BinaryOp, typename ExprType<TRhs>::type>>;
template<typename TLhs, typename BinaryOp, typename TRhs> using ifExprAndExprBinary = ifExprAndExpr<TLhs, TRhs, BinaryExpression<typename ExprType<TLhs>::type, BinaryOp, typename ExprType<TRhs>::type>>;

#define BINARY_EXPR_EXPR(methodName, BinaryOp) \
	template<typename TLhs, typename TRhs> static inline typename ifExprAndExprBinary<TLhs, BinaryOp<typename ExprType<TLhs>::type::primitive_type>, TRhs>::type methodName(const TLhs& lhs, const TRhs& rhs) \
	{ \
		typedef typename ExprType<TLhs>::type::primitive_type primitive_type; \
		typedef typename ExprType<TLhs>::type lhs_type; \
		typedef typename ExprType<TRhs>::type rhs_type; \
		typedef BinaryExpression<lhs_type, BinaryOp<primitive_type>, rhs_type> result_t; \
		return result_t(static_cast<lhs_type>(lhs), static_cast<rhs_type>(rhs)); \
	}

#define BINARY_EXPR_CONST(methodName, BinaryOp) \
	template<typename TLhs, typename TRhs> static inline typename ifExprAndConstBinary<TLhs, BinaryOp<typename ExprType<TLhs>::type::primitive_type>, TRhs>::type methodName(const TLhs& lhs, const TRhs& rhs) \
	{ \
		typedef typename ExprType<TLhs>::type::primitive_type primitive_type; \
		typedef typename ExprType<TLhs>::type lhs_type; \
		typedef ConstExpression<primitive_type> rhs_type; \
		typedef BinaryExpression<lhs_type, BinaryOp<primitive_type>, rhs_type> result_t; \
		return result_t(static_cast<lhs_type>(lhs), rhs_type(rhs, lhs.size())); \
	}

#define BINARY_CONST_EXPR(methodName, BinaryOp) \
	template<typename TLhs, typename TRhs> static inline typename ifConstAndExprBinary<TLhs, BinaryOp<typename ExprType<TRhs>::type::primitive_type>, TRhs>::type methodName(const TLhs& lhs, const TRhs& rhs) \
	{ \
		typedef typename ExprType<TRhs>::type::primitive_type primitive_type; \
		typedef ConstExpression<primitive_type> lhs_type; \
		typedef typename ExprType<TRhs>::type rhs_type; \
		typedef BinaryExpression<lhs_type, BinaryOp<primitive_type>, rhs_type> result_t; \
		return result_t(lhs_type(lhs, rhs.size()), static_cast<rhs_type>(rhs)); \
	}

BINARY_EXPR_CONST(operator+, simd::binary::add)
BINARY_EXPR_CONST(operator/, simd::binary::div)
BINARY_EXPR_CONST(operator|, simd::binary::max)
BINARY_EXPR_CONST(operator&, simd::binary::min)
BINARY_EXPR_CONST(operator*, simd::binary::mul)
BINARY_EXPR_CONST(operator-, simd::binary::sub)

BINARY_CONST_EXPR(operator+, simd::binary::add)
BINARY_CONST_EXPR(operator/, simd::binary::div)
BINARY_CONST_EXPR(operator|, simd::binary::max)
BINARY_CONST_EXPR(operator&, simd::binary::min)
BINARY_CONST_EXPR(operator*, simd::binary::mul)
BINARY_CONST_EXPR(operator-, simd::binary::sub)

BINARY_EXPR_EXPR(operator+, simd::binary::add)
BINARY_EXPR_EXPR(operator/, simd::binary::div)
BINARY_EXPR_EXPR(operator|, simd::binary::max)
BINARY_EXPR_EXPR(operator&, simd::binary::min)
BINARY_EXPR_EXPR(operator*, simd::binary::mul)
BINARY_EXPR_EXPR(operator-, simd::binary::sub)

namespace std
{
	template<typename TExpr> static inline typename ifExprLike<TExpr, typename ExprType<TExpr>::type::primitive_type>::type sum(const TExpr& inp_Data)
	{
		typedef typename ExprType<TExpr>::type	expression_type;
		typedef typename expression_type::simd_hlpr		simd_hlpr;
		typedef typename expression_type::simd_type		simd_type;
		typedef typename expression_type::primitive_type	primitive_type;

		expression_type myExp(static_cast<expression_type>(inp_Data));

		primitive_type dPrim = primitive_type();
		simd_type dSimd(simd_hlpr::load());
		primitive_type* pLhs(reinterpret_cast<primitive_type*>(&dSimd));

		size_t iThis(0), iNext(iThis + simd_hlpr::STEP);
		while (iNext <= myExp.size())
		{
			simd_hlpr::store(pLhs, simd::binary::add<primitive_type>::operate(dSimd, myExp[iThis]));

			iThis = iNext;
			iNext += simd_hlpr::STEP;
		}

		for (size_t iIndx(0); iIndx < simd_hlpr::STEP; iIndx++) 
		{
			dPrim += pLhs[iIndx]; 
		}

		simd_hlpr::store(pLhs, myExp[iThis]);
		for (size_t iIndx(0); iIndx < myExp.size() - iThis; iIndx++)
		{
			dPrim += pLhs[iIndx];
		}

		return dPrim;
	}

	template<typename TLhs, typename TRhs> static inline typename ifExprAndExpr<TLhs, TRhs, typename ExprType<TLhs>::type::primitive_type>::type dot(const TLhs& lhs, const TRhs& rhs)
	{
		return sum(lhs * rhs);
	}

	template<typename TExpr> static inline typename ifExprLike<TExpr, typename ExprType<TExpr>::type::primitive_type>::type product(const TExpr& inp_Data)
	{
		typedef typename ExprType<TExpr>::type	expression_type;
		typedef typename expression_type::simd_hlpr		simd_hlpr;
		typedef typename expression_type::simd_type		simd_type;
		typedef typename expression_type::primitive_type	primitive_type;

		expression_type myExp(static_cast<expression_type>(inp_Data));

		primitive_type dPrim = 1;
		simd_type dSimd(simd_hlpr::load(1));
		primitive_type* pLhs(reinterpret_cast<primitive_type*>(&dSimd));

		size_t iThis(0), iNext(iThis + simd_hlpr::STEP);
		while (iNext <= myExp.size())
		{
			simd_hlpr::store(pLhs, simd::binary::mul<primitive_type>::operate(dSimd, myExp[iThis]));

			iThis = iNext;
			iNext += simd_hlpr::STEP;
		}

		for (size_t iIndx(0); iIndx < simd_hlpr::STEP; iIndx++) 
		{
			dPrim *= pLhs[iIndx]; 
		}

		simd_hlpr::store(pLhs, myExp[iThis]);
		for (size_t iIndx(0); iIndx < myExp.size() - iThis; iIndx++)
		{
			dPrim *= pLhs[iIndx];
		}

		return dPrim;
	}
}
#endif
