#ifndef INCLUDE_H_FACTORY
#define INCLUDE_H_FACTORY
#include "CommonDefs.h"
#ifdef WINDOWS
#pragma once
#endif // WINDOWS

#include "SmartMap.h"

template<typename TKey, typename TOut, typename ...TIn> struct Factory : public CSimpleMap<TKey, TOut(*)(TIn...)>
{
	typedef TOut(*_Myfunc)(TIn...);
	typedef Factory<TKey, TOut, TIn...>	_Myt;
	typedef BaseFactory<TKey, _Myfunc>	_Mybase;

	typedef typename _Mybase::key_type		key_type;
	typedef typename _Mybase::mapped_type		mapped_type;
	typedef typename _Mybase::size_type		size_type;
	typedef typename _Mybase::value_type		value_type;
	typedef typename _Mybase::difference_type		difference_type;
	typedef typename _Mybase::key_compare			key_compare;
	typedef typename _Mybase::allocator_type		allocator_type;
	typedef typename _Mybase::reference			reference;
	typedef typename _Mybase::const_reference		const_reference;
	typedef typename _Mybase::pointer			pointer;
	typedef typename _Mybase::const_pointer			const_pointer;
	typedef typename _Mybase::iterator			iterator;
	typedef typename _Mybase::const_iterator		const_iterator;
	typedef typename _Mybase::reverse_iterator		reverse_iterator;
	typedef typename _Mybase::const_reverse_iterator	const_reverse_iterator;

	Factory(void) :_Mybase() {}

	bool Create(TKey inp_Key, TOut& out_Result, TIn&&... inp_ConstructorData)
	{
		iterator itr(_Mybase::find(inp_Key));

		if (itr != _Mybase::cend()) return false;

		out_Result = (itr->second)(std::forward<TIn>(inp_ConstructorData)...);
		return true;
	}

	~Factory(void) {}
};

template<typename TKey, typename TOut> struct Factory<TKey, TOut> : public CSimpleMap<TKey, TOut(*)(void)>
{
	typedef TOut(*_Myfunc)(void);
	typedef Factory<TKey, TOut>			_Myt;
	typedef BaseFactory<TKey, _Myfunc>	_Mybase;

	typedef typename _Mybase::key_type		key_type;
	typedef typename _Mybase::mapped_type		mapped_type;
	typedef typename _Mybase::size_type		size_type;
	typedef typename _Mybase::value_type		value_type;
	typedef typename _Mybase::difference_type		difference_type;
	typedef typename _Mybase::key_compare			key_compare;
	typedef typename _Mybase::allocator_type		allocator_type;
	typedef typename _Mybase::reference			reference;
	typedef typename _Mybase::const_reference		const_reference;
	typedef typename _Mybase::pointer			pointer;
	typedef typename _Mybase::const_pointer			const_pointer;
	typedef typename _Mybase::iterator			iterator;
	typedef typename _Mybase::const_iterator		const_iterator;
	typedef typename _Mybase::reverse_iterator		reverse_iterator;
	typedef typename _Mybase::const_reverse_iterator	const_reverse_iterator;

	Factory(void) :_Mybase() {}

	bool Create(TKey inp_Key, TOut& out_Result)
	{
		iterator itr(_Mybase::find(inp_Key));

		if (itr != _Mybase::cend()) return false;

		out_Result = (itr->second)(); 
		return true;
	}

	~Factory(void) {}
};
#endif
