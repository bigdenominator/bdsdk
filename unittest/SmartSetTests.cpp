#ifndef INCLUDE_H_SMARTSETTESTS
#define INCLUDE_H_SMARTSETTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/SmartSet.h"

class SetTests : public testing::Test
{
public:
	enum :size_t { START = 0, SIZE = 20 };

	template<typename T> void Add(T& inp_cSet)
	{
		typedef typename T::key_type key_type;

		for (size_t i(START); i < SIZE; i++) 
		{
			inp_cSet.insert(static_cast<key_type>(i * 10)); 
		}
	}
};

TEST_F(SetTests, SimpleSet)
{
	typedef CSimpleSet<int32_t> test_type;
	test_type cBase;
	EXPECT_EQ(0, cBase.size());

	Add(cBase);
	EXPECT_EQ(SIZE, cBase.size());
	EXPECT_FALSE(cBase.insert(100).second);
	EXPECT_EQ(SIZE, cBase.size());

	EXPECT_TRUE(cBase.insert(200).second);
	EXPECT_EQ(SIZE + 1, cBase.size());

	EXPECT_TRUE(cBase.insert(-1.).second);
	EXPECT_EQ(SIZE + 2, cBase.size());

	EXPECT_FALSE(cBase.insert(-1).second);
	EXPECT_EQ(SIZE + 2, cBase.size());

	EXPECT_EQ(1, cBase.erase(-1));
	EXPECT_EQ(1, cBase.erase(200));
	EXPECT_EQ(SIZE, cBase.size());

	test_type cTest1;
	EXPECT_TRUE(cTest1.empty());
	EXPECT_EQ(0, cTest1.size());

	cTest1 = cBase;
	EXPECT_FALSE(cTest1.empty());
	EXPECT_EQ(cTest1.size(), cBase.size());

	for (auto itr = cBase.cbegin(); itr != cBase.cend(); itr++)
	{
		EXPECT_TRUE(cTest1.find(*itr) != cTest1.cend());
	}

	test_type cTest2(cBase);
	EXPECT_FALSE(cTest2.empty());
	EXPECT_EQ(cTest2.size(), cBase.size());

	FOREACH(cBase, itr)
	{
		EXPECT_TRUE(cTest2.find(*itr) != cTest2.cend());
	}
}

TEST_F(SetTests, foreach)
{
	typedef CSmartSet<double> MySet;

	MySet cBase;
	cBase.insert(1);
	cBase.insert(2);

	int iInst(1);
	auto foo = [&iInst](MySet::const_reference curr)
	{
		EXPECT_EQ(iInst, curr);

		iInst += 1;
	};

	cBase.foreach(foo);
}

TEST_F(SetTests, Iterator)
{
	typedef CSimpleSet<int32_t> test_type;

	CSmartPtr<test_type> cBasePtr(CreateSmart<test_type>());
	Add(*cBasePtr);

	auto itrBase(iterators::GetItr(cBasePtr));

	itrBase.begin();
	EXPECT_EQ(0, itrBase.key());
	EXPECT_EQ(0, itrBase.value());
	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase++;
	EXPECT_EQ(10, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase.last();
	EXPECT_EQ(190, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase++;
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_TRUE(itrBase.isEnd());

	itrBase--;
	EXPECT_EQ(190, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	for (itrBase.last(); !itrBase.isBegin(); itrBase--);
	EXPECT_EQ(0, itrBase.value());

	EXPECT_TRUE(itrBase.find(10));
	EXPECT_EQ(10, itrBase.value());

	EXPECT_FALSE(itrBase.find(200));
	EXPECT_TRUE(itrBase.isEnd());

	EXPECT_TRUE(itrBase.lower_bound(150));
	EXPECT_EQ(150, itrBase.key());

	EXPECT_TRUE(itrBase.lower_bound(-1));
	EXPECT_EQ(0, itrBase.key());

	EXPECT_FALSE(itrBase.lower_bound(200));

	EXPECT_TRUE(itrBase.upper_bound(150));
	EXPECT_EQ(160, itrBase.key());

	EXPECT_TRUE(itrBase.upper_bound(-1));
	EXPECT_EQ(0, itrBase.key());

	EXPECT_FALSE(itrBase.upper_bound(200));
};

TEST_F(SetTests, MultiSet)
{
	typedef CSimpleMultiSet<int32_t> test_type;

	CSmartPtr<test_type> cBasePtr(CreateSmart<test_type>());
	auto itrBase(iterators::GetItr(cBasePtr));

	EXPECT_EQ(0, cBasePtr->size());
	EXPECT_TRUE(cBasePtr->empty());
	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_TRUE(itrBase.isEnd());

	const int32_t	iIndxStart(0);
	const int32_t	iIndxMax(21);

	const int32_t	iRepStart(0);
	const int32_t	iRepCnt(2);

	int32_t			iEntryCnt(0);

	int32_t			iRepIndx;
	int32_t			iIndx;

	cBasePtr->insert(iIndxStart);	iEntryCnt++;						// Add one initial element - only add one so we can test for BEGIN

	for (iRepIndx = iRepStart; iRepIndx < iRepCnt; iRepIndx++)
	{
		for (iIndx = iIndxStart + 1; iIndx < iIndxMax; iIndx++)
		{
			cBasePtr->insert(iIndx); iEntryCnt++;
		}
	}
	cBasePtr->insert(iIndxMax); iEntryCnt++;	// Add one last element - only add one so we can test for END 

	// Test initial load conditions and size
	EXPECT_EQ(iEntryCnt, cBasePtr->size());

	// Test Extraction, reset to first element added
	itrBase.begin();
	iIndx = iIndxStart;

	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());
	EXPECT_EQ(iIndx, itrBase.value());

	for (itrBase++, iIndx++; iIndx < iIndxMax; iIndx++)
	{
		for (iRepIndx = iRepStart; iRepIndx < iRepCnt; iRepIndx++, itrBase++)
		{
			EXPECT_FALSE(itrBase.isBegin());
			EXPECT_FALSE(itrBase.isEnd());
			EXPECT_EQ(iIndx, itrBase.value());
		}
	}

	// Test for Last element
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());
	EXPECT_EQ(iIndx, itrBase.value());
	itrBase++;
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_TRUE(itrBase.isEnd());

	//Test MultiMap copy
	CSmartPtr<test_type> cTargetPtr(CreateSmart<test_type>());
	auto itrTarget(iterators::GetItr(cTargetPtr));

	EXPECT_TRUE(cTargetPtr->empty());
	EXPECT_EQ(0, cTargetPtr->size());

	*cTargetPtr = *cBasePtr;

	EXPECT_FALSE(cTargetPtr->empty());
	EXPECT_EQ(cTargetPtr->size(), cBasePtr->size());

	for (itrBase.begin(), itrTarget.begin(); !itrBase.isEnd(); itrBase++, itrTarget++)
	{
		EXPECT_EQ(itrBase.value(), itrTarget.value());
	}

	iIndx = iIndxStart;
	EXPECT_EQ(iIndx, itrBase.begin().value());
	EXPECT_TRUE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	itrBase++;
	iIndx++;
	EXPECT_EQ(iIndx, itrBase.value());
	EXPECT_FALSE(itrBase.isBegin());
	EXPECT_FALSE(itrBase.isEnd());

	for (itrBase.last(); !itrBase.isBegin(); itrBase--);
	EXPECT_EQ(iIndxStart, itrBase.value());

	EXPECT_TRUE(itrBase.find(10));
	EXPECT_EQ(10, itrBase.value());
	itrBase++;
	EXPECT_EQ(10, itrBase.value());			// When Repeat Count is 2 or greater

	EXPECT_TRUE(itrBase.lower_bound(15));
	EXPECT_EQ(15, itrBase.key());

	EXPECT_EQ(14, (--itrBase).value());
	EXPECT_TRUE(itrBase.find(10));
	EXPECT_EQ(10., itrBase.value());
	EXPECT_EQ(9, (--itrBase).value());
	itrBase++;
	EXPECT_EQ(10, itrBase.value());
	itrBase++;
	EXPECT_EQ(10, itrBase.value());
	itrBase++;
	EXPECT_EQ(11, itrBase.value());
	EXPECT_EQ(11, (++itrBase).value());
	EXPECT_EQ(12, (++itrBase).value());

	EXPECT_TRUE(itrBase.find(15));
	EXPECT_EQ(15, itrBase.value());

	EXPECT_EQ(cBasePtr->size() / 2, itrBase.last().key());
	EXPECT_EQ(iIndxStart, itrBase.begin().key());

	cTargetPtr->clear();
	EXPECT_TRUE(cTargetPtr->empty());
	EXPECT_EQ(0, cTargetPtr->size());
};

TEST_F(SetTests, SPUD)
{
	typedef CSmartSet<double> test_type;

	auto itrBase(iterators::GetItr(CreateSmart<test_type>())), itrTest1(iterators::GetItr(CreateSmart<test_type>())), itrTest2(iterators::GetItr(CreateSmart<test_type>()));

	Add(*itrBase.data());

	EXPECT_TRUE(CopyViaPack(*itrBase.data(), *itrTest1.data()));
	EXPECT_EQ(SIZE, itrTest1.data()->size());
	EXPECT_EQ(SIZE, itrBase.data()->size());

	test_type::key_type iIndx;
	for (iIndx = START, itrBase.begin(), itrTest1.begin(); !itrBase.isEnd(); iIndx++, itrBase++, itrTest1++)
	{
		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrBase.value());
		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrTest1.value());
	}

	EXPECT_TRUE(CopyViaSerial(*itrBase.data(), *itrTest2.data()));
	EXPECT_EQ(SIZE, itrTest2.data()->size());
	EXPECT_EQ(SIZE, itrBase.data()->size());

	for (iIndx = START, itrBase.begin(), itrTest2.begin(); !itrBase.isEnd(); iIndx++, itrBase++, itrTest2++)
	{
		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrBase.value());
		EXPECT_EQ(static_cast<double>(iIndx)* 10, itrTest2.value());
	}
}
#endif
