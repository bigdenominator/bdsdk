#ifndef INCLUDE_H_ALLSOLVERS
#define INCLUDE_H_ALLSOLVERS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "solvers/GridSearch.h"
#include "solvers/Integration.h"
#include "solvers/Interpolation.h"
#include "solvers/Roots.h"
#include "solvers/Simplex.h"

#endif