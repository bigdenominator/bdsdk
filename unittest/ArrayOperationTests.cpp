#ifndef INCLUDE_H_ARRAYOPERATIONSTESTS
#define INCLUDE_H_ARRAYOPERATIONSTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/SmartDate.h"
#include "containers/SmartArray.h"
#include "containers/SmartVector.h"

class PolynomialTest : public testing::Test
{
protected:
	const size_t miSize = 10;

	const double dTolerance = 0.00000001;

	CVectorDbl	mcInputData;
	CVectorDbl	mcOutputData;

public:
	void SetUp(void)
	{
		mcInputData.resize(miSize);
		for (size_t iIndx(0); iIndx < miSize; iIndx++)
		{
			mcInputData[iIndx] = static_cast<double>(iIndx);
		}
		mcOutputData.resize(miSize);
	}

	void TearDown(void) {}
};

TEST(SmartArrayTools, absDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cFrom(100, 2.0);
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (iIndx % 2 == 1) { cFrom[iIndx] = -1.0; }
	}

	CVectorDbl cTo(std::abs(cFrom));
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (cFrom[iIndx] < 0)
		{
			EXPECT_GT(dTolerance, fabs(cFrom[iIndx] + cTo[iIndx]));
		}
		else
		{
			EXPECT_GT(dTolerance, fabs(cFrom[iIndx] - cTo[iIndx]));
		}
	}
}

TEST(SmartArrayTools, absFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cFrom(100, 2.0);
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (iIndx % 2 == 1) { cFrom[iIndx] = -1.0; }
	}

	CVectorFlt cTo(std::abs(cFrom));
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (cFrom[iIndx] < 0)
		{
			EXPECT_GT(fTolerance, fabs(cFrom[iIndx] + cTo[iIndx]));
		}
		else
		{
			EXPECT_GT(fTolerance, fabs(cFrom[iIndx] - cTo[iIndx]));
		}
	}
}

TEST(SmartArrayTools, absInt32)
{
	CVectorInt cFrom(100, 2.0);
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (iIndx % 2 == 1) { cFrom[iIndx] = -1.0; }
	}

	CVectorInt cTo(std::abs(cFrom));
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (cFrom[iIndx] < 0)
		{
			EXPECT_EQ(-cFrom[iIndx], cTo[iIndx]);
		}
		else
		{
			EXPECT_EQ(cFrom[iIndx], cTo[iIndx]);
		}
	}
}

TEST(SmartArrayTools, absInt64)
{
	CVectorLng cFrom(100, 2.0);
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (iIndx % 2 == 1) { cFrom[iIndx] = -1.0; }
	}

	CVectorLng cTo(std::abs(cFrom));
	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		if (cFrom[iIndx] < 0)
		{
			EXPECT_EQ(-cFrom[iIndx], cTo[iIndx]);
		}
		else
		{
			EXPECT_EQ(cFrom[iIndx], cTo[iIndx]);
		}
	}
}


TEST(SmartArrayTools, addArrayDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	CVectorDbl cResult(104);

	cResult = cArray1 + cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(3.0 - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 + cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(4.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, addArrayFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	CVectorFlt cResult(104);

	cResult = cArray1 + cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(3.0f - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(0.0f - cResult[iIndx]));
	}

	cResult = cArray1 + cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(4.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, addArrayInt32)
{
	CVectorInt cArray1(104, 1), cArray2(100, 2), cArray3(104, 3);
	CVectorInt cResult(104);

	cResult = cArray1 + cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(3, cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 + cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(4, cResult[iIndx]);
	}
}

TEST(SmartArrayTools, addArrayInt64)
{
	CVectorLng cArray1(104, 1LL), cArray2(100, 2LL), cArray3(104, 3LL);
	CVectorLng cResult(104);

	cResult = cArray1 + cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(3LL, cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0LL, cResult[iIndx]);
	}

	cResult = cArray1 + cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(4LL, cResult[iIndx]);
	}
}


TEST(SmartArrayTools, addConstDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0);
	CVectorDbl cResult(cArray1 + 3.0);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(4.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, addConstFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f);
	CVectorFlt cResult(cArray1 + 3.0f);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(4.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, addConstInt32)
{
	CVectorInt cArray1(104, 1);
	CVectorInt cResult(cArray1 + 3);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(4, cResult[iIndx]);
	}
}

TEST(SmartArrayTools, addConstInt64)
{
	CVectorLng cArray1(104, 1LL);
	CVectorLng cResult(cArray1 + 3LL);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(4LL, cResult[iIndx]);
	}
}


TEST(SmartArrayTools, cosDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cFrom(100, 0.0);
	CVectorDbl cTo(std::cos(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(cTo[iIndx] - 1.0));
	}
}

TEST(SmartArrayTools, cosFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cFrom(100, 0.0);
	CVectorFlt cTo(std::cos(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(cTo[iIndx] - 1.0f));
	}
}


TEST(SmartArrayTools, divideArrayDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	CVectorDbl cResult(104);

	cResult = cArray1 / cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(1.0 / 2.0 - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 / cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(1.0 / 3.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, divideArrayFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	CVectorFlt cResult(104);

	cResult = cArray1 / cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(1.0f / 2.0f - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(0.0f - cResult[iIndx]));
	}

	cResult = cArray1 / cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(1.0f / 3.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, divideConstDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0);
	CVectorDbl cResult(cArray1 / 3.0);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(1.0 / 3.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, divideConstFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f);
	CVectorFlt cResult(cArray1 / 3.0f);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(1.0f / 3.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, divideIntoConstDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 3.0);
	CVectorDbl cResult(1.0 / cArray1);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(1.0 / 3.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, divideIntoConstFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 3.f);
	CVectorFlt cResult(1.0f / cArray1);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(1.0f / 3.0f - cResult[iIndx]));
	}
}


TEST(SmartArrayTools, dotProductDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	
	EXPECT_GT(dTolerance, fabs(std::dot(cArray1, cArray2) - 2.0 * 100.0));
	EXPECT_GT(dTolerance, fabs(std::dot(cArray1, cArray3) - 3.0 * 104.0));
}

TEST(SmartArrayTools, dotProductFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	
	EXPECT_GT(fTolerance, fabs(std::dot(cArray1, cArray2) - 2.0f * 100.0f));
	EXPECT_GT(fTolerance, fabs(std::dot(cArray1, cArray3) - 3.0f * 104.0f));
}

TEST(SmartArrayTools, dotProductInt32)
{
	CVectorInt cArray1(104, 1), cArray2(100, 2), cArray3(104, 3);

	EXPECT_EQ(2 * 100, std::dot(cArray1, cArray2));
	EXPECT_EQ(3 * 104, std::dot(cArray1, cArray3));
}

TEST(SmartArrayTools, dotProductInt64)
{
	CVectorLng cArray1(104, 1LL), cArray2(100, 2LL), cArray3(104, 3LL);

	EXPECT_EQ(2 * 100, std::dot(cArray1, cArray2));
	EXPECT_EQ(3 * 104, std::dot(cArray1, cArray3));
}


TEST(SmartArrayTools, expDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cFrom(100, 1.0);
	CVectorDbl cTo(std::exp(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(cTo[iIndx]) - dEXP);
	}
}

TEST(SmartArrayTools, expFlt)
{
	const float fTolerance(0.000001f);

	CVectorFlt cFrom(100, 1.0);
	CVectorFlt cTo(std::exp(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(cTo[iIndx]) - fEXP);
	}
}


TEST(SmartArrayTools, lnDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cFrom(100, dEXP);
	CVectorDbl cTo(std::ln(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(cTo[iIndx]) - 1.0);
	}
}

TEST(SmartArrayTools, lnFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cFrom(100, dEXP);
	CVectorFlt cTo(std::ln(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(cTo[iIndx]) - 1.0f);
	}
}


TEST(SmartArrayTools, maxDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	CVectorDbl cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2 == 1)
		{
			cArray3[iIndx] = -1.0;
			if (iIndx < 100) { cArray2[iIndx] = -1.0; }
		}
	}

	cResult = cArray1 | cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(max(cArray1[iIndx], cArray2[iIndx]) - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 | cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(max(cArray1[iIndx], cArray3[iIndx]) - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, maxFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	CVectorFlt cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2 == 1)
		{
			cArray3[iIndx] = -1.0f;
			if (iIndx < 100) { cArray2[iIndx] = -1.0f; }
		}
	}

	cResult = cArray1 | cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(max(cArray1[iIndx], cArray2[iIndx]) - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 | cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(max(cArray1[iIndx], cArray3[iIndx]) - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, maxInt32)
{
	CVectorInt cArray1(104, 1), cArray2(100, 2), cArray3(104, 3);
	CVectorInt cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2 == 1)
		{
			cArray3[iIndx] = -1;
			if (iIndx < 100) { cArray2[iIndx] = -1; }
		}
	}

	cResult = cArray1 | cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(max(cArray1[iIndx], cArray2[iIndx]), cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 | cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(max(cArray1[iIndx], cArray3[iIndx]), cResult[iIndx]);
	}
}

TEST(SmartArrayTools, maxInt64)
{
	CVectorLng cArray1(104, 1LL), cArray2(100, 2LL), cArray3(104, 3LL);
	CVectorLng cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2LL == 1LL)
		{
			cArray3[iIndx] = -1LL;
			if (iIndx < 100) { cArray2[iIndx] = -1LL; }
		}
	}

	cResult = cArray1 | cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(max(cArray1[iIndx], cArray2[iIndx]), cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 | cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(max(cArray1[iIndx], cArray3[iIndx]), cResult[iIndx]);
	}
}


TEST(SmartArrayTools, minDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	CVectorDbl cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2 == 1)
		{
			cArray3[iIndx] = -1.0;
			if (iIndx < 100) { cArray2[iIndx] = -1.0; }
		}
	}

	cResult = cArray1 & cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(min(cArray1[iIndx], cArray2[iIndx]) - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 & cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(min(cArray1[iIndx], cArray3[iIndx]) - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, minFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	CVectorFlt cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2 == 1)
		{
			cArray3[iIndx] = -1.0f;
			if (iIndx < 100) { cArray2[iIndx] = -1.0f; }
		}
	}

	cResult = cArray1 & cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(min(cArray1[iIndx], cArray2[iIndx]) - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 & cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(min(cArray1[iIndx], cArray3[iIndx]) - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, minInt32)
{
	CVectorInt cArray1(104, 1), cArray2(100, 2), cArray3(104, 3);
	CVectorInt cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2 == 1)
		{
			cArray3[iIndx] = -1;
			if (iIndx < 100) { cArray2[iIndx] = -1; }
		}
	}

	cResult = cArray1 & cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(min(cArray1[iIndx], cArray2[iIndx]), cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 & cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(min(cArray1[iIndx], cArray3[iIndx]), cResult[iIndx]);
	}
}

TEST(SmartArrayTools, minInt64)
{
	CVectorLng cArray1(104, 1LL), cArray2(100, 2LL), cArray3(104, 3LL);
	CVectorLng cResult(104);

	for (size_t iIndx(0); iIndx < 104; iIndx++)
	{
		if (iIndx % 2LL == 1LL)
		{
			cArray3[iIndx] = -1LL;
			if (iIndx < 100) { cArray2[iIndx] = -1LL; }
		}
	}

	cResult = cArray1 & cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(min(cArray1[iIndx], cArray2[iIndx]), cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 & cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(min(cArray1[iIndx], cArray3[iIndx]), cResult[iIndx]);
	}
}


TEST(SmartArrayTools, multiplyArrayDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	CVectorDbl cResult(104);

	cResult = cArray1 * cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(2.0 - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 * cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(3.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, multiplyArrayFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	CVectorFlt cResult(104);

	cResult = cArray1 * cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(2.0f - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(0.0f - cResult[iIndx]));
	}

	cResult = cArray1 * cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(3.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, multiplyArrayInt32)
{
	CVectorInt cArray1(104, 1), cArray2(100, 2), cArray3(104, 3);
	CVectorInt cResult(104);

	cResult = cArray1 * cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(2, cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 * cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(3, cResult[iIndx]);
	}
}


TEST(SmartArrayTools, multiplyConstDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0);
	CVectorDbl cResult(cArray1 * 3.0);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(3.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, multiplyConstFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f);
	CVectorFlt cResult(cArray1 * 3.f);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(3.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, multiplyConstInt32)
{
	CVectorInt cArray1(104, 1);
	CVectorInt cResult(cArray1 * 3);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(3, cResult[iIndx]);
	}
}


TEST_F(PolynomialTest, SmartArray)
{
	double coef[] = { 8.0 * dPI, 4.0 * dPI, 2.0 * dPI, 1.0 * dPI };

	mcOutputData = std::polynomial(mcInputData, coef, 4);
	for (CArrayDbl<4>::size_type iIndx(0); iIndx < miSize; iIndx++)
	{
		EXPECT_GE(dTolerance, fabs(mcOutputData[iIndx] - CMath::polynomial(mcInputData[iIndx], coef, 4)));
	}
}

TEST_F(PolynomialTest, SmartVector)
{
	double coef[] = { 8.0 * dPI, 4.0 * dPI, 2.0 * dPI, 1.0 * dPI };

	mcOutputData = std::polynomial(mcInputData, coef, 4);
	for (size_t iIndx(0); iIndx < miSize; iIndx++)
	{
		EXPECT_GE(dTolerance, fabs(mcOutputData[iIndx] - CMath::polynomial(mcInputData[iIndx], coef, 4)));
	}
}


TEST(SmartArrayTools, powDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, dEXP);
	CVectorDbl cResult(std::pow(cArray1, 2));

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(dEXP * dEXP - cResult[iIndx]));
	}

	cArray1 = dTWOPI;
	cResult = std::pow(cArray1, 0.5);
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(dSQRT2PI - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, powFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, fEXP);
	CVectorFlt cResult(std::pow(cArray1, 2));

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(fEXP * fEXP - cResult[iIndx]));
	}

	cArray1 = fTWOPI;
	cResult = std::pow(cArray1, 0.5f);
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(fSQRT2PI - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, powInt32)
{
	CVectorInt cArray1(104, 2);
	CVectorInt cResult(std::pow(cArray1, 3));

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(8, cResult[iIndx]);
	}
}

TEST(SmartArrayTools, powInt64)
{
	CVectorLng cArray1(104, 2);
	CVectorLng cResult(std::pow(cArray1, 3));

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(8, cResult[iIndx]);
	}
}


TEST(SmartArrayTools, sinDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cFrom(100, 0.5 * dPI);
	CVectorDbl cTo(std::sin(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(cTo[iIndx] - 1.0));
	}
}

TEST(SmartArrayTools, sinFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cFrom(100, 0.5f * fPI);
	CVectorFlt cTo(std::sin(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(cTo[iIndx] - 1.0f));
	}
}


TEST(SmartArrayTools, sortDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cTest(10);
	cTest[0] = 3.0;
	cTest[1] = 6.0;
	cTest[2] = 9.0;
	cTest[3] = 2.0;
	cTest[4] = 5.0;
	cTest[5] = 8.0;
	cTest[6] = 1.0;
	cTest[7] = 4.0;
	cTest[8] = 7.0;
	cTest[9] = 0.0;

	cTest.sort();
	EXPECT_GT(dTolerance, fabs(cTest[0] - 0.0));
	EXPECT_GT(dTolerance, fabs(cTest[1] - 1.0));
	EXPECT_GT(dTolerance, fabs(cTest[2] - 2.0));
	EXPECT_GT(dTolerance, fabs(cTest[3] - 3.0));
	EXPECT_GT(dTolerance, fabs(cTest[4] - 4.0));
	EXPECT_GT(dTolerance, fabs(cTest[5] - 5.0));
	EXPECT_GT(dTolerance, fabs(cTest[6] - 6.0));
	EXPECT_GT(dTolerance, fabs(cTest[7] - 7.0));
	EXPECT_GT(dTolerance, fabs(cTest[8] - 8.0));
	EXPECT_GT(dTolerance, fabs(cTest[9] - 9.0));
}

TEST(SmartArrayTools, sortFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cTest(10);
	cTest[0] = 3.0f;
	cTest[1] = 6.0f;
	cTest[2] = 9.0f;
	cTest[3] = 2.0f;
	cTest[4] = 5.0f;
	cTest[5] = 8.0f;
	cTest[6] = 1.0f;
	cTest[7] = 4.0f;
	cTest[8] = 7.0f;
	cTest[9] = 0.0f;

	cTest.sort();
	EXPECT_GT(fTolerance, fabs(cTest[0] - 0.0f));
	EXPECT_GT(fTolerance, fabs(cTest[1] - 1.0f));
	EXPECT_GT(fTolerance, fabs(cTest[2] - 2.0f));
	EXPECT_GT(fTolerance, fabs(cTest[3] - 3.0f));
	EXPECT_GT(fTolerance, fabs(cTest[4] - 4.0f));
	EXPECT_GT(fTolerance, fabs(cTest[5] - 5.0f));
	EXPECT_GT(fTolerance, fabs(cTest[6] - 6.0f));
	EXPECT_GT(fTolerance, fabs(cTest[7] - 7.0f));
	EXPECT_GT(fTolerance, fabs(cTest[8] - 8.0f));
	EXPECT_GT(fTolerance, fabs(cTest[9] - 9.0f));
}

TEST(SmartArrayTools, sortInt32)
{
	CVectorInt cTest(10);
	cTest[0] = 3;
	cTest[1] = 6;
	cTest[2] = 9;
	cTest[3] = 2;
	cTest[4] = 5;
	cTest[5] = 8;
	cTest[6] = 1;
	cTest[7] = 4;
	cTest[8] = 7;
	cTest[9] = 0;

	cTest.sort();
	EXPECT_EQ(0, cTest[0]);
	EXPECT_EQ(1, cTest[1]);
	EXPECT_EQ(2, cTest[2]);
	EXPECT_EQ(3, cTest[3]);
	EXPECT_EQ(4, cTest[4]);
	EXPECT_EQ(5, cTest[5]);
	EXPECT_EQ(6, cTest[6]);
	EXPECT_EQ(7, cTest[7]);
	EXPECT_EQ(8, cTest[8]);
	EXPECT_EQ(9, cTest[9]);
}

TEST(SmartArrayTools, sortInt64)
{
	CVectorLng cTest(10);
	cTest[0] = 3LL;
	cTest[1] = 6LL;
	cTest[2] = 9LL;
	cTest[3] = 2LL;
	cTest[4] = 5LL;
	cTest[5] = 8LL;
	cTest[6] = 1LL;
	cTest[7] = 4LL;
	cTest[8] = 7LL;
	cTest[9] = 0LL;

	cTest.sort();
	EXPECT_EQ(0LL, cTest[0]);
	EXPECT_EQ(1LL, cTest[1]);
	EXPECT_EQ(2LL, cTest[2]);
	EXPECT_EQ(3LL, cTest[3]);
	EXPECT_EQ(4LL, cTest[4]);
	EXPECT_EQ(5LL, cTest[5]);
	EXPECT_EQ(6LL, cTest[6]);
	EXPECT_EQ(7LL, cTest[7]);
	EXPECT_EQ(8LL, cTest[8]);
	EXPECT_EQ(9LL, cTest[9]);
}


TEST(SmartArrayTools, sqrtDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cFrom(100, 2.0 * dPI);
	CVectorDbl cTo(std::sqrt(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(cTo[iIndx] - dSQRT2PI));
	}
}

TEST(SmartArrayTools, sqrtFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cFrom(100, 2.0f * fPI);
	CVectorFlt cTo(std::sqrt(cFrom));

	for (size_t iIndx(0); iIndx < 100; iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(cTo[iIndx] - fSQRT2PI));
	}
}


TEST(SmartArrayTools, subtractArrayDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0), cArray2(100, 2.0), cArray3(104, 3.0);
	CVectorDbl cResult(104);

	cResult = cArray1 - cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(-1.0 - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(0.0 - cResult[iIndx]));
	}

	cResult = cArray1 - cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(-2.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, subtractArrayFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f), cArray2(100, 2.f), cArray3(104, 3.f);
	CVectorFlt cResult(104);

	cResult = cArray1 - cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(-1.0f - cResult[iIndx]));
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(0.0f - cResult[iIndx]));
	}

	cResult = cArray1 - cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(-2.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, subtractArrayInt32)
{
	CVectorInt cArray1(104, 1), cArray2(100, 2), cArray3(104, 3);
	CVectorInt cResult(104);

	cResult = cArray1 - cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(-1, cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0, cResult[iIndx]);
	}

	cResult = cArray1 - cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(-2, cResult[iIndx]);
	}
}

TEST(SmartArrayTools, subtractArrayInt64)
{
	CVectorLng cArray1(104, 1LL), cArray2(100, 2LL), cArray3(104, 3LL);
	CVectorLng cResult(104);

	cResult = cArray1 - cArray2;
	for (size_t iIndx(0); iIndx < cArray2.size(); iIndx++)
	{
		EXPECT_EQ(-1LL, cResult[iIndx]);
	}
	for (size_t iIndx(cArray2.size()); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(0LL, cResult[iIndx]);
	}

	cResult = cArray1 - cArray3;
	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(-2LL, cResult[iIndx]);
	}
}


TEST(SmartArrayTools, subtractConstDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 1.0);
	CVectorDbl cResult(cArray1 - 3.0);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(-2.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, subtractConstFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 1.f);
	CVectorFlt cResult(cArray1 - 3.0f);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(-2.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, subtractConstInt32)
{
	CVectorInt cArray1(104, 1);
	CVectorInt cResult(cArray1 - 3);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(-2, cResult[iIndx]);
	}
}

TEST(SmartArrayTools, subtractConstInt64)
{
	CVectorLng cArray1(104, 1LL);
	CVectorLng cResult(cArray1 - 3LL);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(-2LL, cResult[iIndx]);
	}
}


TEST(SmartArrayTools, subtractIntoConstDbl)
{
	const double dTolerance(0.0000001);

	CVectorDbl cArray1(104, 3.0);
	CVectorDbl cResult(1.0 - cArray1);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(dTolerance, fabs(-2.0 - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, subtractIntoConstFlt)
{
	const float fTolerance(0.0000001f);

	CVectorFlt cArray1(104, 3.f);
	CVectorFlt cResult(1.0f - cArray1);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_GT(fTolerance, fabs(-2.0f - cResult[iIndx]));
	}
}

TEST(SmartArrayTools, subtractIntoConstInt32)
{
	CVectorInt cArray1(104, 3);
	CVectorInt cResult(1 - cArray1);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(-2, cResult[iIndx]);
	}
}

TEST(SmartArrayTools, subtractIntoConstInt64)
{
	CVectorLng cArray1(104, 3LL);
	CVectorLng cResult(1LL - cArray1);

	for (size_t iIndx(0); iIndx < cResult.size(); iIndx++)
	{
		EXPECT_EQ(-2LL, cResult[iIndx]);
	}
}
#endif
