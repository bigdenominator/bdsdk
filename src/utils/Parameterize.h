#ifndef INCLUDE_H_PARAMETERIZE
#define INCLUDE_H_PARAMETERIZE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <functional>

using namespace std::placeholders;

template<typename F, typename ...TParams> auto MakeParameterized(F&& inp_Fn, TParams&&... Args) -> decltype(std::bind(std::forward<F>(inp_Fn), _1, std::forward<TParams>(Args)...))
{
	return std::bind(std::forward<F>(inp_Fn), _1, std::forward<TParams>(Args)...);
}
#endif