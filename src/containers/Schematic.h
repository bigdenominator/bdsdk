#ifndef INCLUDE_H_SCHEMATIC
#define INCLUDE_H_SCHEMATIC

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/SmartContainers.h"

#define SIMPLESTRUCT(name, members) \
	struct name \
	{ \
	public: \
		typedef name	_Myt; \
		DUMMY members; \
		name(void) {} \
		name(const _Myt&) = default; \
		name(_Myt&&) = default; \
		inline _Myt& operator =(const _Myt&) = default; \
		inline _Myt& operator =(_Myt&&) = default; \
	}; \
	typedef CSmartPtr<name> name##Ptr;

#define SMARTSTRUCT(name, members, toSPUD) \
	struct name : public CContainer<FRM_TYPELIST::NONSCHEMATIC>\
	{ \
	public: \
		typedef CContainer<FRM_TYPELIST::NONSCHEMATIC> _Mybase; \
		typedef name	_Myt; \
		DUMMY members; \
		name(void) {} \
		name(const _Myt&) = default; \
		name(_Myt&&) = default; \
		inline _Myt& operator =(const _Myt&) = default; \
		inline _Myt& operator =(_Myt&&) = default; \
	protected: \
		SPUD toSPUD \
	}; \
	typedef CSmartPtr<name> name##Ptr;

#define SCHEMATIC(name, typeId, members, toSPUD) \
	struct name; \
	struct name : public CContainer<typeId> \
	{ \
	public: \
		typedef CContainer<typeId> _Mybase; \
		typedef name	_Myt; \
		DUMMY members; \
		name(void) :_Mybase() {} \
		name(const _Myt&) = default; \
		name(_Myt&&) = default; \
		inline _Myt& operator =(const _Myt&) = default; \
		inline _Myt& operator =(_Myt&&) = default; \
	protected: \
		SPUD toSPUD \
	}; \
typedef CSmartPtr<name> name##Ptr; 
#endif
