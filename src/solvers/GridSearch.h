#ifndef INCLUDE_H_GRIDSEARCH
#define INCLUDE_H_GRIDSEARCH

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/all.h"

template <typename TData> using CGridDim = CSmartVector<TData>;
template <typename TData, size_t iDIMS> using CGridDims = CSmartArray <CGridDim<TData>, iDIMS >;
template <typename TPkt, size_t iDIM> struct gridHelper
{
	typedef typename TPkt::dim_size_type size_type;

	enum :uint32_t { iNEXT = iDIM - 1 };

	static inline void loop(TPkt& inp_sPkt)
	{
		const size_type iTrials(inp_sPkt.mcGridSpecs[iNEXT].size());
		for (size_type iTrial(0); iTrial < iTrials; iTrial++)
		{
			inp_sPkt.mcTest[iNEXT] = inp_sPkt.mcGridSpecs[iNEXT][iTrial];
			gridHelper<TPkt, iNEXT>::loop(inp_sPkt);
		}
	}
};
template <typename TPkt> struct gridHelper<TPkt, 0>
{
	typedef typename TPkt::dim_size_type size_type;

	static inline void loop(TPkt& inp_sPkt)
	{
		auto dTest(inp_sPkt.mcObjFn(inp_sPkt.mcTest));
		if (dTest < inp_sPkt.mdMinValue)
		{
			inp_sPkt.mdMinValue = dTest;
			inp_sPkt.mcMin = inp_sPkt.mcTest;
		}
	}
};

template <typename Func, typename TInput, typename TReturn, size_t iDims> struct SGridSearch 
{
public:
	typedef SGridSearch<Func, TInput, TReturn, iDims>	_Myt;

	typedef Func	function_type;
	typedef TInput	input_type;
	typedef TReturn	return_type;

	typedef typename input_type::value_type	value_type;

	typedef CGridDim<value_type>			grid_dim;
	typedef CGridDims<value_type, iDims>	grid;

	typedef typename grid_dim::size_type	dim_size_type;
	typedef typename grid::size_type		grid_size_type;

	enum :grid_size_type { DIMS = iDims };

	const grid&		mcGridSpecs;

	function_type	mcObjFn;

	input_type	mcMin;
	input_type	mcTest;

	return_type	mdMinValue;

	SGridSearch(function_type inp_ObjFn, const grid& inp_cGridSpecs) 
		:mcObjFn(inp_ObjFn), mcGridSpecs(inp_cGridSpecs), mcMin(DIMS), mcTest(DIMS)
	{
		for (grid_size_type iIndx(0); iIndx < DIMS; iIndx++)
		{
			mcMin[iIndx] = mcGridSpecs[iIndx][0];
		}
		mdMinValue = this->mcObjFn(mcMin);

		gridHelper<_Myt, DIMS>::loop(*this);
	}
};

namespace Solvers
{
	template<typename TData> static inline void BuildGridDim(const TData& inp_Low, const TData& inp_High, CGridDim<TData>& inp_cGridDim)
	{
		typedef CGridDim<TData>					dim_type;
		typedef typename dim_type::size_type	size_type;
		typedef typename dim_type::value_type	value_type;

		const size_type lSteps(inp_cGridDim.size() - 1);
		const value_type dStep((inp_High - inp_Low) / static_cast<value_type>(lSteps));
		value_type dCurrent(inp_Low);

		for (size_type iIndx(0); iIndx < lSteps; iIndx++, dCurrent += dStep)
		{
			inp_cGridDim[iIndx] = dCurrent;
		}
		inp_cGridDim[lSteps] = inp_High;
	}

	template<typename TFunc, typename TInput, size_t iDims> static inline
		auto SearchGrid(TFunc inp_ObjFn, CGridDims<typename TInput::value_type, iDims>& inp_cGridSpecs, TInput& arg_min) -> decltype(inp_ObjFn(arg_min))
	{
		typedef decltype(inp_ObjFn(arg_min)) return_t;
		typedef SGridSearch<TFunc, TInput, return_t, iDims> grid_t;

		CUniquePtr<grid_t> gridPtr(CreateUnique<grid_t>(inp_ObjFn, inp_cGridSpecs));
		arg_min = gridPtr->mcMin;

		return gridPtr->mdMinValue;
	}
}
#endif
