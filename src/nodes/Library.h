#ifndef INCLUDE_H_LIBRARYTOOLS
#define INCLUDE_H_LIBRARYTOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/all.h"
#include "nodes/BaseNode.h"
#include "nodes/ITechnology.h"

// Nodes
typedef CSmartPtr<CBaseNode> CNodePtr;
typedef CNodePtr(*fNodeFactory)(void);
typedef CSmartSet<TYPELIST_t> CTypeLst;
typedef CSmartPtr<CTypeLst> CTypeLstPtr;

SMARTSTRUCT(SNodeSpec, (fNodeFactory mfNodeFactory; CTypeLstPtr mcRequestsPtr; CTypeLstPtr mcSendsPtr; CTypeLstPtr mcViewsPtr;), (mcRequestsPtr, mcSendsPtr, mcViewsPtr))

// Externals
typedef CUniquePtr<ITechnology> CTechPtr;
typedef CTechPtr(*fTechFactory)(void);

// Library
typedef CSmartMap<TYPELIST_t, SNodeSpecPtr> CNodeSpecLst;
typedef CSimpleMap<TECHNOLOGY_t, fTechFactory> CTechSpecLst;
typedef CSimpleMap<TYPELIST_t, fnSchematic> CSchemaFnLst;

typedef CSmartPtr<CNodeSpecLst> CNodeSpecLstPtr;
typedef CSmartPtr<CTechSpecLst> CTechSpecLstPtr;
typedef CSmartPtr<CSchemaFnLst> CSchemaFnLstPtr;

SIMPLESTRUCT(SLibBootstrap, (string msLicense; CEvntHndlrIntrPtr mcEvntHndlrPtr; CNodeSpecLstPtr mcNodeSpecLstPtr; CTechSpecLstPtr mcTechSpecLstPtr; CSchemaFnLstPtr mcSchemaFnLstPtr;));
typedef SLibBootstrap*	SLibBootstrapRwPtr;
typedef int(*fLibEntry)(void*);

namespace LibraryEnv
{
	template<typename T> static void addType(CTypeLstPtr inp_cTypeLstPtr)
	{
		inp_cTypeLstPtr->insert(T::TypeId);
	}
	template<typename TList> struct TypeLstHlpr
	{
		typedef typename TList::Head	Head;
		typedef typename TList::Tail	Tail;

		static void add(CTypeLstPtr inp_cTypeLstPtr)
		{
			addType<Head>(inp_cTypeLstPtr);
			TypeLstHlpr<Tail>::add(inp_cTypeLstPtr);
		}
	};
	template<> struct TypeLstHlpr<TypeList<>>
	{
		static void add(CTypeLstPtr) {}
	};

	template<typename T> static void addTypeFn(CSchemaFnLstPtr inp_cSchemaFnLstPtr)
	{
		fnSchematic foo = [](SBlackBoxPtr& inp_cBlackBoxPtr) -> CContainerIntrPtr
		{
			CSmartPtr<T> tmpPtr(CreateSmart<T>());
			inp_cBlackBoxPtr = CreateUnique<SGreyBox<T>>(tmpPtr);

			return reinterpret_cast<CContainerIntrPtr>(tmpPtr.get());
		};

		inp_cSchemaFnLstPtr->add(T::TypeId, foo);
	}
	template<typename TList> struct TypeFnLstHlpr
	{
		typedef typename TList::Head	Head;
		typedef typename TList::Tail	Tail;

		static void add(CSchemaFnLstPtr inp_cSchemaFnLstPtr)
		{
			addTypeFn<Head>(inp_cSchemaFnLstPtr);
			TypeFnLstHlpr<Tail>::add(inp_cSchemaFnLstPtr);
		}
	};
	template<> struct TypeFnLstHlpr<TypeList<>>
	{
		static void add(CSchemaFnLstPtr) {}
	};

	template<typename TNode> static void addSchemas(CSchemaFnLstPtr inp_cSchemaFnLstPtr)
	{
		TypeFnLstHlpr<TypeList<typename TNode::SSchematic>>::add(inp_cSchemaFnLstPtr);
		TypeFnLstHlpr<typename TNode::_Myrequests>::add(inp_cSchemaFnLstPtr);
		TypeFnLstHlpr<typename TNode::_Mysends>::add(inp_cSchemaFnLstPtr);
		TypeFnLstHlpr<typename TNode::_Myviews>::add(inp_cSchemaFnLstPtr);
	}
	template<typename TList> struct SchemaFnLstHlpr
	{
		typedef typename TList::Head	Head;
		typedef typename TList::Tail	Tail;

		static void add(CSchemaFnLstPtr inp_cSchemaFnLstPtr)
		{
			addSchemas<Head>(inp_cSchemaFnLstPtr);
			SchemaFnLstHlpr<Tail>::add(inp_cSchemaFnLstPtr);
		}
	};
	template<> struct SchemaFnLstHlpr<TypeList<>>
	{
		static void add(CSchemaFnLstPtr) {}
	};

	template<typename TNode> static void addNode(CNodeSpecLstPtr inp_cNodeSpecLstPtr)
	{
		SNodeSpecPtr sSpecPtr(CreateSmart<SNodeSpec>());

		sSpecPtr->mfNodeFactory = [](void) -> CNodePtr { return CreateSmart<TNode>(); };
		sSpecPtr->mcRequestsPtr = CreateSmart<CTypeLst>();
		sSpecPtr->mcSendsPtr = CreateSmart<CTypeLst>();
		sSpecPtr->mcViewsPtr = CreateSmart<CTypeLst>();

		TypeLstHlpr<typename TNode::_Myrequests>::add(sSpecPtr->mcRequestsPtr);
		TypeLstHlpr<typename TNode::_Mysends>::add(sSpecPtr->mcSendsPtr);
		TypeLstHlpr<typename TNode::_Myviews>::add(sSpecPtr->mcViewsPtr);

		inp_cNodeSpecLstPtr->add(TNode::TypeId, sSpecPtr);
	}
	template<typename TList> struct NodeSpecLstHlpr
	{
		typedef typename TList::Head	Head;
		typedef typename TList::Tail	Tail;

		static void add(CNodeSpecLstPtr inp_cNodeSpecLstPtr)
		{
			addNode<Head>(inp_cNodeSpecLstPtr);
			NodeSpecLstHlpr<Tail>::add(inp_cNodeSpecLstPtr);
		}
	};
	template<> struct NodeSpecLstHlpr<TypeList<>>
	{
		static void add(CNodeSpecLstPtr) {}
	};

	template<typename TTech> static void addTech(CTechSpecLstPtr inp_cTechSpecLstPtr)
	{
		inp_cTechSpecLstPtr->add(TTech::TechId, [](void) -> CTechPtr { return CreateUnique<TTech>(); });
	}
	template<typename TList> struct TechSpecLstHlpr
	{
		typedef typename TList::Head	Head;
		typedef typename TList::Tail	Tail;

		static void add(CTechSpecLstPtr inp_cTechSpecLstPtr)
		{
			addTech<Head>(inp_cTechSpecLstPtr);
			TechSpecLstHlpr<Tail>::add(inp_cTechSpecLstPtr);
		}
	};
	template<> struct TechSpecLstHlpr<TypeList<>>
	{
		static void add(CTechSpecLstPtr) {}
	};
}

#define PUBLISH(LibCode, Nodes, Techs) \
	enum LIBRARY :uint32_t { ID = LibCode }; \
	typedef TypeList< DUMMY Nodes > PublishedLibNodeLst; \
	typedef TypeList< DUMMY Techs > PublishedLibTechLst;
#endif
