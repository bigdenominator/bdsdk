#ifndef INCLUDE_H_DATEMATHSTESTS
#define INCLUDE_H_DATEMATHSTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/DateMath.h"

TEST(DateMathTests, DateAdd)
{
	EXPECT_EQ(20140820, CDateMath::DateAdd(20140614, PERIODS::DAY, 67, false));
	EXPECT_EQ(20150615, CDateMath::DateAdd(20140614, PERIODS::DAY, 366, false));
	EXPECT_EQ(20140714, CDateMath::DateAdd(20140614, PERIODS::DAY, 30, false));

	EXPECT_EQ(20140712, CDateMath::DateAdd(20140614, PERIODS::WEEK, 4, false));
	EXPECT_EQ(20140531, CDateMath::DateAdd(20140614, PERIODS::WEEK, -2, false));

	EXPECT_EQ(20141014, CDateMath::DateAdd(20140614, PERIODS::MONTH, 4, false));
	EXPECT_EQ(20151014, CDateMath::DateAdd(20140614, PERIODS::MONTH, 16, false));
	EXPECT_EQ(20141030, CDateMath::DateAdd(20140630, PERIODS::MONTH, 4, false));
	EXPECT_EQ(20141031, CDateMath::DateAdd(20140630, PERIODS::MONTH, 4, true));

	EXPECT_EQ(20140930, CDateMath::DateAdd(20140630, PERIODS::QUARTER, 1, true));
	EXPECT_EQ(20141230, CDateMath::DateAdd(20140930, PERIODS::QUARTER, 1, false));
	EXPECT_EQ(20141231, CDateMath::DateAdd(20140930, PERIODS::QUARTER, 1, true));

	EXPECT_EQ(20180614, CDateMath::DateAdd(20140614, PERIODS::YEAR, 4, false));
	EXPECT_EQ(20170630, CDateMath::DateAdd(20140630, PERIODS::YEAR, 3, false));
	EXPECT_EQ(20170630, CDateMath::DateAdd(20140630, PERIODS::YEAR, 3, true));
	EXPECT_EQ(20160228, CDateMath::DateAdd(20140228, PERIODS::YEAR, 2, false));
	EXPECT_EQ(20160229, CDateMath::DateAdd(20140228, PERIODS::YEAR, 2, true));
	EXPECT_EQ(20140228, CDateMath::DateAdd(20120229, PERIODS::YEAR, 2, false));
	EXPECT_EQ(20140228, CDateMath::DateAdd(20120229, PERIODS::YEAR, 2, true));
}

TEST(DateMathTests, DateDiff)
{
	EXPECT_EQ(0, CDateMath::DateDiff(20130624, 20130624, PERIODS::DAY));
	EXPECT_EQ(31, CDateMath::DateDiff(20130724, 20130824, PERIODS::DAY));
	EXPECT_EQ(-83, CDateMath::DateDiff(20130624, 20130402, PERIODS::DAY));

	EXPECT_EQ(0, CDateMath::DateDiff(20130724, 20130730, PERIODS::WEEK));
	EXPECT_EQ(1, CDateMath::DateDiff(20130724, 20130731, PERIODS::WEEK));
	EXPECT_EQ(4, CDateMath::DateDiff(20130724, 20130821, PERIODS::WEEK));

	EXPECT_EQ(0, CDateMath::DateDiff(20130724, 20130731, PERIODS::MONTH));
	EXPECT_EQ(1, CDateMath::DateDiff(20130724, 20130801, PERIODS::MONTH));
	EXPECT_EQ(1, CDateMath::DateDiff(20130724, 20130831, PERIODS::MONTH));
	EXPECT_EQ(-1, CDateMath::DateDiff(20130724, 20130615, PERIODS::MONTH));
	EXPECT_EQ(25, CDateMath::DateDiff(20130724, 20150831, PERIODS::MONTH));

	EXPECT_EQ(0, CDateMath::DateDiff(20130724, 20130831, PERIODS::QUARTER));
	EXPECT_EQ(1, CDateMath::DateDiff(20130724, 20131031, PERIODS::QUARTER));
	EXPECT_EQ(6, CDateMath::DateDiff(20130724, 20150215, PERIODS::QUARTER));

	EXPECT_EQ(0, CDateMath::DateDiff(20130724, 20130831, PERIODS::YEAR));
	EXPECT_EQ(1, CDateMath::DateDiff(20130724, 20140131, PERIODS::YEAR));
	EXPECT_EQ(1, CDateMath::DateDiff(20130724, 20141231, PERIODS::YEAR));
}

TEST(DateMathTests, Deserialize)
{
	LongDate lDate;

	EXPECT_TRUE(CDateMath::Deserialize(string("20100101"), lDate));
	EXPECT_TRUE(20100101 == lDate);

	EXPECT_TRUE(CDateMath::Deserialize(string("1/1/2010"), lDate));
	EXPECT_FALSE(20100101 == lDate);
}

TEST(DateMathTests, Deserialize_delimited)
{
	LongDate lDate;

	EXPECT_TRUE(CDateMath::Deserialize(string("1/1/2010"), "/", lDate));
	EXPECT_TRUE(20100101 == lDate);

	EXPECT_TRUE(CDateMath::Deserialize(string("01-01-2010"), "-", lDate));
	EXPECT_TRUE(20100101 == lDate);

	EXPECT_TRUE(CDateMath::Deserialize(string("1?1?2010"), "?", lDate));
	EXPECT_TRUE(20100101 == lDate);

	EXPECT_FALSE(CDateMath::Deserialize(string("1/1-2010"), "/", lDate));
	EXPECT_FALSE(CDateMath::Deserialize(string("01012010"), "", lDate));
}

TEST(DateMathTests, FindOrdinalDayOfMonth)
{
	EXPECT_EQ(20140618, CDateMath::FindOrdinalDayOfMonth(2014, MONTHSOFYEAR::JUNE, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD));
	EXPECT_EQ(20120224, CDateMath::FindOrdinalDayOfMonth(2012, MONTHSOFYEAR::FEBRUARY, DAYSOFWEEK::FRIDAY, POSITIONINMONTH::FOURTH));
	EXPECT_EQ(20140730, CDateMath::FindOrdinalDayOfMonth(2014, MONTHSOFYEAR::JULY, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::LAST));
}

TEST(DateMathTests, GetDay)
{
	EXPECT_EQ(20, CDateMath::GetDay(20130620));
}

TEST(DateMathTests, GetDayOfWeek)
{
	EXPECT_EQ(DAYSOFWEEK::SATURDAY, CDateMath::GetDayOfWeek(20140614));
	EXPECT_EQ(DAYSOFWEEK::TUESDAY, CDateMath::GetDayOfWeek(20150616));
}

TEST(DateMathTests, GetDayOfYear)
{
	EXPECT_EQ(31, CDateMath::GetDayOfYear(20140131));
	EXPECT_EQ(105, CDateMath::GetDayOfYear(20140415));
}

TEST(DateMathTests, GetDaysInMonth)
{
	//Test the number of days in the month matrix
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20000101)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::JANUARY));
	EXPECT_EQ(29, CDateMath::GetDaysInMonth(20000201)); EXPECT_EQ(29, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::FEBRUARY));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20000301)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::MARCH));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20000401)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::APRIL));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20000501)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::MAY));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20000601)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::JUNE));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20000701)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::JULY));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20000801)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::AUGUST));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20000901)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::SEPTEMBER));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20001001)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::OCTOBER));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20001101)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::NOVEMBER));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20001201)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2000, MONTHSOFYEAR::DECEMBER));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20010101)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::JANUARY));
	EXPECT_EQ(28, CDateMath::GetDaysInMonth(20010201)); EXPECT_EQ(28, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::FEBRUARY));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20010301)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::MARCH));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20010401)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::APRIL));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20010501)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::MAY));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20010601)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::JUNE));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20010701)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::JULY));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20010801)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::AUGUST));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20010901)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::SEPTEMBER));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20011001)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::OCTOBER));
	EXPECT_EQ(30, CDateMath::GetDaysInMonth(20011101)); EXPECT_EQ(30, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::NOVEMBER));
	EXPECT_EQ(31, CDateMath::GetDaysInMonth(20011201)); EXPECT_EQ(31, CDateMath::GetDaysInMonth(2001, MONTHSOFYEAR::DECEMBER));
}

TEST(DateMathTests, GetDaysInYear)
{
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19900101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19910101));
	EXPECT_EQ(366, CDateMath::GetDaysInYear(19920101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19930101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19940101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19950101));
	EXPECT_EQ(366, CDateMath::GetDaysInYear(19960101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19970101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19980101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(19990101));

	EXPECT_EQ(366, CDateMath::GetDaysInYear(20000101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20010101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20020101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20030101));
	EXPECT_EQ(366, CDateMath::GetDaysInYear(20040101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20050101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20060101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20070101));
	EXPECT_EQ(366, CDateMath::GetDaysInYear(20080101));
	EXPECT_EQ(365, CDateMath::GetDaysInYear(20090101));
}

TEST(DateMathTests, GetMonth)
{
	EXPECT_EQ(MONTHSOFYEAR::JUNE, CDateMath::GetMonth(20130620));
}

TEST(DateMathTests, GetQuarter)
{
	EXPECT_EQ(QUARTERS::FIRST, CDateMath::GetQuarter(20130101));
	EXPECT_EQ(QUARTERS::FIRST, CDateMath::GetQuarter(20130331));

	EXPECT_EQ(QUARTERS::SECOND, CDateMath::GetQuarter(20130401));
	EXPECT_EQ(QUARTERS::SECOND, CDateMath::GetQuarter(20130630));

	EXPECT_EQ(QUARTERS::THIRD, CDateMath::GetQuarter(20130701));
	EXPECT_EQ(QUARTERS::THIRD, CDateMath::GetQuarter(20130930));

	EXPECT_EQ(QUARTERS::FOURTH, CDateMath::GetQuarter(20131001));
	EXPECT_EQ(QUARTERS::FOURTH, CDateMath::GetQuarter(20131231));
}

TEST(DateMathTests, GetYear)
{
	EXPECT_EQ(2013, CDateMath::GetYear(20130620));
}

TEST(DateMathTests, IsLeapYear)
{
	EXPECT_FALSE(CDateMath::IsLeapYear(2013));
	EXPECT_TRUE(CDateMath::IsLeapYear(2012));
	EXPECT_TRUE(CDateMath::IsLeapYear(2000));
	EXPECT_FALSE(CDateMath::IsLeapYear(1900));
}

TEST(DateMathTests, IsWeekday)
{
	EXPECT_FALSE(CDateMath::IsWeekday(20140614));
	EXPECT_FALSE(CDateMath::IsWeekday(20140615)); 
	EXPECT_TRUE(CDateMath::IsWeekday(20140616));
	EXPECT_TRUE(CDateMath::IsWeekday(20140617));
	EXPECT_TRUE(CDateMath::IsWeekday(20140618));
	EXPECT_TRUE(CDateMath::IsWeekday(20140619));
	EXPECT_TRUE(CDateMath::IsWeekday(20140620));
	
}

TEST(DateMathTests, IsWeekend)
{
	EXPECT_TRUE(CDateMath::IsWeekend(20140614));
	EXPECT_TRUE(CDateMath::IsWeekend(20140615));
	EXPECT_FALSE(CDateMath::IsWeekend(20140616));
	EXPECT_FALSE(CDateMath::IsWeekend(20140617));
	EXPECT_FALSE(CDateMath::IsWeekend(20140618));
	EXPECT_FALSE(CDateMath::IsWeekend(20140619));
	EXPECT_FALSE(CDateMath::IsWeekend(20140620));
}

TEST(DateMathTests, JulianDateToLongDate)
{
	EXPECT_EQ(20000103, CDateMath::JulianDateToLongDate(2451547L));
	EXPECT_EQ(-47130101, CDateMath::JulianDateToLongDate(0L));
	EXPECT_EQ(-47141230, CDateMath::JulianDateToLongDate(-2L));
}

TEST(DateMathTests, JulianDateToMDY)
{
	int32_t iYear, iDay;
	MONTHSOFYEAR Month;

	CDateMath::JulianDateToMDY(2451547L, Month, iDay, iYear);
	EXPECT_EQ(2000, iYear);
	EXPECT_EQ(MONTHSOFYEAR::JANUARY, Month);
	EXPECT_EQ(3, iDay);

	CDateMath::JulianDateToMDY(0L, Month, iDay, iYear);
	EXPECT_EQ(-4713, iYear);
	EXPECT_EQ(MONTHSOFYEAR::JANUARY, Month);
	EXPECT_EQ(1, iDay);

	CDateMath::JulianDateToMDY(-2L, Month, iDay, iYear);
	EXPECT_EQ(-4714, iYear);
	EXPECT_EQ(MONTHSOFYEAR::DECEMBER, Month);
	EXPECT_EQ(30, iDay);
}

TEST(DateMathTests, LongDateToJulianDate)
{
	EXPECT_EQ(2451547, CDateMath::LongDateToJulianDate(20000103));
	EXPECT_EQ(0, CDateMath::LongDateToJulianDate(-47130101));
}

TEST(DateMathTests, LongDateToMDY)
{
	int32_t iYear, iDay;
	MONTHSOFYEAR Month;

	CDateMath::LongDateToMDY(20130624L, Month, iDay, iYear);
	EXPECT_EQ(2013, iYear);
	EXPECT_EQ(MONTHSOFYEAR::JUNE, Month);
	EXPECT_EQ(24, iDay);
}

TEST(DateMathTests, MDYToJulianDate)
{
	EXPECT_EQ(2451547, CDateMath::MDYToJulianDate(MONTHSOFYEAR::JANUARY, 3, 2000));
}

TEST(DateMathTests, MDYToLongDate)
{
	EXPECT_EQ(20130624, CDateMath::MDYToLongDate(MONTHSOFYEAR::JUNE, 24, 2013));
}

TEST(DateMathTests, ConversionConsistency)
{
	int32_t iYear, iDay;
	MONTHSOFYEAR Month;

	EXPECT_EQ(19770726, CDateMath::JulianDateToLongDate(CDateMath::LongDateToJulianDate(19770726)));
	EXPECT_EQ(20140614, CDateMath::JulianDateToLongDate(CDateMath::LongDateToJulianDate(20140614)));
	
	CDateMath::LongDateToMDY(19770726, Month, iDay, iYear);
	EXPECT_EQ(19770726, CDateMath::MDYToLongDate(Month, iDay, iYear));

	CDateMath::LongDateToMDY(20140614, Month, iDay, iYear);
	EXPECT_EQ(20140614, CDateMath::MDYToLongDate(Month, iDay, iYear));
}

TEST(DateMathTests, QuarterToMonth)
{
	EXPECT_EQ(MONTHSOFYEAR::JANUARY, CDateMath::QuarterToMonth(QUARTERS::FIRST, QUARTERMONTHS::FIRST));
	EXPECT_EQ(MONTHSOFYEAR::FEBRUARY, CDateMath::QuarterToMonth(QUARTERS::FIRST, QUARTERMONTHS::MIDDLE));
	EXPECT_EQ(MONTHSOFYEAR::MARCH, CDateMath::QuarterToMonth(QUARTERS::FIRST, QUARTERMONTHS::LAST));

	EXPECT_EQ(MONTHSOFYEAR::APRIL, CDateMath::QuarterToMonth(QUARTERS::SECOND, QUARTERMONTHS::FIRST));
	EXPECT_EQ(MONTHSOFYEAR::MAY, CDateMath::QuarterToMonth(QUARTERS::SECOND, QUARTERMONTHS::MIDDLE));
	EXPECT_EQ(MONTHSOFYEAR::JUNE, CDateMath::QuarterToMonth(QUARTERS::SECOND, QUARTERMONTHS::LAST));

	EXPECT_EQ(MONTHSOFYEAR::JULY, CDateMath::QuarterToMonth(QUARTERS::THIRD, QUARTERMONTHS::FIRST));
	EXPECT_EQ(MONTHSOFYEAR::AUGUST, CDateMath::QuarterToMonth(QUARTERS::THIRD, QUARTERMONTHS::MIDDLE));
	EXPECT_EQ(MONTHSOFYEAR::SEPTEMBER, CDateMath::QuarterToMonth(QUARTERS::THIRD, QUARTERMONTHS::LAST));

	EXPECT_EQ(MONTHSOFYEAR::OCTOBER, CDateMath::QuarterToMonth(QUARTERS::FOURTH, QUARTERMONTHS::FIRST));
	EXPECT_EQ(MONTHSOFYEAR::NOVEMBER, CDateMath::QuarterToMonth(QUARTERS::FOURTH, QUARTERMONTHS::MIDDLE));
	EXPECT_EQ(MONTHSOFYEAR::DECEMBER, CDateMath::QuarterToMonth(QUARTERS::FOURTH, QUARTERMONTHS::LAST));
}

TEST(DateMathTests, TranslateMonth)
{
	MONTHSOFYEAR	Month;
	string			sMonth;

	CDateMath::TranslateMonth("JAN", Month); EXPECT_EQ(MONTHSOFYEAR::JANUARY, Month);
	CDateMath::TranslateMonth("FEB", Month); EXPECT_EQ(MONTHSOFYEAR::FEBRUARY, Month);
	CDateMath::TranslateMonth("MAR", Month); EXPECT_EQ(MONTHSOFYEAR::MARCH, Month);
	CDateMath::TranslateMonth("APR", Month); EXPECT_EQ(MONTHSOFYEAR::APRIL, Month);
	CDateMath::TranslateMonth("MAY", Month); EXPECT_EQ(MONTHSOFYEAR::MAY, Month);
	CDateMath::TranslateMonth("JUN", Month); EXPECT_EQ(MONTHSOFYEAR::JUNE, Month);
	CDateMath::TranslateMonth("JUL", Month); EXPECT_EQ(MONTHSOFYEAR::JULY, Month);
	CDateMath::TranslateMonth("AUG", Month); EXPECT_EQ(MONTHSOFYEAR::AUGUST, Month);
	CDateMath::TranslateMonth("SEP", Month); EXPECT_EQ(MONTHSOFYEAR::SEPTEMBER, Month);
	CDateMath::TranslateMonth("OCT", Month); EXPECT_EQ(MONTHSOFYEAR::OCTOBER, Month);
	CDateMath::TranslateMonth("NOV", Month); EXPECT_EQ(MONTHSOFYEAR::NOVEMBER, Month);
	CDateMath::TranslateMonth("DEC", Month); EXPECT_EQ(MONTHSOFYEAR::DECEMBER, Month);

	CDateMath::TranslateMonth(MONTHSOFYEAR::JANUARY, sMonth);		EXPECT_EQ("JAN", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::FEBRUARY, sMonth);	EXPECT_EQ("FEB", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::MARCH, sMonth);		EXPECT_EQ("MAR", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::APRIL, sMonth);		EXPECT_EQ("APR", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::MAY, sMonth);			EXPECT_EQ("MAY", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::JUNE, sMonth);		EXPECT_EQ("JUN", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::JULY, sMonth);		EXPECT_EQ("JUL", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::AUGUST, sMonth);		EXPECT_EQ("AUG", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::SEPTEMBER, sMonth);	EXPECT_EQ("SEP", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::OCTOBER, sMonth);		EXPECT_EQ("OCT", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::NOVEMBER, sMonth);	EXPECT_EQ("NOV", sMonth);
	CDateMath::TranslateMonth(MONTHSOFYEAR::DECEMBER, sMonth);	EXPECT_EQ("DEC", sMonth);
}

TEST(DateMathTests, TranslateDayOfWeek)
{
	DAYSOFWEEK	DayOfWeek;
	string		sDayOfWeek;

	CDateMath::TranslateDayOfWeek("SUN", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::SUNDAY);
	CDateMath::TranslateDayOfWeek("MON", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::MONDAY);
	CDateMath::TranslateDayOfWeek("TUE", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::TUESDAY);
	CDateMath::TranslateDayOfWeek("WED", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::WEDNESDAY);
	CDateMath::TranslateDayOfWeek("THU", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::THURSDAY);
	CDateMath::TranslateDayOfWeek("FRI", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::FRIDAY);
	CDateMath::TranslateDayOfWeek("SAT", DayOfWeek);	EXPECT_EQ(DayOfWeek, DAYSOFWEEK::SATURDAY);

	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::SUNDAY, sDayOfWeek);		EXPECT_EQ(sDayOfWeek, "SUN");
	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::MONDAY, sDayOfWeek);		EXPECT_EQ(sDayOfWeek, "MON");
	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::TUESDAY, sDayOfWeek);		EXPECT_EQ(sDayOfWeek, "TUE");
	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::WEDNESDAY, sDayOfWeek);	EXPECT_EQ(sDayOfWeek, "WED");
	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::THURSDAY, sDayOfWeek);	EXPECT_EQ(sDayOfWeek, "THU");
	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::FRIDAY, sDayOfWeek);		EXPECT_EQ(sDayOfWeek, "FRI");
	CDateMath::TranslateDayOfWeek(DAYSOFWEEK::SATURDAY, sDayOfWeek);	EXPECT_EQ(sDayOfWeek, "SAT");
}

TEST(DateMathTests, ValidateDate)
{
	//Test the days counts matrix
	EXPECT_TRUE(CDateMath::ValidateDate(20000131L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000132L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000229L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000230L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000331L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000332L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000430L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000431L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000531L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000532L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000630L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000631L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000731L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000732L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000831L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000832L));

	EXPECT_TRUE(CDateMath::ValidateDate(20000930L));
	EXPECT_FALSE(CDateMath::ValidateDate(20000931L));

	EXPECT_TRUE(CDateMath::ValidateDate(20001031L));
	EXPECT_FALSE(CDateMath::ValidateDate(20001032L));

	EXPECT_TRUE(CDateMath::ValidateDate(20001130L));
	EXPECT_FALSE(CDateMath::ValidateDate(20001131L));

	EXPECT_TRUE(CDateMath::ValidateDate(20001231L));
	EXPECT_FALSE(CDateMath::ValidateDate(20001232L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010131L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010132L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010228L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010229L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010331L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010332L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010430L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010431L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010531L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010532L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010630L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010631L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010731L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010732L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010831L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010832L));

	EXPECT_TRUE(CDateMath::ValidateDate(20010930L));
	EXPECT_FALSE(CDateMath::ValidateDate(20010931L));

	EXPECT_TRUE(CDateMath::ValidateDate(20011031L));
	EXPECT_FALSE(CDateMath::ValidateDate(20011032L));

	EXPECT_TRUE(CDateMath::ValidateDate(20011130L));
	EXPECT_FALSE(CDateMath::ValidateDate(20011131L));

	EXPECT_TRUE(CDateMath::ValidateDate(20011231L));
	EXPECT_FALSE(CDateMath::ValidateDate(20011232L));
    
	//ValidateDate Function Test
	EXPECT_TRUE(CDateMath::ValidateDate(-20130620L));//negative value
	EXPECT_FALSE(CDateMath::ValidateDate(0L));//0 value
	EXPECT_FALSE(CDateMath::ValidateDate(20130026L));// month is 0
	EXPECT_FALSE(CDateMath::ValidateDate(20130600L));// day is 0

	EXPECT_FALSE(CDateMath::ValidateDate(000020130620L)); //preceding zeros
	EXPECT_FALSE(CDateMath::ValidateDate(06202013L));//MMDDYYY format
	EXPECT_FALSE(CDateMath::ValidateDate(201306200L));//zeros appending
	EXPECT_FALSE(CDateMath::ValidateDate(20062013L));//DDMMYYYY format
	EXPECT_FALSE(CDateMath::ValidateDate(20139920L));//Month greater than 12
}

TEST(DateMathTests, ValidDay)
{
	EXPECT_EQ(1, CDateMath::ValidDay(2014, MONTHSOFYEAR::JUNE, -1));
	EXPECT_EQ(1, CDateMath::ValidDay(2014, MONTHSOFYEAR::JUNE, 1));
	EXPECT_EQ(15, CDateMath::ValidDay(2014, MONTHSOFYEAR::JUNE, 15));
	EXPECT_EQ(30, CDateMath::ValidDay(2014, MONTHSOFYEAR::JUNE, 30));
	EXPECT_EQ(30, CDateMath::ValidDay(2014, MONTHSOFYEAR::JUNE, 31));

	EXPECT_EQ(28, CDateMath::ValidDay(2014, MONTHSOFYEAR::FEBRUARY, 28));
	EXPECT_EQ(28, CDateMath::ValidDay(2014, MONTHSOFYEAR::FEBRUARY, 29));
	EXPECT_EQ(28, CDateMath::ValidDay(2014, MONTHSOFYEAR::FEBRUARY, 30));

	EXPECT_EQ(28, CDateMath::ValidDay(2012, MONTHSOFYEAR::FEBRUARY, 28));
	EXPECT_EQ(29, CDateMath::ValidDay(2012, MONTHSOFYEAR::FEBRUARY, 29));
	EXPECT_EQ(29, CDateMath::ValidDay(2012, MONTHSOFYEAR::FEBRUARY, 30));
}
#endif
