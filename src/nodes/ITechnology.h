#ifndef INCLUDE_H_ITECHNOLOGY
#define INCLUDE_H_ITECHNOLOGY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/Schematic.h"
#include "nodes/EventDef.h"

typedef int32_t TECHNOLOGY_t;
enum FRM_TECHNOLOGY :TECHNOLOGY_t { FRM_TECHEND = 1000 };

enum class IO_DIR { INPUT, OUTPUT };

SMARTSTRUCT(SActivity, (string msCommand; SUMMARY_FORMAT mFormat; IO_DIR mDirection; CContainerIntrPtr mcContainerPtr; CFeedback mcFeedback;), (mFormat, mDirection, msCommand))
typedef CSmartPtr<SActivity> SActivityPtr;

struct ITechnology
{
	CFeedback	mcFeedback;

	virtual ~ITechnology(void) {}

	virtual void Close(void) = 0;
	virtual SActivityPtr Configure(const string&, IO_DIR, SUMMARY_FORMAT) = 0;
	virtual bool Open(const string&) = 0;
	virtual bool Receive(SActivity&) = 0;
	virtual bool Send(SActivity&) = 0;
};

template<TECHNOLOGY_t iTECH> struct CTechnology : public ITechnology
{
	enum :TECHNOLOGY_t { TechId = iTECH };
};
#endif