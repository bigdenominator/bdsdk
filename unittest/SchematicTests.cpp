#ifndef INCLUDE_H_SCHEMATICTESTS
#define INCLUDE_H_SCHEMATICTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "containers/Schematic.h"
#include "containers/SmartMap.h"
#include "containers/SmartVector.h"

class SchematicTests: public testing::Test
{
public:
	typedef CSmartMap < CDate, CVectorIntPtr > TestMap;
	typedef CSmartPtr<TestMap> TestMapPtr;

	SIMPLESTRUCT(TestStruct, (int miTest; double mdTest; TestMapPtr mcTestPtr;));
	SMARTSTRUCT(TestSmartStruct, (int miTest; double mdTest; TestMapPtr mcTestPtr;), (miTest, mdTest, mcTestPtr));
	SCHEMATIC(TestSchematic, 999, (int miTest; double mdTest; TestMapPtr mcTestPtr;), (miTest, mdTest, mcTestPtr));

	const CDate mdt1 = 20150101;
	const CDate mdt2 = 20160101;
	const int miTest = 0;
	const double mdTest = 1;
	const TestMapPtr mcTestPtr = CreateSmart<TestMap>();

	void SetUp(void)
	{
		mcTestPtr->add(mdt1, CreateSmart<CVectorInt>(10, 1));
		mcTestPtr->add(mdt2, CreateSmart<CVectorInt>(20, 2));
	}

	void TearDown(void) {}
};

TEST_F(SchematicTests, SimpleStruct)
{
	TestStructPtr myPtr(CreateSmart<TestStruct>());
	myPtr->miTest = miTest;
	myPtr->mdTest = mdTest;
	myPtr->mcTestPtr = mcTestPtr;

	int iTest = myPtr->miTest;
	double dTest = myPtr->mdTest;
	TestMapPtr cTestPtr = myPtr->mcTestPtr;

	EXPECT_EQ(0, iTest);
	EXPECT_EQ(1.0, dTest);
	EXPECT_EQ(2, cTestPtr->size());

	auto itr(cTestPtr->find(mdt1));
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(10, itr->second->size());
	EXPECT_EQ(1, (*itr->second)[0]);

	itr = cTestPtr->find(mdt2);
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(20, itr->second->size());
	EXPECT_EQ(2, (*itr->second)[0]);
}

TEST_F(SchematicTests, SmartStruct)
{
	TestSmartStructPtr myPtr(CreateSmart<TestSmartStruct>()), destPtr(CreateSmart<TestSmartStruct>());
	myPtr->miTest = miTest;
	myPtr->mdTest = mdTest;
	myPtr->mcTestPtr = mcTestPtr;

	int iTest = myPtr->miTest;
	double dTest = myPtr->mdTest;
	TestMapPtr cTestPtr = myPtr->mcTestPtr;

	EXPECT_EQ(0, iTest);
	EXPECT_EQ(1.0, dTest);
	EXPECT_EQ(2, cTestPtr->size());

	auto itr(cTestPtr->find(mdt1));
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(10, itr->second->size());
	EXPECT_EQ(1, (*itr->second)[0]);

	itr = cTestPtr->find(mdt2);
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(20, itr->second->size());
	EXPECT_EQ(2, (*itr->second)[0]);
	
	CopyViaPack(*myPtr, *destPtr);

	SSerial sSource, sDest;
	myPtr->Serialize(sSource);
	destPtr->Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);

	destPtr = CreateSmart<TestSmartStruct>(*myPtr);
	sDest.reset(); sDest.msBuffer = sNULL;
	destPtr->Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);

	destPtr = CreateSmart<TestSmartStruct>();
	*destPtr = *myPtr;
	sDest.reset(); sDest.msBuffer = sNULL;
	destPtr->Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);
}

TEST_F(SchematicTests, Schematic)
{
	TestSchematicPtr myPtr(CreateSmart<TestSchematic>()), destPtr(CreateSmart<TestSchematic>());
	myPtr->miTest = miTest;
	myPtr->mdTest = mdTest;
	myPtr->mcTestPtr = mcTestPtr;

	int iTest = myPtr->miTest;
	double dTest = myPtr->mdTest;
	TestMapPtr cTestPtr = myPtr->mcTestPtr;

	EXPECT_EQ(0, iTest);
	EXPECT_EQ(1.0, dTest);
	EXPECT_EQ(2, cTestPtr->size());

	auto itr(cTestPtr->find(mdt1));
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(10, itr->second->size());
	EXPECT_EQ(1, (*itr->second)[0]);

	itr = cTestPtr->find(mdt2);
	EXPECT_TRUE(itr != cTestPtr->cend());
	EXPECT_EQ(20, itr->second->size());
	EXPECT_EQ(2, (*itr->second)[0]);

	CopyViaPack(*myPtr, *destPtr);

	SSerial sSource, sDest;
	myPtr->Serialize(sSource);
	destPtr->Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);

	destPtr = CreateSmart<TestSchematic>(*myPtr);
	sDest.reset(); sDest.msBuffer = sNULL;
	destPtr->Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);

	destPtr = CreateSmart<TestSchematic>();
	*destPtr = *myPtr;
	sDest.reset(); sDest.msBuffer = sNULL;
	destPtr->Serialize(sDest);
	EXPECT_EQ(sSource.msBuffer, sDest.msBuffer);
}
#endif