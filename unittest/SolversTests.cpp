#ifndef INCLUDE_H_SOLVERSTESTS
#define INCLUDE_H_SOLVERSTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "solvers/all.h"


static double testFn1(const CVectorDbl& inp_cValues, CVectorDblPtr inp_cParametersPtr)
{
	return statistics::GetMean(std::pow(inp_cValues - *inp_cParametersPtr, 2));
}

static double testFn2(const CVectorDbl& inp_cValues)
{
	return statistics::GetMean(std::pow(inp_cValues - CSmartArray<double, 2>({5.0, 10.0}), 2));
}

TEST(ToolsSolversTests, InterpolateDoubleParabolic)
{
	typedef CSmartMap<double, double> CMyMap;
	typedef pair<double, double> pair_double;

	const double dTolerance = 0.00000001;

	CMyMap cMap;
	cMap.add(1, 0.00);
	cMap.add(2, 0.25);
	cMap.add(3, 0.38);
	cMap.add(4, 0.50);
	cMap.add(5, 0.75);
	cMap.add(6, 0.10);
	cMap.add(7, 0.08);
	cMap.add(8, 0.70);

	double d1(Solvers::InterpolateQuadratic(pair_double(2, 0.25), pair_double(3, 0.38), pair_double(4, 0.50), 3.5));
	double d2(Solvers::InterpolateQuadratic(pair_double(3, 0.38), pair_double(4, 0.50), pair_double(5, 0.75), 3.5));

	EXPECT_GT(dTolerance, fabs(Solvers::InterpolateDoubleParabolic(cMap, 3.5) - 0.5 * (d1 + d2)));
}

TEST(ToolsSolversTests, InterpolationLinear)
{
	const double dTolerance = 0.00000001;

	EXPECT_EQ(12, Solvers::InterpolateLinear(1, 5, 3, 10, 4, true));
	EXPECT_EQ(10, Solvers::InterpolateLinear(1, 5, 3, 10, 4, false));

	EXPECT_GT(dTolerance, fabs(Solvers::InterpolateLinear(1, 5.0, 3, 10.0, 4, true) - 12.5));

	CDate cLower(20111231), cUpper(20121231), cTarget(20120301);
	EXPECT_GT(dTolerance, fabs(Solvers::InterpolateLinear(cLower, 5.0, cUpper, 11.0, cTarget, false) - 6.0));
}

TEST(ToolsSolversTests, InterpolationLinearVectors)
{
	const double dTolerance = 0.00000001;

	CVectorDbl v1(10);
	CVectorDbl v2(10);
	
	for (size_t i(0); i < 10; i++)
	{
		v1[i] = 10 * i;
		v2[i] = i;
	}

	CVectorDbl t1(Solvers::InterpolateLinear(1, v1, 10, v2, 6));
	CVectorDbl t2(Solvers::InterpolateLinear(1, v1, 10, v2, 11));
	CVectorDbl t3(Solvers::InterpolateLinear(1, v1, 10, v2, 11, false));
	for (size_t i(0); i < 10; i++)
	{
		EXPECT_GT(dTolerance, fabs(t1[i] - 5 * i));
		EXPECT_GT(dTolerance, fabs(t2[i]));
		EXPECT_GT(dTolerance, fabs(t3[i] - i));
	}
}

TEST(ToolsSolversTests, InterpolationLinearMap)
{
	const double dTolerance = 0.00000001;
	const int32_t iSize(100);

	CSmartMap<int32_t, double> cMap;
	for (int32_t iIndx(0); iIndx < iSize; iIndx += 2)
	{
		cMap.add(iIndx, static_cast<double>(iIndx));
	}

	EXPECT_GT(dTolerance, fabs(Solvers::InterpolateLinear(cMap, 51, false) - 51.0));
}

TEST(ToolsSolversTests, InterpolationLinearMapOfVectors)
{
	const double dTolerance = 0.00000001;
	const int32_t iSize(10);

	CSmartMap<int32_t, CVectorDbl> cMap;
	CVectorDbl tmp(10);
	for (int32_t iIndx(0); iIndx < iSize; iIndx++)
	{
		for (int i(0); i < tmp.size(); i++)
		{
			tmp[i] = (10 - iIndx) * i;
		}
		cMap.add(iIndx, tmp);
	}

	CVectorDbl t1(Solvers::InterpolateLinear(cMap, 6));
	CVectorDbl t2(Solvers::InterpolateLinear(cMap, 11));
	CVectorDbl t3(Solvers::InterpolateLinear(cMap, 11, false));
	for (size_t i(0); i < 10; i++)
	{
		EXPECT_GT(dTolerance, fabs(t1[i] - 4 * i));
		EXPECT_GT(dTolerance, fabs(t2[i] + i));
		EXPECT_GT(dTolerance, fabs(t3[i] - i));
	}
}

TEST(ToolsSolversTests, InterpolationLinearMapOfVectorPtrs)
{
	const double dTolerance = 0.00000001;
	const int32_t iSize(10);

	CSmartMap<int32_t, CVectorDblPtr> cMap;
	for (int32_t iIndx(0); iIndx < iSize; iIndx++)
	{
		CVectorDblPtr tmp(CreateSmart<CVectorDbl>(10));
		for (int i(0); i < tmp->size(); i++)
		{
			(*tmp)[i] = (10 - iIndx) * i;
		}
		cMap.add(iIndx, tmp);
	}

	CVectorDbl t1(Solvers::InterpolateLinear(cMap, 6));
	CVectorDbl t2(Solvers::InterpolateLinear(cMap, 11));
	CVectorDbl t3(Solvers::InterpolateLinear(cMap, 11, false));
	CVectorDbl t4(Solvers::InterpolateLinear(cMap, -1));
	CVectorDbl t5(Solvers::InterpolateLinear(cMap, -1, false));
	for (size_t i(0); i < 10; i++)
	{
		EXPECT_GT(dTolerance, fabs(t1[i] - 4 * i));
		EXPECT_GT(dTolerance, fabs(t2[i] + i));
		EXPECT_GT(dTolerance, fabs(t3[i] - i));
		EXPECT_GT(dTolerance, fabs(t4[i] - 11 * i));
		EXPECT_GT(dTolerance, fabs(t5[i] - 10 * i));
	}
}

TEST(ToolsSolversTests, InterpolationQuadratic)
{
	typedef pair<double, double> pair_double;

	const double dTolerance = 0.000001;

	pair_double p1(1, 0);
	pair_double p2(4, 1.5863);
	pair_double p3(6, 1.7918);

	EXPECT_GT(dTolerance, fabs(Solvers::InterpolateQuadratic(p1, p2, p3, 2.0) - 0.699173));
}

TEST(ToolsSolversTests, GridSearch)
{
	typedef CGridDims<double, 2> MyGrid;
	
	const double dTolerance(0.0000001);

	MyGrid cGridSpecs;
	cGridSpecs[0].resize(11); Solvers::BuildGridDim(0.0, 10.0, cGridSpecs[0]);
	cGridSpecs[1].resize(26); Solvers::BuildGridDim(0.0, 50.0, cGridSpecs[1]);

	CVectorDbl cMin(MyGrid::SIZE);
	
	Solvers::SearchGrid(testFn2, cGridSpecs, cMin);
	EXPECT_GT(dTolerance, fabs(cMin[0] - 5.0));
	EXPECT_GT(dTolerance, fabs(cMin[1] - 10.0));
	
	CVectorDblPtr cParamsPtr(CreateSmart<CVectorDbl>(MyGrid::SIZE));
	(*cParamsPtr)[0] = 5;
	(*cParamsPtr)[1] = 10;

	auto cFn(MakeParameterized(testFn1, cParamsPtr));
	EXPECT_GT(dTolerance, fabs(Solvers::SearchGrid(cFn, cGridSpecs, cMin)));
	EXPECT_GT(dTolerance, fabs(cMin[0] - 5.0));
	EXPECT_GT(dTolerance, fabs(cMin[1] - 10.0));
}

TEST(ToolsSolversTests, GridSearchFixedDim)
{
	typedef CGridDims<double, 2> MyGrid;

	const double dTolerance(0.0000001);

	MyGrid cGridSpecs;
	cGridSpecs[0].resize(11); Solvers::BuildGridDim(0.0, 10.0, cGridSpecs[0]);
	cGridSpecs[1].resize(1); Solvers::BuildGridDim(0.0, 50.0, cGridSpecs[1]);

	CVectorDbl cMin(MyGrid::SIZE);

	EXPECT_GT(dTolerance, fabs(Solvers::SearchGrid(testFn2, cGridSpecs, cMin) - 800));
	EXPECT_GT(dTolerance, fabs(cMin[0] - 5.0));
	EXPECT_GT(dTolerance, fabs(cMin[1] - 50.0));

	CVectorDblPtr cParamsPtr(CreateSmart<CVectorDbl>(MyGrid::SIZE));
	(*cParamsPtr)[0] = 5;
	(*cParamsPtr)[1] = 10;

	auto cFn(MakeParameterized(testFn1, cParamsPtr));
	EXPECT_GT(dTolerance, fabs(Solvers::SearchGrid(cFn, cGridSpecs, cMin) - 800));
	EXPECT_GT(dTolerance, fabs(cMin[0] - 5.0));
	EXPECT_GT(dTolerance, fabs(cMin[1] - 50.0)); 
}

TEST(ToolsSolversTests, Simplex)
{
	const CVectorDbl::size_type iSize(3);
	const double dTolerance(0.00000001);
	const uint32_t iMaxIter(100000);

	double dMin(0.0);
	uint32_t iIter(100000);
	CVectorDbl cPoint(iSize, 1.0);
	CVectorDbl cStep(iSize, 0.01);

	CVectorDblPtr cParamsPtr(CreateSmart<CVectorDbl>(iSize));
	(*cParamsPtr)[0] = 2.0;
	(*cParamsPtr)[1] = 10.0;
	(*cParamsPtr)[2] = -dPI;

	auto cFn(MakeParameterized(testFn1, cParamsPtr));
	EXPECT_TRUE(Solvers::MinimizeSimplex(cFn, iMaxIter, dTolerance, cPoint, cStep, iIter, dMin));
	EXPECT_TRUE(1 < iIter); 
	EXPECT_GT(0.0005, fabs(cPoint[0] - 2.0));
	EXPECT_GT(0.0005, fabs(cPoint[1] - 10.0));
	EXPECT_GT(0.0005, fabs(cPoint[2] + dPI));
	EXPECT_GT(dTolerance * 10, fabs(dMin));

	cPoint = 1.0;
	EXPECT_TRUE(Solvers::MinimizeSimplex(testFn2, iMaxIter, dTolerance, cPoint, cStep, iIter, dMin));
	EXPECT_TRUE(1 < iIter);
	EXPECT_GT(0.0005, fabs(cPoint[0] - 5.0));
	EXPECT_GT(0.0005, fabs(cPoint[1] - 10.0));
}

TEST(ToolsSolversTests, SimplexFixedDim)
{
	const CVectorDbl::size_type iSize(3);
	const double dTolerance(0.00000001);
	const uint32_t iMaxIter(100000);

	double dMin(0.0);
	uint32_t iIter(100000);
	CVectorDbl cPoint(iSize, 1.0);
	CVectorDbl cStep(iSize, 0.01);
	cStep[1] = 0.0;

	CVectorDblPtr cParamsPtr(CreateSmart<CVectorDbl>(iSize));
	(*cParamsPtr)[0] = 2.0;
	(*cParamsPtr)[1] = 10.0;
	(*cParamsPtr)[2] = -dPI;

	auto cFn(MakeParameterized(testFn1, cParamsPtr));
	EXPECT_TRUE(Solvers::MinimizeSimplex(cFn, iMaxIter, dTolerance, cPoint, cStep, iIter, dMin));
	EXPECT_TRUE(1 < iIter);
	EXPECT_GT(0.0005, fabs(cPoint[0] - 2.0));
	EXPECT_GT(0.0005, fabs(cPoint[1] - 1.0));
	EXPECT_GT(0.0005, fabs(cPoint[2] + dPI));
	EXPECT_GT(dTolerance * 10, fabs(dMin - 27.0));

	cPoint = 1.0;
	EXPECT_TRUE(Solvers::MinimizeSimplex(testFn2, iMaxIter, dTolerance, cPoint, cStep, iIter, dMin));
	EXPECT_TRUE(1 < iIter);
	EXPECT_GT(0.0005, fabs(cPoint[0] - 5.0));
	EXPECT_GT(0.0005, fabs(cPoint[1] - 1.0));
}

TEST(ToolsSolversTests, root)
{
	const double dTolerance = 0.00000001;
	double dResult;

	EXPECT_TRUE(Solvers::root([](const double& inp_dValue) {return inp_dValue * inp_dValue - 1.0; }, 0.0, 2.0, dTolerance, dResult));
	EXPECT_GT(dTolerance, fabs(dResult - 1.0));

	EXPECT_TRUE(Solvers::root([](const double& inp_dValue) {return CMath::sin(inp_dValue) - d_SQRT2; }, -0.5 * dPI, 0.5 * dPI, dTolerance, dResult));
	EXPECT_GT(dTolerance, fabs(dResult - 0.25 * dPI));

	EXPECT_TRUE(Solvers::root([](const double& inp_dValue) {return CMath::cos(inp_dValue); }, 0.0, dPI, dTolerance, dResult));
	EXPECT_GT(dTolerance, fabs(dResult - 0.5 * dPI));

	auto foo = [](double inp_dValue, CVectorDblPtr inp_cParamsPtr)
	{
		return CMath::polynomial(inp_dValue, inp_cParamsPtr->data(), inp_cParamsPtr->size());
	};

	CVectorDblPtr cParamPtr(CreateSmart<CVectorDbl>(3, 1.0));
	(*cParamPtr)[1] = 2.0;

	auto cFn(MakeParameterized(foo, cParamPtr));
	EXPECT_TRUE(Solvers::root(cFn, -2.0, 1.0, dTolerance * dTolerance, dResult));
	EXPECT_GT(dTolerance, fabs(dResult + 1.0));
}

TEST(ToolsSolversTests, SimpsonsRule)
{
	const double dTolerance = 0.00000001;
	double dResult;

	dResult = Solvers::SimpsonsRule([](const double& inp_dValue){ return inp_dValue * inp_dValue; }, 1.0, 2.0, 2);
	EXPECT_GT(dTolerance, fabs(dResult - 7.0 / 3.0));

	auto foo = [](double inp_dValue, CVectorDblPtr inp_cArrayPtr){ return inp_dValue * inp_dValue - static_cast<double>(inp_cArrayPtr->size()) * inp_dValue + 1.0; };
	auto cFn(MakeParameterized(foo, CreateSmart<CVectorDbl>(3)));

	dResult = Solvers::SimpsonsRule(cFn, 0.0, 6.0, 2);
	EXPECT_GT(dTolerance, fabs(dResult - 24.0));
}
#endif
