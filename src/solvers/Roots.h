#ifndef INCLUDE_H_ROOTS
#define INCLUDE_H_ROOTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

namespace Solvers
{
	template<typename F, typename TValue> static inline bool root(F inp_fUnivariate, const TValue& inp_dMin, const TValue& inp_dMax, const TValue& inp_dPrecision, TValue& inp_dResult)
	{
		TValue dPrecision(fabs(inp_dPrecision));
		TValue dX0(inp_dMin), dX1(inp_dMax);
		TValue dY0(inp_fUnivariate(dX0)), dY1(inp_fUnivariate(dX1));
		TValue dEval;

		for (int32_t iIter(0); iIter < 100; iIter++)
		{
			inp_dResult = InterpolateLinear(dY0, dX0, dY1, dX1, 0.0);
			dEval = inp_fUnivariate(inp_dResult);

			if (fabs(dEval) < dPrecision) return true;

			if (fabs(dY0) < fabs(dY1))
			{
				dX1 = inp_dResult;
				dY1 = dEval;
			}
			else
			{
				dX0 = inp_dResult;
				dY0 = dEval;
			}
		}
		return false;
	}
}
#endif
