#ifndef INCLUDE_H_SMARTMATRIX
#define INCLUDE_H_SMARTMATRIX

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/SmartArray.h"
#include "containers/SmartVector.h"

template<typename T, size_t iROWS, size_t iCOLS> using CSmartMatrix = CSmartArray<CSmartArray<T, iCOLS>, iROWS>;
template<typename T, size_t iROWS, size_t iCOLS> static inline CSmartArray<T, iROWS> operator*(const CSmartMatrix<T, iROWS, iCOLS>& lhs, const CSmartArray<T, iCOLS>& rhs)
{
	CSmartArray<T, iROWS> cResult;
	for (size_t iRow(0); iRow < iROWS; iRow++)
	{
		cResult[iRow] = std::dot(lhs[iRow], rhs);
	}
	return cResult;
}

template<typename T, size_t iROWS, size_t iCOLS> using CRandomMatrix = CSmartArray<CSmartArray<CSmartVector<T>, iCOLS>, iROWS>;
template<typename T, size_t iROWS, size_t iCOLS> static inline CSmartArray<CSmartVector<T>, iROWS> operator*(const CRandomMatrix<T, iROWS, iCOLS>& lhs, const CSmartArray<CSmartVector<T>, iCOLS>& rhs)
{
	CSmartArray<CSmartVector<T>, iROWS> cResult;
	for (size_t iRow(0); iRow < iROWS; iRow++)
	{
		cResult[iRow] = 0;
		for (size_t iCol(0); iCol < iCOLS; iCol++)
		{
			auto tmp(lhs[iRow][iCol] * rhs[iCol]);
			cResult[iRow].resize(tmp.size());
			cResult[iRow] += tmp;
		}
	}

	return cResult;
}
#endif
