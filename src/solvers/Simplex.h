#ifndef INCLUDE_H_SIMPLEX
#define INCLUDE_H_SIMPLEX

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/all.h"
#include "math/statistics.h"

template <typename Func, typename TInput, typename TReturn> struct SSimplex
{
public:
	typedef SSimplex<Func, TInput, TReturn>	_Myt;

	typedef Func	function_type;
	typedef TInput	input_type;
	typedef TReturn	return_type;

	typedef typename input_type::size_type	size_type;
	typedef typename input_type::value_type	value_type;

	SSimplex(function_type inp_ObjFn, input_type& inp_cPoint, input_type& inp_cStep) :mcObjFn(inp_ObjFn), miSimplexPoints(inp_cPoint.size() + 1),
		mcPoints(miSimplexPoints), mcResults(miSimplexPoints), mcCentroid(miSimplexPoints - 1), mcReflection(miSimplexPoints - 1), mcExpansion(miSimplexPoints - 1), mcContraction(miSimplexPoints - 1)
	{
		for (size_type iPoint(0); iPoint < miSimplexPoints; iPoint++)
		{
			mcPoints[iPoint] = inp_cPoint;

			if (iPoint < inp_cStep.size())
			{
				mcPoints[iPoint][iPoint] += inp_cStep[iPoint];
			}
			mcResults[iPoint] = this->mcObjFn(mcPoints[iPoint]);
		}

		rank();
		calcCentroid();
	}

	bool Minimize(const uint32_t& inp_iMaxIter, const return_type& inp_dTolerance, uint32_t& inp_iIterations, input_type& inp_cArgMin, return_type& inp_dMinValue)
	{
		for (inp_iIterations = 0; inp_iIterations < inp_iMaxIter; inp_iIterations++)
		{
			iterate();
			if (error() < inp_dTolerance)
			{
				inp_dMinValue = mcResults[miIndxBest];
				inp_cArgMin = mcPoints[miIndxBest];
				return true;
			}
		}
		return false;
	}

private:
	typedef CSmartVector<input_type>	points_t;
	typedef CSmartVector<return_type>	results_t;

	const value_type mdReflectionFrac = -1.0;
	const value_type mdExpansionFrac = -2.0;
	const value_type mdContractionFrac = 0.5;
	const value_type mdReductionFrac = 0.5;

	const size_type	miSimplexPoints;

	function_type	mcObjFn;

	points_t	mcPoints;
	results_t	mcResults;

	input_type	mcCentroid;
	input_type	mcReflection;
	input_type	mcExpansion;
	input_type	mcContraction;

	size_type	miIndxBest;
	size_type	miIndxNextBest;
	size_type	miIndxWorst;

	inline void calcCentroid(void)
	{
		mcCentroid = 0.0;
		for (size_type iPoint(0); iPoint < miIndxWorst; iPoint++)
		{
			mcCentroid += mcPoints[iPoint];
		}
		for (size_type iPoint(miIndxWorst + 1); iPoint < miSimplexPoints; iPoint++)
		{
			mcCentroid += mcPoints[iPoint];
		}
		mcCentroid /= miSimplexPoints - 1;
	}

	inline void iterate(void)
	{
		mcReflection = mcCentroid + mdReflectionFrac * (mcPoints[miIndxWorst] - mcCentroid);

		return_type dReflection(this->mcObjFn(mcReflection));
		if (dReflection < mcResults[miIndxBest])
		{
			mcExpansion = mcCentroid + mdExpansionFrac * (mcPoints[miIndxWorst] - mcCentroid);

			return_type dExpansion(this->mcObjFn(mcExpansion));
			if (dExpansion < dReflection)
			{
				updateWorst(dExpansion, mcExpansion);
			}
			else
			{
				updateWorst(dReflection, mcReflection);
			}
		}
		else if (dReflection < mcResults[miIndxNextBest])
		{
			updateWorst(dReflection, mcReflection);
		}
		else
		{
			mcContraction = mcCentroid + mdContractionFrac * (mcPoints[miIndxWorst] - mcCentroid);

			return_type dContraction(this->mcObjFn(mcContraction));
			if (dContraction < mcResults[miIndxWorst])
			{
				updateWorst(dContraction, mcContraction);
			}
			else
			{
				reducePoints();
			}
		}
	}

	inline void rank(void)
	{
		if (mcResults[0] < mcResults[1])
		{
			miIndxBest = 0;
			miIndxWorst = 1;
		}
		else
		{
			miIndxBest = 1;
			miIndxWorst = 0;
		}

		miIndxNextBest = miIndxWorst;
		for (size_type iPoint(2); iPoint < miSimplexPoints; iPoint++)
		{
			if (mcResults[iPoint] < mcResults[miIndxBest])
			{
				miIndxNextBest = miIndxBest;
				miIndxBest = iPoint;
			}
			else if (mcResults[iPoint] < mcResults[miIndxNextBest])
			{
				miIndxNextBest = iPoint;
			}
			else if (mcResults[iPoint] > mcResults[miIndxWorst])
			{
				miIndxWorst = iPoint;
			}
		}
	}

	inline void reducePoints(void)
	{
		for (size_type iPoint(0); iPoint < miSimplexPoints; iPoint++)
		{
			if (iPoint != miIndxBest)
			{
				mcPoints[iPoint] = mcPoints[miIndxBest] + mdReductionFrac * (mcPoints[iPoint] - mcPoints[miIndxBest]);
				mcResults[iPoint] = this->mcObjFn(mcPoints[iPoint]);
			}
		}

		rank();
		calcCentroid();
	}

	inline return_type error(void)
	{
		return statistics::GetStdDevPopulation(mcResults);
	}

	inline void updateWorst(const return_type& inp_dNewY, const input_type& inp_cNewX)
	{
		mcPoints[miIndxWorst] = inp_cNewX;
		mcResults[miIndxWorst] = inp_dNewY;

		rank();
		mcCentroid += (inp_cNewX - mcPoints[miIndxWorst]) / (miSimplexPoints - 1);
	}
};

namespace Solvers
{
	template<typename TFunc, typename TInput, typename TReturn> static inline
		bool MinimizeSimplex(TFunc inp_fArrayFunc, const uint32_t& inp_iMaxIter, const typename TInput::value_type& inp_dTol, TInput& inp_cPoint, TInput& inp_cStep, uint32_t& inp_iIterations, TReturn& inp_dMinValue)
	{
		return CreateUnique<SSimplex<TFunc, TInput, TReturn>>(inp_fArrayFunc, inp_cPoint, inp_cStep)->Minimize(inp_iMaxIter, inp_dTol, inp_iIterations, inp_cPoint, inp_dMinValue);
	}
}
#endif
