#ifndef INCLUDE_H_TUPLE
#define INCLUDE_H_TUPLE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "utils/TypeLists.h"
#include "containers/SmartContainers.h"

template <typename TList> struct Tuple : public Tuple<typename TList::Tail >
{
public:
	typedef TList							_Mylist;
	typedef Tuple<typename TList::Tail >	_Mybase;
	typedef Tuple<TList>					_Myt;
	typedef typename TList::Head			current_t;

	template<uint32_t TIndx> using Types = typename _Mylist::template Types<TIndx>;

	template<uint32_t TIndx> inline typename Types<TIndx>::At& At(void)
	{
		typedef typename Types<TIndx>::SubList	SubList;

		return Tuple<SubList>::myValue; 
	}

protected:
	current_t	myValue;
};
template <> struct Tuple<TypeList<>> {};

template <typename TList> struct SmartTuple : public SmartTuple<typename TList::Tail >
{
public:
	typedef TList							_Mylist;
	typedef SmartTuple<typename TList::Tail >	_Mybase;
	typedef SmartTuple<TList>					_Myt;
	typedef typename TList::Head			current_t;

	template<uint32_t TIndx> using Types = typename _Mylist::template Types<TIndx>;

	template<uint32_t TIndx> inline typename Types<TIndx>::At& At(void)
	{
		typedef typename Types<TIndx>::SubList	SubList;

		return SmartTuple<SubList>::myValue;
	}

	DERIVED_SPUD(myValue);

	current_t	myValue;
};
template <> struct SmartTuple<TypeList<>> : public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
	virtual bool deserialize(SSerial&) { return true; }
	virtual void pack(SPack&) const {}
	virtual SPack::size_type packSize(void) const { return 0; }
	virtual void serialize(SSerial&) const {}
	virtual bool unpack(SPack&) { return true; }	
};
#endif
