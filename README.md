﻿The **Big Denominator** *EDGE* is a C++ based general-purpose application development and deployment framework. Its simple and intuitive interface facilitates the rapid development, testing, deployment, and administration of high performance applications that seamlessly scale from a single computer to a full network cluster. It is targeted to those needing customized solutions but who may lack the skills and/or investment required for full application development.

The *EDGE* library offers ...

  * **intuitive** and **simple** syntax for describing the problem you are trying to solve
  * **high performance** vector / array processing through extensible expression templates
  * **parallel execution** through custom-administered multi-threading
  * **distributed computing** using a simple API ("Request", "Send", and "View") with portable, extensible data structures

Though *EDGE* components are stand-alone tools, available for use in user applications, full **Big Denominator** application development and execution requires the *Directed Graph Engine* ([DGE][]). This executable and dll/so handles system configuration and administration as well as graph execution. It is freely downloadable but requires a license. Feel free to [contact][] us for a trial license. 

----

# v0.9: 2016-08-05 #

Release 0.9 represents a major step in the history of **Big Denominator** - the initial deployment of the *EDGE* library. *EDGE v0.9* is written in C++11 and therefore compatible with a wide range of C++ compilers. Currently, *EDGE* is tested using Visual Studo 2015 (Win64) and GCC 4.8.2 (Linux). Testing with additional compilers is on the near-term horizon.

----

## + Contents ##

When installed on a Windows machine, the *EDGE* toolkit creates a *BDSDK* environmental variable that references the installation directory. The following subdirectories and files are installed to this directory:

* LICENSE.txt - Contains the terms of the **EDGE** license.

----

**bin/**  *Subdirectory for binaries.*

Contains debug and release versions of the [tcmalloc][] binary.

----

**lib/**  *Subdirectory for libraries.*

Contains debug and release versions of the [tcmalloc][] binary.

----

**src/**  *Subdirectory for all header and source code files.*

* BDLink.cpp - Implements the API for a **Big Denominator DGE**-compatible library. This is the only source code file necessary for building such a library.
* BDLink.h - Defines the API for a **Big Denominator DGE**-compatible library.
* bdsdk.h - Master header file which includes all *EDGE* components (see below).
* CommonDefs.h - Top level header file with tools and configuration settings - included in all other *EDGE* header files.

----

**utils/**  *Base utilities do not depend on any other components.*

* src/utils/AlignedAllocator.h - Memory allocator guaranteeing alignment, necessary for [SIMD][] operations
* src/utils/all.h - Master header file which includes all components of this subdirectory.
* src/utils/BDGate.h - Advanced programming tool for blocking access to shared objects.
* src/utils/BDWaitRelease.h - Advanced programming tool for thread synchronization.
* src/utils/Log.h - Simplified logging tool - output to a text file.
* src/utils/Parameterize.h - Convert a multivariate method into a univariate method by binding additional variables to specific values.
* src/utils/PerfTools.h - Performance analysis tools, many of which are under construction.
* src/utils/SmartPointers.h - Defines shared and unique pointers for simplified yet advanced memory management. DO NOT USE RAW POINTERS! EVER!
* src/utils/States.h - Tool / library for US state indexing.
* src/utils/Strings.h - Additional string manipulation tools.
* src/utils/Timer.h - Clock-cycle timing class.
* src/utils/TypeLists.h - Foundational meta-programming tool, makes a type out of a list of other types.

----

**src/containers**  *Containers are data structures capable of packaging themselves up for transmission across networks.*

In addition to out-of-the-box containers, such as vectors and maps, *EDGE* provides intuitive syntax for user defined containers.

* src/containers/all.h - Master header file which includes all components of this subdirectory.
* src/containers/ArrayBased.h - Base class and tools for use by SmartArray and SmartVector.
* src/containers/DateMath.h - Base methods for date logic, used predominately by the SmartDate class.
* src/containers/Expressions.h - Defines the expression templates used by Array/Vector operations.
* src/containers/IContainer.h - Defines the interface for all containers - guaranteeing their portability.
* src/containers/ItrBundle.h - Synchronized iteration through a group of iterators.
* src/containers/Schematic.h - MACROS for defining data portable data structures for use by the **DGE**
* src/containers/SIMD.h - Defines [SIMD][] operations.
* src/containers/SimpleMath.h - Some simple math utilities helpful to containers.
* src/containers/SmartArray.h - Portable, [SIMD][]-enabled array of fixed size.
* src/containers/SmartContainers.h - Base class for all containers; contains tools for container portability.
* src/containers/SmartDate.h - Date class.
* src/containers/SmartMap.h - Portable, sorted key-value pairs.
* src/containers/SmartMatrix.h - Defines matrix multiplication operation.
* src/containers/SmartSet.h - Portable, sorted set of values.
* src/containers/SmartVector.h - Portable, [SIMD][]-enabled array of dynamic size.
* src/containers/Tuple.h - Fixed-size collection of heterogeneous values.

----

**src/math**  *The math toolkit operates on array-based data using [SIMD][] as much as possible.*

* src/math/all.h - Master header file which includes all components of this subdirectory.
* src/math/RandomNumbers.h - Random number generator.
* src/math/Statistics.h - Basic statistical summarization of array-based data.

----

**src/nodes**  *The nodes toolkit is required for building [DGE][]-compatible libraries.*

* src/nodes/all.h - Master header file which includes all components of this subdirectory.
* src/nodes/BaseNode.h - Defines the interface for nodes on the directed graph.
* src/nodes/EventDef.h - Defines the data structures encapsulating the edges of the graph.
* src/nodes/Feedback.h - Stores error messages and log info.
* src/nodes/IEvntHndlr.h - Defines the interface for the [DGE][].
* src/nodes/ITechnology.h - Defines the interface for user-defined input/output technologies (e.g., Oracle database)
* src/nodes/Library.h - Defines how libraries self-document for the [DGE][].
* src/nodes/TransformNode.h - Base class for all transformation nodes (i.e., component model)

----

**src/probability**  *Standardized toolkit with pdf, cdf, and inverse cdf of common probability distributions.*

* src/probability/all.h - Master header file which includes all components of this subdirectory.
* src/probability/Binomial.h - Discrete binomial distribution.
* src/probability/Logistic.h - Continuous logistic distribution.
* src/probability/Normal.h - Continuous normal distribution.
* src/probability/Poisson.h - Discrete Poisson distribution.
* src/probability/ProbTools.h - Basic probability tools.

----

**src/solvers**  *Basic analytic toolbox.*

* src/solvers/all.h - Master header file which includes all components of this subdirectory.
* src/solvers/GridSearch.h - Grid search minimization algorithm
* src/solvers/Integration.h - Integration routines
* src/solvers/Interpolation.h - Interpolation routines
* src/solvers/Roots.h - Root finding routines
* src/solvers/Simplex.h - Simplex minimization algorithm

----

**src/unittest**  *Toolbox for using gtest for unit testing.*

* src/unittest/all.h - Master header file which includes all components of this subdirectory.
* src/unittest/gtest_main.cc - gtest-based main function
* src/unittest/UnitTestDefs.h - Header file for use in gtest-based unit tests

----

Also, if you have Microsoft Visual Studio installed, you will receive ...

* A project template for creating [DGE][]-compatible libraries.
* An item template for creating a Transform Node.
* An item template for creating an I/O Node.

----

## + License ##

Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
(See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)

----

## + Copyright ##

© 2016 [Big Denominator][], LLC. All rights reserved.

[Big Denominator]: http://bigdenominator.com "Big Denominator"

[contact]: http://bigdenominator.com/contact/ "Contact Us"

[DGE]: http://bigdenominator.com/dge "Directed Graph Engine"

[SIMD]: https://en.wikipedia.org/wiki/SIMD/ "single-instruction, multiple data"

[tcmalloc]: https://github.com/gperftools/gperftools/ "tcmalloc"
