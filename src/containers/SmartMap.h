#ifndef INCLUDE_H_SMARTMAP
#define INCLUDE_H_SMARTMAP

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <map>
#include "utils/SmartPointers.h"
#include "containers/SmartContainers.h"

template<typename TContainer> class CBaseMap : public TContainer
{
public:
	typedef TContainer				_Myroot;
	typedef TContainer				_Mybase;
	typedef CBaseMap<TContainer>	_Myt;

	typedef typename _Mybase::key_type		key_type;
	typedef typename _Mybase::mapped_type	mapped_type;
	typedef typename _Mybase::size_type		size_type;
	typedef typename _Mybase::value_type	value_type;
	typedef typename _Mybase::difference_type			difference_type;
	typedef typename _Mybase::key_compare				key_compare;
	typedef typename _Mybase::allocator_type			allocator_type;
	typedef typename _Mybase::reference					reference;
	typedef typename _Mybase::const_reference			const_reference;
	typedef typename _Mybase::pointer					pointer;
	typedef typename _Mybase::const_pointer				const_pointer;
	typedef typename _Mybase::iterator					iterator;
	typedef typename _Mybase::const_iterator			const_iterator;
	typedef typename _Mybase::reverse_iterator			reverse_iterator;
	typedef typename _Mybase::const_reverse_iterator	const_reverse_iterator;

	~CBaseMap(void) {}

	template<typename T> inline _Myt& operator =(T&& rhs) { _Mybase::operator=(std::forward<T>(rhs)); return *this; }

	template<typename F> inline void foreach(F inp_Function)
	{
		FOREACH((*this), itr)
		{
			inp_Function(*itr);
		}
	}

	reference back(void) { return *last(); }
	const_reference back(void) const { return *last(); }

	reference front(void) { return *this->begin(); }
	const_reference front(void) const { return *this->cbegin(); }

	iterator last(void)
	{
		iterator itr(this->end());
		if (!this->empty()) { itr--; }

		return itr;
	}
	const_iterator last(void) const
	{
		const_iterator itr(this->cend());
		if (!this->empty()) { itr--; }

		return itr;
	}
	
	void swap(_Myt& rhs) { _Mybase::swap(rhs); }

protected:
	template<typename ...Args> CBaseMap(Args&&... myArgs) :_Mybase(std::forward<Args>(myArgs)...) {}
};

template<typename TContainer, bool bMulti = false> class CBaseSimpleMap;

template<typename TContainer> class CBaseSimpleMap<TContainer, false> : public CBaseMap<TContainer>
{
public:
	typedef CBaseMap<TContainer>				_Mybase;
	typedef CBaseSimpleMap<TContainer, false>	_Myt;

	typedef typename _Mybase::key_type		key_type;
	typedef typename _Mybase::mapped_type	mapped_type;
	typedef typename _Mybase::size_type		size_type;
	typedef typename _Mybase::value_type	value_type;
	typedef typename _Mybase::difference_type			difference_type;
	typedef typename _Mybase::key_compare				key_compare;
	typedef typename _Mybase::allocator_type			allocator_type;
	typedef typename _Mybase::reference					reference;
	typedef typename _Mybase::const_reference			const_reference;
	typedef typename _Mybase::pointer					pointer;
	typedef typename _Mybase::const_pointer				const_pointer;
	typedef typename _Mybase::iterator					iterator;
	typedef typename _Mybase::const_iterator			const_iterator;
	typedef typename _Mybase::reverse_iterator			reverse_iterator;
	typedef typename _Mybase::const_reverse_iterator	const_reverse_iterator;

	template<typename ...Args> CBaseSimpleMap(Args&&... myArgs) :_Mybase(std::forward<Args>(myArgs)...) {}

	~CBaseSimpleMap(void) {}

	template<typename T> inline _Myt& operator =(T&& rhs) { _Mybase::operator=(std::forward<T>(rhs)); return *this; }

	inline mapped_type&  operator[](const key_type& inp_Key)
	{
		iterator itr(_Mybase::find(inp_Key));

		if (itr != _Mybase::cend()) return itr->second;

		return this->insert(value_type(std::piecewise_construct, std::forward_as_tuple(inp_Key), std::tuple<>())).first->second;
	}

	inline mapped_type&  operator[](key_type&& inp_Key)
	{
		iterator itr(_Mybase::find(inp_Key));

		return this->insert(value_type(std::piecewise_construct, std::forward_as_tuple(std::move(inp_Key)), std::tuple<>())).first->second;
	}

	template<typename T> auto add(const key_type& inp_Key, T&& inp_Mapped) -> decltype(this->insert(value_type(inp_Key, std::forward<T>(inp_Mapped))))
	{
		return this->insert(value_type(inp_Key, std::forward<T>(inp_Mapped))); 
	}
};

template<typename TContainer> class CBaseSimpleMap<TContainer, true> : public CBaseMap<TContainer>
{
public:
	typedef CBaseMap<TContainer>				_Mybase;
	typedef CBaseSimpleMap<TContainer, true>	_Myt;

	typedef typename _Mybase::key_type		key_type;
	typedef typename _Mybase::mapped_type	mapped_type;
	typedef typename _Mybase::size_type		size_type;
	typedef typename _Mybase::value_type	value_type;
	typedef typename _Mybase::difference_type			difference_type;
	typedef typename _Mybase::key_compare				key_compare;
	typedef typename _Mybase::allocator_type			allocator_type;
	typedef typename _Mybase::reference					reference;
	typedef typename _Mybase::const_reference			const_reference;
	typedef typename _Mybase::pointer					pointer;
	typedef typename _Mybase::const_pointer				const_pointer;
	typedef typename _Mybase::iterator					iterator;
	typedef typename _Mybase::const_iterator			const_iterator;
	typedef typename _Mybase::reverse_iterator			reverse_iterator;
	typedef typename _Mybase::const_reverse_iterator	const_reverse_iterator;

	~CBaseSimpleMap(void) {}

	template<typename ...Args> CBaseSimpleMap(Args&&... myArgs) :_Mybase(std::forward<Args>(myArgs)...) {}

	template<typename T> inline _Myt& operator =(T&& rhs) { _Mybase::operator=(std::forward<T>(rhs)); return *this; }

	inline mapped_type&  operator[](const key_type& inp_Key)
	{
		iterator itr(_Mybase::find(inp_Key));

		if (itr != _Mybase::cend()) return itr->second;

		return _Mybase::insert(value_type(std::piecewise_construct, std::forward_as_tuple(inp_Key), std::tuple<>()))->second;
	}

	inline mapped_type&  operator[](key_type&& inp_Key)
	{
		iterator itr(_Mybase::find(inp_Key));

		return _Mybase::insert(value_type(std::piecewise_construct, std::forward_as_tuple(std::move(inp_Key)), std::tuple<>()))->second;
	}

	iterator add(const key_type& inp_Key, const mapped_type& inp_Mapped) { return _Mybase::insert(value_type(inp_Key, inp_Mapped)); }
};

template<typename TContainer, bool bMulti = false> class CBaseContainerMap : public CBaseSimpleMap<TContainer, bMulti>, public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
public:
	typedef CBaseSimpleMap<TContainer, bMulti>		_Mybase;
	typedef CBaseContainerMap<TContainer, bMulti>	_Myt;

	typedef typename _Mybase::key_type		key_type;
	typedef typename _Mybase::mapped_type	mapped_type;
	typedef typename _Mybase::size_type		size_type;
	typedef typename _Mybase::value_type	value_type;
	typedef typename _Mybase::difference_type			difference_type;
	typedef typename _Mybase::key_compare				key_compare;
	typedef typename _Mybase::allocator_type			allocator_type;
	typedef typename _Mybase::reference					reference;
	typedef typename _Mybase::const_reference			const_reference;
	typedef typename _Mybase::pointer					pointer;
	typedef typename _Mybase::const_pointer				const_pointer;
	typedef typename _Mybase::iterator					iterator;
	typedef typename _Mybase::const_iterator			const_iterator;
	typedef typename _Mybase::reverse_iterator			reverse_iterator;
	typedef typename _Mybase::const_reverse_iterator	const_reverse_iterator;

	~CBaseContainerMap(void) {}

	template<typename ...Args> CBaseContainerMap(Args&&... myArgs) :_Mybase(std::forward<Args>(myArgs)...) {}

	template<typename T> inline _Myt& operator =(T&& rhs) { _Mybase::operator=(std::forward<T>(rhs)); return *this; }

protected:
	virtual bool deserialize(SSerial& inp_sSerial)
	{
		size_type iSize(0);
		_Mybase::clear();

		SPUD::deserializeTools(inp_sSerial, iSize);
		for (size_type iIndx(0); iIndx < iSize; iIndx++)
		{
			key_type k; mapped_type m;
			SPUD::deserializeTools(inp_sSerial, k, m);
			_Mybase::insert(value_type(k, m));
		}

		return true;
	}

	virtual void pack(SPack& inp_sPack) const
	{
		SPUD::packTools(inp_sPack, _Mybase::size());
		for (const_iterator itr(_Mybase::begin()); itr != _Mybase::cend(); itr++)
		{
			SPUD::packTools(inp_sPack, itr->first, itr->second);
		}
	}

	virtual SPack::size_type packSize(void) const
	{
		SPack::size_type iPackSize(SPUD::packSizeTools(_Mybase::size()));

		for (const_iterator itr(_Mybase::begin()); itr != _Mybase::cend(); itr++)
		{
			iPackSize += SPUD::packSizeTools(itr->first, itr->second);
		}

		return iPackSize;
	}

	virtual void serialize(SSerial& inp_sSerial) const
	{
		SPUD::serializeTools(inp_sSerial, _Mybase::size());
		for (const_iterator itr(_Mybase::begin()); itr != _Mybase::cend(); itr++)
		{
			SPUD::serializeTools(inp_sSerial, itr->first, itr->second);
		}
	}

	virtual bool unpack(SPack& inp_sPack)
	{
		_Mybase::clear();

		size_type iSize(0);
		SPUD::unpackTools(inp_sPack, iSize);

		for (size_type iIndx(0); iIndx < iSize; iIndx++)
		{
			key_type k; mapped_type m;
			SPUD::unpackTools(inp_sPack, k, m);
			_Mybase::insert(value_type(k, m));
		}

		return true;
	}
};

template<typename TKey, typename TValue> using CSimpleMap = CBaseSimpleMap<map<TKey, TValue>, false>;
template<typename TKey, typename TValue> using CSimpleMultiMap = CBaseSimpleMap<multimap<TKey, TValue>, true>;

template<typename TKey, typename TValue> using CSmartMap = CBaseContainerMap<map<TKey, TValue>, false>;
template<typename TKey, typename TValue> using CSmartMultiMap = CBaseContainerMap<multimap<TKey, TValue>, true>;

template<typename TMap> class const_MapItr
{
public:
	typedef TMap				_Mymap;
	typedef const_MapItr<TMap>	_Myt;

	typedef typename _Mymap::key_type			key_type;
	typedef typename _Mymap::mapped_type		mapped_type;
	typedef typename _Mymap::value_type			value_type;
	typedef typename _Mymap::const_pointer		pointer;
	typedef typename _Mymap::const_reference	reference;

	const_MapItr(void) {}

	const_MapItr(CSmartPtr<_Mymap> inp_cDataPtr) { attach(inp_cDataPtr); }

	inline reference operator*(void) const { return *mcItr; }

	inline pointer operator->(void) const
	{
		return (std::pointer_traits<pointer>::pointer_to(**this));
	}

	inline _Myt& operator++(void)
	{
		++mcItr;
		return (*this);
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return (temp);
	}

	inline _Myt& operator--(void)
	{
		--mcItr;
		return (*this);
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--*this;
		return (temp);
	}

	inline bool operator==(const _Myt& rhs) const
	{
		return (mcItr == rhs.mcItr);
	}

	inline bool operator!=(const _Myt& rhs) const
	{
		return (!(*this == rhs));
	}

	inline _Myt& attach(CSmartPtr<_Mymap> inp_cDataPtr)
	{
		mcDataPtr = inp_cDataPtr;
		mcItr = mcDataPtr->begin();

		return *this;
	}

	inline _Myt& begin(void) { mcItr = mcDataPtr->begin(); return *this; }

	inline CSmartPtr<_Mymap> data(void) const { return mcDataPtr; }

	inline _Myt& end(void) { mcItr = mcDataPtr->end();  return *this; }

	inline bool isBegin(void) const { return (mcItr == mcDataPtr->cbegin()); }

	inline bool isEnd(void) const { return (mcItr == mcDataPtr->cend()); }

	inline bool find(const key_type& inp_Key)
	{
		mcItr = mcDataPtr->find(inp_Key);

		return !isEnd();
	}

	inline key_type key(void) const { return mcItr->first; }

	inline _Myt& last(void)
	{
		end();
		if (!mcDataPtr->empty()) mcItr--;

		return *this;
	}

	inline bool lower_bound(const key_type& inp_Key)
	{
		mcItr = mcDataPtr->lower_bound(inp_Key);

		return !isEnd();
	}

	inline reference pair(void) const { return *mcItr; }

	inline bool upper_bound(const key_type& inp_Key)
	{
		mcItr = mcDataPtr->upper_bound(inp_Key);

		return !isEnd();
	}

	inline mapped_type value(void) const { return mcItr->second; }

protected:
	typedef typename _Mymap::iterator iterator;

	CSmartPtr<_Mymap>	mcDataPtr;
	iterator			mcItr;
};

template<typename TMap> class MapItr : public const_MapItr<TMap>
{
public:
	typedef TMap			_Mymap;

	typedef const_MapItr<_Mymap>	_Mybase;
	typedef MapItr<_Mymap>			_Myt;

	typedef typename _Mymap::key_type		key_type;
	typedef typename _Mymap::mapped_type	mapped_type;
	typedef typename _Mymap::value_type		value_type;
	typedef typename _Mymap::pointer		pointer;
	typedef typename _Mymap::reference		reference;

	MapItr(void) :_Mybase() {}

	MapItr(CSmartPtr<_Mymap> inp_cMapPtr) :_Mybase(inp_cMapPtr) {}

	inline reference operator*(void) const { return ((reference)**(_Mybase *)this); }

	inline pointer operator->(void) const
	{
		return (std::pointer_traits<pointer>::pointer_to(**this));
	}

	inline _Myt& operator++(void)
	{
		++*(_Mybase *)this;
		return (*this);
	}

	inline _Myt operator++(int)
	{
		_Myt temp = *this;
		++(*this);
		return (temp);
	}

	inline _Myt& operator--(void)
	{
		--*(_Mybase *)this;
		return (*this);
	}

	inline _Myt operator--(int)
	{
		_Myt temp = *this;
		--*this;
		return (temp);
	}

	inline _Myt& begin(void) { _Mybase::begin(); return *this; }

	inline _Myt& end(void) { _Mybase::end();  return *this; }

	inline _Myt& last(void) { _Mybase::last(); return *this; }

	inline reference pair(void) const { return *_Mybase::mcItr; }

	inline mapped_type& value(void) const { return _Mybase::mcItr->second; }
};


template<typename T, bool hasRootType = HasRootType<T>::value> struct IsMapImpl
{
	enum :int { value = 0 };
};
template<typename T> struct IsMapImpl<T, true>
{
	enum :int { value = std::is_base_of<CBaseMap<typename T::_Myroot>, T>::value };
};
template<typename T> using IsMap = IsMapImpl<T>;

template<typename T, typename U> using ifMap = std::enable_if<IsMap<T>::value, U>;

namespace iterators
{
	template<typename T> static inline typename ifMap<T, const_MapItr<T>>::type GetConstItr(CSmartPtr<T> inp_cMapPtr) { return const_MapItr<T>(inp_cMapPtr); }
	template<typename T> static inline typename ifMap<T, MapItr<T>>::type GetItr(CSmartPtr<T> inp_cMapPtr) { return MapItr<T>(inp_cMapPtr); }
}
#endif
