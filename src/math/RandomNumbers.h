#ifndef INCLUDE_H_RANDOMNUMBERS
#define INCLUDE_H_RANDOMNUMBERS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

//See Numerical Recipes 3rd Edition, section 7.1

class CRandomNumberGenerator
{
public:
	CRandomNumberGenerator(const uint64_t& inp_lRandomNumberSeed) { initialize(inp_lRandomNumberSeed, mlState1, mlState2, mlState3); }

	inline double GetNext(void) { return 5.42101086242752217E-20 * nextRandom(mlState1, mlState2, mlState3); }

	template <typename T> inline void GetNext(T& inp_cData)
	{
		for (typename T::size_type iIndx(0); iIndx < inp_cData.size(); iIndx++)
		{
			inp_cData[iIndx] = GetNext();
		}
	}

private:
	uint64_t mlState1;
	uint64_t mlState2;
	uint64_t mlState3;

	static inline void initialize(const uint64_t& inp_lRandomNumberSeed, uint64_t& inp_lState1, uint64_t& inp_lState2, uint64_t& inp_lState3)
	{
		inp_lState1 = 4101842887655102017LL;
		inp_lState2 = inp_lRandomNumberSeed^inp_lState1;
		inp_lState3 = 1;

		nextRandom(inp_lState1, inp_lState2, inp_lState3);
		inp_lState1 = inp_lState2;
		nextRandom(inp_lState1, inp_lState2, inp_lState3);
		inp_lState3 = inp_lState1;
		nextRandom(inp_lState1, inp_lState2, inp_lState3);
	}

	static inline uint64_t nextRandom(uint64_t& inp_lState1, uint64_t& inp_lState2, uint64_t& inp_lState3)
	{
		uint64_t lComposite;

		updateXorShift(inp_lState1, 17, 31, 8, true);
		updateLCG(inp_lState2, 2862933555777941757LL, 7046029254386353087LL);
		updateMWC(inp_lState3, 4294957665U);

		lComposite = inp_lState2;
		updateXorShift(lComposite, 21, 35, 4, false);

		return (lComposite + inp_lState1) ^ inp_lState3;
	}

	static inline void updateLCG(uint64_t& inp_lRandom, const uint64_t& inp_lMultiplier, const uint64_t& inp_lAdd)
	{
		inp_lRandom = inp_lMultiplier * inp_lRandom + inp_lAdd;
	}

	static inline void updateMWC(uint64_t& inp_lRandom, const uint64_t& inp_lMultiplier)
	{
		inp_lRandom = inp_lMultiplier * (inp_lRandom & 0xffffffff) + (inp_lRandom >> 32);
	}

	static inline void updateXorShift(uint64_t& inp_lRandom, const uint64_t& inp_lShift1, const uint64_t& inp_lShift2, const uint64_t& inp_lShift3, const bool& inp_bStartRightShift)
	{
		if (inp_bStartRightShift)
		{
			inp_lRandom ^= inp_lRandom >> inp_lShift1;
			inp_lRandom ^= inp_lRandom << inp_lShift2;
			inp_lRandom ^= inp_lRandom >> inp_lShift3;
		}
		else
		{
			inp_lRandom ^= inp_lRandom << inp_lShift1;
			inp_lRandom ^= inp_lRandom >> inp_lShift2;
			inp_lRandom ^= inp_lRandom << inp_lShift3;
		}
	}
};
#endif