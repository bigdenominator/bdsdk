#ifndef INCLUDE_H_BIGDENOMINATORLIB
#define INCLUDE_H_BIGDENOMINATORLIB

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"
#ifdef WINDOWS
#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>					// Windows Header Files:
#ifdef BIGDENOMINATORLIB_EXPORTS
#define BIGDENOMINATORLIB_API __declspec(dllexport)
#define EXPORT extern "C" 
#else
#define BIGDENOMINATORLIB_API __declspec(dllimport)
#endif
#else
#ifdef BIGDENOMINATORLIB_EXPORTS
#define BIGDENOMINATORLIB_API __attribute__((visibility("default")))
#else
#define BIGDENOMINATORLIB_API
#endif
#endif

#include "nodes/Library.h"
#include "Publisher.h"

extern CEvntHndlrIntrPtr mcEvntHndlrIntrPtr;

extern "C" BIGDENOMINATORLIB_API int BootstrapLib(SLibBootstrapRwPtr  inp_sModelLibListPtr);
#endif
