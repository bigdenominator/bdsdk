#ifndef INCLUDE_H_INTEGRATION
#define INCLUDE_H_INTEGRATION

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

namespace Solvers
{
	template<typename TFunc, typename TValue> static inline
		auto SimpsonsRule(TFunc inp_ObjFunc, const TValue& inp_From, const TValue& inp_To, uint32_t inp_iPoints) -> decltype(inp_ObjFunc(inp_From))
	{
		const TValue dDelta((inp_To - inp_From) / static_cast<TValue>(inp_iPoints));

		TValue dThis(inp_From);
		auto dResult(inp_ObjFunc(inp_From));
		for (uint32_t lIndx(1); lIndx < inp_iPoints; lIndx++)
		{
			dThis += dDelta;
			dResult += inp_ObjFunc(dThis) * (lIndx % 2 == 0 ? 2.0 : 4.0);
		}
		dResult += inp_ObjFunc(inp_To);
		dResult *= dDelta / 3.0;

		return dResult;
	}
}
#endif
