#ifndef INCLUDE_H_SMARTVECTORTESTS
#define INCLUDE_H_SMARTVECTORTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#pragma warning( disable : 4244 )

#include "containers/SmartVector.h"


template<typename T> static void RunSpud(const T& inp_cFrom, T& inp_cTo)
{
	T sTemp;

	CopyViaPack(inp_cFrom, sTemp);
	CopyViaSerial(sTemp, inp_cTo);
}

class VectorDblTest : public testing::Test
{
public:
	typedef double					value_type;
	typedef CVectorDbl				test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>		Itr;

	vector<double>	myData;

	void SetUp(void) { myData = { 5.l, 9.l, 3.l, 8.l, 6.l, 4.l, 2.l, 1.l, 7.l, 0.l }; };

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class VectorFltTest : public testing::Test
{
public:
	typedef float					value_type;
	typedef CVectorFlt				test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>		Itr;

	vector<float>	myData;

	void SetUp(void) { myData = { 5.0, 9.0, 3.0, 8.0, 6.0, 4.0, 2.0, 1.0, 7.0, 0.0 }; };

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class VectorIntTest : public testing::Test
{
public:
	typedef int32_t					value_type;
	typedef CVectorInt				test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>		Itr;

	vector<int32_t>	myData;

	void SetUp(void) { myData = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }; };

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class VectorLngTest : public testing::Test
{
public:
	typedef int64_t					value_type;
	typedef CVectorLng				test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>		Itr;

	vector<int64_t>	myData;

	void SetUp(void) { myData = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }; };

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class VectorTestPtr : public testing::Test
{
public:
	typedef CVectorDbl	Element;
	typedef CSmartVector<Element>	test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>		Itr;

	vector<double>	myData;

	void SetUp(void) { myData = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }; };

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			for (test_type::size_type j(0); j < inp_cBase[iIndx].size(); j++)
			{
				EXPECT_EQ(cTest[iIndx][j], inp_cBase[iIndx][j]);
			}
		}
	}
};


TEST_F(VectorDblTest, constructor)
{
	test_type	ctest_type1(std::initializer_list<value_type>{ 5.l, 9.l, 3.l, 8.l, 6.l, 4.l, 2.l, 1.l, 7.l, 0.l });
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2(10, 2.0);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, ctest_type2[iIndx]);
	}

	test_type	ctest_type3(ctest_type1);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type3[iIndx]);
	}
}

TEST_F(VectorFltTest, constructor)
{
	test_type	ctest_type1(std::initializer_list<value_type>{ 5.0, 9.0, 3.0, 8.0, 6.0, 4.0, 2.0, 1.0, 7.0, 0.0 });
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2(10, 2.0);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, ctest_type2[iIndx]);
	}

	test_type	ctest_type3(ctest_type1);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type3[iIndx]);
	}
}

TEST_F(VectorIntTest, constructor)
{
	test_type	ctest_type1(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2(10, 2.0);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, ctest_type2[iIndx]);
	}

	test_type	ctest_type3(ctest_type1);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type3[iIndx]);
	}
}

TEST_F(VectorLngTest, constructor)
{
	test_type	ctest_type1(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2(10, 2.0);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, ctest_type2[iIndx]);
	}

	test_type	ctest_type3(ctest_type1);
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type3[iIndx]);
	}
}

TEST_F(VectorTestPtr, constructor)
{
	test_type	ctest_type1(5, Element(std::initializer_list<Element::value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }));
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < ctest_type1[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], ctest_type1[iIndx][j]);
		}
	}

	test_type	ctest_type2(5, Element(10, 2.0));
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < ctest_type2[iIndx].size(); j++)
		{
			EXPECT_EQ(2.0, ctest_type2[iIndx][j]);
		}
	}

	test_type	ctest_type3(ctest_type1);
	for (test_type::size_type iIndx(0); iIndx < ctest_type3.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < ctest_type3[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], ctest_type3[iIndx][j]);
		}
	}
}


TEST_F(VectorDblTest, Initialization)
{
	test_type	ctest_type1;

	ctest_type1 = 0;
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, ctest_type1[iIndx]);
	}

	ctest_type1 = { 5.l, 9.l, 3.l, 8.l, 6.l, 4.l, 2.l, 1.l, 7.l, 0.l };
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2;
	ctest_type2 = ctest_type1;
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type2[iIndx]);
	}
}

TEST_F(VectorFltTest, Initialization)
{
	test_type	ctest_type1;

	ctest_type1 = 0;
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, ctest_type1[iIndx]);
	}

	ctest_type1 = { 5.0, 9.0, 3.0, 8.0, 6.0, 4.0, 2.0, 1.0, 7.0, 0.0 };
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2;
	ctest_type2 = ctest_type1;
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type2[iIndx]);
	}
}

TEST_F(VectorIntTest, Initialization)
{
	test_type	ctest_type1;

	ctest_type1 = 0;
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, ctest_type1[iIndx]);
	}

	ctest_type1 = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 };
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2;
	ctest_type2 = ctest_type1;
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type2[iIndx]);
	}
}

TEST_F(VectorLngTest, Initialization)
{
	test_type	ctest_type1;

	ctest_type1 = 0;
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, ctest_type1[iIndx]);
	}

	ctest_type1 = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 };
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type1[iIndx]);
	}

	test_type	ctest_type2;
	ctest_type2 = ctest_type1;
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], ctest_type2[iIndx]);
	}
}

TEST_F(VectorTestPtr, Initialization)
{
	test_type	ctest_type1;

	ctest_type1 = Element(10, 0);
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < ctest_type1[iIndx].size(); j++)
		{
			EXPECT_EQ(0.l, ctest_type1[iIndx][j]);
		}
	}

	ctest_type1 = Element(std::initializer_list<Element::value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	for (test_type::size_type iIndx(0); iIndx < ctest_type1.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < ctest_type1[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], ctest_type1[iIndx][j]);
		}
	}

	test_type	ctest_type2(5);
	ctest_type2 = ctest_type1;
	for (test_type::size_type iIndx(0); iIndx < ctest_type2.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < ctest_type1[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], ctest_type1[iIndx][j]);
		}
	}
}


TEST_F(VectorDblTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(VectorFltTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(VectorIntTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(VectorLngTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(VectorTestPtr, SPUD)
{
	TestSpud(test_type(5, Element(std::initializer_list<Element::value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 })));
}


TEST_F(VectorDblTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(1024, cBase.capacity());
	EXPECT_EQ(10, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());

	cBase.push_back(99);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(99, cBase.back());
	EXPECT_EQ(99, cBase[10]);
	EXPECT_EQ(12, cBase[9]);

	cBase.reserve(cBase.capacity() + 1);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());
	EXPECT_EQ(99, cBase[10]);

	cBase.pop_back();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.pop_back();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());
}

TEST_F(VectorFltTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(1024, cBase.capacity());
	EXPECT_EQ(10, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());

	cBase.push_back(99);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(99, cBase.back());
	EXPECT_EQ(99, cBase[10]);
	EXPECT_EQ(12, cBase[9]);

	cBase.reserve(cBase.capacity() + 1);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());
	EXPECT_EQ(99, cBase[10]);

	cBase.pop_back();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.pop_back();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());
}

TEST_F(VectorIntTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(1024, cBase.capacity());
	EXPECT_EQ(10, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());

	cBase.push_back(99);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(99, cBase.back());
	EXPECT_EQ(99, cBase[10]);
	EXPECT_EQ(12, cBase[9]);

	cBase.reserve(cBase.capacity() + 1);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());
	EXPECT_EQ(99, cBase[10]);

	cBase.pop_back();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.pop_back();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());
}

TEST_F(VectorLngTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(1024, cBase.capacity());
	EXPECT_EQ(10, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());

	cBase.push_back(99);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(99, cBase.back());
	EXPECT_EQ(99, cBase[10]);
	EXPECT_EQ(12, cBase[9]);

	cBase.reserve(cBase.capacity() + 1);
	EXPECT_EQ(11, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());
	EXPECT_EQ(99, cBase[10]);

	cBase.pop_back();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(2048, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(10, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.pop_back();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());

	cBase.shrink_to_fit();
	EXPECT_EQ(9, cBase.size());
	EXPECT_EQ(1024, cBase.capacity());
}


TEST_F(VectorDblTest, sum)
{
	const test_type::size_type iIndxCnt(100);
	const test_type::value_type dTolerance(0.0000001);

	test_type cTest(iIndxCnt);
	for (test_type::size_type iIndx(0); iIndx < iIndxCnt; iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_GT(dTolerance, fabs(std::sum(cTest) - static_cast<test_type::value_type>(iIndxCnt * (iIndxCnt - 1)) / 2.0));
}

TEST_F(VectorFltTest, sum)
{
	const test_type::size_type iIndxCnt(100);
	const test_type::value_type fTolerance(0.0000001f);

	test_type cTest(iIndxCnt);
	for (test_type::size_type iIndx(0); iIndx < iIndxCnt; iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_GT(fTolerance, fabs(std::sum(cTest) - static_cast<test_type::value_type>(iIndxCnt * (iIndxCnt - 1)) / 2.0f));
}

TEST_F(VectorIntTest, sum)
{
	const test_type::size_type iIndxCnt(100);

	test_type cTest(iIndxCnt);
	for (test_type::size_type iIndx(0); iIndx < iIndxCnt; iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_EQ(static_cast<test_type::value_type>(iIndxCnt * (iIndxCnt - 1)) / 2, std::sum(cTest));
}

TEST_F(VectorLngTest, sum)
{
	const test_type::size_type iIndxCnt(100);

	test_type cTest(iIndxCnt);
	for (test_type::size_type iIndx(0); iIndx < iIndxCnt; iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_EQ(static_cast<test_type::value_type>(iIndxCnt * (iIndxCnt - 1)) / 2, std::sum(cTest));
}


TEST_F(VectorDblTest, product)
{
	const test_type::size_type iIndxCnt(100);
	const test_type::value_type dTolerance(0.0000001);

	test_type cTest(iIndxCnt, 2.0);
	for (test_type::size_type iIndx(0); iIndx < iIndxCnt; iIndx++)
	{
		if (iIndx % 2 == 1) cTest[iIndx] = 0.5;
	}

	EXPECT_GT(dTolerance, fabs(std::product(cTest) - 1.0));
}

TEST_F(VectorFltTest, product)
{
	const test_type::size_type iIndxCnt(100);
	const test_type::value_type fTolerance(0.0000001f);

	test_type cTest(iIndxCnt, 2.0);
	for (test_type::size_type iIndx(0); iIndx < iIndxCnt; iIndx++)
	{
		if (iIndx % 2 == 1) cTest[iIndx] = 0.5f;
	}

	EXPECT_GT(fTolerance, fabs(std::product(cTest) - 1.0f));
}

TEST_F(VectorIntTest, product)
{
	const test_type::size_type iIndxCnt(10);

	test_type cTest(iIndxCnt, 2);

	EXPECT_EQ(1024, std::product(cTest));
}

TEST_F(VectorLngTest, product)
{
	const test_type::size_type iIndxCnt(10);

	test_type cTest(iIndxCnt, 2);

	EXPECT_EQ(1024, std::product(cTest));
}


TEST_F(VectorDblTest, repeat)
{
	const test_type::value_type dTolerance(0.0000001);
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_GT(dTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}

TEST_F(VectorFltTest, repeat)
{
	const test_type::value_type fTolerance(0.0000001f);
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_GT(fTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}

TEST_F(VectorIntTest, repeat)
{
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}

TEST_F(VectorLngTest, repeat)
{
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}


TEST_F(VectorDblTest, repeatElements)
{
	const test_type::value_type dTolerance(0.0000001);
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_GT(dTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}

TEST_F(VectorFltTest, repeatElements)
{
	const test_type::value_type fTolerance(0.0000001f);
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_GT(fTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}

TEST_F(VectorIntTest, repeatElements)
{
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}

TEST_F(VectorLngTest, repeatElements)
{
	const test_type::size_type iReps(10);

	test_type cTest(104);
	test_type cResult1(104 * iReps), cResult2(104 * iReps - 1);

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}


TEST_F(VectorDblTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// division
	cTest = cBase / 0.5;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	test_type	cLhs(10, 0.5);
	cTest = cBase / cLhs;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2, cTest[iIndx]);
	}

	// multiplication
	cTest = cBase * 2.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	cTest = cBase * cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1(10);
	test_type	cAlt2(10);

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx) * (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}

TEST_F(VectorFltTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// division
	cTest = cBase / 0.5;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	test_type	cLhs(10, 0.5);
	cTest = cBase / cLhs;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2, cTest[iIndx]);
	}

	// multiplication
	cTest = cBase * 2.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	cTest = cBase * cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1(10);
	test_type	cAlt2(10);

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}

TEST_F(VectorIntTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// multiplication
	cTest = cBase * 2.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	cTest = cBase * cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1(10);
	test_type	cAlt2(10);

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}

TEST_F(VectorLngTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1(10);
	test_type	cAlt2(10);

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}


TEST_F(VectorDblTest, const_Iterator)
{
	CSmartPtr<test_type>	cBasePtr(CreateSmart<test_type>(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }));

	cItr itr1(cBasePtr);
	LOOP(itr1)
	{
		EXPECT_EQ(myData[itr1.key()], *itr1);
	}

	itr1 -= 5;
	EXPECT_EQ(4, *itr1);

	itr1 += 2;
	EXPECT_EQ(1, *itr1);

	cItr itr2(itr1 - 4);
	EXPECT_EQ(8, *itr2);

	itr1 = itr2 + 3;
	EXPECT_EQ(2, *itr1);

	EXPECT_FALSE(itr2 == itr1);
	EXPECT_TRUE(itr2 != itr1);

	EXPECT_TRUE(itr1 > itr2);
	EXPECT_FALSE(itr1 < itr2);

	EXPECT_TRUE(itr1 >= itr2);
	EXPECT_FALSE(itr1 <= itr2);

	EXPECT_TRUE(itr2 < itr1);
	EXPECT_FALSE(itr2 > itr1);

	EXPECT_TRUE(itr2 <= itr1);
	EXPECT_FALSE(itr2 >= itr1);

	itr1 = itr2;
	EXPECT_FALSE(itr2 != itr1);
	EXPECT_TRUE(itr2 == itr1);

	EXPECT_FALSE(itr1 > itr2);
	EXPECT_FALSE(itr1 < itr2);

	EXPECT_TRUE(itr1 >= itr2);
	EXPECT_TRUE(itr1 <= itr2);

	EXPECT_FALSE(itr2 < itr1);
	EXPECT_FALSE(itr2 > itr1);

	EXPECT_TRUE(itr2 <= itr1);
	EXPECT_TRUE(itr2 >= itr1);
}

TEST_F(VectorDblTest, const_Factory)
{
	auto itr(iterators::GetConstItr(CreateSmart<test_type>(5)));
	EXPECT_EQ(5, itr.data()->size());
}

TEST_F(VectorDblTest, Iterator)
{
	CSmartPtr<test_type>	cBasePtr(CreateSmart<test_type>(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }));

	Itr itr1(cBasePtr);
	LOOP(itr1)
	{
		EXPECT_EQ(myData[itr1.key()], *itr1);
		*itr1 += 1;
		EXPECT_EQ(myData[itr1.key()] + 1, *itr1);
	}
}

TEST_F(VectorDblTest, Factory)
{
	auto itr(iterators::GetItr(CreateSmart<test_type>(5)));
	EXPECT_EQ(5, itr.data()->size());
}
#endif
