#ifndef INCLUDE_H_IEVNTHNDLR
#define INCLUDE_H_IEVNTHNDLR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "nodes/EventDef.h"

class IEvntHndlr
{
public:
	virtual ~IEvntHndlr(void) {};

	virtual void Attach(void) = 0;
	virtual STransportPtr GetTransport(void) = 0;
	virtual STransportPtr Receive(TYPELIST_t) const = 0;
	virtual bool Request(STransportPtr) const = 0;
	virtual void ReturnTransport(STransportPtr) const = 0;
	virtual bool Send(STransportPtr) const = 0;
	virtual STransportPtr View(TYPELIST_t) const = 0;
};

typedef IEvntHndlr*           CEvntHndlrIntrPtr;

static CEvntHndlrIntrPtr      mcEvntHndlrIntrPtr(nullptr);
#endif
