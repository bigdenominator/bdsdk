#ifndef INCLUDE_H_SMARTARRAYTESTS
#define INCLUDE_H_SMARTARRAYTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#pragma warning( disable : 4244 )

#include "containers/SmartArray.h"

static const int32_t	iTestIterations(1000);

template<typename T> static void RunSpud(const T& inp_cFrom, T& inp_cTo)
{
	T sTemp;

	CopyViaPack(inp_cFrom, sTemp);
	CopyViaSerial(sTemp, inp_cTo);
}

class ArrayDblTest : public testing::Test
{
public:
	typedef double					value_type;
	typedef CArrayDbl<10>			test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>			Itr;

	vector<double>	myData;
	
	void SetUp(void) { myData = { 5.l, 9.l, 3.l, 8.l, 6.l, 4.l, 2.l, 1.l, 7.l, 0.l }; }

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class ArrayFltTest : public testing::Test
{
public:
	typedef float					value_type;
	typedef CArrayFlt<10>			test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>			Itr;

	vector<float>	myData;

	void SetUp(void) { myData = { 5.0, 9.0, 3.0, 8.0, 6.0, 4.0, 2.0, 1.0, 7.0, 0.0 }; }

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class ArrayIntTest : public testing::Test
{
public:
	typedef int32_t					value_type;
	typedef CArrayInt<10>			test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>			Itr;

	vector<int32_t>	myData;

	void SetUp(void) { myData = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }; }

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class ArrayLngTest : public testing::Test
{
public:
	typedef int64_t					value_type;
	typedef CArrayLng<10>			test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>			Itr;

	vector<int64_t>	myData;

	void SetUp(void) { myData = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }; }

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			EXPECT_EQ(inp_cBase[iIndx], cTest[iIndx]);
		}
	}
};

class ArrayPtrTest : public testing::Test
{
public:
	typedef CArrayDbl<10>	Element;
	typedef CSmartArray<Element, 5>	test_type;

	typedef const_ArrayItr<test_type>	cItr;
	typedef ArrayItr<test_type>			Itr;

	vector<double>	myData;

	void SetUp(void) { myData = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }; }

	void TestSpud(const test_type& inp_cBase)
	{
		test_type cTest;
		RunSpud(inp_cBase, cTest);

		EXPECT_EQ(inp_cBase.size(), cTest.size());
		for (test_type::size_type iIndx(0); iIndx < inp_cBase.size(); iIndx++)
		{
			for (test_type::size_type j(0); j < inp_cBase[iIndx].size(); j++)
			{
				EXPECT_EQ(cTest[iIndx][j], inp_cBase[iIndx][j]);
			}
		}
	}
};


TEST_F(ArrayDblTest, constructor)
{
	test_type	cTest1(std::initializer_list<value_type>{ 5.l, 9.l, 3.l, 8.l, 6.l, 4.l, 2.l, 1.l, 7.l, 0.l });
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2(2.0);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, cTest2[iIndx]);
	}

	test_type	cTest3(cTest1);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest3[iIndx]);
	}
}

TEST_F(ArrayFltTest, constructor)
{
	test_type	cTest1(std::initializer_list<value_type>{ 5.0, 9.0, 3.0, 8.0, 6.0, 4.0, 2.0, 1.0, 7.0, 0.0 });
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2(2.0);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, cTest2[iIndx]);
	}

	test_type	cTest3(cTest1);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest3[iIndx]);
	}
}

TEST_F(ArrayIntTest, constructor)
{
	test_type	cTest1(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2(2.0);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, cTest2[iIndx]);
	}

	test_type	cTest3(cTest1);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest3[iIndx]);
	}
}

TEST_F(ArrayLngTest, constructor)
{
	test_type	cTest1(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2(2.0);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(2.0, cTest2[iIndx]);
	}

	test_type	cTest3(cTest1);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest3[iIndx]);
	}
}

TEST_F(ArrayPtrTest, constructor)
{
	test_type	cTest1(Element(std::initializer_list<Element::value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }));
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < cTest1[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], cTest1[iIndx][j]);
		}
	}

	test_type	cTest2(Element(2.0));
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < cTest2[iIndx].size(); j++)
		{
			EXPECT_EQ(2.0, cTest2[iIndx][j]);
		}
	}

	test_type	cTest3(cTest1);
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < cTest3[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], cTest3[iIndx][j]);
		}
	}
}


TEST_F(ArrayDblTest, Initialization)
{
	test_type	cTest1;

	cTest1 = 0;
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, cTest1[iIndx]);
	}

	cTest1 = { 5.l, 9.l, 3.l, 8.l, 6.l, 4.l, 2.l, 1.l, 7.l, 0.l };
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2;
	cTest2 = cTest1;
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest2[iIndx]);
	}
}

TEST_F(ArrayFltTest, Initialization)
{
	test_type	cTest1;

	cTest1 = 0;
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, cTest1[iIndx]);
	}

	cTest1 = { 5.0, 9.0, 3.0, 8.0, 6.0, 4.0, 2.0, 1.0, 7.0, 0.0 };
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2;
	cTest2 = cTest1;
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest2[iIndx]);
	}
}

TEST_F(ArrayIntTest, Initialization)
{
	test_type	cTest1;

	cTest1 = 0;
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, cTest1[iIndx]);
	}

	cTest1 = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 };
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2;
	cTest2 = cTest1;
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest2[iIndx]);
	}
}

TEST_F(ArrayLngTest, Initialization)
{
	test_type	cTest1;

	cTest1 = 0;
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(0.l, cTest1[iIndx]);
	}

	cTest1 = { 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 };
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest1[iIndx]);
	}

	test_type	cTest2;
	cTest2 = cTest1;
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		EXPECT_EQ(myData[iIndx], cTest2[iIndx]);
	}
}

TEST_F(ArrayPtrTest, Initialization)
{
	test_type	cTest1;

	cTest1 = Element(0);
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < cTest1[iIndx].size(); j++)
		{
			EXPECT_EQ(0.l, cTest1[iIndx][j]);
		}
	}

	cTest1 = Element(std::initializer_list<Element::value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	for (test_type::size_type iIndx(0); iIndx < cTest1.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < cTest1[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], cTest1[iIndx][j]);
		}
	}

	test_type	cTest2;
	cTest2 = cTest1;
	for (test_type::size_type iIndx(0); iIndx < cTest2.size(); iIndx++)
	{
		for (test_type::size_type j(0); j < cTest2[iIndx].size(); j++)
		{
			EXPECT_EQ(myData[j], cTest2[iIndx][j]);
		}
	}
}


TEST_F(ArrayDblTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(ArrayFltTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(ArrayIntTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(ArrayLngTest, SPUD)
{
	TestSpud(std::initializer_list<value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
}

TEST_F(ArrayPtrTest, SPUD)
{
	TestSpud(ArrayPtrTest::Element(std::initializer_list<Element::value_type>{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }));
}


TEST_F(ArrayDblTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(test_type::SIZE, cBase.max_size());
	EXPECT_EQ(test_type::SIZE, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());
	
	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());
}

TEST_F(ArrayFltTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(test_type::SIZE, cBase.max_size());
	EXPECT_EQ(test_type::SIZE, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());
}

TEST_F(ArrayIntTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(test_type::SIZE, cBase.max_size());
	EXPECT_EQ(test_type::SIZE, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());
}

TEST_F(ArrayLngTest, Methods)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });

	EXPECT_FALSE(cBase.empty());
	EXPECT_EQ(test_type::SIZE, cBase.max_size());
	EXPECT_EQ(test_type::SIZE, cBase.size());

	EXPECT_EQ(5, cBase.front());
	EXPECT_EQ(8, cBase.at(3));
	EXPECT_EQ(2, cBase[6]);
	EXPECT_EQ(0, cBase.back());

	cBase.front() = 1;
	cBase.at(3) = 2;
	cBase[6] = 3;
	cBase.back() = 4;

	EXPECT_EQ(1, cBase.front());
	EXPECT_EQ(2, cBase.at(3));
	EXPECT_EQ(3, cBase[6]);
	EXPECT_EQ(4, cBase.back());

	cBase = 12;
	EXPECT_EQ(12, cBase.front());
	EXPECT_EQ(12, cBase.at(3));
	EXPECT_EQ(12, cBase[6]);
	EXPECT_EQ(12, cBase.back());
}


TEST_F(ArrayDblTest, sum)
{
	const test_type::value_type dTolerance(0.0000001);

	test_type cTest;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_GT(dTolerance, fabs(std::sum(cTest) - static_cast<test_type::value_type>(cTest.size() * (cTest.size() - 1)) / 2.0));
}

TEST_F(ArrayFltTest, sum)
{
	const test_type::value_type fTolerance(0.0000001f);

	test_type cTest;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_GT(fTolerance, fabs(std::sum(cTest) - static_cast<test_type::value_type>(cTest.size() * (cTest.size() - 1)) / 2.0f));
}

TEST_F(ArrayIntTest, sum)
{
	test_type cTest;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_EQ(static_cast<test_type::value_type>(cTest.size() * (cTest.size() - 1)) / 2, std::sum(cTest));
}

TEST_F(ArrayLngTest, sum)
{
	test_type cTest;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_EQ(static_cast<test_type::value_type>(cTest.size() * (cTest.size() - 1)) / 2, std::sum(cTest));
}


TEST_F(ArrayDblTest, product)
{
	const test_type::value_type dTolerance(0.0000001);

	test_type cTest(2.0);
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		if (iIndx % 2 == 1) { cTest[iIndx] = 0.5; }
	}

	EXPECT_GT(dTolerance, fabs(std::product(cTest) - 1.0));
}

TEST_F(ArrayFltTest, product)
{
	const test_type::value_type fTolerance(0.0000001f);

	test_type cTest(2.0f);
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		if (iIndx % 2 == 1) { cTest[iIndx] = 0.5f; }
	}

	EXPECT_GT(fTolerance, fabs(std::product(cTest) - 1.0f));
}

TEST_F(ArrayIntTest, product)
{
	test_type cTest(2);

	EXPECT_EQ(1024, std::product(cTest));
}

TEST_F(ArrayLngTest, product)
{
	test_type cTest(2);

	EXPECT_EQ(1024, std::product(cTest));
}


TEST_F(ArrayDblTest, repeat)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::value_type dTolerance(0.0000001);
	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_GT(dTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}

TEST_F(ArrayFltTest, repeat)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::value_type fTolerance(0.0000001f);
	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_GT(0.0000001f, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}

TEST_F(ArrayIntTest, repeat)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}

TEST_F(ArrayLngTest, repeat)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeat(cTest));
	for (test_type::size_type iRep(0), lFull(0); iRep < iReps; iRep++)
	{
		for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeat(cTest));
}


TEST_F(ArrayDblTest, repeatElements)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::value_type dTolerance(0.0000001);
	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_GT(dTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest)); 
}

TEST_F(ArrayFltTest, repeatElements)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::value_type fTolerance(0.0000001f);
	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_GT(fTolerance, fabs(cTest[iIndx] - cResult1[lFull]));
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}

TEST_F(ArrayIntTest, repeatElements)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}

TEST_F(ArrayLngTest, repeatElements)
{
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10> test_type1;
	typedef CSmartArray<test_type::value_type, test_type::SIZE * 10 - 1> test_type2;

	const test_type::size_type iReps(10);

	test_type cTest;
	test_type1 cResult1;
	test_type2 cResult2;

	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		cTest[iIndx] = static_cast<test_type::value_type>(iIndx);
	}

	EXPECT_TRUE(cResult1.repeatElements(cTest));
	for (test_type::size_type iIndx(0), lFull(0); iIndx < cTest.size(); iIndx++)
	{
		for (test_type::size_type iRep(0); iRep < iReps; iRep++, lFull++)
		{
			EXPECT_EQ(cTest[iIndx], cResult1[lFull]);
		}
	}
	EXPECT_FALSE(cResult2.repeatElements(cTest));
}


TEST_F(ArrayDblTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// division
	cTest = cBase / 0.5;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	test_type	cLhs(0.5);
	cTest = cBase / cLhs;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2, cTest[iIndx]);
	}

	// multiplication
	cTest = cBase * 2.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	cTest = cBase * cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1;
	test_type	cAlt2;

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}

TEST_F(ArrayFltTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// division
	cTest = cBase / 0.5;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	test_type	cLhs(0.5);
	cTest = cBase / cLhs;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2, cTest[iIndx]);
	}

	// multiplication
	cTest = cBase * 2.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	cTest = cBase * cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1;
	test_type	cAlt2;

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}

TEST_F(ArrayIntTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// multiplication
	cTest = cBase * 2.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * 2.0, cTest[iIndx]);
	}

	cTest = cBase * cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] * cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1;
	test_type	cAlt2;

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}

TEST_F(ArrayLngTest, Operators)
{
	test_type	cBase(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 });
	test_type	cTest;

	// addition
	cTest = cBase + 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + 1.0, cTest[iIndx]);
	}

	cTest = cBase + cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] + cBase[iIndx], cTest[iIndx]);
	}

	// subtraction
	cTest = cBase - 1.0;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - 1.0, cTest[iIndx]);
	}

	cTest = cBase - cBase;
	for (test_type::size_type iIndx(0); iIndx < cTest.size(); iIndx++)
	{
		EXPECT_EQ(cBase[iIndx] - cBase[iIndx], cTest[iIndx]);
	}

	test_type	cAlt1;
	test_type	cAlt2;

	for (test_type::size_type iIndx(0); iIndx < cAlt1.size(); iIndx++)
	{
		cAlt1[iIndx] = static_cast<test_type::value_type>(iIndx)* (iIndx % 2 == 0 ? 1 : -1);
	}

	cAlt2 = (cAlt1 * -1) & cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(-static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}

	cAlt2 = (cAlt1 * -1) | cAlt1;
	for (test_type::size_type iIndx(0); iIndx < cAlt2.size(); iIndx++)
	{
		EXPECT_EQ(static_cast<test_type::value_type>(iIndx), cAlt2[iIndx]);
	}
}


TEST_F(ArrayDblTest, const_Iterator)
{
	CSmartPtr<test_type>	cBasePtr(CreateSmart<test_type>(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }));

	cItr itr1(cBasePtr);
	LOOP(itr1)
	{
		EXPECT_EQ(myData[itr1.key()], *itr1);
	}

	itr1 -= 5;
	EXPECT_EQ(4, *itr1);

	itr1 += 2;
	EXPECT_EQ(1, *itr1);

	cItr itr2(itr1 - 4);
	EXPECT_EQ(8, *itr2);

	itr1 = itr2 + 3;
	EXPECT_EQ(2, *itr1);

	EXPECT_FALSE(itr2 == itr1);
	EXPECT_TRUE(itr2 != itr1);

	EXPECT_TRUE(itr1 > itr2);
	EXPECT_FALSE(itr1 < itr2);

	EXPECT_TRUE(itr1 >= itr2);
	EXPECT_FALSE(itr1 <= itr2);

	EXPECT_TRUE(itr2 < itr1);
	EXPECT_FALSE(itr2 > itr1);

	EXPECT_TRUE(itr2 <= itr1);
	EXPECT_FALSE(itr2 >= itr1);

	itr1 = itr2;
	EXPECT_FALSE(itr2 != itr1);
	EXPECT_TRUE(itr2 == itr1);

	EXPECT_FALSE(itr1 > itr2);
	EXPECT_FALSE(itr1 < itr2);

	EXPECT_TRUE(itr1 >= itr2);
	EXPECT_TRUE(itr1 <= itr2);

	EXPECT_FALSE(itr2 < itr1);
	EXPECT_FALSE(itr2 > itr1);

	EXPECT_TRUE(itr2 <= itr1);
	EXPECT_TRUE(itr2 >= itr1);
}

TEST_F(ArrayDblTest, const_Factory)
{
	auto itr(iterators::GetConstItr(CreateSmart<test_type>(5)));
	EXPECT_EQ(test_type::SIZE, itr.data()->size());
}

TEST_F(ArrayDblTest, Iterator)
{
	CSmartPtr<test_type>	cBasePtr(CreateSmart<test_type>(std::initializer_list<value_type>{ 5, 9, 3, 8, 6, 4, 2, 1, 7, 0 }));

	Itr itr1(cBasePtr);
	LOOP(itr1)
	{
		EXPECT_EQ(myData[itr1.key()], *itr1);
		*itr1 += 1;
		EXPECT_EQ(myData[itr1.key()] + 1, *itr1);
	}
}

TEST_F(ArrayDblTest, Factory)
{
	auto itr(iterators::GetItr(CreateSmart<test_type>(5)));
	EXPECT_EQ(test_type::SIZE, itr.data()->size());
}
#endif