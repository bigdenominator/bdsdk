#ifndef INCLUDE_H_MATHTOOLS
#define INCLUDE_H_MATHTOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <cmath>
#include <limits>

enum class INTERPOLATIONTYPE { LINEAR, DOUBLE_PARABOLIC };
enum class ROUNDINGTYPE { UP, DOWN, NEAREST, NONE };

static const double d_LN2(1.4426950408889634073599);
static const double d_SQRT2(0.70710678118654752440);
static const double dEPSILON(std::numeric_limits<double>::epsilon());
static const double dEXP(2.71828182845904523536);
static const double dMAXEXP(708);
static const double dINFINITY(std::numeric_limits<double>::infinity());
static const double dLN2(0.69314718055994530941);
static const double dPI(3.14159265358979323846);
static const double dSQRT2PI(2.506628274631);
static const double d_SQRT2PI(1.0 / dSQRT2PI);
static const double dTWOPI(6.28318530717958647692);

static const float f_LN2(1.4426950408889634073599f);
static const float f_SQRT2(0.70710678118654752440f);
static const float fEPSILON(std::numeric_limits<float>::epsilon());
static const float fEXP(2.71828182845904523536f);
static const float fMAXEXP(88.72283905206835f);
static const float fINFINITY(std::numeric_limits<float>::infinity());
static const float fLN2(0.69314718055994530941f);
static const float fPI(3.14159265358979323846f);
static const float fSQRT2PI(2.506628274631f);
static const float f_SQRT2PI(1.f / fSQRT2PI);
static const float fTWOPI(6.28318530717958647692f);

class CMath
{
protected:
	static const int32_t iMAXFACT = 171;
	static const int32_t iMAXLNFACT = 2000;

	static inline double lnGamma(const double& inp_dValue)
	{
		if (inp_dValue < 0.0){ return NAN; }

		double PX[14] =
		{
			57.1562356658629235,
			-59.5979603554754912,
			14.1360979747417471,
			-0.491913816097620199,
			0.339946499848118887e-4,
			0.465236289270485756e-4,
			-0.983744753048795646e-4,
			0.158088703224912494e-3,
			-0.210164441724104883e-3,
			0.217439618115212643e-3,
			-0.16431810653676389e-3,
			0.844182239838527433e-4,
			-0.261908384015814087e-4,
			0.368991826595316234e-5
		};

		double dTemp(inp_dValue + 5.2421875), dValue(inp_dValue), dCum(0.999999999999997092);
		
		for (int32_t iCof(0); iCof < 14; iCof++) dCum += PX[iCof] / ++dValue;

		return (inp_dValue + 0.5) * CMath::ln(dTemp) - dTemp + CMath::ln(2.5066282746310005 * dCum / inp_dValue);
	}

public:
	template<typename TData> static inline void BoxMuller(const TData& inp_dUniform1, const TData& inp_dUniform2, TData& inp_dStdNormal1, TData& inp_dStdNormal2)
	{
		TData dWorking(sqrt(-2.0 * ln(inp_dUniform1)));

		SinCos(dTWOPI * inp_dUniform2, inp_dStdNormal1, inp_dStdNormal2);
		inp_dStdNormal1 *= dWorking;
		inp_dStdNormal2 *= dWorking;
	}

	template<typename TData> static inline double cos(const TData& inp_dAngle)
	{
		TData dCos, dSin;
		SinCos(inp_dAngle, dSin, dCos);

		return dCos;
	}

	static inline double exp(const double& inp_dValue)
	{
		if (inp_dValue > dMAXEXP){ return dINFINITY; }
		if (inp_dValue < -dMAXEXP){ return 0.0; }

		double PX[6] = {
			1.26177193074810590878E-4,
			0.0,
			3.02994407707441961300E-2,
			0.0,
			9.99999999999999999910E-1,
			0.0
		};

		double QX[7] =
		{
			3.00198505138664455042E-6,
			0,
			2.52448340349684104192E-3,
			0,
			2.27265548208155028766E-1,
			0,
			2.00000000000000000009E0
		};

		double dX(floor(d_LN2 * inp_dValue + 0.5));
		int32_t iPowerOf2(static_cast<int32_t>(dX));
		double dPx, dQx;

		dX = inp_dValue - dX * dLN2;

		dPx = polynomial(dX, PX, 6);
		dQx = polynomial(dX, QX, 7);

		return ldexp((dQx + dPx) / (dQx - dPx), iPowerOf2);
	}

	static inline float exp(const float& inp_fValue)
	{
		if (inp_fValue > fMAXEXP){ return fINFINITY; }
		if (inp_fValue < -fMAXEXP){ return 0.0f; }

		float PX[9] =
		{
			1.9875691500E-4f,
			1.3981999507E-3f,
			1.3981999507E-3f,
			8.3334519073E-3f,
			4.1665795894E-2f,
			1.6666665459E-1f,
			5.0000001201E-1f, 
			1.0f,
			1.0f
		};

		float fX(floor(f_LN2 * inp_fValue + 0.5f));
		int32_t iPowerOf2(static_cast<int32_t>(fX));

		return ldexp(polynomial(inp_fValue - fX * fLN2, PX, 9), iPowerOf2);
	}

	static inline long double factorial(uint64_t inp_iNum)
	{
		long double dResult(1);

		while (inp_iNum > 0)
		{
			dResult *= static_cast<long double>(inp_iNum);
			inp_iNum--;
		}

		return dResult;
	}

	static inline double ln(const double& inp_dValue)
	{
		if (inp_dValue < 0.0){ return NAN; }
		if (inp_dValue == 0.0){ return -dINFINITY; }

		double PX[21] =
		{
			-0.0500000000000000,
			0.0526315789473684,
			-0.0555555555555556,
			0.0588235294117647,
			-0.0625000000000000,
			0.0666666666666667,
			-0.0714285714285714,
			0.0769230769230769,
			-0.0833333333333333,
			0.0909090909090909,
			-0.1000000000000000,
			0.1111111111111111,
			-0.1250000000000000,
			0.1428571428571429,
			-0.1666666666666667,
			0.2000000000000000,
			-0.2500000000000000,
			0.3333333333333333,
			-0.5000000000000000,
			1.0000000000000000,
			0.00000000000000000
		};
		int32_t iExp;
		double dFrac;

		dFrac = frexp(inp_dValue, &iExp);
		if (dFrac < d_SQRT2)
		{
			iExp--;
			dFrac *= 2;
		}

		dFrac--;

		return polynomial(dFrac, PX, 21) + iExp * dLN2; 
	}
	
	static inline float ln(const float& inp_fValue)
	{
		if (inp_fValue < 0.0f){ return NAN; }
		if (inp_fValue == 0.0f){ return -fINFINITY; }

		float PX[21] =
		{
			-0.0500000000000000f,
			0.0526315789473684f,
			-0.0555555555555556f,
			0.0588235294117647f,
			-0.0625000000000000f,
			0.0666666666666667f,
			-0.0714285714285714f,
			0.0769230769230769f,
			-0.0833333333333333f,
			0.0909090909090909f,
			-0.1000000000000000f,
			0.1111111111111111f,
			-0.1250000000000000f,
			0.1428571428571429f,
			-0.1666666666666667f,
			0.2000000000000000f,
			-0.2500000000000000f,
			0.3333333333333333f,
			-0.5000000000000000f,
			1.0000000000000000f,
			0.00000000000000000f
		};
		int32_t iExp;
		float fFrac;

		fFrac = frexp(inp_fValue, &iExp);
		if (fFrac < f_SQRT2)
		{
			iExp--;
			fFrac *= 2.0f;
		}

		fFrac--;

		return polynomial(fFrac, PX, 21) + iExp * fLN2;
	}

	template <typename TData, typename TCof> static inline TData polynomial(const TData& inp_X, const TCof* inp_CoefPtr, const size_t& inp_iSize)
	{
		TData	Value(inp_CoefPtr[0]);
		for (size_t iIndx(1); iIndx < inp_iSize; iIndx++)
		{
			Value = Value * inp_X + static_cast<TData>(inp_CoefPtr[iIndx]);
		}
		return Value;
	}

	template <typename TData> static inline TData roundMultiple(const TData& inp_NumberToRound, const ROUNDINGTYPE& inp_RoundType, const TData& inp_MultipleOf)
	{
		if (inp_RoundType == ROUNDINGTYPE::NONE){ return inp_NumberToRound; }

		int32_t lRoundingFactors(static_cast<int32_t>(inp_NumberToRound / inp_MultipleOf));
		TData RoundDn(static_cast<TData>(lRoundingFactors)* inp_MultipleOf);

		if (inp_RoundType == ROUNDINGTYPE::DOWN){ return RoundDn; }

		TData RoundUp(lRoundingFactors > 0 ? RoundDn + inp_MultipleOf : RoundDn - inp_MultipleOf);

		if (inp_RoundType == ROUNDINGTYPE::UP){ return RoundUp; }

		return abs(inp_NumberToRound - RoundDn) < abs(inp_NumberToRound - RoundUp) ? RoundDn : RoundUp;
	}
	
	template <typename TData> static inline double roundDigits(const TData& inp_dNumberToRound, const ROUNDINGTYPE& inp_RoundType, const int32_t& inp_iSignificantDigits)
	{
		return roundMultiple(inp_dNumberToRound, inp_RoundType, pow(10.0, -inp_iSignificantDigits));
	}

	template<typename TData> static inline double sin(const TData& inp_dAngle)
	{
		TData dCos, dSin;
		SinCos(inp_dAngle, dSin, dCos);

		return dSin;
	}

	static inline void SinCos(const double& inp_dAngle, double& inp_dSin, double& inp_dCos)
	{
		double dSIN[6] =
		{
			1.58962301576546568060E-10,
			-2.50507477628578072866E-8,
			2.75573136213857245213E-6,
			-1.98412698295895385996E-4,
			8.33333333332211858878E-3,
			-1.66666666666666307295E-1
		};

		double dCOS[6]=
		{
			-1.13585365213876817300E-11,
			2.08757008419747316778E-9,
			-2.75573141792967388112E-7,
			2.48015872888517045348E-5,
			-1.38888888888730564116E-3,
			4.16666666666665929218E-2
		};

		double dWorkingAngle;
		double dAngleSqrd;
		int32_t  iShifts;

		// Shift angle to interval [-pi/4, +pi/4]
		dWorkingAngle = fabs(inp_dAngle);
		iShifts = int32_t(4.0 / dPI * dWorkingAngle);
		iShifts = ((iShifts + 1) & (~1)) >> 1;
		dWorkingAngle -= double(iShifts) * dPI * 0.5;

		// Pade polynomial approximates for sin/cos
		dAngleSqrd = dWorkingAngle * dWorkingAngle;

		inp_dSin = polynomial(dAngleSqrd, dSIN, 6);
		double sTemp[4] = { inp_dSin, 0.0, 1.0, 0.0 };
		inp_dSin = polynomial(dWorkingAngle, sTemp, 4);

		inp_dCos = polynomial(dAngleSqrd, dCOS, 6);
		double cTemp[3] = { inp_dCos, -0.5, 1.0 };
		inp_dCos = polynomial(dAngleSqrd, cTemp, 3);

		// impact of shifts
		if (iShifts & 1) swap(inp_dCos, inp_dSin);

		if ((iShifts & 2) != 0.0) { inp_dSin = -inp_dSin; }
		iShifts--;
		if ((iShifts & 2) == 0.0) { inp_dCos = -inp_dCos; }
		if (inp_dAngle < 0.0)  { inp_dSin = -inp_dSin; }
	}

	static inline void SinCos(const float& inp_fAngle, float& inp_fSin, float& inp_fCos)
	{
		float fWorkingAngle;
		float fAngleSqrd;
		int32_t  iShifts;

		// Shift angle to interval [-pi/4, +pi/4]
		fWorkingAngle = fabs(inp_fAngle);
		iShifts = int32_t(4.0 / dPI * fWorkingAngle);
		iShifts = ((iShifts + 1) & (~1)) >> 1;
		fWorkingAngle -= float(iShifts) * fPI * 0.5f;

		// Pade polynomial approximates for sin/cos
		fAngleSqrd = fWorkingAngle * fWorkingAngle;

		float dSIN[4] = { -1.9515295891E-4f, 8.3321608736E-3f, -1.6666654611E-1f * fWorkingAngle, fWorkingAngle };
		float dCOS[5] = { 2.443315711809948E-5f, -1.388731625493765E-3f, 4.166664568298827E-2f, -0.5f, 1.0f };

		inp_fSin = polynomial(fAngleSqrd, dSIN, 4);
		inp_fCos = polynomial(fAngleSqrd, dCOS, 5);

		// impact of shifts
		if (iShifts & 1) swap(inp_fCos, inp_fSin);

		if ((iShifts & 2) != 0.0) { inp_fSin = -inp_fSin; }
		iShifts--;
		if ((iShifts & 2) == 0.0) { inp_fCos = -inp_fCos; }
		if (inp_fAngle < 0.0)  { inp_fSin = -inp_fSin; }
	}
	
	template<typename TData> static inline double tan(const TData& inp_dAngle)
	{
		TData dCos, dSin;
		SinCos(inp_dAngle, dSin, dCos);

		return dSin / dCos;
	}
};


template<size_t ILhs, size_t IRhs> struct gcd
{
	enum :size_t { value = gcd<IRhs, ILhs % IRhs>::value };
};
template<size_t IVal> struct gcd<0, IVal>
{
	enum :size_t { value = IVal };
};
template<size_t IVal> struct gcd<IVal, 0>
{
	enum :size_t { value = IVal };
};

template<size_t ILhs, size_t IRhs> struct lcm
{
	enum :size_t { value = ILhs * IRhs / gcd<ILhs, IRhs>::value };
};
template<> struct lcm<0, 0>
{
	enum :size_t { value = 0 };
};
#endif
