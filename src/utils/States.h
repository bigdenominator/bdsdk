#ifndef INCLUDE_H_STATES
#define INCLUDE_H_STATES

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

static const string	sErrorStateCode("Invalid state code");

#define STATE_LIST \
	STATE_AK, STATE_AL, STATE_AR, STATE_AZ, STATE_CA, STATE_CO, STATE_CT, STATE_DC, STATE_DE, \
	STATE_FL, STATE_GA, STATE_GU, STATE_HI, STATE_IA, STATE_ID, STATE_IL, STATE_IN, STATE_KS, \
	STATE_KY, STATE_LA, STATE_MA, STATE_MD, STATE_ME, STATE_MI, STATE_MN, STATE_MO, STATE_MS, \
	STATE_MT, STATE_NC, STATE_ND, STATE_NE, STATE_NH, STATE_NJ, STATE_NM, STATE_NV, STATE_NY, \
	STATE_OH, STATE_OK, STATE_OR, STATE_PA, STATE_PR, STATE_RI, STATE_SC, STATE_SD, STATE_TN, \
	STATE_TX, STATE_UT, STATE_VA, STATE_VI, STATE_VT, STATE_WA, STATE_WI, STATE_WV, STATE_WY, \
	NATIONAL, END

enum class USA { STATE_LIST };

static const string ALASKA("AK");
static const string ALABAMA("AL");
static const string ARKANSAS("AR");
static const string ARIZONA("AZ");
static const string CALIFORNIA("CA");
static const string COLORADO("CO");
static const string CONNECTICUT("CT");
static const string DC("DC");
static const string DELAWARE("DE");
static const string FLORIDA("FL");
static const string GEORGIA("GA");
static const string GUAM("GU");
static const string HAWAII("HI");
static const string IOWA("IA");
static const string IDAHO("ID");
static const string ILLINOIS("IL");
static const string INDIANA("IN");
static const string KANSAS("KS");
static const string KENTUCKY("KY");
static const string LOUISIANA("LA");
static const string MASSACHUSETTS("MA");
static const string MARYLAND("MD");
static const string MAINE("ME");
static const string MICHIGAN("MI");
static const string MINNESOTA("MN");
static const string MISSOURI("MO");
static const string MISSISSIPPI("MS");
static const string MONTANA("MT");
static const string NORTHCAROLINA("NC");
static const string NORTHDAKOTA("ND");
static const string NEBRASKA("NE");
static const string NEWHAMPSHIRE("NH");
static const string NEWJERSEY("NJ");
static const string NEWMEXICO("NM");
static const string NEVADA("NV");
static const string NEWYORK("NY");
static const string OHIO("OH");
static const string OKLAHOMA("OK");
static const string OREGON("OR");
static const string PENNSYLVANIA("PA");
static const string PUERTORICO("PR");
static const string RHODEISLAND("RI");
static const string SOUTHCAROLINA("SC");
static const string SOUTHDAKOTA("SD");
static const string TENNESSEE("TN");
static const string TEXAS("TX");
static const string UTAH("UT");
static const string VIRGINIA("VA");
static const string VIRGINISLANDS("VI");
static const string VERMONT("VT");
static const string WASHINGTON("WA");
static const string WISCONSIN("WI");
static const string WESTVIRGINIA("WV");
static const string WYOMING("WY");
static const string US("US");

namespace US_STATES
{
	static inline USA GetStateId(const string& inp_sStateIdText)
	{
		if (inp_sStateIdText == "AK"){return USA::STATE_AK;}
		if (inp_sStateIdText == "AL"){return USA::STATE_AL;}
		if (inp_sStateIdText == "AR"){return USA::STATE_AR;}
		if (inp_sStateIdText == "AZ"){return USA::STATE_AZ;}
		if (inp_sStateIdText == "CA"){return USA::STATE_CA;}
		if (inp_sStateIdText == "CO"){return USA::STATE_CO;}
		if (inp_sStateIdText == "CT"){return USA::STATE_CT;}
		if (inp_sStateIdText == "DC"){return USA::STATE_DC;}
		if (inp_sStateIdText == "DE"){return USA::STATE_DE;}
		if (inp_sStateIdText == "FL"){return USA::STATE_FL;}
		if (inp_sStateIdText == "GA"){return USA::STATE_GA;}
		if (inp_sStateIdText == "GU"){return USA::STATE_GU;}
		if (inp_sStateIdText == "HI"){return USA::STATE_HI;}
		if (inp_sStateIdText == "IA"){return USA::STATE_IA;}
		if (inp_sStateIdText == "ID"){return USA::STATE_ID;}
		if (inp_sStateIdText == "IL"){return USA::STATE_IL;}
		if (inp_sStateIdText == "IN"){return USA::STATE_IN;}
		if (inp_sStateIdText == "KS"){return USA::STATE_KS;}
		if (inp_sStateIdText == "KY"){return USA::STATE_KY;}
		if (inp_sStateIdText == "LA"){return USA::STATE_LA;}
		if (inp_sStateIdText == "MA"){return USA::STATE_MA;}
		if (inp_sStateIdText == "MD"){return USA::STATE_MD;}
		if (inp_sStateIdText == "ME"){return USA::STATE_ME;}
		if (inp_sStateIdText == "MI"){return USA::STATE_MI;}
		if (inp_sStateIdText == "MN"){return USA::STATE_MN;}
		if (inp_sStateIdText == "MO"){return USA::STATE_MO;}
		if (inp_sStateIdText == "MS"){return USA::STATE_MS;}
		if (inp_sStateIdText == "MT"){return USA::STATE_MT;}
		if (inp_sStateIdText == "NC"){return USA::STATE_NC;}
		if (inp_sStateIdText == "ND"){return USA::STATE_ND;}
		if (inp_sStateIdText == "NE"){return USA::STATE_NE;}
		if (inp_sStateIdText == "NH"){return USA::STATE_NH;}
		if (inp_sStateIdText == "NJ"){return USA::STATE_NJ;}
		if (inp_sStateIdText == "NM"){return USA::STATE_NM;}
		if (inp_sStateIdText == "NV"){return USA::STATE_NV;}
		if (inp_sStateIdText == "NY"){return USA::STATE_NY;}
		if (inp_sStateIdText == "OH"){return USA::STATE_OH;}
		if (inp_sStateIdText == "OK"){return USA::STATE_OK;}
		if (inp_sStateIdText == "OR"){return USA::STATE_OR;}
		if (inp_sStateIdText == "PA"){return USA::STATE_PA;}
		if (inp_sStateIdText == "PR"){return USA::STATE_PR;}
		if (inp_sStateIdText == "RI"){return USA::STATE_RI;}
		if (inp_sStateIdText == "SC"){return USA::STATE_SC;}
		if (inp_sStateIdText == "SD"){return USA::STATE_SD;}
		if (inp_sStateIdText == "TN"){return USA::STATE_TN;}
		if (inp_sStateIdText == "TX"){return USA::STATE_TX;}
		if (inp_sStateIdText == "UT"){return USA::STATE_UT;}
		if (inp_sStateIdText == "VA"){return USA::STATE_VA;}
		if (inp_sStateIdText == "VI"){return USA::STATE_VI;}
		if (inp_sStateIdText == "VT"){return USA::STATE_VT;}
		if (inp_sStateIdText == "WA"){return USA::STATE_WA;}
		if (inp_sStateIdText == "WI"){return USA::STATE_WI;}
		if (inp_sStateIdText == "WV"){return USA::STATE_WV;}
		if (inp_sStateIdText == "WY"){return USA::STATE_WY;}
		if (inp_sStateIdText == "US"){return USA::NATIONAL; }
		throw CException(string("Unknown state code"), __FILE__, __LINE__);
	}

	static inline bool ValidateStateId(const string& inp_sStateIdText)
	{
		if (inp_sStateIdText == "AK"){ return true; }
		if (inp_sStateIdText == "AL"){ return true; }
		if (inp_sStateIdText == "AR"){ return true; }
		if (inp_sStateIdText == "AZ"){ return true; }
		if (inp_sStateIdText == "CA"){ return true; }
		if (inp_sStateIdText == "CO"){ return true; }
		if (inp_sStateIdText == "CT"){ return true; }
		if (inp_sStateIdText == "DC"){ return true; }
		if (inp_sStateIdText == "DE"){ return true; }
		if (inp_sStateIdText == "FL"){ return true; }
		if (inp_sStateIdText == "GA"){ return true; }
		if (inp_sStateIdText == "GU"){ return true; }
		if (inp_sStateIdText == "HI"){ return true; }
		if (inp_sStateIdText == "IA"){ return true; }
		if (inp_sStateIdText == "ID"){ return true; }
		if (inp_sStateIdText == "IL"){ return true; }
		if (inp_sStateIdText == "IN"){ return true; }
		if (inp_sStateIdText == "KS"){ return true; }
		if (inp_sStateIdText == "KY"){ return true; }
		if (inp_sStateIdText == "LA"){ return true; }
		if (inp_sStateIdText == "MA"){ return true; }
		if (inp_sStateIdText == "MD"){ return true; }
		if (inp_sStateIdText == "ME"){ return true; }
		if (inp_sStateIdText == "MI"){ return true; }
		if (inp_sStateIdText == "MN"){ return true; }
		if (inp_sStateIdText == "MO"){ return true; }
		if (inp_sStateIdText == "MS"){ return true; }
		if (inp_sStateIdText == "MT"){ return true; }
		if (inp_sStateIdText == "NC"){ return true; }
		if (inp_sStateIdText == "ND"){ return true; }
		if (inp_sStateIdText == "NE"){ return true; }
		if (inp_sStateIdText == "NH"){ return true; }
		if (inp_sStateIdText == "NJ"){ return true; }
		if (inp_sStateIdText == "NM"){ return true; }
		if (inp_sStateIdText == "NV"){ return true; }
		if (inp_sStateIdText == "NY"){ return true; }
		if (inp_sStateIdText == "OH"){ return true; }
		if (inp_sStateIdText == "OK"){ return true; }
		if (inp_sStateIdText == "OR"){ return true; }
		if (inp_sStateIdText == "PA"){ return true; }
		if (inp_sStateIdText == "PR"){ return true; }
		if (inp_sStateIdText == "RI"){ return true; }
		if (inp_sStateIdText == "SC"){ return true; }
		if (inp_sStateIdText == "SD"){ return true; }
		if (inp_sStateIdText == "TN"){ return true; }
		if (inp_sStateIdText == "TX"){ return true; }
		if (inp_sStateIdText == "UT"){ return true; }
		if (inp_sStateIdText == "VA"){ return true; }
		if (inp_sStateIdText == "VI"){ return true; }
		if (inp_sStateIdText == "VT"){ return true; }
		if (inp_sStateIdText == "WA"){ return true; }
		if (inp_sStateIdText == "WI"){ return true; }
		if (inp_sStateIdText == "WV"){ return true; }
		if (inp_sStateIdText == "WY"){ return true; }
		if (inp_sStateIdText == "US"){ return true; }

		return false;
	}

	static inline const string& GetStateAbbrev(USA inp_iStateId)
	{
		if (inp_iStateId == USA::STATE_AK){return ALASKA;}
		if (inp_iStateId == USA::STATE_AL){return ALABAMA;}
		if (inp_iStateId == USA::STATE_AR){return ARKANSAS;}
		if (inp_iStateId == USA::STATE_AZ){return ARIZONA;}
		if (inp_iStateId == USA::STATE_CA){return CALIFORNIA;}
		if (inp_iStateId == USA::STATE_CO){return COLORADO;}
		if (inp_iStateId == USA::STATE_CT){return CONNECTICUT;}
		if (inp_iStateId == USA::STATE_DC){return DC;}
		if (inp_iStateId == USA::STATE_DE){return DELAWARE;}
		if (inp_iStateId == USA::STATE_FL){return FLORIDA;}
		if (inp_iStateId == USA::STATE_GA){return GEORGIA;}
		if (inp_iStateId == USA::STATE_GU){return GUAM;}
		if (inp_iStateId == USA::STATE_HI){return HAWAII;}
		if (inp_iStateId == USA::STATE_IA){return IOWA;}
		if (inp_iStateId == USA::STATE_ID){return IDAHO;}
		if (inp_iStateId == USA::STATE_IL){return ILLINOIS;}
		if (inp_iStateId == USA::STATE_IN){return INDIANA;}
		if (inp_iStateId == USA::STATE_KS){return KANSAS;}
		if (inp_iStateId == USA::STATE_KY){return KENTUCKY;}
		if (inp_iStateId == USA::STATE_LA){return LOUISIANA;}
		if (inp_iStateId == USA::STATE_MA){return MASSACHUSETTS;}
		if (inp_iStateId == USA::STATE_MD){return MARYLAND;}
		if (inp_iStateId == USA::STATE_ME){return MAINE;}
		if (inp_iStateId == USA::STATE_MI){return MICHIGAN;}
		if (inp_iStateId == USA::STATE_MN){return MINNESOTA;}
		if (inp_iStateId == USA::STATE_MO){return MISSOURI;}
		if (inp_iStateId == USA::STATE_MS){return MISSISSIPPI;}
		if (inp_iStateId == USA::STATE_MT){return MONTANA;}
		if (inp_iStateId == USA::STATE_NC){return NORTHCAROLINA;}
		if (inp_iStateId == USA::STATE_ND){return NORTHDAKOTA;}
		if (inp_iStateId == USA::STATE_NE){return NEBRASKA;}
		if (inp_iStateId == USA::STATE_NH){return NEWHAMPSHIRE;}
		if (inp_iStateId == USA::STATE_NJ){return NEWJERSEY;}
		if (inp_iStateId == USA::STATE_NM){return NEWMEXICO;}
		if (inp_iStateId == USA::STATE_NV){return NEVADA;}
		if (inp_iStateId == USA::STATE_NY){return NEWYORK;}
		if (inp_iStateId == USA::STATE_OH){return OHIO;}
		if (inp_iStateId == USA::STATE_OK){return OKLAHOMA;}
		if (inp_iStateId == USA::STATE_OR){return OREGON;}
		if (inp_iStateId == USA::STATE_PA){return PENNSYLVANIA;}
		if (inp_iStateId == USA::STATE_PR){return PUERTORICO;}
		if (inp_iStateId == USA::STATE_RI){return RHODEISLAND;}
		if (inp_iStateId == USA::STATE_SC){return SOUTHCAROLINA;}
		if (inp_iStateId == USA::STATE_SD){return SOUTHDAKOTA;}
		if (inp_iStateId == USA::STATE_TN){return TENNESSEE;}
		if (inp_iStateId == USA::STATE_TX){return TEXAS;}
		if (inp_iStateId == USA::STATE_UT){return UTAH;}
		if (inp_iStateId == USA::STATE_VA){return VIRGINIA;}
		if (inp_iStateId == USA::STATE_VI){return VIRGINISLANDS;}
		if (inp_iStateId == USA::STATE_VT){return VERMONT;}
		if (inp_iStateId == USA::STATE_WA){return WASHINGTON;}
		if (inp_iStateId == USA::STATE_WI){return WISCONSIN;}
		if (inp_iStateId == USA::STATE_WV){return WESTVIRGINIA;}
		if (inp_iStateId == USA::STATE_WY){return WYOMING;}
		if (inp_iStateId == USA::NATIONAL){return US; }
		throw CException(sErrorStateCode, __FILE__, __LINE__);
	}
}
#endif
