#ifndef INCLUDE_H_BASENODE
#define INCLUDE_H_BASENODE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "nodes/IEvntHndlr.h"

class CBaseNode
{
public:
	virtual bool Initialize(void) { return true; }
	virtual size_t Run(void) = 0;
	virtual ~CBaseNode(void) { mcEvntHndlrLocalPtr = nullptr; }

protected:
	CEvntHndlrIntrPtr	mcEvntHndlrLocalPtr;
	CFeedback			mcFeedback;

	CBaseNode(void) :mcEvntHndlrLocalPtr(mcEvntHndlrIntrPtr) {}

	CFeedback& GetFeedback(void) { return mcFeedback; }
};
#endif
