#ifndef INCLUDE_H_UNITTESTUTILS
#define INCLUDE_H_UNITTESTUTILS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "gtest/gtest.h"
#include "utils/Log.h"
#include "utils/SmartPointers.h"

static string GetTestName(void)
{
	const ::testing::TestInfo*		cTestInfo(::testing::UnitTest::GetInstance()->current_test_info());
	return 	cTestInfo->name();
}

static string GetTestCaseName(void)
{
	const ::testing::TestInfo*		cTestInfo(::testing::UnitTest::GetInstance()->current_test_info());
	return cTestInfo->test_case_name();
}

static string ParseArgs(int argc, char* argv[], string inp_sTag)
{
	string sTag(sNULL);

	for (int iArg(0); iArg < argc; iArg++)
	{
		string::size_type iStart(1);
		sTag = CString::DelimitedSubString(argv[iArg], "=", iStart);

		if (sTag == inp_sTag) return CString::DelimitedSubString(argv[iArg], "=", iStart);
	}

	return sNULL;
}

struct MyArgs
{
public:
	string	msSourceFile;
	string	msWorkingDir;
};

extern MyArgs myArgs;
extern string sDatabasePath, sDatabaseFile, sTestCommand;
extern CSmartPtr<CLogFile> cLogPtr;
#endif